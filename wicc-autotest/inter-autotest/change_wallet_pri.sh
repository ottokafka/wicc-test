#!/bin/bash

# 私有链，钱包被锁会停止挖矿，此脚本实现删除钱包文件并重新导入钱包，使钱包变为未被加密状态

host='http://10.0.0.111:1968'
username='waykichain'
password='wicc123'
# 钱包加密的密码
walletpass="1234567890"
# 导出钱包文件存放位置
walleturl="/root/wallet"
# wallet.dat文件存放位置
walletdataurl="/root/wicc/WaykiChain_pri/data/wallet.dat"


# 钱包解锁
echo "##############解锁钱包#################"
curl -u $username:$password -d '{"jsonrpc":"2.0","id":"curltext","method":"walletpassphrase","params":['\""${walletpass}"\"',3600]}' -H 'content-type:application/json;' $host

sleep 10

# 导出钱包文件
echo "##############导出钱包#################"
curl -u $username:$password -d '{"jsonrpc":"2.0","id":"curltext","method":"dumpwallet","params":['\""${walleturl}"\"']}' -H 'content-type:application/json;' $host

sleep 5

# 删除钱包文件，路径：/data/wallet.dat
echo "##############删除旧钱包#################"
rm -f $walletdataurl

# 重启容器
echo '##############重启容器#################'

docker stop waykicoind-pri

docker start waykicoind-pri

docker ps

sleep 15

# 导入钱包
echo '##############导入钱包#################'
curl -u $username:$password -d '{"jsonrpc":"2.0","id":"curltext","method":"importwallet","params":['\""${walleturl}"\"']}' -H 'content-type:application/json;' $host
