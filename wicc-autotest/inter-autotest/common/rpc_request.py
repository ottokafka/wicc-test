import requests
import json
import base64

from utils.log import mylog

logger = mylog('TestLog').getlog()


class WiccRPC(object):
    def __init__(self, username, password):
        self.username = username
        self.password = password
        bytesString = (username + ":" + password).encode(encoding='utf-8')
        token = base64.b64encode(bytesString)
        self.headers = {'content-type': 'application/json', 'Authorization': 'Basic ' + token.decode('utf-8')}

    def call(self, host, methodname, params):
        payloads = [{
            "method": methodname,
            'params': params,
            'jsonrpc': '2.0',
            'id': 'curltext'
        }]
        response = requests.post(url=host, data=json.dumps(payloads), headers=self.headers)
        if response.status_code == 200:
            return response.status_code, response.json()
        else:
            print('RPC call failed, error is ', response.status_code)
            return response.status_code, response


if __name__ == '__main__':
    rpc = WiccRPC('waykichain', 'wicc123')
    status_code, response = rpc.call('http://10.0.0.19:1968', 'getinfo', [])
    # logger.info(status_code)
    # logger.info(response)
    print(response)
    print(response[0]["result"])


