from time import sleep

import pytest
import allure

from utils.read_property import ReadProperty
from utils.log import mylog
from wiccsdk.account import Account
from wiccsdk.transaction import Transaction
from wiccsdk.wallet import Wallet

logger = mylog(__name__).getlog()


class TestCDPTransaction(object):
    @pytest.fixture(scope='class', autouse=True)
    def setup(self):
        prop = ReadProperty()
        # env = ReadEnv()
        # host = 'http://' + env.read('ip') + ":" + str(env.read('port'))
        # rpcuser = env.read('username')
        # rpcpassword = env.read('password')
        # walletpassword = prop.read('common', 'walletpassword')

        host = prop.read('common', 'host')
        rpcuser = prop.read('common', 'rpcuser')
        rpcpassword = prop.read('common', 'rpcpassword')
        walletpassword = prop.read('common', 'walletpassword')
        creaction = prop.read('common', 'creation_addr')
        init = prop.read('common', 'init_addr')

        wallet = Wallet(host, rpcuser, rpcpassword)
        account = Account(host, rpcuser, rpcpassword)
        tran = Transaction(host, rpcuser, rpcpassword)
        isLocked = wallet.islocked()
        # 解锁钱包
        if isLocked:
            status, response = wallet.walletpassphrase([walletpassword, 360000])
            assert status == 200
        else:
            pass
        assert not wallet.islocked()
        # res = account.send([init, creaction, "100000000", 10000])
        # assert res
        res = account.feedingPrice(init, "USD", 2, 2)
        assert res
        logger.info('class setup')
        sleep(6)

    @pytest.fixture(scope='class', autouse=True)
    def teardown(self, request):
        def fin():
            logger.info('class teardown')

        request.addfinalizer(fin)

    @pytest.fixture(scope='function', autouse=True)
    def setup_function(self):
        prop = ReadProperty()
        # env = ReadEnv()
        # self.host = 'http://' + env.read('ip') + ":" + str(env.read('port'))
        # self.rpcuser = env.read('username')
        # self.rpcpassword = env.read('password')
        self.host = prop.read('common', 'host')
        self.rpcuser = prop.read('common', 'rpcuser')
        self.rpcpassword = prop.read('common', 'rpcpassword')
        self.creaction = prop.read('common', 'creation_addr')
        self.risk = prop.read('common', 'risk_addr')
        self.contracturl = prop.read('common', 'contracturl')
        self.init = prop.read('common', 'init_addr')
        self.registerfee = int(prop.read('fee', 'register'))
        self.sendfee = int(prop.read('fee', 'send'))
        self.deployfee = int(prop.read('fee', 'contractdeploy'))
        self.cdpfee = int(prop.read('fee', 'cdp'))

        logger.info('function setup')

    @pytest.fixture(scope='function', autouse=True)
    def teardown_function(self):
        logger.info('function teardown')

    @pytest.fixture(scope='function', autouse=False)
    def test_param(self, request):
        return request.param

    @allure.feature('Test submitstakecdptx create')
    def test_submitstakecdptx_create_normal(self):
        """
        @author:otto.kafka
        test submitstakecdptx when normal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 50000000000
        stake_amount = 1900000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 1.9)

        # Create a CDP
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['result']['txid'] is not None
            assert r['error'] is None

    @allure.feature('Test submitstakecdptx create')
    def test_submitstakecdptx_create_twice(self):
        """
        @author:elaine.tan
        test submitstakecdptx when address has an open cdp
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 190000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None

        # Create a CDP
        txid = ''
        params = [address, str(stake_amount), str(mint_amount)]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['result']['txid'] is not None
            assert r['error'] is None
            txid = r['result']['txid']
        sleep(10)

        # create twice
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "has-open-cdp" in r['error']['message']

    @allure.feature('Test submitstakecdptx create')
    def test_submitstakecdptx_create_twice2(self):
        """
        @author:elaine.tan
        test submitstakecdptx when address has a closed cdp
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 190000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        # 向address转WUSD，用来全部赎回
        addr = account.getNewAddrWithEnoughWUSD(self.init, stake_amount + self.sendfee)
        assert addr is not None
        res = account.send([addr, address, "WUSD:" + str(stake_amount), "WUSD:"+ str(self.sendfee)])
        print(res)
        assert res

        # Create a CDP
        txid = ''
        params = [address, str(stake_amount), str(mint_amount)]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['result']['txid'] is not None
            assert r['error'] is None
            txid = r['result']['txid']
        sleep(10)

        # close cdp
        redparams = [address, txid, mint_amount, stake_amount]
        status_code, response = tran.submitredeemcdptx(redparams)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None
        sleep(10)

        # create twice
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitstakecdptx create address')
    def test_submitstakecdptx_create_address_none(self):
        """
        @author:elaine.tan
        test submitstakecdptx create CDP when address is null
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)
        stake_amount = 20000000000
        price = transaction.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)

        # address is null
        address = None
        params = [address, str(stake_amount), str(mint_amount)]
        status_code, response = transaction.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type null, expected str" in r['error']['message']

    @allure.feature('Test submitstakecdptx create address')
    def test_submitstakecdptx_create_address_unregistered(self):
        """
        @author:elaine.tan
        test submitstakecdptx create CDP when address is unregistered
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)
        stake_amount = 20000000000
        price = transaction.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)

        # address is not registered
        address = account.newaddress()
        params = [address, str(stake_amount), str(mint_amount)]
        status_code, response = transaction.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "account not exists" in r['error']['message']

    @allure.feature('Test submitstakecdptx create address')
    def test_submitstakecdptx_create_address_invalid(self):
        """
        @author:elaine.tan
        test submitstakecdptx create CDP when address is invalid
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)
        stake_amount = 20000000000
        price = transaction.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)

        # address is invalid
        address = "invalid"
        params = [address, str(stake_amount), str(mint_amount)]
        status_code, response = transaction.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Invalid addr" in r['error']['message']

    @allure.feature('Test submitstakecdptx create address')
    def test_submitstakecdptx_create_address_notexist(self):
        """
        @author:elaine.tan
        test submitstakecdptx create CDP when address is not exist
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)
        stake_amount = 20000000000
        price = transaction.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)

        # address not exist
        address = "wgj7tTJz24xGKW5E9NSc6d6UNEKyNEKVGK"
        params = [address, str(stake_amount), str(mint_amount)]
        status_code, response = transaction.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "not found" in r['error']['message']

    @allure.feature('Test submitstakecdptx create address')
    def test_submitstakecdptx_create_address_int(self):
        """
        @author:elaine.tan
        test submitstakecdptx create CDP when address is int
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)
        stake_amount = 20000000000
        price = transaction.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)

        # address is int
        address = 123
        params = [address, str(stake_amount), str(mint_amount)]
        status_code, response = transaction.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type int, expected str" in r['error']['message']

    @allure.feature('Test submitstakecdptx create address')
    def test_submitstakecdptx_create_address_zero(self):
        """
        @author:elaine.tan
        test submitstakecdptx create CDP when address is 0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)
        stake_amount = 20000000000
        price = transaction.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)

        # address is 0
        address = 0
        params = [address, str(stake_amount), str(mint_amount)]
        status_code, response = transaction.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type int, expected str" in r['error']['message']

    @allure.feature('Test submitstakecdptx create address')
    def test_submitstakecdptx_create_address_exceed(self):
        """
        @author:elaine.tan
        test submitstakecdptx create CDP when address < 0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)
        stake_amount = 20000000000
        price = transaction.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)

        # address < 0
        address = -1
        params = [address, str(stake_amount), str(mint_amount)]
        status_code, response = transaction.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type int, expected str" in r['error']['message']

    @allure.feature('Test submitstakecdptx create')
    def test_submitstakecdptx_create_stakeamount_none(self):
        """
        @author:elaine.tan
        test submitstakecdptx when stake_amount is null
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 500000000
        mint_amount = "500000"
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None

        # wicc is null
        stake_amount = None
        params = [address, stake_amount, mint_amount]
        status_code, response = transaction.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type null, expected str" in r['error']['message']

    @allure.feature('Test submitstakecdptx create')
    def test_submitstakecdptx_create_stakeamount_zero(self):
        """
        @author:elaine.tan
        test submitstakecdptx when stake_amount is 0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 500000000
        mint_amount = "500000"
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None

        # wicc is 0
        stake_amount = "0"
        params = [address, stake_amount, mint_amount]
        status_code, response = transaction.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "stake_amount is zero" in r['error']['message']

    @allure.feature('Test submitstakecdptx create')
    def test_submitstakecdptx_create_stakeamount_exceed(self):
        """
        @author:elaine.tan
        test submitstakecdptx when stake_amount < 0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 500000000
        mint_amount = "500000"
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None

        # wicc < 0
        stake_amount = "-100"
        params = [address, stake_amount, mint_amount]
        status_code, response = transaction.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "bcoinsToStake ComboMoney format error" in r['error']['message']

    @allure.feature('Test submitstakecdptx create')
    def test_submitstakecdptx_create_stakeamount_small(self):
        """
        @author:elaine.tan
        test submitstakecdptx when stake_amount < $1
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 500000000
        mint_amount = "500000"
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None

        # wicc < $1
        price = account.getprice("WICC")
        stakeVal = 80000000
        stake_amount = int(stakeVal / price)
        address = account.getnewaddresswithWICC(self.init, balance)
        params = [address, str(stake_amount), mint_amount]
        status_code, response = transaction.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "total-staked-bcoins-too-small" in r['error']['message']

    @allure.feature('Test submitstakecdptx create')
    def test_submitstakecdptx_create_stakeamount_equal(self):
        """
        @author:elaine.tan
        test submitstakecdptx when stake_amount = $1
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 500000000
        mint_amount = "500000"
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None

        # wicc = $1
        price = account.getprice("WICC")
        stakeVal = 100000000
        stake_amount = int(stakeVal / price)
        params = [address, str(stake_amount), mint_amount]
        status_code, response = transaction.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['result']['txid'] is not None
            assert r['error'] is None

    @allure.feature('Test submitstakecdptx create')
    def test_submitstakecdptx_create_stakeamount_balance(self):
        """
        @author:elaine.tan
        test submitstakecdptx when stake_amount > balance
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 500000000
        mint_amount = "500000"
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None

        # wicc > balance
        stake_amount = balance * 2
        params = [address, str(stake_amount), mint_amount]
        status_code, response = transaction.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "bcoins-insufficient-error" in r['error']['message']

    @allure.feature('Test submitstakecdptx create')
    def test_submitstakecdptx_create_stakeamount_small(self):
        """
        @author:elaine.tan
        test submitstakecdptx when stake_amount > 1
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 500000000
        mint_amount = "500000"
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None

        # wicc > 1
        price = account.getprice("WICC")
        stakeVal = 120000000
        stake_amount = int(stakeVal / price)
        params = [address, str(stake_amount), mint_amount]
        status_code, response = transaction.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['result']['txid'] is not None
            assert r['error'] is None

    @allure.feature('Test submitstakecdptx create')
    def test_submitstakecdptx_create_mintamount_None(self):
        """
        @author:elaine.tan
        test submitstakecdptx when mint_amount is null
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 500000000
        stake_amount = 100000000
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None

        # WUSD is null
        mint_amount = None
        params = [address, str(stake_amount), mint_amount]
        status_code, response = transaction.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type null, expected str" in r['error']['message']

    @allure.feature('Test submitstakecdptx create')
    def test_submitstakecdptx_create_mintamount_zero(self):
        """
        @author:elaine.tan
        test submitstakecdptx when mint_amount = 0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 500000000
        stake_amount = 100000000
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None

        # WUSD is 0
        mint_amount = "0"
        params = [address, str(stake_amount), mint_amount]
        status_code, response = transaction.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "mint_amount is zero" in r['error']['message']

    @allure.feature('Test submitstakecdptx create')
    def test_submitstakecdptx_create_mintamount_negative(self):
        """
        @author:elaine.tan
        test submitstakecdptx when mint_amount < 0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 500000000
        stake_amount = 100000000
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None

        # WUSD < 0
        mint_amount = "-100"
        params = [address, str(stake_amount), mint_amount]
        status_code, response = transaction.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "scoinsToMint ComboMoney format error" in r['error']['message']

    @allure.feature('Test submitstakecdptx create')
    def test_submitstakecdptx_create_smallrate(self):
        """
        @author:elaine.tan
        test submitstakecdptx when rate < 190%
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 500000000
        stake_amount = 100000000
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None

        # rate < 190%
        rate = 1.8
        price = transaction.getprice("WICC")
        print(8, price)
        mint_amount = int((stake_amount * price) / rate)
        params = [address, str(stake_amount), str(mint_amount)]
        print(8, params)
        status_code, response = transaction.submitstakecdptx(params)
        print(8, response)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "CDP-collateral-ratio-toosmall" in r['error']['message']

    @allure.feature('Test submitstakecdptx create')
    def test_submitstakecdptx_create_largerate(self):
        """
        @author:elaine.tan
        test submitstakecdptx when rate > 190%
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 500000000
        stake_amount = 100000000
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None

        # rate > 190%
        rate = 3
        price = transaction.getprice("WICC")
        mint_amount = int((stake_amount * price) / rate)
        params = [address, str(stake_amount), str(mint_amount)]
        status_code, response = transaction.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitstakecdptx create')
    def test_submitstakecdptx_create_fee_None(self):
        """
        @author:elaine.tan
        test submitstakecdptx when fee is null
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 500000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None

        # fee is null
        fee = None
        params = [address, str(stake_amount), str(mint_amount), fee]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type null, expected str" in r['error']['message']

    @allure.feature('Test submitstakecdptx create')
    def test_submitstakecdptx_create_fee_negative(self):
        """
        @author:elaine.tan
        test submitstakecdptx when fee < 0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 500000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None

        # address = account.getnewaddresswithbalance(self.init, balance)
        # assert address is not None
        # fee < 100000
        fee = "9999"
        params = [address, str(stake_amount), str(mint_amount), "", "WICC:" + fee + ":sawi"]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "The given fee is too small" in r['error']['message']

    @allure.feature('Test submitstakecdptx create')
    def test_submitstakecdptx_create_fee_zero(self):
        """
        @author:elaine.tan
        test submitstakecdptx when fee = 0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 500000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None

        # fee is 0
        fee = "0"
        params = [address, str(stake_amount), str(mint_amount), "", "WICC:" + fee]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "The given fee is too small" in r['error']['message']

    @allure.feature('Test submitstakecdptx create')
    def test_submitstakecdptx_create_fee_invalid(self):
        """
        @author:elaine.tan
        test submitstakecdptx when fee = 100000 but invalid format
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 500000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None

        fee = "100000"
        params = [address, str(stake_amount), str(mint_amount), "", "WICC:" + fee + ":abc"]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Invalid combo money format" in r['error']['message']

    @allure.feature('Test submitstakecdptx create')
    def test_submitstakecdptx_create_fee_equal(self):
        """
        @author:elaine.tan
        test submitstakecdptx when fee = 100000
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 500000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None

        fee = str(self.cdpfee)
        params = [address, str(stake_amount), str(mint_amount), "", "WICC:" + fee + ":sawi"]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitstakecdptx create')
    def test_submitstakecdptx_create_fee_greater(self):
        """
        @author:elaine.tan
        test submitstakecdptx when fee > 100000
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 500000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None

        # fee > 100000
        fee = str(self.cdpfee * 2)
        params = [address, str(stake_amount), str(mint_amount), "", "WICC:" + fee + ":sawi"]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitstakecdptx create')
    def test_submitstakecdptx_create_fee_WUSD(self):
        """
        @author:elaine.tan
        test submitstakecdptx when fee is WUSD
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 500000000
        stake_amount = 100000000
        fee = str(self.cdpfee)
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None

        sender = account.getNewAddrWithEnoughWUSD(self.init, max(int(fee)+self.sendfee, mint_amount))
        assert sender is not None
        res = account.send([sender, address, "WUSD:"+fee, "WUSD:"+str(self.sendfee)])
        assert res
        sleep(6)

        # fee is WUSD
        params = [address, str(stake_amount), str(mint_amount), "", "WUSD:" + fee + ":sawi"]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitstakecdptx create')
    def test_submitstakecdptx_create_fee_WGRT(self):
        """
        @author:elaine.tan
        test submitstakecdptx when fee is WGRT
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 500000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None

        # fee > 100000
        fee = "100000"
        params = [address, str(stake_amount), str(mint_amount), "", "WGRT:" + fee + ":sawi"]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print(r['error']['message'])

    @allure.feature('Test submitstakecdptx add')
    def test_submitstakecdptx_add_normal(self):
        """
        @author:elaine.tan
        test add submitstakecdptx when normal:
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = ""
        status_code, response = tran.submitstakecdptx(params)
        for r in response:
            assert r['result'] is not None
            assert r['result']['txid'] is not None
            txid = r['result']['txid']

        params = [address, str(stake_amount), str(mint_amount), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['result']['txid'] is not None
            assert r['error'] is None

    @allure.feature('Test submitstakecdptx add')
    def test_submitstakecdptx_add_stakeamount_None(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when stake_amount is None
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake * price) / 2)

        address = account.getnewaddresswithWICC(self.init, balance)
        params = [address, str(stake), str(mint_amount)]
        txid = account.getNewCDP(params)

        # stake=None
        stake_amount = None
        params = [address, stake_amount, str(mint_amount), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type null, expected str" in r['error']['message']

    @allure.feature('Test submitstakecdptx add')
    def test_submitstakecdptx_add_stakeamount_negative(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when stake_amount<0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake * price) / 2)

        address = account.getnewaddresswithWICC(self.init, balance)
        params = [address, str(stake), str(mint_amount)]
        txid = account.getNewCDP(params)

        # stake<0
        stake_amount = "-100"
        params = [address, stake_amount, str(mint_amount), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "bcoinsToStake ComboMoney format error" in r['error']['message']

    @allure.feature('Test submitstakecdptx add')
    def test_submitstakecdptx_add_stakeamount_int(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when stake_amount is int
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake * price) / 2)

        address = account.getnewaddresswithWICC(self.init, balance)
        params = [address, str(stake), str(mint_amount)]
        txid = account.getNewCDP(params)

        # stake is int
        stake_amount = 100
        params = [address, stake_amount, str(mint_amount), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type int, expected str" in r['error']['message']

    @allure.feature('Test submitstakecdptx add')
    def test_submitstakecdptx_add_stakeamount_zero(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when stake_amount = 0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake * price) / 2)

        address = account.getnewaddresswithWICC(self.init, balance)
        params = [address, str(stake), str(mint_amount)]
        txid = account.getNewCDP(params)

        # stake=0
        stake_amount = "0"
        params = [address, stake_amount, str(mint_amount), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "CDP-collateral-ratio-toosmall" in r['error']['message']

    @allure.feature('Test submitstakecdptx add')
    def test_submitstakecdptx_add_stakeamount_balance(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when stake_amount > balance
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake * price) / 2)

        address = account.getnewaddresswithWICC(self.init, balance)
        params = [address, str(stake), str(mint_amount)]
        txid = account.getNewCDP(params)

        # stake>balance
        stake_amount = "200000000000"
        params = [address, stake_amount, str(mint_amount), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "bcoins-insufficient-error" in r['error']['message']

    @allure.feature('Test submitstakecdptx add')
    def test_submitstakecdptx_add_stakeamount_invalid(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when stake_amount is invalid str
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake * price) / 2)

        address = account.getnewaddresswithWICC(self.init, balance)
        params = [address, str(stake), str(mint_amount)]
        txid = account.getNewCDP(params)

        # stake is invalid str
        stake_amount = "invalid"
        params = [address, stake_amount, str(mint_amount), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "bcoinsToStake ComboMoney format error" in r['error']['message']

    @allure.feature('Test submitstakecdptx add')
    def test_submitstakecdptx_add_stakeamount_small(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when stake_amount < 1WICC
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake * price) / 2)

        address = account.getnewaddresswithWICC(self.init, balance)
        params = [address, str(stake), str(mint_amount)]
        txid = account.getNewCDP(params)

        # stake<1WICC
        stake_amount = "50000000"
        params = [address, stake_amount, "0", txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitstakecdptx add')
    def test_submitstakecdptx_add_address_None(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when address is None
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 300000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)

        cdpaddr = account.getnewaddresswithWICC(self.init, balance)
        params = [cdpaddr, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)

        # address=None
        address = None
        params = [address, str(stake_amount), str(mint_amount), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type null, expected str" in r['error']['message']

    @allure.feature('Test submitstakecdptx add')
    def test_submitstakecdptx_add_address_notexist(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when address not exist
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 300000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)

        cdpaddr = account.getnewaddresswithWICC(self.init, balance)
        params = [cdpaddr, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)

        # address not exist
        address = "wgNbJoNmHKCTGfzgzo1NNQdNQeDErUKGiB"
        params = [address, str(stake_amount), str(mint_amount), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "not found" in r['error']['message']

    @allure.feature('Test submitstakecdptx add')
    def test_submitstakecdptx_add_address_int(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when address is int
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 300000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)

        cdpaddr = account.getnewaddresswithWICC(self.init, balance)
        params = [cdpaddr, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)

        # address is int
        address = 123
        params = [address, str(stake_amount), str(mint_amount), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type int, expected str" in r['error']['message']

    @allure.feature('Test submitstakecdptx add')
    def test_submitstakecdptx_add_address_invalid(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when address is invalid str
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 300000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)

        cdpaddr = account.getnewaddresswithWICC(self.init, balance)
        params = [cdpaddr, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)

        # address is invalid str
        address = "invalid"
        params = [address, str(stake_amount), str(mint_amount), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Invalid addr" in r['error']['message']

    @allure.feature('Test submitstakecdptx add')
    def test_submitstakecdptx_add_address_notregister(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when address is not register
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 300000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)

        cdpaddr = account.getnewaddresswithWICC(self.init, balance)
        params = [cdpaddr, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)

        # address is not registered
        address = account.newaddress()
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "account not exists" in r['error']['message']

    @allure.feature('Test submitstakecdptx add')
    def test_submitstakecdptx_add_address_notenough(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when address has not enough money
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 300000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)

        cdpaddr = account.getnewaddresswithWICC(self.init, balance)
        params = [cdpaddr, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)

        # address has no enough money
        params = [cdpaddr, str(balance), str(mint_amount), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "bcoins-insufficient-error" in r['error']['message']

    @allure.feature('Test submitstakecdptx add')
    def test_submitstakecdptx_add_address_notowner(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when address is not cdp owner
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 300000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)

        cdpaddr = account.getnewaddresswithWICC(self.init, balance)
        params = [cdpaddr, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)

        # address is not CDP owner
        address = account.getnewaddresswithWICC(self.init, stake_amount * 2)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "permission-denied" in r['error']['message']

    @allure.feature('Test submitstakecdptx add')
    def test_submitstakecdptx_add_mintunnormal_None(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when mint_amount is None
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 300000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)

        # mint is None
        mint_amount = None
        params = [address, str(stake_amount), mint_amount, txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type null, expected str" in r['error']['message']

    @allure.feature('Test submitstakecdptx add')
    def test_submitstakecdptx_add_mintunnormal_negative(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when mint_amount <0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 300000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)

        # mint<0
        mint_amount = "-100"
        params = [address, str(stake_amount), mint_amount, txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "scoinsToMint ComboMoney format error" in r['error']['message']

    @allure.feature('Test submitstakecdptx add')
    def test_submitstakecdptx_add_mintunnormal_zero(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when mint_amount =0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 300000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)

        # mint=0
        mint_amount = "0"
        params = [address, str(stake_amount), mint_amount, txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitstakecdptx add')
    def test_submitstakecdptx_add_mintunnormal_larger(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when mint_amount > all WUSD
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 300000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)

        # mint>all WUSD
        mint_amount = "300000000"
        params = [address, str(stake_amount), mint_amount, txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "CDP-collateral-ratio-toosmall" in r['error']['message']

    @allure.feature('Test submitstakecdptx add')
    def test_submitstakecdptx_add_mintunnormal_int(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when mint_amount is int
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 300000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)

        # mint is int
        mint_amount = 100
        params = [address, str(stake_amount), mint_amount, txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type int, expected str" in r['error']['message']

    @allure.feature('Test submitstakecdptx add')
    def test_submitstakecdptx_add_mintunnormal_invalid(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when mint_amount is invalid str
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 300000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)

        # mint is invalid str
        mint_amount = "invalid"
        params = [address, str(stake_amount), mint_amount, txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "scoinsToMint ComboMoney format error" in r['error']['message']

    @allure.feature('Test submitstakecdptx add')
    def test_submitstakecdptx_add_mintunnormal_balance(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when mint_amount > cdp balance
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 300000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)

        # mint>可贷出
        params = [address, str(stake_amount), str(stake_amount * 3), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "CDP-collateral-ratio-toosmall" in r['error']['message']

    @allure.feature('Test submitstakecdptx add')
    def test_submitstakecdptx_add_fee_None(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when fee is None
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 400000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)

        # fee is None
        fee = None
        params = [address, str(stake_amount), str(mint_amount), txid, fee]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Invalid json value type: null" in r['error']['message']

    @allure.feature('Test submitstakecdptx add')
    def test_submitstakecdptx_add_fee_negative(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when fee<0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 400000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)

        # fee<0
        fee = "-100"
        params = [address, str(stake_amount), str(mint_amount), txid, fee]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Invalid combo money format" in r['error']['message']

    @allure.feature('Test submitstakecdptx add')
    def test_submitstakecdptx_add_fee_zero(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when fee=0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 400000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)

        # fee=0
        fee = "0"
        params = [address, str(stake_amount), str(mint_amount), txid, fee]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "fee is too small" in r['error']['message']

    @allure.feature('Test submitstakecdptx add')
    def test_submitstakecdptx_add_fee_small(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when fee< 100000
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 400000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)

        # fee<100000
        fee = "9999"
        params = [address, str(stake_amount), str(mint_amount), txid, fee]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "fee is too small" in r['error']['message']

    @allure.feature('Test submitstakecdptx add')
    def test_submitstakecdptx_add_fee_invalid(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when fee is invalid
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 400000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)

        # fee is invalid str
        fee = "invalid"
        params = [address, str(stake_amount), str(mint_amount), txid, fee]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Invalid combo money format" in r['error']['message']

    def test_submitstakecdptx_add_fee_int(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when fee is int
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 400000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)

        # fee is int
        fee = 100
        params = [address, str(stake_amount), str(mint_amount), txid, fee]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "fee is too small" in r['error']['message']

    def test_submitstakecdptx_add_fee_wicc(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when fee is wicc
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 400000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)

        # fee = 100000 is WICC
        fee = str(self.cdpfee)
        params = [address, str(stake_amount), str(mint_amount), txid, "WICC:" + fee + ":sawi"]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

    def test_submitstakecdptx_add_fee_WUSD(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when fee is wusd
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 400000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        sleep(10)
        # fee > 100000 is WUSD
        fee = str(self.cdpfee+10000)
        params = [address, str(stake_amount), str(mint_amount), txid, "WUSD:" + fee + ":sawi"]
        status_code, response = tran.submitstakecdptx(params)
        print(response)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

    def test_submitstakecdptx_add_fee_WGRT(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when fee is wgrt
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 400000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)

        # fee > 100000 is WGRT
        fee = "110000"
        params = [address, str(stake_amount), str(mint_amount), txid, "WGRT:" + fee + ":sawi"]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Fee symbol is WGRT" in r['error']['message']

    @allure.feature('Test submitstakecdptx add')
    def test_submitstakecdptx_add_cdpid_None(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when txid is None
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        params = [address, str(stake_amount), str(mint_amount)]
        cdptxid = account.getNewCDP(params)
        assert cdptxid is not None

        # txid=None
        txid = None
        params = [address, str(stake_amount), str(mint_amount), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type null, expected str" in r['error']['message']

    @allure.feature('Test submitstakecdptx add')
    def test_submitstakecdptx_add_cdpid_zero(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when txid is zero
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        params = [address, str(stake_amount), str(mint_amount)]
        cdptxid = account.getNewCDP(params)
        assert cdptxid is not None

        # txid=0
        txid = "0"
        params = [address, str(stake_amount), str(mint_amount), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Get param cdp_id error" in r['error']['message']

    def test_submitstakecdptx_add_cdpid_int(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when txid is int
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        params = [address, str(stake_amount), str(mint_amount)]
        cdptxid = account.getNewCDP(params)
        assert cdptxid is not None

        # txid is int
        txid = 100
        params = [address, str(stake_amount), str(mint_amount), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type int, expected str" in r['error']['message']

    def test_submitstakecdptx_add_cdpid_notexist(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when txid is not exist
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        params = [address, str(stake_amount), str(mint_amount)]
        cdptxid = account.getNewCDP(params)
        assert cdptxid is not None

        # txid not exist
        txid = "ef7a777ddb3ea85b75b3ceaac15f269f270994a3b55165a2439c48664a7f3bf6"
        params = [address, str(stake_amount), str(mint_amount), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "cdp-not-exist" in r['error']['message']

    def test_submitstakecdptx_add_cdpid_invalid(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when txid is invalid
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        params = [address, str(stake_amount), str(mint_amount)]
        cdptxid = account.getNewCDP(params)
        assert cdptxid is not None

        # txid invalid
        txid = "invalid"
        params = [address, str(stake_amount), str(mint_amount), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "param cdp_id error" in r['error']['message']

    def test_submitstakecdptx_add_cdpid_closed(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when txid is closed cdp
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 300000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        params = [address, str(stake_amount), str(mint_amount)]
        cdptxid = account.getNewCDP(params)
        assert cdptxid is not None
        sendaddr = account.getNewAddrWithEnoughWUSD(self.init, mint_amount + self.sendfee)
        assert sendaddr is not None
        res = tran.send([sendaddr, address, "WUSD:" + str(mint_amount), "WUSD:"+str(self.sendfee)])
        assert res
        sleep(6)

        # close CDP
        params = [address, cdptxid, mint_amount, stake_amount]
        resredeem = tran.submitredeemcdptx(params)
        print(resredeem)
        sleep(6)

        # cdp is closed,txid is not exists
        params = [address, str(stake_amount), str(mint_amount), cdptxid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "cdp-not-exist" in r['error']['message']

    @allure.feature('Test submitstakecdptx add')
    def test_submitstakecdptx_add_rate(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when before rate=190%:
        # add and loan,add and loan=0,add=0 and loan
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 20000000000
        # create rate = 190%
        stake_amount = 190000000
        price = tran.getprice("WICC")
        assert price is not None
        mint_amount = int((stake_amount * price) / 1.9)
        address = account.getnewaddresswithWICC(self.init, balance)
        logger.info(address)
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(5)

        # before rate=190%,now rate<190%,error
        params = [address, str(stake_amount), str(mint_amount * 2), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "CDP-collateral-ratio-toosmall" in r['error']['message']

        # before rate=190%,now rate=190%,pass
        params = [address, str(stake_amount), str(mint_amount), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None
        sleep(5)

        # before rate=190%,now rate>190%,pass
        params = [address, str(stake_amount * 2), str(mint_amount), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None
        sleep(5)

        # 调整抵押率=190%
        params = [address, str(stake_amount), str(mint_amount * 2), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None
        sleep(5)

        # before rate=190%,just loan，error
        params = [address, "0", str(mint_amount), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "CDP-collateral-ratio-toosmall" in r['error']['message']

        # before rate=190%,just stake，pass
        params = [address, str(stake_amount), "0", txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

    # Passed
    @allure.feature('Test submitstakecdptx add')
    def test_submitstakecdptx_add_rate2(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when before rate>190%:
        add and loan,add and loan=0,add=0 and loan
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 20000000000
        # create rate > 190%(190%*2)
        stake_amount = 190000000
        price = tran.getprice("WICC")
        assert price is not None
        mint_amount = int((stake_amount * price) / 1.9)
        address = account.getnewaddresswithWICC(self.init, balance)
        logger.info(address)
        params = [address, str(stake_amount * 2), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(5)

        # before rate>190%,(b1+b2)/(a1+a2)<190%,error
        params = [address, str(stake_amount), str(mint_amount * 4), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "CDP-collateral-ratio-toosmall" in r['error']['message']

        # before rate>190%,(b1+b2)/(a1+a2)>190%,pass
        params = [address, str(stake_amount), str(mint_amount), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None
        sleep(5)

        # before rate>190%,(b1+b2)/(a1+a2)=190%,pass
        params = [address, str(stake_amount), str(mint_amount * 2), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None
        sleep(5)

        # 调整抵押率，使抵押率>190%
        params = [address, str(stake_amount * 3), str(mint_amount), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None
        sleep(5)

        # before rate>190%,just loan(total<190%),error
        params = [address, "0", str(mint_amount * 10), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "CDP-collateral-ratio-toosmall" in r['error']['message']

        # before rate>190%,just loan(total>190%),pass
        params = [address, "0", str(mint_amount), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None
        sleep(5)

        # before rate>190%,just stake,pass
        params = [address, str(stake_amount), "0", txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None
        sleep(5)

        # before rate>190%,just loan(total=190%),pass
        params = [address, "0", str(mint_amount * 2), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

    # Passed
    @allure.feature('Test submitstakecdptx add')
    def test_submitstakecdptx_add_rate3(self):
        """
        @author:elaine.tan
        test submitstakecdptx add cdpstake when before rate<190%:
        add and loan,add and loan=0,add=0 and loan
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 20000000000
        # create rate = 190%
        stake = 190000000
        price = tran.getprice("WICC")
        assert price is not None
        mint = int((stake * price) / 1.9)
        address = account.getnewaddresswithWICC(self.init, balance)
        logger.info(address)
        params = [address, str(stake), str(mint)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(5)

        # 喂价，使抵押率低于190%
        result = account.feedingPrice(self.init, "USD", price - 0.1, price - 0.1)
        assert result is True
        sleep(15)
        stake_amount = 190000000
        price2 = tran.getprice("WICC")
        assert price2 is not None
        mint_amount = int((stake_amount * price2) / 1.9)

        # before rate<190%,just loan,error
        params = [address, "0", str(mint_amount), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "CDP-collateral-ratio-toosmall" in r['error']['message']

        # before rate<190%,now rate=190%,pass
        params = [address, str(stake_amount), str(mint_amount), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

        # 喂价，使抵押率低于190%
        result = account.feedingPrice(self.init, "USD", price2 - 0.1, price2 - 0.1)
        assert result is True
        sleep(15)
        stake_amount = 190000000
        price3 = tran.getprice("WICC")
        assert price3 is not None
        mint_amount = int((stake_amount * price3) / 1.9)

        # before rate<190%,now rate>190%,pass
        params = [address, str(int(stake_amount * 1.1)), str(mint_amount), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

        # 喂价，使抵押率低于190%
        result = account.feedingPrice(self.init, "USD", price3 - 0.2, price3 - 0.2)
        assert result is True
        sleep(15)

        # before rate<190%,just stake,pass
        params = [address, str(stake_amount), "0", txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

        # 喂价
        sleep(30)
        result = account.feedingPrice(self.init, "USD", 2, 2)
        assert result is True
        sleep(6)

    @allure.feature('Test submitredeemcdptx')
    def test_submitredeemcdptx_normal_part(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when normal part redeem
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 190000000
        price = tran.getprice("WICC")
        assert price is not None
        mint_amount = int((stake_amount * price) / 1.9)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None

        sleep(5)
        params = [address, txid, int(mint_amount / 2), int(stake_amount / 2)]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitredeemcdptx')
    def test_submitredeemcdptx_normal_allredeem(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when normal all redeem
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 300000000
        stake_amount = 190000000
        price = tran.getprice("WICC")
        assert price is not None
        mint_amount = int((stake_amount * price) / 1.9)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None

        # 向地址转WUSD
        sender = account.getnewaddresswithWICC(self.init, balance)
        params = [sender, str(stake_amount), str(mint_amount)]
        sendertxid = account.getNewCDP(params)
        assert sendertxid is not None
        sleep(6)
        res = account.send([sender, address, "WUSD:" + str(mint_amount), self.sendfee])
        assert res
        sleep(6)

        # all repay , all redeem
        params = [address, txid, int(mint_amount), int(stake_amount)]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

        sleep(6)
        status_code, response = tran.getcdp([txid])
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "not exist" in r['error']['message']

    @allure.feature('Test submitredeemcdptx')
    def test_submitredeemcdptx_normal_allrepay(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when normal all repay
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 190000000
        price = tran.getprice("WICC")
        assert price is not None
        mint_amount = int((stake_amount * price) / 1.9)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        print(txid)
        assert txid is not None

        # 向地址转WUSD
        sender = account.getnewaddresswithWICC(self.init, balance)
        params = [sender, str(stake_amount), str(mint_amount)]
        sendertxid = account.getNewCDP(params)
        assert sendertxid is not None
        sleep(6)
        res = account.send([sender, address, "WUSD:" + str(mint_amount), self.sendfee])
        assert res
        sleep(6)

        # all repay ,redeem = 0
        params = [address, txid, int(mint_amount), 0]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

        sleep(6)
        status_code, response = tran.getcdp([txid])
        print(1111, response)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['cdp'] is not None

        # part redeem
        params = [address, txid, 0, int(stake_amount / 2)]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

        sleep(6)
        status_code, response = tran.getcdp([txid])
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['cdp'] is not None

        # all redeem
        params = [address, txid, 0, int(stake_amount / 2)]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

        sleep(6)
        status_code, response = tran.getcdp([txid])
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "not exist" in r['error']['message']

    # @allure.feature('Test submitredeemcdptx')
    # def test_submitredeemcdptx_allredeem_over(self):
    #     """
    #     @author:elaine.tan
    #     test submitredeemcdptx when 归还量与赎回量超过待归还与待赎回时
    #     """
    #     account = Account(self.host, self.rpcuser, self.rpcpassword)
    #     tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
    #     balance = 200000000
    #     stake_amount = 200000000
    #     price = tran.getprice("WICC")
    #     assert price is not None
    #     mint_amount = int((stake_amount * price) / 2)
    #     address = account.getnewaddresswithWICC(self.init, balance)
    #     assert address is not None
    #     params = [address, str(stake_amount), str(mint_amount)]
    #     txid = account.getNewCDP(params)
    #     assert txid is not None
    #
    #     # 向地址转WUSD
    #     sender = account.getnewaddresswithWICC(self.init, balance)
    #     params = [sender, str(stake_amount), str(mint_amount)]
    #     sendertxid = account.getNewCDP(params)
    #     assert sendertxid is not None
    #     sleep(6)
    #     res = tran.send([sender, address, "WUSD:" + str(mint_amount), 10000])
    #     sleep(6)
    #
    #     # all repay , all redeem
    #     params = [address, txid, int(mint_amount), int(stake_amount)]
    #     status_code, response = tran.submitredeemcdptx(params)
    #     assert status_code == 200
    #     for r in response:
    #         assert r['error'] is None
    #         assert r['result'] is not None
    #         assert r['result']['txid'] is not None
    #
    #     sleep(6)
    #     status_code, response = tran.getcdp([txid])
    #     assert status_code == 200
    #     for r in response:
    #         assert r['error'] is not None
    #         assert r['result'] is None
    #         assert "not exist" in r['error']['message']

    @allure.feature('Test submitredeemcdptx address')
    def test_submitredeemcdptx_address_None(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when address is None
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 1.9)
        assert mint_amount is not 0

        cdpaddr = account.getnewaddresswithWICC(self.init, balance)
        params = [cdpaddr, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(8)

        repay_amount = int(mint_amount / 2)
        # address=None
        address = None
        params = [address, txid, repay_amount, stake_amount]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type null, expected str" in r['error']['message']

    @allure.feature('Test submitredeemcdptx address')
    def test_submitredeemcdptx_address_notexist(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when address not exist
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 1.9)
        assert mint_amount is not 0

        cdpaddr = account.getnewaddresswithWICC(self.init, balance)
        params = [cdpaddr, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(8)

        repay_amount = int(mint_amount / 2)
        # address not exist
        address = "wgNbJoNmHKCTGfzgzo1NNQdNQeDErUKGiB"
        params = [address, txid, repay_amount, stake_amount]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "not found" in r['error']['message']

    @allure.feature('Test submitredeemcdptx address')
    def test_submitredeemcdptx_address_int(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when address is int
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 1.9)
        assert mint_amount is not 0

        cdpaddr = account.getnewaddresswithWICC(self.init, balance)
        params = [cdpaddr, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(8)

        repay_amount = int(mint_amount / 2)
        # address is int
        address = 123
        params = [address, txid, repay_amount, stake_amount]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type int, expected str" in r['error']['message']

    @allure.feature('Test submitredeemcdptx address')
    def test_submitredeemcdptx_address_invalid(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when address is invalid str
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 1.9)
        assert mint_amount is not 0

        cdpaddr = account.getnewaddresswithWICC(self.init, balance)
        params = [cdpaddr, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(8)

        repay_amount = int(mint_amount / 2)
        # address is invalid str
        address = "invalid"
        params = [address, txid, repay_amount, stake_amount]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Invalid addr" in r['error']['message']

    @allure.feature('Test submitredeemcdptx address')
    def test_submitredeemcdptx_address_unregister(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when address is not register
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 1.9)
        assert mint_amount is not 0

        cdpaddr = account.getnewaddresswithWICC(self.init, balance)
        params = [cdpaddr, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(8)

        repay_amount = int(mint_amount / 2)
        # address is not registe
        address = account.newaddress()
        assert address is not None
        params = [address, txid, repay_amount, stake_amount]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "account not exists" in r['error']['message']

    @allure.feature('Test submitredeemcdptx address')
    def test_submitredeemcdptx_address_notowner(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when address is not cdp owner
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 1.9)
        assert mint_amount is not 0

        cdpaddr = account.getnewaddresswithWICC(self.init, balance)
        params = [cdpaddr, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(8)

        repay_amount = int(mint_amount / 2)
        # address is not CDP owner
        address = account.getnewaddresswithWICC(self.init, stake_amount * 2)
        assert address is not None
        params = [address, txid, repay_amount, stake_amount]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "permission-denied" in r['error']['message']

    @allure.feature('Test submitredeemcdptx address')
    def test_submitredeemcdptx_address_notenough(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when address has no enough money
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 1.9)
        assert mint_amount is not 0

        cdpaddr = account.getnewaddresswithWICC(self.init, balance)
        params = [cdpaddr, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(8)

        # address has no enough money
        params = [cdpaddr, txid, mint_amount, stake_amount]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "account-balance-insufficient" in r['error']['message']

    @allure.feature('Test submitredeemcdptx cdpid')
    def test_submitredeemcdptx_cdpid_None(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when cdpid is None
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 1.9)
        assert mint_amount is not 0
        cdpaddr = account.getnewaddresswithWICC(self.init, balance)

        # cdpid is Null
        txid = None
        params = [cdpaddr, txid, mint_amount - 100, stake_amount - 100]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type null, expected str" in r['error']['message']

    @allure.feature('Test submitredeemcdptx cdpid')
    def test_submitredeemcdptx_cdpid_closed(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when cdpid closed
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 1.9)
        assert mint_amount is not 0
        cdpaddr = account.getnewaddresswithWICC(self.init, balance)
        params = [cdpaddr, str(stake_amount), str(mint_amount)]
        cdpid = account.getNewCDP(params)
        assert cdpid is not None

        # close cdp
        sendaddr = account.getnewaddresswithWICC(self.init, balance)
        params = [sendaddr, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(6)
        res = account.send([sendaddr, cdpaddr, "WUSD:" + str(mint_amount), self.sendfee])
        sleep(6)
        redparams = [cdpaddr, cdpid, mint_amount, stake_amount]
        status_code, response = tran.submitredeemcdptx(redparams)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None
        sleep(6)

        # cdpid is closed
        params = [cdpaddr, cdpid, mint_amount - 100, stake_amount - 100]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "cdp-not-exist" in r['error']['message']

    @allure.feature('Test submitredeemcdptx cdpid')
    def test_submitredeemcdptx_cdpid_invalid(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when cdpid not exist
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 1.9)
        assert mint_amount is not 0
        cdpaddr = account.getnewaddresswithWICC(self.init, balance)

        # cdpid is invalid str
        txid = "invalid"
        params = [cdpaddr, txid, mint_amount - 100, stake_amount - 100]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "empty-cdpid" in r['error']['message']

    @allure.feature('Test submitredeemcdptx cdpid')
    def test_submitredeemcdptx_cdpid_int(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when cdpid not exist
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 1.9)
        assert mint_amount is not 0
        cdpaddr = account.getnewaddresswithWICC(self.init, balance)

        # cdpid is not exist
        txid = "f800ee4433cfb02bda6a7f441bdd1cb3cc95fcb4e7abed7bae515f39bdd26117"
        params = [cdpaddr, txid, mint_amount - 100, stake_amount - 100]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "cdp-not-exist" in r['error']['message']

    @allure.feature('Test submitredeemcdptx cdpid')
    def test_submitredeemcdptx_cdpid_notexist(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when cdpid is int
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 1.9)
        assert mint_amount is not 0
        cdpaddr = account.getnewaddresswithWICC(self.init, balance)

        # cdpid is int
        txid = 123
        params = [cdpaddr, txid, mint_amount - 100, stake_amount - 100]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type int, expected str" in r['error']['message']

    @allure.feature('Test submitredeemcdptx cdpid')
    def test_submitredeemcdptx_cdpid_zero(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when cdpid is 0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 1.9)
        assert mint_amount is not 0
        cdpaddr = account.getnewaddresswithWICC(self.init, balance)

        # cdpid is 0
        txid = 0
        params = [cdpaddr, txid, mint_amount - 100, stake_amount - 100]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type int, expected str" in r['error']['message']

    @allure.feature('Test submitredeemcdptx redeem_amount')
    def test_submitredeemcdptx_redeem_exceed(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when redeem_amount WICC >CDP stake
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 190000000
        price = tran.getprice("WICC")
        assert price is not None
        mint_amount = int((stake_amount * price) / 1.9)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(6)

        repay_amount = int(mint_amount / 2)
        # redeem > stake
        redeem_amount = stake_amount * 2
        params = [address, txid, repay_amount, redeem_amount]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "invalid-collatera-ratio" in r['error']['message']

    @allure.feature('Test submitredeemcdptx redeem_amount')
    def test_submitredeemcdptx_redeem_negative(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when redeem_amount WICC < 0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 190000000
        price = tran.getprice("WICC")
        assert price is not None
        mint_amount = int((stake_amount * price) / 1.9)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(6)
        repay_amount = int(mint_amount / 2)
        # redeem<0
        redeem_amount = -10000
        params = [address, txid, repay_amount, redeem_amount]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Invalid amount" in r['error']['message']

    @allure.feature('Test submitredeemcdptx redeem_amount')
    def test_submitredeemcdptx_redeem_None(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when redeem_amount WICC is None
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 190000000
        price = tran.getprice("WICC")
        assert price is not None
        mint_amount = int((stake_amount * price) / 1.9)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(6)

        repay_amount = int(mint_amount / 2)
        # redeem is None
        redeem_amount = None
        params = [address, txid, repay_amount, redeem_amount]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type null, expected real" in r['error']['message']

    @allure.feature('Test submitredeemcdptx redeem_amount')
    def test_submitredeemcdptx_redeem_invalid(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when redeem_amount WICC is invalid str
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 190000000
        price = tran.getprice("WICC")
        assert price is not None
        mint_amount = int((stake_amount * price) / 1.9)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(6)

        repay_amount = int(mint_amount / 2)
        # redeem is invalid str
        redeem_amount = "invalid"
        params = [address, txid, repay_amount, redeem_amount]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type str, expected real" in r['error']['message']

    @allure.feature('Test submitredeemcdptx redeem_amount')
    def test_submitredeemcdptx_repay_balance(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when repay_amount WUSD > balance
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 190000000
        price = tran.getprice("WICC")
        assert price is not None
        mint_amount = int((stake_amount * price) / 1.9)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(6)

        redeem_amount = int(stake_amount / 2)
        # redeem > balance
        repay_amount = mint_amount * 2
        params = [address, txid, repay_amount, redeem_amount]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "account-balance-insufficient" in r['error']['message']

    @allure.feature('Test submitredeemcdptx redeem_amount')
    def test_submitredeemcdptx_repay_exceed(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when repay_amount WUSD > balance
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 190000000
        price = tran.getprice("WICC")
        assert price is not None
        mint_amount = int((stake_amount * price) / 1.9)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sender = account.getNewAddrWithEnoughWUSD(self.init, mint_amount)
        res = account.send([sender, address, "WUSD:" + str(mint_amount - self.sendfee), "WUSD:"+str(self.sendfee)])
        assert res
        sleep(6)

        redeem_amount = int(stake_amount / 2)
        # redeem > stake
        repay_amount = mint_amount + 10000
        params = [address, txid, repay_amount, redeem_amount]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitredeemcdptx redeem_amount')
    def test_submitredeemcdptx_repay_negative(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when repay_amount WUSD < 0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 190000000
        price = tran.getprice("WICC")
        assert price is not None
        mint_amount = int((stake_amount * price) / 1.9)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(6)
        redeem_amount = int(stake_amount / 2)
        # repay<0
        repay_amount = -10000
        params = [address, txid, repay_amount, redeem_amount]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Invalid amount" in r['error']['message']

    @allure.feature('Test submitredeemcdptx redeem_amount')
    def test_submitredeemcdptx_repay_None(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when repay_amount WUSD is None
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 190000000
        price = tran.getprice("WICC")
        assert price is not None
        mint_amount = int((stake_amount * price) / 1.9)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(6)

        redeem_amount = int(mint_amount / 2)
        repay_amount = None
        params = [address, txid, repay_amount, redeem_amount]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type null, expected real" in r['error']['message']

    @allure.feature('Test submitredeemcdptx redeem_amount')
    def test_submitredeemcdptx_repay_invalid(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when repay_amount WUSD is invalid str
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 190000000
        price = tran.getprice("WICC")
        assert price is not None
        mint_amount = int((stake_amount * price) / 1.9)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(6)

        redeem_amount = int(mint_amount / 2)
        repay_amount = "invalid"
        params = [address, txid, repay_amount, redeem_amount]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type str, expected real" in r['error']['message']

    @allure.feature('Test submitredeemcdptx fee')
    def test_submitredeemcdptx_fee_none(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when fee is None
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 190000000
        price = tran.getprice("WICC")
        assert price is not None
        mint_amount = int((stake_amount * price) / 1.9)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(6)

        # Redeem Fee is Null
        fee = None
        params = [address, txid, mint_amount, stake_amount, fee]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Invalid json value type: null" in r['error']['message']

    @allure.feature('Test submitredeemcdptx fee')
    def test_submitredeemcdptx_fee_default(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when fee is default
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 190000000
        price = tran.getprice("WICC")
        assert price is not None
        mint_amount = int((stake_amount * price) / 1.9)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(6)

        # fee is default
        params = [address, txid, int(mint_amount / 2), int(stake_amount / 2)]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitredeemcdptx fee')
    def test_submitredeemcdptx_fee_exceed(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when fee is < 0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 190000000
        price = tran.getprice("WICC")
        assert price is not None
        mint_amount = int((stake_amount * price) / 1.9)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(6)

        # fee<0
        fee = -1000
        params = [address, txid, mint_amount, stake_amount, "WICC:" + str(fee)]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Invalid combo money format" in r['error']['message']

    @allure.feature('Test submitredeemcdptx fee')
    def test_submitredeemcdptx_fee_zero(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when fee = 0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 190000000
        price = tran.getprice("WICC")
        assert price is not None
        mint_amount = int((stake_amount * price) / 1.9)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(6)

        # fee=0
        fee = 0
        params = [address, txid, mint_amount, stake_amount, fee]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "fee is too small" in r['error']['message']

    @allure.feature('Test submitredeemcdptx fee')
    def test_submitredeemcdptx_fee_zero2(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when fee = 0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 190000000
        price = tran.getprice("WICC")
        assert price is not None
        mint_amount = int((stake_amount * price) / 1.9)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(6)

        # fee=0
        fee = 0
        params = [address, txid, mint_amount, stake_amount, "WUSD:" + str(fee)]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "fee is too small" in r['error']['message']

    @allure.feature('Test submitredeemcdptx fee')
    def test_submitredeemcdptx_fee_notenough(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when fee is < 100000
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 190000000
        price = tran.getprice("WICC")
        assert price is not None
        mint_amount = int((stake_amount * price) / 1.9)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(6)

        # fee<100000
        fee = 9999
        params = [address, txid, mint_amount, stake_amount, "WICC:" + str(fee)]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "fee is too small" in r['error']['message']

    @allure.feature('Test submitredeemcdptx fee')
    def test_submitredeemcdptx_fee_normal(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when fee = 100000
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 190000000
        price = tran.getprice("WICC")
        assert price is not None
        mint_amount = int((stake_amount * price) / 1.9)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(6)

        # fee=100000
        fee = self.cdpfee
        params = [address, txid, int(mint_amount / 2), int(stake_amount / 2), "WICC:" + str(fee)]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitredeemcdptx fee')
    def test_submitredeemcdptx_fee_normal2(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when fee = 100000
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 190000000
        price = tran.getprice("WICC")
        assert price is not None
        mint_amount = int((stake_amount * price) / 1.9)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(6)

        # fee=100000
        fee = self.cdpfee
        params = [address, txid, int(mint_amount / 2), int(stake_amount / 2), fee]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitredeemcdptx fee')
    def test_submitredeemcdptx_fee_normal3(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when fee >100000
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 190000000
        price = tran.getprice("WICC")
        assert price is not None
        mint_amount = int((stake_amount * price) / 1.9)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(6)

        # fee=100000
        fee = self.cdpfee+10000
        params = [address, txid, int(mint_amount / 2), int(stake_amount / 2), "WICC:" + str(fee)]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitredeemcdptx fee')
    def test_submitredeemcdptx_fee_wicc(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when fee is WICC
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 190000000
        price = tran.getprice("WICC")
        assert price is not None
        mint_amount = int((stake_amount * price) / 1.9)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(6)

        # fee is wicc
        fee = self.cdpfee
        params = [address, txid, int(mint_amount / 2), int(stake_amount / 2), "WICC:" + str(fee) + ":sawi"]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitredeemcdptx fee')
    def test_submitredeemcdptx_fee_wusd(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when fee is WUSD
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 190000000
        price = tran.getprice("WICC")
        assert price is not None
        mint_amount = int((stake_amount * price) / 1.9)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(6)

        # fee=100000
        fee = self.cdpfee
        params = [address, txid, int(mint_amount / 2), int(stake_amount / 2), "WUSD:" + str(fee)]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitredeemcdptx rate')
    def test_submitredeemcdptx_rate_equal2(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when before rate=190%:
        after rate >190%
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 20000000000
        # create rate = 190%
        stake_amount = 190000000
        price = tran.getprice("WICC")
        assert price is not None
        mint_amount = int((stake_amount * price) / 1.9)
        address = account.getnewaddresswithWICC(self.init, balance)
        logger.info(address)
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(6)

        # all rate > 190%
        params = [address, txid, int(mint_amount / 2), int(stake_amount / 4)]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None
        sleep(6)
        print(tran.getcdp([txid]))

    @allure.feature('Test submitredeemcdptx rate')
    def test_submitredeemcdptx_rate_equal(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when before rate=190%:
        only repay,only redeem,after rate >190%,after rate =190%,after rate <190%
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 20000000000
        # create rate = 190%
        stake_amount = 190000000
        price = tran.getprice("WICC")
        assert price is not None
        mint_amount = int((stake_amount * price) / 1.9)
        address = account.getnewaddresswithWICC(self.init, balance)
        logger.info(address)
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(5)

        # all rate< 190%
        params = [address, txid, int(mint_amount / 2), int(stake_amount / 2 + 100000)]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "invalid-collatera-ratio" in r['error']['message']

        # only redeem
        params = [address, txid, 0, stake_amount]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "invalid-collatera-ratio" in r['error']['message']

        # all rate= 190%
        params = [address, txid, int(mint_amount / 2), int(stake_amount / 2)]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None
        sleep(5)
        print(tran.getcdp([txid]))

        # only repay
        params = [address, txid, 1000000, 0]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitredeemcdptx rate')
    def test_submitredeemcdptx_leftover_equal(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when before rate=190%:
        leftover < 0.9，leftover > 0.9,leftover = 0.9
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 20000000000
        price = tran.getprice("WICC")
        # get sender
        stake = 18000000000
        mint = int(stake * price / 2)
        sender = account.getnewaddresswithWICC(self.init, balance)
        params = [sender, str(stake), str(mint)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(5)

        # create rate = 190%
        stake_amount = 190000000
        assert price is not None
        mint_amount = int((stake_amount * price) / 1.9)
        address = account.getnewaddresswithWICC(self.init, balance)
        logger.info(address)
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        res = account.send([sender, address, "WUSD:" + str(mint), self.sendfee])
        assert res
        sleep(6)

        # leftover < 0.9
        amount = 80000000 / price
        params = [address, txid, int(mint_amount), stake_amount - amount]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "total-staked-bcoins-too-small" in r['error']['message']

        # leftover > 0.9
        amount = 100000000 / price
        params = [address, txid, int(mint_amount), stake_amount - amount]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

        # leftover = 0.9
        amount = (100000000 - 90000000) / price
        params = [address, txid, int(mint_amount), int(amount)]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

        sleep(10)
        # leftover < 0.9
        amount = 90000000 / price
        params = [address, txid, 0, int(amount / 2)]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "total-staked-bcoins-too-small" in r['error']['message']

        # leftover = 0
        params = [address, txid, 0, int(amount)]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitredeemcdptx rate')
    def test_submitredeemcdptx_rate_small(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when before rate<190%:
        only repay,only redeem,after rate >190%,after rate =190%,after rate <190%
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 20000000000
        # create rate = 190%
        stake_amount = 190000000
        price = tran.getprice("WICC")
        assert price is not None
        mint_amount = int((stake_amount * price) / 1.9)
        address = account.getnewaddresswithWICC(self.init, balance)
        logger.info(address)
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(5)

        # set rate<190%
        res = account.feedingPrice(self.init, "USD", price - 0.05, price - 0.05)
        assert res
        sleep(6)

        # all rate< 190%
        params = [address, txid, int(mint_amount / 2), int(stake_amount / 2 + 100000)]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "invalid-collatera-ratio" in r['error']['message']

        # only redeem
        params = [address, txid, 0, stake_amount]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "invalid-collatera-ratio" in r['error']['message']

        # only repay
        params = [address, txid, 100, 0]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None
        sleep(6)

        # all rate > 190%
        params = [address, txid, int(mint_amount / 2), int(mint_amount / 4)]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

        res = account.feedingPrice(self.init, "USD", 2, 2)
        assert res
        sleep(12)

    @allure.feature('Test submitredeemcdptx rate')
    def test_submitredeemcdptx_rate_small2(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when before rate<190%:
        after rate =190%
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 20000000000
        # create rate = 190%
        stake_amount = 190000000
        price = tran.getprice("WICC")
        assert price is not None
        mint_amount = int((stake_amount * price) / 1.9)
        address = account.getnewaddresswithWICC(self.init, balance)
        logger.info(address)
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(6)
        res = account.feedingPrice(self.init, "USD", price - 0.1, price - 0.1)
        assert res
        sleep(12)
        # get price
        pri = account.getprice("WICC")
        # get cdpinfo
        wicc = 0
        wusd = 0
        status, response = tran.getcdp([txid])
        assert status == 200
        for r in response:
            wicc = r['result']['cdp']['total_bcoin']
            wusd = r['result']['cdp']['total_scoin']

        repay = int(wusd / 2)
        redeem = wicc - (1.9 * (wusd - repay) / pri)
        # all rate= 190%
        params = [address, txid, int(repay), int(redeem)]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

        res = account.feedingPrice(self.init, "USD", 2, 2)
        assert res
        sleep(12)

    @allure.feature('Test submitredeemcdptx rate')
    def test_submitredeemcdptx_rate_large(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when before rate>190%:
        only repay,only redeem,after rate >190%,after rate <190%
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 20000000000
        # create rate > 190%
        stake_amount = 190000000
        price = tran.getprice("WICC")
        assert price is not None
        mint_amount = int((stake_amount * price) / 1.9)
        address = account.getnewaddresswithWICC(self.init, balance)
        logger.info(address)
        params = [address, str(stake_amount * 2), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(6)

        # all rate< 190%
        params = [address, txid, int(mint_amount / 10), int(stake_amount * 2 - 10000)]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "invalid-collatera-ratio" in r['error']['message']

        # only redeem,after < 190%
        params = [address, txid, 0, stake_amount * 2]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "invalid-collatera-ratio" in r['error']['message']

        # all rate > 190%
        params = [address, txid, int(mint_amount / 2), int(stake_amount)]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None
        sleep(6)

        # only repay
        params = [address, txid, 10000, 0]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None
        sleep(6)

        # only redeem,after > 190%
        params = [address, txid, 0, 10000]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None
        sleep(6)

        # get cdpinfo
        wicc = 0
        wusd = 0
        status, response = tran.getcdp([txid])
        assert status == 200
        for r in response:
            print("cdpinfo:", response)
            wicc = r['result']['cdp']['total_bcoin']
            wusd = r['result']['cdp']['total_scoin']

        sleep(10)
        # get price
        pri = account.getprice("WICC")
        print(pri)
        repay = int(wusd / 10)
        redeem = int(wicc - 1.9 * (wusd - repay) / pri)
        # all rate= 190%
        params = [address, txid, int(repay), int(redeem)]
        print("redeem param:", params)
        status_code, response = tran.submitredeemcdptx(params)
        print("redeem res:", response)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None
        sleep(6)

    @allure.feature('Test submitredeemcdptx rate')
    def test_submitredeemcdptx_rate_large2(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when before rate>190%:
        after rate =190%
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 20000000000
        # create rate > 190%
        stake_amount = 190000000
        price = tran.getprice("WICC")
        assert price is not None
        mint_amount = int((stake_amount * price) / 1.9)
        address = account.getnewaddresswithWICC(self.init, balance)
        logger.info(address)
        params = [address, str(stake_amount * 2), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(6)

        # all rate = 190%
        params = [address, txid, int(mint_amount / 2), int(stake_amount + stake_amount / 2)]
        status_code, response = tran.submitredeemcdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

    #
    # @allure.feature('Test submitredeemcdptx rate')
    # def test_submitredeemcdptx_leftover_equal(self):
    #     """
    #     @author:elaine.tan
    #     test submitredeemcdptx when before rate=190%:
    #     leftover < 0.9，leftover > 0.9,leftover = 0.9
    #     """
    #     account = Account(self.host, self.rpcuser, self.rpcpassword)
    #     tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
    #     balance = 20000000000
    #     price = tran.getprice("WICC")
    #     # get sender
    #     stake = 18000000000
    #     mint = int(stake*price/2)
    #     sender = account.getnewaddresswithbalance(self.init, balance)
    #     params = [sender, str(stake), str(mint)]
    #     txid = account.getNewCDP(params)
    #     assert txid is not None
    #     sleep(5)
    #
    #     # create rate = 190%
    #     stake_amount = 190000000
    #     assert price is not None
    #     mint_amount = int((stake_amount * price) / 1.9)
    #     address = account.getnewaddresswithbalance(self.init, balance)
    #     logger.info(address)
    #     params = [address, str(stake_amount), str(mint_amount)]
    #     txid = account.getNewCDP(params)
    #     assert txid is not None
    #     res = tran.send([sender,address,"WUSD:"+str(mint),10000])
    #     sleep(6)
    #
    #     # leftover < 0.9
    #     amount = 80000000/price
    #     params = [address, txid, int(mint_amount), stake_amount-amount]
    #     status_code, response = tran.submitredeemcdptx(params)
    #     assert status_code == 200
    #     for r in response:
    #         assert r['result'] is None
    #         assert r['error'] is not None
    #         assert "total-staked-bcoins-too-small" in r['error']['message']
    #
    #     # leftover > 0.9
    #     amount = 100000000 / price
    #     params = [address, txid, int(mint_amount), stake_amount-amount]
    #     status_code, response = tran.submitredeemcdptx(params)
    #     assert status_code == 200
    #     for r in response:
    #         assert r['result'] is not None
    #         assert r['error'] is None
    #         assert r['result']['txid'] is not None
    #
    #     # leftover = 0.9
    #     amount = (100000000-90000000) / price
    #     params = [address, txid, int(mint_amount), int(amount)]
    #     status_code, response = tran.submitredeemcdptx(params)
    #     assert status_code == 200
    #     for r in response:
    #         assert r['result'] is not None
    #         assert r['error'] is None
    #         assert r['result']['txid'] is not None
    #
    #     sleep(10)
    #     # leftover < 0.9
    #     amount = 90000000/price
    #     params = [address, txid, 0, int(amount/2)]
    #     status_code, response = tran.submitredeemcdptx(params)
    #     assert status_code == 200
    #     for r in response:
    #         assert r['result'] is None
    #         assert r['error'] is not None
    #         assert "total-staked-bcoins-too-small" in r['error']['message']
    #
    #     # leftover = 0
    #     params = [address, txid, 0, int(amount)]
    #     status_code, response = tran.submitredeemcdptx(params)
    #     assert status_code == 200
    #     for r in response:
    #         assert r['result'] is not None
    #         assert r['error'] is None
    #         assert r['result']['txid'] is not None

    @allure.feature('Test cdpInterest')
    def test_assert_stablefee(self):
        """
        @author:elaine.tan
        test cdpInterest when add stake
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(20)
        status_code, response = tran.getcdp([txid])
        height1 = ""
        for r in response:
            height1 = r['result']['cdp']['last_height']
            balance = r['result']['cdp']['total_scoin']
            amount = balance / 100000000
        wusdbefore = account.gettokenbyaddr(address, "WUSD")

        mint_amount = 20000
        staketxid = ""
        params = [address, "20000", str(mint_amount), txid]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            staketxid = r['result']['txid']
        sleep(40)

        interest = account.getCdpInterest(txid, height1, amount)
        wusdend = account.gettokenbyaddr(address, "WUSD")

        assert interest == wusdbefore - wusdend + mint_amount

        # 验证稳定费系统挂单
        status, res = tran.getdexorder([staketxid])
        for r in res:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['order_side'] == "BUY"
            assert r['result']['coin_symbol'] == "WUSD"
            assert r['result']['asset_symbol'] == "WGRT"
            assert r['result']['coin_amount'] == interest

    @allure.feature('Test cdpInterest')
    def test_assert_stablefee2(self):
        """
        @author:elaine.tan
        test cdpInterest when redeem
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(20)
        status_code, response = tran.getcdp([txid])
        height1 = ""
        for r in response:
            height1 = r['result']['cdp']['last_height']
            balance = r['result']['cdp']['total_scoin']
            amount = balance / 100000000
        wusdbefore = account.gettokenbyaddr(address, "WUSD")
        print("###########################")
        print(height1, "...", amount)
        print("1111111", balance, amount)

        repay_amount = 500
        params = [address, txid, repay_amount, 0]
        status_code, response = tran.submitredeemcdptx(params)
        print(response)
        assert status_code == 200
        sleep(40)

        interest = account.getCdpInterest(txid, height1, amount)
        wusdend = account.gettokenbyaddr(address, "WUSD")

        assert interest == wusdbefore - wusdend - repay_amount
        print("###########################")
        print(interest, wusdbefore - wusdend - repay_amount)
        print("###########################")

    @allure.feature('Test getcdp')
    def test_getcdp_normal(self):
        """
        @author:elaine.tan
        test getcdp when txid is normal,cdp txid is closed
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000000
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None

        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sendaddr = account.getNewAddrWithEnoughWUSD(self.init, 100000000)
        assert sendaddr is not None
        res = account.send([sendaddr, address, "WUSD:90000000", "WUSD:"+str(self.sendfee)])
        assert res
        sleep(6)
        # txid is normal
        status_code, response = tran.getcdp([txid])
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['cdp']['cdpid'] == txid

        params = [address, txid, mint_amount, stake_amount]
        # close CDP
        tran.submitredeemcdptx(params)
        sleep(6)
        # cdp is closed,txid is not exists
        status_code, response = tran.getcdp([txid])
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "does not exist" in r['error']['message']

    @allure.feature('Test getcdp')
    def test_getcdp_None(self):
        """
        @author:elaine.tan
        test getcdp when txid is null
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # txid = null
        params = [None]
        status_code, response = tran.getcdp(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type null, expected str" in r['error']['message']

    @allure.feature('Test getcdp')
    def test_getcdp_zero(self):
        """
        @author:elaine.tan
        test getcdp when txid is 0
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # txid = 0
        params = [0]
        status_code, response = tran.getcdp(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type int, expected str" in r['error']['message']

    @allure.feature('Test getcdp')
    def test_getcdp_number(self):
        """
        @author:elaine.tan
        test getcdp when txid is number
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # txid = 100
        params = [100]
        status_code, response = tran.getcdp(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type int, expected str" in r['error']['message']

    @allure.feature('Test getcdp')
    def test_getcdp_invalid(self):
        """
        @author:elaine.tan
        test getcdp when txid is invalid
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # txid = "invalid"
        params = ["invalid"]
        status_code, response = tran.getcdp(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "does not exist" in r['error']['message']

    @allure.feature('Test getcdp')
    def test_getcdp_notexist(self):
        """
        @author:elaine.tan
        test getcdp when not exist、closed cdp
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # txid not exist
        params = ["29dac25431620b60298843a1275cc09f96a79c2a3c9706b87bcd202b15aa2394"]
        status_code, response = tran.getcdp(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "does not exist" in r['error']['message']

    @allure.feature('Test getcdp')
    def test_getcdp_closed(self):
        """
        @author:elaine.tan
        test getcdp when cdp is closed
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 110000000

        # address is normal
        txid = ""
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            txid = r['result']['txid']
        sleep(15)
        status, response = tran.getusercdp([address])
        assert status == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None

        sendaddr = account.getNewAddrWithEnoughWUSD(self.init, 100000000)
        assert sendaddr is not None
        res = account.send([sendaddr, address, "WUSD:90000000", "WUSD:"+str(self.sendfee)])
        assert res
        sleep(6)

        # close CDP
        params = [address, txid, mint_amount, stake_amount]
        a = tran.submitredeemcdptx(params)
        sleep(6)
        # cdp is closed,txid is not exists
        status_code, response = tran.getcdp([txid])
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "does not exist" in r['error']['message']

    @allure.feature('Test getusercdp')
    def test_getusercdp_None(self):
        """
        @author:elaine.tan
        test getusercdp when address is null
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 110000000

        # address is None
        params = [None]
        status, response = tran.getusercdp(params)
        assert status == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type null, expected str" in r['error']['message']

    @allure.feature('Test getusercdp')
    def test_getusercdp_zero(self):
        """
        @author:elaine.tan
        test getusercdp when address is 0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 110000000

        # address is 0
        params = [0]
        status, response = tran.getusercdp(params)
        assert status == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type int, expected str" in r['error']['message']

    @allure.feature('Test getusercdp')
    def test_getusercdp_invalid(self):
        """
        @author:elaine.tan
        test getusercdp when address is invalid str
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 110000000

        # address is ***
        params = ["***"]
        status, response = tran.getusercdp(params)
        assert status == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Invalid addr" in r['error']['message']

        # address is invalid
        params = ["invalid"]
        status, response = tran.getusercdp(params)
        assert status == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Invalid addr" in r['error']['message']

    @allure.feature('Test getusercdp')
    def test_getusercdp_nocdp(self):
        """
        @author:elaine.tan
        test getusercdp when address has no cdp
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 110000000

        # address has no cdp
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        status, response = tran.getusercdp([address])
        assert status == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result'] == {}

    @allure.feature('Test getusercdp')
    def test_getusercdp_number(self):
        """
        @author:elaine.tan
        test getusercdp when address is number
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 110000000

        # address is 100
        params = [100]
        status, response = tran.getusercdp(params)
        assert status == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type int, expected str" in r['error']['message']

    @allure.feature('Test getusercdp')
    def test_getusercdp_unregistered(self):
        """
        @author:elaine.tan
        test getusercdp when address is unregistered
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 110000000

        # address is not registered
        address = account.newaddress()
        assert address is not None
        status, response = tran.getusercdp([address])
        assert status == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print(r['error']['message'])
            assert "" in r['error']['message']

    @allure.feature('Test getusercdp')
    def test_getusercdp_normal(self):
        """
        @author:elaine.tan
        test getusercdp when normal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 110000000

        # address is normal
        txid = ""
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            txid = r['result']['txid']
        sleep(15)
        status, response = tran.getusercdp([address])
        assert status == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None

    @allure.feature('Test getusercdp')
    def test_getusercdp_closed(self):
        """
        @author:elaine.tan
        test getusercdp when cdp is closed
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 110000000

        # address is normal
        txid = ""
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            txid = r['result']['txid']
        sleep(15)
        status, response = tran.getusercdp([address])
        assert status == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None

        sendaddr = account.getNewAddrWithEnoughWUSD(self.init, 100000000)
        assert sendaddr is not None
        tran.send([sendaddr, address, "WUSD:90000000", "WUSD:"+str(self.sendfee)])
        sleep(6)

        # close CDP
        params = [address, txid, mint_amount, stake_amount]
        a = tran.submitredeemcdptx(params)
        sleep(6)
        # cdp is closed,txid is not exists
        status_code, response = tran.getusercdp([address])
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result'] == {}

    @allure.feature('Test submitliquidatecdptx')
    def test_submitliquidatecdptx_normal(self):
        """
        @author:elaine.tan
        test submitliquidatecdptx when normal:
        all liquidate by onself when liquidate rate = 150%, CDP owner returns
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        liquiAddr = account.getNewAddrWithEnoughWUSD(self.init, balance)
        assert liquiAddr is not None
        sleep(6)
        owner_wicc_before = account.gettokenbyaddr(address, "WICC")
        liqui_wicc_before = account.gettokenbyaddr(liquiAddr, "WICC")

        # feeding price ,set rate=150%
        result = account.feedingPrice(self.init, "USD", price * 0.75, price * 0.75)
        assert result is True
        sleep(15)
        price2 = account.getprice("WICC")
        assert price2 != None
        # 获取清算前的风险备用金
        risk_free_before = account.gettokenbyaddr(self.risk, "WUSD")

        # liquidate
        fee = self.cdpfee
        liqui_amount = int(1.0961 * mint_amount)
        liqui_txid = ''
        params = [liquiAddr, txid, liqui_amount, fee]
        print("liqui:", params)
        status_code, response = tran.submitliquidatecdptx(params)
        print("liqui res:", response)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None
            liqui_txid = r['result']['txid']
        sleep(6)

        # after liquidate
        # assert cdp closed
        status_code, response = tran.getcdp([txid])
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "not exist" in r['error']['message']

        # assert liquidate addr freeamount
        liqui_wusd_after = account.gettokenbyaddr(liquiAddr, "WUSD")
        liqui_wicc_after = account.gettokenbyaddr(liquiAddr, "WICC")
        # 清算人获得资产
        liqui_get_amount = int((1.13 * mint_amount) / price2)
        # liqui_get_amount = liqui_amount/0.97/price2
        print(liqui_get_amount)
        assert liqui_wusd_after == balance - liqui_amount
        assert liqui_wicc_after == int(liqui_wicc_before + liqui_get_amount - fee)

        # assert owner addr
        owner_wicc_after = account.gettokenbyaddr(address, "WICC")
        # CDP主人获得stake_amount - 清算人获得资产
        assert owner_wicc_after == owner_wicc_before + (stake_amount - liqui_get_amount)

        # 风险备用金
        temp = int(9610000 / 2)
        risk_free_after = account.gettokenbyaddr(self.risk, "WUSD")
        assert risk_free_after == risk_free_before + temp
        # 系统挂单
        status, res = tran.getdexorder([liqui_txid])
        for r in res:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['order_side'] == 'BUY'
            assert r['result']['coin_amount'] == temp

        sleep(30)
        result = account.feedingPrice(self.init, "USD", int(price), int(price))
        assert result
        sleep(10)

    @allure.feature('Test submitliquidatecdptx')
    def test_submitliquidatecdptx_address_None(self):
        """
        @author:elaine.tan
        test submitliquidatecdptx when address is None
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None

        addr = None
        params = [addr, txid, stake_amount]
        status_code, response = tran.submitliquidatecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "value is type null, expected str" in r['error']['message']

    @allure.feature('Test submitliquidatecdptx')
    def test_submitliquidatecdptx_address_unregistered(self):
        """
        @author:elaine.tan
        test submitliquidatecdptx when address is unregistered
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None

        addr = account.newaddress()
        params = [addr, txid, stake_amount]
        status_code, response = tran.submitliquidatecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "account not exists" in r['error']['message']

    @allure.feature('Test submitliquidatecdptx')
    def test_submitliquidatecdptx_address_invalid(self):
        """
        @author:elaine.tan
        test submitliquidatecdptx when address is invalid
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None

        addr = "invalid"
        params = [addr, txid, stake_amount]
        status_code, response = tran.submitliquidatecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "Invalid addr" in r['error']['message']

    @allure.feature('Test submitliquidatecdptx')
    def test_submitliquidatecdptx_address_zero(self):
        """
        @author:elaine.tan
        test submitliquidatecdptx when address is 0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None

        addr = 0
        params = [addr, txid, stake_amount]
        status_code, response = tran.submitliquidatecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "value is type int, expected str" in r['error']['message']

    @allure.feature('Test submitliquidatecdptx')
    def test_submitliquidatecdptx_address_negative(self):
        """
        @author:elaine.tan
        test submitliquidatecdptx when address is negative
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None

        addr = -100
        params = [addr, txid, stake_amount]
        status_code, response = tran.submitliquidatecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "value is type int, expected str" in r['error']['message']

    @allure.feature('Test submitliquidatecdptx')
    def test_submitliquidatecdptx_address_int(self):
        """
        @author:elaine.tan
        test submitliquidatecdptx when address is int
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None

        addr = 100
        params = [addr, txid, stake_amount]
        status_code, response = tran.submitliquidatecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "value is type int, expected str" in r['error']['message']

    @allure.feature('Test submitliquidatecdptx')
    def test_submitliquidatecdptx_address_balance(self):
        """
        @author:elaine.tan
        test submitliquidatecdptx when address has no wusd
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None

        addr = account.getnewaddresswithWICC(self.init, 20000)
        params = [addr, txid, stake_amount]
        status_code, response = tran.submitliquidatecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "account-scoins-insufficient" in r['error']['message']

    @allure.feature('Test submitliquidatecdptx')
    def test_submitliquidatecdptx_address_balance1(self):
        """
        @author:elaine.tan
        test submitliquidatecdptx when address has no wicc
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None

        fee = self.cdpfee
        addr = account.getnewaddresswithWICC(self.init, stake_amount + fee)
        assert addr is not None
        txid2 = account.getNewCDP([addr, str(stake_amount), str(mint_amount), "", str(fee)])
        assert txid2 is not None
        sleep(6)
        params = [addr, txid, mint_amount]
        status_code, response = tran.submitliquidatecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "deduct-account-fee-failed" in r['error']['message']

    @allure.feature('Test submitliquidatecdptx')
    def test_submitliquidatecdptx_txid_None(self):
        """
        @author:elaine.tan
        test submitliquidatecdptx when cdpid is None
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        addr = account.getnewaddresswithWICC(self.init, 10)
        txid = None
        params = [addr, txid, 10000]
        status_code, response = tran.submitliquidatecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "value is type null, expected str" in r['error']['message']

    @allure.feature('Test submitliquidatecdptx')
    def test_submitliquidatecdptx_txid_zero(self):
        """
        @author:elaine.tan
        test submitliquidatecdptx when cdpid is 0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        addr = account.getnewaddresswithWICC(self.init, 10)
        txid = 0
        params = [addr, txid, 10000]
        status_code, response = tran.submitliquidatecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "value is type int, expected str" in r['error']['message']

    @allure.feature('Test submitliquidatecdptx')
    def test_submitliquidatecdptx_txid_notexist(self):
        """
        @author:elaine.tan
        test submitliquidatecdptx when cdpid is notexist
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        addr = account.getnewaddresswithWICC(self.init, 10)
        txid = "f8c0a5a15e52d5192fc1a2da981b25b23d65d622e7912f26b30329ae26a3659d"
        params = [addr, txid, 10000]
        status_code, response = tran.submitliquidatecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "cdp-not-exist" in r['error']['message']

    @allure.feature('Test submitliquidatecdptx')
    def test_submitliquidatecdptx_txid_invalid(self):
        """
        @author:elaine.tan
        test submitliquidatecdptx when cdpid is invalid
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        addr = account.getnewaddresswithWICC(self.init, 10)
        txid = "invalid"
        params = [addr, txid, 10000]
        status_code, response = tran.submitliquidatecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "Get param cdp_id error" in r['error']['message']

    @allure.feature('Test submitliquidatecdptx')
    def test_submitliquidatecdptx_txid_closed(self):
        """
        @author:elaine.tan
        test submitliquidatecdptx when cpd is closed
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(6)

        addr = account.getNewAddrWithEnoughWUSD(self.init, mint_amount)
        assert addr is not None
        res = account.send([addr, address, "WUSD:" + str(mint_amount), self.sendfee])
        assert res
        sleep(6)
        # 赎回关闭cdp
        tran.submitredeemcdptx([address, txid, mint_amount, stake_amount])
        sleep(6)
        params = [addr, txid, stake_amount]
        status_code, response = tran.submitliquidatecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "cdp-not-exist" in r['error']['message']

    @allure.feature('Test submitliquidatecdptx')
    def test_submitliquidatecdptx_txid_notready(self):
        """
        @author:elaine.tan
        test submitliquidatecdptx when cpd is not ready to liquidated
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 300000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(6)

        params = [address, txid, stake_amount]
        status_code, response = tran.submitliquidatecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "cdp-not-liquidate-ready" in r['error']['message']

    @allure.feature('Test submitliquidatecdptx')
    def test_submitliquidatecdptx_amount_None(self):
        """
        @author:elaine.tan
        test submitliquidatecdptx when liquidate amount is None
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(6)

        amount = None
        params = [address, txid, amount]
        status_code, response = tran.submitliquidatecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            print(r['error']['message'])
            assert "value is type null, expected real" in r['error']['message']

    @allure.feature('Test submitliquidatecdptx')
    def test_submitliquidatecdptx_amount_negative(self):
        """
        @author:elaine.tan
        test submitliquidatecdptx when liquidate amount < 0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(6)

        amount = -100
        params = [address, txid, amount]
        status_code, response = tran.submitliquidatecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "Invalid amount" in r['error']['message']

    @allure.feature('Test submitliquidatecdptx')
    def test_submitliquidatecdptx_amount_str(self):
        """
        @author:elaine.tan
        test submitliquidatecdptx when liquidate amount is string
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(6)

        amount = -100
        params = [address, txid, amount]
        status_code, response = tran.submitliquidatecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "Invalid amount" in r['error']['message']

    @allure.feature('Test submitliquidatecdptx')
    def test_submitliquidatecdptx_amount_zero(self):
        """
        @author:elaine.tan
        test submitliquidatecdptx when liquidate amount = 0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(6)

        # feeding price ,set rate=150%
        result = account.feedingPrice(self.init, "USD", price * 0.75, price * 0.75)
        assert result
        sleep(20)

        amount = 0
        params = [address, txid, amount]
        status_code, response = tran.submitliquidatecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "invalid-liquidate-amount" in r['error']['message']

        sleep(20)
        result = account.feedingPrice(self.init, "USD", int(price), int(price))
        assert result
        sleep(10)

    @allure.feature('Test submitliquidatecdptx')
    def test_submitliquidatecdptx_amount_balance(self):
        """
        @author:elaine.tan
        test submitliquidatecdptx when liquidate amount > balance
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 300000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(6)

        amount = mint_amount * 2
        params = [address, txid, amount]
        status_code, response = tran.submitliquidatecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "account-scoins-insufficient" in r['error']['message']

    @allure.feature('Test submitliquidatecdptx')
    def test_submitliquidatecdptx_normal2(self):
        """
        @author:elaine.tan
        test submitliquidatecdptx when normal:
        all liquidate by onself when liquidate 104% <rate< 113%,CDP owner no returns
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        liquiAddr = account.getNewAddrWithEnoughWUSD(self.init, balance)
        assert liquiAddr is not None
        sleep(6)
        owner_wicc_before = account.gettokenbyaddr(address, "WICC")
        liqui_wicc_before = account.gettokenbyaddr(liquiAddr, "WICC")

        # feeding price ,set rate=110%
        result = account.feedingPrice(self.init, "USD", price * 0.55, price * 0.55)
        assert result is True
        sleep(15)
        price2 = account.getprice("WICC")
        assert price2 != None

        # liquidate
        # liqui_amount = int(0.97*1.13*mint_amount)
        fee = self.cdpfee
        liqui_amount = int(0.97 * stake_amount * price2)
        params = [liquiAddr, txid, liqui_amount, fee]
        status_code, response = tran.submitliquidatecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None
        sleep(6)

        # after liquidate
        # assert cdp closed
        status_code, response = tran.getcdp([txid])
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "not exist" in r['error']['message']

        # assert liquidate addr freeamount
        liqui_wusd_after = account.gettokenbyaddr(liquiAddr, "WUSD")
        liqui_wicc_after = account.gettokenbyaddr(liquiAddr, "WICC")
        assert liqui_wusd_after == balance - liqui_amount
        assert liqui_wicc_after == int(liqui_wicc_before + stake_amount - fee)
        print(1, liqui_wusd_after, balance - liqui_amount)
        print(2, liqui_wicc_after, int(liqui_wicc_before + stake_amount - fee))

        # assert owner addr
        # 无反还
        owner_wicc_after = account.gettokenbyaddr(address, "WICC")
        assert owner_wicc_after == owner_wicc_before
        print(3, owner_wicc_after, owner_wicc_before)

        sleep(30)
        result = account.feedingPrice(self.init, "USD", int(price), int(price))
        assert result
        sleep(10)

    @allure.feature('Test submitliquidatecdptx')
    def test_submitliquidatecdptx_leftover_small(self):
        """
        @author:elaine.tan
        test submitliquidatecdptx when normal:
        part liquidate and leftover < 0.9
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 300000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        liquiAddr = account.getNewAddrWithEnoughWUSD(self.init, balance)
        assert liquiAddr is not None
        sleep(6)

        # feeding price ,set rate=110%
        result = account.feedingPrice(self.init, "USD", price * 0.75, price * 0.75)
        assert result is True
        sleep(15)
        price2 = account.getprice("WICC")
        assert price2 != None

        # liquidate
        fee = "WICC:"+str(self.cdpfee)+":sawi"
        # liqui_amount = int(0.97*stake_amount*price2)
        liqui_amount = mint_amount
        params = [liquiAddr, txid, liqui_amount, fee]
        status_code, response = tran.submitliquidatecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "total-staked-bcoins-too-small" in r['error']['message']
        sleep(6)

        sleep(30)
        result = account.feedingPrice(self.init, "USD", int(price), int(price))
        assert result
        sleep(10)

    @allure.feature('Test submitliquidatecdptx')
    def test_submitliquidatecdptx_leftover_equal(self):
        """
        @author:elaine.tan
        test submitliquidatecdptx when normal:
        part liquidate and leftover = 0.9
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 300000000
        stake_amount = 100000000
        sleep(10)
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        liquiAddr = account.getNewAddrWithEnoughWUSD(self.init, balance)
        assert liquiAddr is not None
        sleep(6)
        print(666, tran.getcdp([txid]))

        # feeding price ,set rate=110%
        result = account.feedingPrice(self.init, "USD", price * 0.75, price * 0.75)
        assert result is True
        sleep(15)
        price2 = account.getprice("WICC")
        assert price2 != None

        # liquidate
        fee = "WUSD:"+str(self.cdpfee)
        liqui_amount = int(1.0961 * mint_amount * 0.4)
        params = [liquiAddr, txid, liqui_amount, fee]
        print(111, params)
        status_code, response = tran.submitliquidatecdptx(params)
        print(111, response)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None
        sleep(6)

        print(999, tran.getcdp([txid]))
        sleep(30)
        result = account.feedingPrice(self.init, "USD", int(price), int(price))
        assert result
        sleep(10)

    @allure.feature('Test submitliquidatecdptx')
    def test_submitliquidatecdptx_leftover_large(self):
        """
        @author:elaine.tan
        test submitliquidatecdptx when normal:
        part liquidate and leftover > 0.9
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 300000000
        stake_amount = 100000000
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        sleep(10)
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sender = account.getNewAddrWithEnoughWUSD(self.init, balance)
        assert sender is not None
        res = account.send([sender, address, "WUSD:" + str(balance), self.sendfee])
        assert res
        sleep(6)
        print(666, tran.getcdp([txid]))

        # feeding price ,set rate=150%
        result = account.feedingPrice(self.init, "USD", price * 0.75, price * 0.75)
        assert result is True
        sleep(15)
        # price2 = account.getprice("WICC")
        # assert price2 != None

        print(666, tran.getcdp([txid]))
        # liquidate
        fee = "WUSD:"+str(self.cdpfee)+":sawi"
        liqui_amount = int(1.0961 * mint_amount * 0.2)
        params = [address, txid, liqui_amount, fee]
        status_code, response = tran.submitliquidatecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None
        sleep(6)
        print(999, tran.getcdp([txid]))

        sleep(30)
        result = account.feedingPrice(self.init, "USD", int(price), int(price))
        assert result
        sleep(10)

    @allure.feature('Test submitliquidatecdptx')
    def test_submitliquidatecdptx_stakeredeem(self):
        """
        @author:elaine.tan
        test submitliquidatecdptx when :
        stake and redeem cdp when cdp is ready to liquidate
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 400000000
        stake_amount = 100000000
        address = account.getnewaddresswithWICC(self.init, balance)
        assert address is not None
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sender = account.getNewAddrWithEnoughWUSD(self.init, balance)
        assert sender is not None
        res = account.send([sender, address, "WUSD:" + str(balance), self.sendfee])
        assert res
        sleep(6)

        # feeding price ,set rate=150%
        res = account.feedingPrice(self.init, "USD", price * 0.75, price * 0.75)
        assert res is True
        sleep(15)
        price2 = account.getprice("WICC")
        assert price2 != None

        mint_amount2 = int((stake_amount * price2) / 2)
        status, response = tran.submitstakecdptx([address, str(stake_amount), str(mint_amount2), txid])
        print("stakeres:", response)
        assert status == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None
        sleep(6)

        status, response = tran.submitredeemcdptx([address, txid, mint_amount, stake_amount])
        print("redeemres:", response)
        assert status == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

        sleep(30)
        result = account.feedingPrice(self.init, "USD", int(price), int(price))
        assert result
        sleep(10)

    @allure.feature('Test submitliquidatecdptx')
    def test_submitliquidatecdptx_part_liquidate(self):
        """
        @author:elaine.tan
        test submitliquidatecdptx when : cdp is part liquidated
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        create_balance = 500000000
        sender_balance = 400000000
        stake_amount = 200000000
        fee = self.cdpfee
        # create addr
        owneraddr = account.getnewaddresswithWICC(self.init, create_balance)
        assert owneraddr is not None
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        params = [owneraddr, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        # liquidate addr
        liquiaddr = account.getNewAddrWithEnoughWUSD(self.init, sender_balance)
        assert owneraddr is not None

        H0 = tran.getcdpinfo(txid, "last_height")
        assert H0 is not None

        # feeding price ,set rate=150%
        res = account.feedingPrice(self.init, "USD", price * 0.75, price * 0.75)
        assert res is True
        sleep(15)
        price2 = account.getprice("WICC")
        assert price2 != None

        owner_start_wicc = account.gettokenbyaddr(owneraddr,"WICC")
        liqui_start_wicc = account.gettokenbyaddr(liquiaddr,"WICC")
        liqui_start_wusd = account.gettokenbyaddr(liquiaddr,"WUSD")
        risk_start_wusd = account.gettokenbyaddr(self.risk,"WUSD")
        sleep(6)
        liqui_amount = 10000
        params = [liquiaddr, txid, liqui_amount, fee]
        status_code, response = tran.submitliquidatecdptx(params)
        print("liquidate res:",response)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None
        sleep(20)
        H1 = tran.getcdpinfo(txid, "last_height")
        assert H1 is not None
        # 验证清算不会改变高度
        assert H0 == H1
        # 验证清算剩余部分
        # 预期结果
        res = account.getliquidateres(liqui_amount,stake_amount*price2,mint_amount)
        expect_liqui_wicc = int(res['re_liquidater_value']/price2)
        expect_risk = int(res['fine']/2)
        expect_owner_wicc = int(res['re_owner_value']/price2)
        expect_left_stake = int(res["left_stake_value"])
        expect_left_mint = int(res["left_mint_value"])
        print("liqui res:",res)

        # 实际结果
        leftstake = tran.getcdpinfo(txid, "total_bcoin")
        leftmint = tran.getcdpinfo(txid, "total_scoin")
        owner_end_wicc = account.gettokenbyaddr(owneraddr, "WICC")
        liqui_end_wicc = account.gettokenbyaddr(liquiaddr, "WICC")
        liqui_end_wusd = account.gettokenbyaddr(liquiaddr, "WUSD")
        risk_end_wusd = account.gettokenbyaddr(self.risk, "WUSD")

        risk = risk_end_wusd - risk_start_wusd
        retr_owner_wicc = owner_end_wicc - owner_start_wicc
        retr_liqui_wicc = abs(liqui_end_wicc - liqui_start_wicc) - fee
        wusd_redu = liqui_end_wusd - liqui_start_wusd

        # 验证是否一致
        wusd_redu == liqui_amount
        retr_liqui_wicc == expect_liqui_wicc
        retr_owner_wicc == expect_owner_wicc
        risk == expect_risk
        leftstake == expect_left_stake
        leftmint == expect_left_mint

    @allure.feature('Test submitliquidatecdptx')
    def test_submitliquidatecdptx_part_stakeredeem(self):
        """
        @author:elaine.tan
        test submitliquidatecdptx when :
        stake and redeem cdp after cdp is part liquidated
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        create_balance = 500000000
        sender_balance = 400000000
        stake_amount = 200000000
        fee = self.cdpfee
        # create addr
        createaddr = account.getnewaddresswithWICC(self.init, create_balance)
        assert createaddr is not None
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)
        params = [createaddr, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        assert txid is not None
        # liquidate addr
        owneraddr = account.getNewAddrWithEnoughWUSD(self.init, sender_balance)
        assert owneraddr is not None
        res = account.send([owneraddr, createaddr, "WUSD:" + str(sender_balance), self.sendfee])
        assert res
        sleep(6)

        H0 = tran.getcdpinfo(txid, "last_height")
        assert H0 is not None

        # feeding price ,set rate=150%
        res = account.feedingPrice(self.init, "USD", price * 0.75, price * 0.75)
        assert res is True
        sleep(15)
        price2 = account.getprice("WICC")
        assert price2 != None

        liqui_amount = 10000
        params = [createaddr, txid, liqui_amount, fee]
        status_code, response = tran.submitliquidatecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None
        sleep(20)
        H1 = tran.getcdpinfo(txid, "last_height")
        assert H1 is not None
        # 验证清算不会改变高度
        assert H0 == H1

        stake_wusd_be = account.gettokenbyaddr(createaddr, "WUSD")
        mint_amount2 = int((stake_amount * price2) / 2)
        params = [createaddr, str(stake_amount), str(mint_amount2), txid]
        status, response = tran.submitstakecdptx(params)
        print("stakeres:", response)
        assert status == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None
        sleep(20)
        left_mint_amount = tran.getcdpinfo(txid, "total_scoin")

        stake_wusd_af = account.gettokenbyaddr(createaddr, "WUSD")
        stake_wusd_af - stake_wusd_be - mint_amount2 == account.getCdpInterest(txid, H0, left_mint_amount)

        params = [createaddr, txid, mint_amount, stake_amount / 2]
        status, response = tran.submitredeemcdptx(params)
        print("redeemres:", response)
        assert status == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

        sleep(30)
        result = account.feedingPrice(self.init, "USD", int(price), int(price))
        assert result
        sleep(10)


if __name__ == '__main__':
    pytest.main(['-s', 'cdp_transaction_test.py::TestCDPTransaction::test_submitliquidatecdptx_part_liquidate'])