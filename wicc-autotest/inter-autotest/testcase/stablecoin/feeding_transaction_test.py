from time import sleep

import pytest
import allure

from utils.read_property import ReadProperty
from utils.log import mylog
from wiccsdk.account import Account
from wiccsdk.block import Block
from wiccsdk.transaction import Transaction
from wiccsdk.wallet import Wallet

logger = mylog(__name__).getlog()


class TestFeedingTransaction(object):
    @pytest.fixture(scope='class', autouse=True)
    def setup(self):
        prop = ReadProperty()
        # env = ReadEnv()
        # host = 'http://' + env.read('ip') + ":" + str(env.read('port'))
        # rpcuser = env.read('username')
        # rpcpassword = env.read('password')
        # walletpassword = prop.read('common', 'walletpassword')

        host = prop.read('common', 'host')
        rpcuser = prop.read('common', 'rpcuser')
        rpcpassword = prop.read('common', 'rpcpassword')
        walletpassword = prop.read('common', 'walletpassword')

        wallet = Wallet(host, rpcuser, rpcpassword)
        isLocked = wallet.islocked()
        # 解锁钱包
        if isLocked:
            status, response = wallet.walletpassphrase([walletpassword, 360000])
            assert status == 200
        else:
            pass

        assert not wallet.islocked()
        logger.info('class setup')

    @pytest.fixture(scope='class', autouse=True)
    def teardown(self, request):
        def fin():
            logger.info('class teardown')

        request.addfinalizer(fin)

    @pytest.fixture(scope='function', autouse=True)
    def setup_function(self):
        prop = ReadProperty()
        # env = ReadEnv()
        # self.host = 'http://' + env.read('ip') + ":" + str(env.read('port'))
        # self.rpcuser = env.read('username')
        # self.rpcpassword = env.read('password')
        self.host = prop.read('common', 'host')
        self.rpcuser = prop.read('common', 'rpcuser')
        self.rpcpassword = prop.read('common', 'rpcpassword')
        self.creaction = prop.read('common', 'creation_addr')
        self.contracturl = prop.read('common', 'contracturl')
        self.init = prop.read('common', 'init_addr')
        self.sendfee = int(prop.read('fee', 'send'))
        self.feedfee = int(prop.read('fee', 'feed'))
        self.stakefee = int(prop.read('fee', 'coinstake'))

        logger.info('function setup')

    @pytest.fixture(scope='function', autouse=True)
    def teardown_function(self):
        logger.info('function teardown')

    @pytest.fixture(scope='function', autouse=False)
    def test_param(self, request):
        return request.param

    # Passed
    @allure.feature('Test submitpricefeedtx')
    def test_submitpricefeedtx_normal(self):
        """
        @author:elaine.tan
        test submitpricefeedtx when normal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        WICCPrice = 100000000
        WGRTPrice = 100000000
        WICC = {"coin": "WICC", "currency": "USD", "price": WICCPrice}
        WGRT = {"coin": "WGRT", "currency": "USD", "price": WGRTPrice}

        address = account.getFeedingAddress(self.init)
        assert address is not None
        logger.info(address)
        params = [address, [WICC, WGRT]]
        staus_code, response = transaction.submitpricefeedtx(params)
        assert staus_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitpricefeedtx')
    def test_submitpricefeedtx_address_None(self):
        """
        @author:elaine.tan
        test submitpricefeedtx when address is null
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        WICCPrice = 100000000
        WGRTPrice = 100000000
        WICC = {"coin": "WICC", "currency": "USD", "price": WICCPrice}
        WGRT = {"coin": "WGRT", "currency": "USD", "price": WGRTPrice}

        # address is Null
        address = None
        params = [address, [WICC, WGRT]]
        staus_code, response = transaction.submitpricefeedtx(params)
        assert staus_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type null, expected str" in r['error']['message']

    @allure.feature('Test submitpricefeedtx')
    def test_submitpricefeedtx_address_invalid(self):
        """
        @author:elaine.tan
        test submitpricefeedtx when address is invalid
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        WICCPrice = 100000000
        WGRTPrice = 100000000
        WICC = {"coin": "WICC", "currency": "USD", "price": WICCPrice}
        WGRT = {"coin": "WGRT", "currency": "USD", "price": WGRTPrice}

        # address is invalid
        address = "invalid"
        params = [address, [WICC, WGRT]]
        staus_code, response = transaction.submitpricefeedtx(params)
        assert staus_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Invalid addr" in r['error']['message']

    @allure.feature('Test submitpricefeedtx')
    def test_submitpricefeedtx_address_unqualified(self):
        """
        @author:elaine.tan
        test submitpricefeedtx when address is normal without WICC
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        WICCPrice = 100000000
        WGRTPrice = 100000000
        WICC = {"coin": "WICC", "currency": "USD", "price": WICCPrice}
        WGRT = {"coin": "WGRT", "currency": "USD", "price": WGRTPrice}

        # address is normal,no WICC
        address = account.getnewaddrwithmatureregid(self.init, 100)
        assert address is not None
        params = [address, [WICC, WGRT]]
        staus_code, response = transaction.submitpricefeedtx(params)
        assert staus_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Account does not have enough WICC" in r['error']['message']

    @allure.feature('Test submitpricefeedtx')
    def test_submitpricefeedtx_address_unqualified1(self):
        """
        @author:elaine.tan
        test submitpricefeedtx when address is miner,no WICC
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        WICCPrice = 100000000
        WGRTPrice = 100000000
        WICC = {"coin": "WICC", "currency": "USD", "price": WICCPrice}
        WGRT = {"coin": "WGRT", "currency": "USD", "price": WGRTPrice}

        # address is miner,no WICC
        address = "0-5"
        transaction.send([self.init, address, 200000, self.sendfee])
        sleep(5)
        params = [address, [WICC, WGRT]]
        staus_code, response = transaction.submitpricefeedtx(params)
        assert staus_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print(r['error']['message'])

    @allure.feature('Test submitpricefeedtx')
    def test_submitpricefeedtx_address_unqualified2(self):
        """
        @author:elaine.tan
        test submitpricefeedtx when address is normal,has WICC
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        WICCPrice = 100000000
        WICC = {"coin": "WICC", "currency": "USD", "price": WICCPrice}

        balance = 21000000000000
        fee = self.stakefee
        address = account.getnewaddrwithmatureregid(self.init, balance+fee*2)
        assert address is not None
        status_code, response = transaction.submitcoinstaketx([address,"WICC",balance,fee])
        assert status_code == 200
        print("stake res:",response)
        sleep(10)
        params = [address, [WICC]]
        staus_code, response = transaction.submitpricefeedtx(params)
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "account-isn't-delegate" in r['error']['message']

    @allure.feature('Test submitpricefeedtx')
    def test_submitpricefeedtx_coin_None(self):
        """
        @author:elaine.tan
        test submitpricefeedtx when coin is null
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        address = account.getFeedingAddress(self.init)
        assert address is not None

        # coin is null
        WICC = {"coin": None, "currency": "USD", "price": 100000000}
        params = [address, [WICC]]
        staus_code, response = transaction.submitpricefeedtx(params)
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "null type not allowed!" in r['error']['message']

    @allure.feature('Test submitpricefeedtx')
    def test_submitpricefeedtx_coin_CNY(self):
        """
        @author:elaine.tan
        test submitpricefeedtx when coin is CNY
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        address = account.getFeedingAddress(self.init)
        assert address is not None

        # coin is CNY
        WICC = {"coin": "CNY", "currency": "USD", "price": 100000000}
        params = [address, [WICC]]
        staus_code, response = transaction.submitpricefeedtx(params)
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Invalid coin symbol" in r['error']['message']

    @allure.feature('Test submitpricefeedtx')
    def test_submitpricefeedtx_coin_invalid(self):
        """
        @author:elaine.tan
        test submitpricefeedtx when coin is invalid
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        address = account.getFeedingAddress(self.init)
        assert address is not None

        # coin is invalid
        WICC = {"coin": "invalid", "currency": "USD", "price": 100000000}
        params = [address, [WICC]]
        staus_code, response = transaction.submitpricefeedtx(params)
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Invalid coin symbol" in r['error']['message']

    @allure.feature('Test submitpricefeedtx')
    def test_submitpricefeedtx_coin_number(self):
        """
        @author:elaine.tan
        test submitpricefeedtx when coin is number
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        address = account.getFeedingAddress(self.init)
        assert address is not None

        # coin is number
        WICC = {"coin": 123, "currency": "USD", "price": 100000000}
        params = [address, [WICC]]
        staus_code, response = transaction.submitpricefeedtx(params)
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type int, expected str" in r['error']['message']

    @allure.feature('Test submitpricefeedtx')
    def test_submitpricefeedtx_currency_None(self):
        """
        @author:elaine.tan
        test submitpricefeedtx when currency is null
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        address = account.getFeedingAddress(self.init)
        assert address is not None

        # currency is null
        currency = None
        WICC = {"coin": "WICC", "currency": currency, "price": 100000000}
        params = [address, [WICC]]
        staus_code, response = transaction.submitpricefeedtx(params)
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "null type not allowed!" in r['error']['message']

    @allure.feature('Test submitpricefeedtx')
    def test_submitpricefeedtx_currency_invalid(self):
        """
        @author:elaine.tan
        test submitpricefeedtx when currency is invalid
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        address = account.getFeedingAddress(self.init)
        assert address is not None

        # currency is invalid
        currency = "invalid"
        WICC = {"coin": "WICC", "currency": currency, "price": 100000000}
        params = [address, [WICC]]
        staus_code, response = transaction.submitpricefeedtx(params)
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Invalid currency type" in r['error']['message']

    @allure.feature('Test submitpricefeedtx')
    def test_submitpricefeedtx_currency_number(self):
        """
        @author:elaine.tan
        test submitpricefeedtx when currency is number
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        address = account.getFeedingAddress(self.init)
        assert address is not None

        # currency is Number
        currency = 123
        WICC = {"coin": "WICC", "currency": currency, "price": 100000000}
        params = [address, [WICC]]
        staus_code, response = transaction.submitpricefeedtx(params)
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type int, expected str" in r['error']['message']

    @allure.feature('Test submitpricefeedtx')
    def test_submitpricefeedtx_currency_CNY(self):
        """
        @author:elaine.tan
        test submitpricefeedtx when currency is invalid
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        address = account.getFeedingAddress(self.init)
        assert address is not None

        # currency is CNY
        currency = "CNY"
        WICC = {"coin": "WICC", "currency": currency, "price": 200000000}
        params = [address, [WICC]]
        staus_code, response = transaction.submitpricefeedtx(params)
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "bad-tx-currency-type" in r['error']['message']

    @allure.feature('Test submitpricefeedtx')
    def test_submitpricefeedtx_price_None(self):
        """
        @author:elaine.tan
        test submitpricefeedtx when currency is null
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        address = account.getFeedingAddress(self.init)
        assert address is not None

        # price is null
        coin = "WGRT"
        currency = "USD"
        price = None
        WICC = {"coin": coin, "currency": currency, "price": price}
        params = [address, [WICC]]
        staus_code, response = transaction.submitpricefeedtx(params)
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "null type not allowed!" in r['error']['message']

    @allure.feature('Test submitpricefeedtx')
    def test_submitpricefeedtx_price_invalid(self):
        """
        @author:elaine.tan
        test submitpricefeedtx when currency is invalid
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        address = account.getFeedingAddress(self.init)
        assert address is not None

        # price is invalid
        coin = "WGRT"
        currency = "USD"
        price = "invalid"
        WICC = {"coin": coin, "currency": currency, "price": price}
        params = [address, [WICC]]
        staus_code, response = transaction.submitpricefeedtx(params)
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type str, expected int" in r['error']['message']

    def test_submitpricefeedtx_price_negative(self):
        """
        @author:elaine.tan
        test submitpricefeedtx when currency is negative
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        address = account.getFeedingAddress(self.init)
        assert address is not None

        # price < 0
        coin = "WGRT"
        currency = "USD"
        price = -10000
        WICC = {"coin": coin, "currency": currency, "price": price}
        params = [address, [WICC]]
        staus_code, response = transaction.submitpricefeedtx(params)
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Invalid price" in r['error']['message']

    def test_submitpricefeedtx_price_zero(self):
        """
        @author:elaine.tan
        test submitpricefeedtx when currency =0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        address = account.getFeedingAddress(self.init)
        assert address is not None

        # price =0
        coin = "WGRT"
        currency = "USD"
        price = 0
        WICC = {"coin": coin, "currency": currency, "price": price}
        params = [address, [WICC]]
        staus_code, response = transaction.submitpricefeedtx(params)
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Invalid price" in r['error']['message']

    def test_submitpricefeedtx_price_large(self):
        """
        @author:elaine.tan
        test submitpricefeedtx when currency is to large
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        address = account.getFeedingAddress(self.init)
        assert address is not None

        # price to large
        coin = "WGRT"
        currency = "USD"
        price = 10000000000000000000
        WICC = {"coin": coin, "currency": currency, "price": price}
        params = [address, [WICC]]
        staus_code, response = transaction.submitpricefeedtx(params)
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Invalid price" in r['error']['message']

    def test_submitpricefeedtx_price_double(self):
        """
        @author:elaine.tan
        test submitpricefeedtx when currency is double
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        address = account.getFeedingAddress(self.init)
        assert address is not None

        # price is not int number
        coin = "WGRT"
        currency = "USD"
        price = 110.11
        WICC = {"coin": coin, "currency": currency, "price": price}
        params = [address, [WICC]]
        staus_code, response = transaction.submitpricefeedtx(params)
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type real, expected int" in r['error']['message']

    @allure.feature('Test submitpricefeedtx')
    def test_submitpricefeedtx_normal_WICC(self):
        """
        @author:elaine.tan
        test submitpricefeedtx single WICC when normal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        WICCPrice = 100000000
        WICC = {"coin": "WICC", "currency": "USD", "price": WICCPrice}

        address = account.getFeedingAddress(self.init)
        logger.info(address)
        assert address is not None

        # only WICC
        params = [address, [WICC]]
        staus_code, response = transaction.submitpricefeedtx(params)
        assert staus_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitpricefeedtx')
    def test_submitpricefeedtx_normal_WGRT(self):
        """
        @author:elaine.tan
        test submitpricefeedtx single WGRT when normal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        WGRTPrice = 50000000
        WGRT = {"coin": "WGRT", "currency": "USD", "price": WGRTPrice}

        address = account.getFeedingAddress(self.init)
        logger.info(address)
        assert address is not None

        # only WGRT
        params = [address, [WGRT]]
        staus_code, response = transaction.submitpricefeedtx(params)
        assert staus_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitpricefeedtx')
    def test_submitpricefeedtx_fee_negative(self):
        """
        @author:elaine.tan
        test submitpricefeedtx when fee <0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        WICCPrice = 100000000
        WICC = {"coin": "WICC", "currency": "USD", "price": WICCPrice}

        address = account.getFeedingAddress(self.init)
        assert address is not None

        # fee<0
        fee = -1
        params = [address, [WICC], fee]
        staus_code, response = transaction.submitpricefeedtx(params)
        assert staus_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Invalid amount" in r['error']['message']

    @allure.feature('Test submitpricefeedtx')
    def test_submitpricefeedtx_fee_zero(self):
        """
        @author:elaine.tan
        test submitpricefeedtx when fee =0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        WICCPrice = 100000000
        WICC = {"coin": "WICC", "currency": "USD", "price": WICCPrice}

        address = account.getFeedingAddress(self.init)
        assert address is not None

        # fee=0
        fee = 0
        params = [address, [WICC], fee]
        staus_code, response = transaction.submitpricefeedtx(params)
        assert staus_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "fee is too small" in r['error']['message']

    @allure.feature('Test submitpricefeedtx')
    def test_submitpricefeedtx_fee_small(self):
        """
        @author:elaine.tan
        test submitpricefeedtx when fee <10000
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        WICCPrice = 100000000
        WICC = {"coin": "WICC", "currency": "USD", "price": WICCPrice}

        address = account.getFeedingAddress(self.init)
        assert address is not None

        # fee<10000
        fee = 9999
        params = [address, [WICC], fee]
        staus_code, response = transaction.submitpricefeedtx(params)
        assert staus_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "fee is too small" in r['error']['message']

    @allure.feature('Test submitpricefeedtx')
    def test_submitpricefeedtx_fee_WUSD(self):
        """
        @author:elaine.tan
        test submitpricefeedtx when fee is WUSD
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)
        sender = account.getNewAddrWithEnoughWUSD(self.init, 500000000)
        assert sender is not None
        WICCPrice = 200000000
        WICC = {"coin": "WICC", "currency": "USD", "price": WICCPrice}
        address = account.getFeedingAddress(self.init)
        assert address is not None
        feedfee = str(self.feedfee)
        res = account.send([sender, address, "WUSD:"+feedfee, self.sendfee])
        assert res
        sleep(6)

        # fee is WUSD
        params = [address, [WICC], "WUSD:"+feedfee+":sawi"]
        print(params)
        staus_code, response = transaction.submitpricefeedtx(params)
        print(response)
        assert staus_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitpricefeedtx')
    def test_submitpricefeedtx_fee_large(self):
        """
        @author:elaine.tan
        test submitpricefeedtx when fee >10000
        """
        sleep(10)
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        WICCPrice = 100000000
        WICC = {"coin": "WICC", "currency": "USD", "price": WICCPrice}

        address = account.getFeedingAddress(self.init)
        assert address is not None

        # fee>10000
        fee = self.feedfee*2
        params = [address, [WICC], fee]
        staus_code, response = transaction.submitpricefeedtx(params)
        assert staus_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitpricefeedtx')
    def test_submitpricefeedtx_fee_equal(self):
        """
        @author:elaine.tan
        test submitpricefeedtx when fee =10000
        """
        sleep(10)
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        WICCPrice = 100000000
        WICC = {"coin": "WICC", "currency": "USD", "price": WICCPrice}

        address = account.getFeedingAddress(self.init)
        assert address is not None

        fee = self.feedfee
        params = [address, [WICC], fee]
        staus_code, response = transaction.submitpricefeedtx(params)
        assert staus_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitpricefeedtx')
    def test_submitpricefeedtx_fee_invalid(self):
        """
        @author:elaine.tan
        test submitpricefeedtx when fee is invalid str
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        WICCPrice = 100000000
        WICC = {"coin": "WICC", "currency": "USD", "price": WICCPrice}

        address = account.getFeedingAddress(self.init)
        assert address is not None

        fee = "invalid"
        params = [address, [WICC], fee]
        staus_code, response = transaction.submitpricefeedtx(params)
        assert staus_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Invalid combo money format" in r['error']['message']

    @allure.feature('Test submitpricefeedtx')
    def test_submitpricefeedtx_twice(self):
        """
        @author:elaine.tan
        test submitpricefeedtx when :
        The same address is submitted twice in the same block
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)
        sleep(10)
        WICCPrice = 100000000
        WGRTPrice = 100000000
        WICC = {"coin": "WICC", "currency": "USD", "price": WICCPrice}
        WGRT = {"coin": "WGRT", "currency": "USD", "price": WGRTPrice}

        address = account.getFeedingAddress(self.init)
        assert address is not None
        params = [address, [WICC, WGRT]]
        staus_code, response = transaction.submitpricefeedtx(params)
        assert staus_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

        sleep(6)
        WICCPrice = 29000000
        WGRTPrice = 29000000
        WICC = {"coin": "WICC", "currency": "USD", "price": WICCPrice}
        WGRT = {"coin": "WGRT", "currency": "USD", "price": WGRTPrice}
        params = [address, [WICC, WGRT]]
        staus_code, response = transaction.submitpricefeedtx(params)
        assert staus_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "duplicated-pricefeed" in r['error']['message']

    @allure.feature('Test getmedianprice')
    def test_getmedianprice_assert(self):
        """
        @author:elaine.tan
        test getmedianprice when:
        assert the median price when: 一个块只有一个喂价交易
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        block = Block(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # 同一块只有一个喂价交易
        address = account.getFeedingAddress(self.init)
        assert address is not None
        # 喂价
        WICCPrices = [1200000000, 195280000, 199030000, 195680000, 198000000, 197800000, 195690000, 113000000]
        for i in range(0, len(WICCPrices)):
            WICC = {"coin": "WICC", "currency": "USD", "price": WICCPrices[i]}
            params = [address, [WICC]]
            staus_code, response = tran.submitpricefeedtx(params)
            assert staus_code == 200
            sleep(3)

        prices = []
        height = block.getblockheight()
        for h in range(0, 11):
            his_height = height - h
            status, response = block.getblock([his_height])
            for re in response:
                txs = re['result']['tx']
                tx_count = len(txs)
                # 如果block中的交易数大于1，则循环
                if tx_count > 1:
                    # 取出所有交易类型为PRICE_FEED_TX的交易的喂价价格
                    for nu in range(1, tx_count):
                        txid = txs[nu]
                        status, res = tran.gettxdetail([txid])
                        for r in res:
                            if r['result'] is not None:
                                if r['result']['tx_type'] == "PRICE_FEED_TX":
                                    price = r['result']['price_points'][0]['price']
                                    prices.append(price)
                else:
                    continue

        # 判断当前块的价格与实际取出11块内的中位数价格是否相等
        print(prices)
        print("median:", tran.getmedian(prices) / 100000000)
        print("actual:", tran.getprice("WICC", height))
        assert tran.getprice("WICC", height) == tran.getmedian(prices) / 100000000


if __name__ == '__main__':
    pytest.main(['-s', 'feeding_transaction_test.py::TestFeedingTransaction::test_getmedianprice_assert'])
