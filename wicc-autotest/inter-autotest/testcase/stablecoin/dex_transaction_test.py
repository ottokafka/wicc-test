from time import sleep

import pytest
import allure

from utils.read_property import ReadProperty
from utils.log import mylog
from wiccsdk.account import Account
from wiccsdk.transaction import Transaction
from wiccsdk.wallet import Wallet
from wiccsdk.block import Block

logger = mylog(__name__).getlog()

# match order
matchaddr = [None, "invalid", 0, -100]
# cancel order
cancel_addr = [None, "invalid", 0, -100]
cancel_txid = cancel_addr
invalid_fee = [None, 0, "invalid", -10000, 999, "WUSD:", "WICC:", "WICC:-10000:sawi", "WICC:0", "WUSD:0", "WUSD:-10000",
               1000000000000000]
normal_fee = [100000, 200000, "WUSD:100000:sawi", "WUSD:210000:sawi", "WUSD:100000", "WICC:100000", "100000:sawi"]
# submitorder
invalid_addr = [None, 0, -100, "invalid", "wcx5YzSHg6EqHEhcTjPTxtfPRRGAN7oQhd"]
invalid_currency = [None, 0, -1000, "invalid", "WGRT", "WICC"]
invalid_assettype = [None, 0, -1000, "invalid", "WUSD"]
invalid_price = [None, 0, -10000, "invalid", 100000000000000]
invalid_amount = [None, 0, -10000, 10000, "invalid", 100000000000000]
invalid_sell_price = [None, 0, -10000, "invalid"]


class TestDEXTransaction(object):
    @pytest.fixture(scope='class', autouse=True)
    def setup(self):
        prop = ReadProperty()
        # env = ReadEnv()
        # host = 'http://' + env.read('ip') + ":" + str(env.read('port'))
        # rpcuser = env.read('username')
        # rpcpassword = env.read('password')
        # walletpassword = prop.read('common', 'walletpassword')

        host = prop.read('common', 'host')
        rpcuser = prop.read('common', 'rpcuser')
        rpcpassword = prop.read('common', 'rpcpassword')
        walletpassword = prop.read('common', 'walletpassword')
        creation = prop.read('common', 'creation_addr')
        match = prop.read('common', 'match_addr')
        init = prop.read('common', 'init_addr')

        wallet = Wallet(host, rpcuser, rpcpassword)
        account = Account(host, rpcuser, rpcpassword)
        tran = Transaction(host, rpcuser, rpcpassword)

        isLocked = wallet.islocked()
        # 解锁钱包
        if isLocked:
            status, response = wallet.walletpassphrase([walletpassword, 360000])
            assert status == 200
        else:
            pass

        assert not wallet.islocked()

        status, response = tran.send([init, match, "100000000", 10000])
        assert status == 200

        res = account.feedingPrice(init, "USD", 2, 2)
        assert res

        logger.info('class setup')

    @pytest.fixture(scope='class', autouse=True)
    def teardown(self, request):
        def fin():
            logger.info('class teardown')

        request.addfinalizer(fin)

    @pytest.fixture(scope='function', autouse=True)
    def setup_function(self):
        prop = ReadProperty()
        # env = ReadEnv()
        # self.host = 'http://' + env.read('ip') + ":" + str(env.read('port'))
        # self.rpcuser = env.read('username')
        # self.rpcpassword = env.read('password')
        self.host = prop.read('common', 'host')
        self.rpcuser = prop.read('common', 'rpcuser')
        self.rpcpassword = prop.read('common', 'rpcpassword')
        self.creation = prop.read('common', 'creation_addr')
        self.contracturl = prop.read('common', 'contracturl')
        self.match = prop.read('common', 'match_addr')
        self.init = prop.read('common', 'init_addr')
        self.dexfee = int(prop.read('fee', 'dex'))
        self.sendfee = int(prop.read('fee', 'send'))

        logger.info('function setup')

    @pytest.fixture(scope='function', autouse=True)
    def teardown_function(self):
        logger.info('function teardown')

    @pytest.fixture(scope='function', autouse=False)
    def test_param(self, request):
        return request.param

    @allure.feature('Test submitdexbuylimitordertx')
    def test_submitdexbuylimitordertx_WICC(self):
        """
        @author:elaine.tan
        test submitdexbuylimitordertx when normal buy WICC
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getNewAddrWithEnoughWUSD(self.init, 100000000)
        logger.info(addr)

        # buy WICC
        amount = 20000000
        price = 200000000
        params = [addr, "WUSD", "WICC", amount, price, "WUSD:"+str(self.dexfee)+":sawi"]
        status_code, response = tran.submitdexbuylimitordertx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

        sleep(10)
        status_code, response = account.getaccountinfo([addr])
        for r in response:
            assert r['result'] is not None
            assert r['result']['tokens']['WUSD']['frozen_amount'] == (amount * price) / 100000000

    @allure.feature('Test submitdexbuylimitordertx')
    def test_submitdexbuylimitordertx_WGRT(self):
        """
        @author:elaine.tan
        test submitdexbuylimitordertx when normal buy WGRT
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getNewAddrWithEnoughWUSD(self.init, 200000000)
        logger.info(addr)

        # buy WGRT
        amount2 = 30000000
        price2 = 50000000
        params = [addr, "WUSD", "WGRT", amount2, price2]
        status_code, response = tran.submitdexbuylimitordertx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

        sleep(10)
        status_code, response = account.getaccountinfo([addr])
        for r in response:
            assert r['result'] is not None
            assert r['result']['tokens']['WUSD']['frozen_amount'] == (amount2 * price2) / 100000000

    # Passed
    @allure.feature('Test submitdexbuylimitordertx')
    def test_submitdexbuylimitordertx_address_none(self):
        """
        @author:otto.kafka
        test submitdexbuylimitordertx when address is null
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getnewaddresswithWICC(self.init, 200000000)
        logger.info(addr)

        # get WUSD
        params = [addr, str(150000000), str(50000000)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(20)

        # buy WICC
        addr = None
        amount = 20000
        price = 100
        params = [addr, "WUSD", "WICC", amount, price]
        status_code, response = tran.submitdexbuylimitordertx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type null, expected str" in r['error']['message']

    # Passed
    @allure.feature('Test submitdexbuylimitordertx')
    def test_submitdexbuylimitordertx_address_unregister(self):
        """
        @author:otto.kafka
        test submitdexbuylimitordertx WICC when address is unregister
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getnewaddresswithWICC(self.init, 200000000)
        logger.info(addr)

        # get WUSD
        params = [addr, str(150000000), str(50000000)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(20)

        # buy WICC
        addr = account.newaddress()
        amount = 20000
        price = 100
        params = [addr, "WUSD", "WICC", amount, price]
        status_code, response = tran.submitdexbuylimitordertx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "The account not exists" in r['error']['message']

    @allure.feature('Test submitdexbuylimitordertx')
    def test_submitdexbuylimitordertx_address_invalid(self):
        """
        @author:otto.kafka
        test submitdexbuylimitordertx WICC when address is invalid
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getnewaddresswithWICC(self.init, 200000000)
        logger.info(addr)

        # get WUSD
        params = [addr, str(150000000), str(50000000)]
        txid = account.getNewCDP(params)
        assert txid is not None
        sleep(20)

        # buy WICC
        addr = "invalid"
        amount = 20000
        price = 100
        params = [addr, "WUSD", "WICC", amount, price]
        status_code, response = tran.submitdexbuylimitordertx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Invalid addr" in r['error']['message']

    @allure.feature('Test submitdexbuylimitordertx')
    @pytest.mark.parametrize('test_param', invalid_addr, indirect=True)
    def test_submitdexbuylimitordertx_address_unnormal(self, test_param):
        """
        @author:elaine.tan
        test submitdexbuylimitordertx  when address is unnormal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getNewAddrWithEnoughWUSD(self.init, 50000000)
        assert addr is not None
        logger.info(addr)

        params = [test_param, "WUSD", "WICC", 200000000, 100000000]
        status_code, response = tran.submitdexbuylimitordertx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print("****res:*****", r['error']['message'])

    @allure.feature('Test submitdexbuymarketordertx')
    @pytest.mark.parametrize('test_param', invalid_addr, indirect=True)
    def test_submitdexbuymarketordertx_address_unnormal(self, test_param):
        """
        @author:elaine.tan
        test submitdexbuymarketordertx  when address is unnormal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getNewAddrWithEnoughWUSD(self.init, 50000000)
        assert addr is not None
        logger.info(addr)

        params = [test_param, "WUSD", 20000000, "WICC"]
        status_code, response = tran.submitdexbuymarketordertx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print("****res:*****", r['error']['message'])

    @allure.feature('Test submitdexselllimitordertx')
    @pytest.mark.parametrize('test_param', invalid_addr, indirect=True)
    def test_submitdexselllimitordertx_address_unnormal(self, test_param):
        """
        @author:elaine.tan
        test submitdexselllimitordertx  when address is unnormal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getnewaddresswithWICC(self.init, 50000000)
        assert addr is not None
        logger.info(addr)

        # limit sell
        params = [test_param, "WUSD", "WICC", 20000000, 100000000]
        status_code, response = tran.submitdexselllimitordertx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print("****res:*****", r['error']['message'])

    @allure.feature('Test submitdexsellmarketordertx')
    @pytest.mark.parametrize('test_param', invalid_addr, indirect=True)
    def test_submitdexsellmarketordertx_address_unnormal(self, test_param):
        """
        @author:elaine.tan
        test submitdexsellmarketordertx  when address is unnormal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getnewaddresswithWICC(self.init, 50000000)
        assert addr is not None
        logger.info(addr)

        # market  sell
        params = [test_param, "WUSD", "WICC", 200000000]
        status_code, response = tran.submitdexsellmarketordertx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print("****res:*****", r['error']['message'])

    @allure.feature('Test submitdexbuylimitordertx')
    @pytest.mark.parametrize('test_param', invalid_currency, indirect=True)
    def test_submitdexbuylimitordertx_currency_unnormal(self, test_param):
        """
        @author:elaine.tan
        test submitdexbuylimitordertx  when currency is unnormal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getNewAddrWithEnoughWUSD(self.init, 50000000)
        assert addr is not None
        logger.info(addr)

        # limit buy
        params = [addr, test_param, "WICC", 20000000, 100000000]
        status_code, response = tran.submitdexbuylimitordertx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print("****res:*****", r['error']['message'])

    @allure.feature('Test submitdexbuymarketordertx')
    @pytest.mark.parametrize('test_param', invalid_currency, indirect=True)
    def test_submitdexbuymarketordertx_currency_unnormal(self, test_param):
        """
        @author:elaine.tan
        test submitdexbuymarketordertx  when currency is unnormal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getNewAddrWithEnoughWUSD(self.init, 50000000)
        assert addr is not None
        logger.info(addr)

        # market buy
        params = [addr, test_param, 20000000, "WICC"]
        status_code, response = tran.submitdexbuymarketordertx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print("****res:*****", r['error']['message'])

    @allure.feature('Test submitdexselllimitordertx')
    @pytest.mark.parametrize('test_param', invalid_currency, indirect=True)
    def test_submitdexselllimitordertx_currency_unnormal(self, test_param):
        """
        @author:elaine.tan
        test submitdexselllimitordertx  when currency is unnormal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getnewaddresswithWICC(self.init, 50000000)
        assert addr is not None
        logger.info(addr)

        # limit sell
        params = [addr, test_param, "WICC", 20000000, 100000000]
        status_code, response = tran.submitdexselllimitordertx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print("****res:*****", r['error']['message'])

    @allure.feature('Test submitdexsellmarketordertx')
    @pytest.mark.parametrize('test_param', invalid_currency, indirect=True)
    def test_submitdexsellmarketordertx_currency_unnormal(self, test_param):
        """
        @author:elaine.tan
        test submitdexsellmarketordertx  when currency is unnormal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getnewaddresswithWICC(self.init, 50000000)
        assert addr is not None
        logger.info(addr)

        # market  sell
        params = [addr, test_param, "WICC", 200000000]
        status_code, response = tran.submitdexsellmarketordertx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print("****res:*****", r['error']['message'])

    @allure.feature('Test submitdexbuylimitordertx')
    @pytest.mark.parametrize('test_param', invalid_assettype, indirect=True)
    def test_submitdexbuylimitordertx_assettype_unnormal(self, test_param):
        """
        @author:elaine.tan
        test submitdexbuylimitordertx  when assert type is unnormal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getNewAddrWithEnoughWUSD(self.init, 50000000)
        assert addr is not None
        logger.info(addr)

        # limit buy
        params = [addr, "WUSD", test_param, 20000000, 100000000]
        status_code, response = tran.submitdexbuylimitordertx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print("****res:*****", r['error']['message'])

    @allure.feature('Test submitdexbuymarketordertx')
    @pytest.mark.parametrize('test_param', invalid_assettype, indirect=True)
    def test_submitdexbuymarketordertx_assettype_unnormal(self, test_param):
        """
        @author:elaine.tan
        test submitdexbuymarketordertx  when assettype is unnormal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getNewAddrWithEnoughWUSD(self.init, 50000000)
        assert addr is not None
        logger.info(addr)

        # market buy
        params = [addr, "WUSD", 20000000, test_param]
        status_code, response = tran.submitdexbuymarketordertx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print("****res:*****", r['error']['message'])

    @allure.feature('Test submitdexselllimitordertx')
    @pytest.mark.parametrize('test_param', invalid_assettype, indirect=True)
    def test_submitdexselllimitordertx_assettype_unnormal(self, test_param):
        """
        @author:elaine.tan
        test submitdexselllimitordertx  when assettype is unnormal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getnewaddresswithWICC(self.init, 50000000)
        assert addr is not None
        logger.info(addr)

        # limit sell
        params = [addr, "WUSD", test_param, 20000000, 100000000]
        status_code, response = tran.submitdexselllimitordertx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print("****res:*****", r['error']['message'])

    @allure.feature('Test submitdexsellmarketordertx')
    @pytest.mark.parametrize('test_param', invalid_assettype, indirect=True)
    def test_submitdexsellmarketordertx_assettype_unnormal(self, test_param):
        """
        @author:elaine.tan
        test submitdexsellmarketordertx  when assettype is unnormal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getnewaddresswithWICC(self.init, 50000000)
        assert addr is not None
        logger.info(addr)

        # market  sell
        params = [addr, "WUSD", test_param, 20000000]
        status_code, response = tran.submitdexsellmarketordertx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print("****res:*****", r['error']['message'])

    @allure.feature('Test submitdexbuylimitordertx')
    @pytest.mark.parametrize('test_param', invalid_price, indirect=True)
    def test_submitdexbuylimitordertx_price_unnormal(self, test_param):
        """
        @author:otto.kafka
        test submitdexbuylimitordertx when price is unnormal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getNewAddrWithEnoughWUSD(self.init, 50000000)
        assert addr is not None
        logger.info(addr)

        params = [addr, "WUSD", "WICC", 10000000, test_param]
        status_code, response = tran.submitdexbuylimitordertx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print("****res:****", r['error']['message'])

    @allure.feature('Test submitdexselllimitordertx')
    @pytest.mark.parametrize('test_param', invalid_sell_price, indirect=True)
    def test_submitdexselllimitordertx_price_unnormal(self, test_param):
        """
        @author:elaine.tan
        test submitdexselllimitordertx when price is unnormal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getnewaddresswithWICC(self.init, 20000000)
        assert addr is not None
        logger.info(addr)

        params = [addr, "WUSD", "WICC", 10000000, test_param]
        status_code, response = tran.submitdexselllimitordertx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print("****res:****", r['error']['message'])

    @allure.feature('Test submitdexbuylimitordertx')
    @pytest.mark.parametrize('test_param', invalid_price, indirect=True)
    def test_submitdexbuylimitordertx_amount_unnormal(self, test_param):
        """
        @author:otto.kafka
        test submitdexbuylimitordertx when amount is unnormal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getNewAddrWithEnoughWUSD(self.init, 50000000)
        assert addr is not None
        logger.info(addr)

        params = [addr, "WUSD", "WICC", test_param, 100000000]
        status_code, response = tran.submitdexbuylimitordertx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print("****res:****", r['error']['message'])

    @allure.feature('Test submitdexselllimitordertx')
    @pytest.mark.parametrize('test_param', invalid_price, indirect=True)
    def test_submitdexselllimitordertx_amount_unnormal(self, test_param):
        """
        @author:elaine.tan
        test submitdexselllimitordertx when amount is unnormal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getnewaddresswithWICC(self.init, 2000000)
        assert addr is not None
        logger.info(addr)

        params = [addr, "WUSD", "WICC", test_param, 100000000]
        status_code, response = tran.submitdexselllimitordertx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print("****res:****", r['error']['message'])

    @allure.feature('Test submitdexsellmarketordertx')
    @pytest.mark.parametrize('test_param', invalid_price, indirect=True)
    def test_submitdexsellmarketordertx_amount_unnormal(self, test_param):
        """
        @author:elaine.tan
        test submitdexsellmarketordertx  when amount is unnormal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getnewaddresswithWICC(self.init, 50000)
        assert addr is not None
        logger.info(addr)

        # market  sell
        params = [addr, "WUSD", "WICC", test_param]
        status_code, response = tran.submitdexsellmarketordertx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print("****res:*****", r['error']['message'])

    @allure.feature('Test submitdexbuymarketordertx')
    @pytest.mark.parametrize('test_param', invalid_price, indirect=True)
    def test_submitdexbuymarketordertx_amount_unnormal(self, test_param):
        """
        @author:elaine.tan
        test submitdexbuymarketordertx  when amount is unnormal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getNewAddrWithEnoughWUSD(self.init, 50000000)
        assert addr is not None
        logger.info(addr)

        # market buy
        params = [addr, "WUSD", test_param, "WICC"]
        status_code, response = tran.submitdexbuymarketordertx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print("****res:*****", r['error']['message'])

    @allure.feature('Test submitdexbuymarketordertx')
    @pytest.mark.parametrize('test_param', invalid_fee, indirect=True)
    def test_submitdexbuymarketordertx_fee_unnormal(self, test_param):
        """
        @author:elaine.tan
        test submitdexbuymarketordertx  when fee is unnormal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getNewAddrWithEnoughWUSD(self.init, 50000000)
        assert addr is not None
        logger.info(addr)

        # market buy
        params = [addr, "WUSD", 10000000, "WICC", test_param]
        status_code, response = tran.submitdexbuymarketordertx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print("****res:*****", r['error']['message'])

    @allure.feature('Test submitdexbuymarketordertx')
    @pytest.mark.parametrize('test_param', normal_fee, indirect=True)
    def test_submitdexbuymarketordertx_fee_normal(self, test_param):
        """
        @author:elaine.tan
        test submitdexbuymarketordertx  when fee is normal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getNewAddrWithEnoughWUSD(self.init, 50000000)
        assert addr is not None
        logger.info(addr)

        # market buy
        params = [addr, "WUSD", 10000000, "WICC", test_param]
        status_code, response = tran.submitdexbuymarketordertx(params)
        assert status_code == 200
        print(response)
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitdexsellmarketordertx')
    @pytest.mark.parametrize('test_param', invalid_fee, indirect=True)
    def test_submitdexsellmarketordertx_fee_unnormal(self, test_param):
        """
        @author:elaine.tan
        test submitdexsellmarketordertx  when fee is unnormal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getnewaddresswithWICC(self.init, 50000000)
        assert addr is not None
        logger.info(addr)

        # market  sell
        params = [addr, "WUSD", "WICC", 10000000, test_param]
        status_code, response = tran.submitdexsellmarketordertx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print("****res:*****", r['error']['message'])

    @allure.feature('Test submitdexsellmarketordertx')
    @pytest.mark.parametrize('test_param', normal_fee, indirect=True)
    def test_submitdexsellmarketordertx_fee_normal(self, test_param):
        """
        @author:elaine.tan
        test submitdexsellmarketordertx  when fee is normal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getNewAddrWithEnoughWUSD(self.init, 50000000)
        assert addr is not None
        res = account.send([self.init, addr, 10000000, self.sendfee])
        assert res
        sleep(6)
        logger.info(addr)

        # market  sell
        params = [addr, "WUSD", "WICC", 10000000, test_param]
        status_code, response = tran.submitdexsellmarketordertx(params)
        assert status_code == 200
        for r in response:
            print(999, response)
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitdexselllimitordertx')
    @pytest.mark.parametrize('test_param', invalid_fee, indirect=True)
    def test_submitdexselllimitordertx_fee_unnormal(self, test_param):
        """
        @author:elaine.tan
        test submitdexselllimitordertx  when fee is unnormal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getnewaddresswithWICC(self.init, 50000000)
        assert addr is not None
        logger.info(addr)

        params = [addr, "WUSD", "WICC", 10000000, 100000000, test_param]
        status_code, response = tran.submitdexselllimitordertx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print("****res:*****", r['error']['message'])

    @allure.feature('Test submitdexselllimitordertx')
    @pytest.mark.parametrize('test_param', normal_fee, indirect=True)
    def test_submitdexselllimitordertx_fee_normal(self, test_param):
        """
        @author:elaine.tan
        test submitdexselllimitordertx  when fee is normal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getNewAddrWithEnoughWUSD(self.init, 50000000)
        assert addr is not None
        res = account.send([self.init, addr, 10000000, self.sendfee])
        assert res
        sleep(6)
        logger.info(addr)

        params = [addr, "WUSD", "WICC", 10000000, 10000000, test_param]
        print(params)
        status_code, response = tran.submitdexselllimitordertx(params)
        print(response)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitdexbuylimitordertx')
    @pytest.mark.parametrize('test_param', invalid_fee, indirect=True)
    def test_submitdexbuylimitordertx_fee_unnormal(self, test_param):
        """
        @author:elaine.tan
        test submitdexbuylimitordertx  when fee is unnormal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getNewAddrWithEnoughWUSD(self.init, 50000000)
        assert addr is not None
        logger.info(addr)

        params = [addr, "WUSD", "WICC", 10000000, 100000000, test_param]
        status_code, response = tran.submitdexbuylimitordertx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print("****res:*****", r['error']['message'])

    @allure.feature('Test submitdexbuylimitordertx')
    @pytest.mark.parametrize('test_param', normal_fee, indirect=True)
    def test_submitdexbuylimitordertx_fee_normal(self, test_param):
        """
        @author:elaine.tan
        test submitdexbuylimitordertx when fee is normal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getNewAddrWithEnoughWUSD(self.init, 50000000)
        assert addr is not None
        logger.info(addr)

        params = [addr, "WUSD", "WICC", 10000000, 10000000, test_param]
        print(params)
        status_code, response = tran.submitdexbuylimitordertx(params)
        print(response)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

    # Passed
    @allure.feature('Test submitdexbuymarketordertx')
    def test_submitdexbuymarketordertx_WICC(self):
        """
        @author:elaine.tan
        test submitdexbuymarketordertx when normal buy WICC
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        addr = account.getNewAddrWithEnoughWUSD(self.init, 100000000)
        logger.info(addr)
        wicc_pri = tran.getprice("WICC")

        # buy WICC
        coin_amount = 20000000
        params = [addr, "WUSD", coin_amount, "WICC", "WUSD:"+str(self.dexfee)+":sawi"]
        status_code, response = tran.submitdexbuymarketordertx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

        sleep(10)
        status_code, response = account.getaccountinfo([addr])
        for r in response:
            assert r['result'] is not None
            assert r['result']['tokens']['WUSD']['frozen_amount'] == coin_amount

    # Passed
    @allure.feature('Test submitdexbuymarketordertx')
    def test_submitdexbuymarketordertx_address_none(self):
        """
        @author:otto.kafka
        test submitdexbuymarketordertx when address is null
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # buy WICC
        addr = None
        wicc_amount = 100
        params = [addr, "WUSD", wicc_amount, "WICC"]
        status_code, response = tran.submitdexbuymarketordertx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type null, expected str" in r['error']['message']

    # Passed
    @allure.feature('Test submitdexbuymarketordertx')
    def test_submitdexbuymarketordertx_address_unregister(self):
        """
        @author:otto.kafka
        test submitdexbuymarketordertx when address not unregister
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # buy WICC
        addr = account.newaddress()
        wicc_amount = 100
        params = [addr, "WUSD", wicc_amount, "WICC"]
        status_code, response = tran.submitdexbuymarketordertx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "account not exists" in r['error']['message']

    @allure.feature('Test submitdexbuymarketordertx')
    def test_submitdexbuymarketordertx_address_invalid(self):
        """
        @author:otto.kafka
        test submitdexbuymarketordertx when address is valid
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # buy WGRT
        addr = "invalid"
        wicc_amount = 100
        params = [addr, "WUSD", wicc_amount, "WGRT"]
        status_code, response = tran.submitdexbuymarketordertx(params)
        print(response)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Invalid addr" in r['error']['message']

    @allure.feature('Test submitdexbuymarketordertx')
    def test_submitdexbuymarketordertx_amount_neg(self):
        """
        @author:otto.kafka
        test submitdexbuymarketordertx when amount < -1
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        addr = account.getNewAddrWithEnoughWUSD(self.init, 100000000)
        logger.info(addr)

        ## buy WGRT
        wgrt_amount = -1
        params = [addr, "WUSD", wgrt_amount, "WGRT"]
        status_code, response = tran.submitdexbuymarketordertx(params)
        print(response)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Invalid amount" in r['error']['message']

    @allure.feature('Test submitdexbuymarketordertx')
    def test_submitdexbuymarketordertx_amount_zero(self):
        """
        @author:otto.kafka
        test submitdexbuymarketordertx when amount = 0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        addr = account.getNewAddrWithEnoughWUSD(self.init, 100000000)
        logger.info(addr)

        # buy WICC
        wgrt_amount = 0
        params = [addr, "WUSD", wgrt_amount, "WICC"]
        status_code, response = tran.submitdexbuymarketordertx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "order-amount-too-small" in r['error']['message']

    @allure.feature('Test submitdexbuymarketordertx')
    def test_submitdexbuymarketordertx_WGRT(self):
        """
        @author:elaine.tan
        test submitdexbuymarketordertx when normal buy WGRT
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getNewAddrWithEnoughWUSD(self.init, 100000000)
        logger.info(addr)
        wgrt_pri = tran.getprice("WGRT")

        # buy WGRT
        coin_amount = 30000000
        params = [addr, "WUSD", coin_amount, "WGRT"]
        status_code, response = tran.submitdexbuymarketordertx(params)
        print(666, response)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

        sleep(10)
        status_code, response = account.getaccountinfo([addr])
        for r in response:
            assert r['result'] is not None
            assert r['result']['tokens']['WUSD']['frozen_amount'] == coin_amount

    @allure.feature('Test submitdexcancelordertx')
    def test_submitdexcancelordertx_marketbuy_WICC(self):
        """
        @author:elaine.tan
        test submitdexcancelordertx when market buy WICC
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        wusd_balance = 100000000
        addr = account.getNewAddrWithEnoughWUSD(self.init, wusd_balance)
        logger.info(addr)
        wicc_pri = tran.getprice("WICC")

        # buy WICC
        coin_amount = 40000000
        params = [addr, "WUSD", coin_amount, "WICC"]
        status_code, response = tran.submitdexbuymarketordertx(params)
        assert status_code == 200
        wiccTxid = ""
        for r in response:
            assert r['result']['txid'] is not None
            wiccTxid = r['result']['txid']

        sleep(10)
        status_code, response = account.getaccountinfo([addr])
        for r in response:
            assert r['result'] is not None
            assert r['result']['tokens']['WUSD']['free_amount'] == wusd_balance - coin_amount
            assert r['result']['tokens']['WUSD']['frozen_amount'] == coin_amount

        # cancle buy wicc tx
        status_code, response = tran.submitdexcancelordertx([addr, wiccTxid])
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

        sleep(10)
        status_code, response = account.getaccountinfo([addr])
        for r in response:
            assert r['result'] is not None
            assert r['result']['tokens']['WUSD']['frozen_amount'] == 0
            assert r['result']['tokens']['WUSD']['free_amount'] == wusd_balance

    @allure.feature('Test submitdexcancelordertx')
    def test_submitdexcancelordertx_marketbuy_WGRT(self):
        """
        @author:elaine.tan
        test submitdexcancelordertx when market buy WGRT
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        wusd_balance = 100000000
        addr = account.getNewAddrWithEnoughWUSD(self.init, wusd_balance)
        logger.info(addr)
        wgrt_pri = tran.getprice("WGRT")

        # buy WGRT
        coin_amount = 80000000
        params = [addr, "WUSD", coin_amount, "WGRT"]
        status_code, response = tran.submitdexbuymarketordertx(params)
        assert status_code == 200
        wgrtTxid = ""
        for r in response:
            assert r['result']['txid'] is not None
            wgrtTxid = r['result']['txid']

        sleep(10)
        status_code, response = account.getaccountinfo([addr])
        for r in response:
            assert r['result'] is not None
            assert r['result']['tokens']['WUSD']['free_amount'] == wusd_balance - coin_amount
            assert r['result']['tokens']['WUSD']['frozen_amount'] == coin_amount

        # cancle buy wgrt tx
        status_code, response = tran.submitdexcancelordertx([addr, wgrtTxid])
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

        sleep(10)
        status_code, response = account.getaccountinfo([addr])
        for r in response:
            assert r['result'] is not None
            assert r['result']['tokens']['WUSD']['frozen_amount'] == 0
            assert r['result']['tokens']['WUSD']['free_amount'] == wusd_balance

    @allure.feature('Test submitdexcancelordertx')
    @pytest.mark.parametrize('test_param', cancel_addr, indirect=True)
    def test_submitdexcancelordertx_address(self, test_param):
        """
        @author:elaine.tan
        test submitdexcancelordertx when address is unnormal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        status_code, response = tran.submitdexcancelordertx([test_param, "123"])
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print(r['error']['message'])

    @allure.feature('Test submitdexcancelordertx')
    def test_submitdexcancelordertx_address_unregister(self):
        """
        @author:elaine.tan
        test submitdexcancelordertx when address is unregister
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.newaddress()
        txid = "69349af580c6e389d3537014fd84c8abaad898026dfff27382eb64ea8e4369a0"
        status_code, response = tran.submitdexcancelordertx([addr, txid])
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "account not exists" in r['error']['message']

    @allure.feature('Test submitdexcancelordertx')
    def test_submitdexcancelordertx_address_notowner(self):
        """
        @author:elaine.tan
        test submitdexcancelordertx when address is not orderowner
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        wicc_balance = 80000000
        creater = account.getnewaddresswithWICC(self.init, wicc_balance)
        logger.info(creater)

        # sell WICC
        fee = self.dexfee
        wicc_amount = 20000000
        params = [creater, "WUSD", "WICC", wicc_amount, fee]
        status_code, response = tran.submitdexsellmarketordertx(params)
        assert status_code == 200
        wiccTxid = ""
        for r in response:
            assert r['result']['txid'] is not None
            wiccTxid = r['result']['txid']
            print(wiccTxid)
            sleep(6)

        addr = account.getnewaddresswithWICC(self.init, fee)
        status_code, response = tran.submitdexcancelordertx([addr, wiccTxid])
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "user-unmatched" in r['error']['message']

    @allure.feature('Test submitdexcancelordertx')
    @pytest.mark.parametrize('test_param', cancel_txid, indirect=True)
    def test_submitdexcancelordertx_txid(self, test_param):
        """
        @author:elaine.tan
        test submitdexcancelordertx when address is unnormal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        addr = account.getEnoughCoindRegisteredAddress(10000)
        status_code, response = tran.submitdexcancelordertx([addr, test_param])
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print(r['error']['message'])

    @allure.feature('Test submitdexcancelordertx')
    def test_submitdexcancelordertx_txid_canceled(self):
        """
        @author:elaine.tan
        test submitdexcancelordertx when txid has been canceled
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        wicc_balance = 100000000
        addr = account.getnewaddresswithWICC(self.init, wicc_balance)
        logger.info(addr)

        # sell WICC
        fee = self.dexfee
        wicc_amount = 20000000
        params = [addr, "WUSD", "WICC", wicc_amount, fee]
        status_code, response = tran.submitdexsellmarketordertx(params)
        assert status_code == 200
        wiccTxid = ""
        for r in response:
            assert r['result']['txid'] is not None
            wiccTxid = r['result']['txid']
            print(wiccTxid)
            sleep(6)

        status_code, response = tran.submitdexcancelordertx([addr, wiccTxid])
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            sleep(6)

        status_code, response = tran.submitdexcancelordertx([addr, wiccTxid])
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Order is inactive or not existed" in r['error']['message']

    @allure.feature('Test submitdexcancelordertx')
    @pytest.mark.parametrize('test_param', invalid_fee, indirect=True)
    def test_submitdexcancelordertx_fee_unnormal(self, test_param):
        """
        @author:elaine.tan
        test submitdexcancelordertx when fee is unnormal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        wicc_balance = 100000000
        addr = account.getnewaddresswithWICC(self.init, wicc_balance)
        logger.info(addr)

        # sell WICC
        fee = self.dexfee
        wicc_amount = 20000000
        params = [addr, "WUSD", "WICC", wicc_amount, fee]
        status_code, response = tran.submitdexsellmarketordertx(params)
        assert status_code == 200
        wiccTxid = ""
        for r in response:
            assert r['result']['txid'] is not None
            wiccTxid = r['result']['txid']
            sleep(6)

        status_code, response = tran.submitdexcancelordertx([addr, wiccTxid, test_param])
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print(r['error']['message'])

    @allure.feature('Test submitdexcancelordertx')
    @pytest.mark.parametrize('test_param', normal_fee, indirect=True)
    def test_submitdexcancelordertx_fee_normal(self, test_param):
        """
        @author:elaine.tan
        test submitdexcancelordertx when fee is normal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        sender = account.getNewAddrWithEnoughWUSD(self.init, 100000000)
        assert sender is not None
        wicc_balance = 100000000
        addr = account.getnewaddresswithWICC(self.init, wicc_balance)
        logger.info(addr)
        res = account.send([sender, addr, "WUSD:50000000", self.sendfee])
        assert res
        sleep(6)

        # sell WICC
        fee = self.dexfee
        wicc_amount = 20000000
        params = [addr, "WUSD", "WICC", wicc_amount, fee]
        status_code, response = tran.submitdexsellmarketordertx(params)
        assert status_code == 200
        wiccTxid = ""
        for r in response:
            assert r['result']['txid'] is not None
            wiccTxid = r['result']['txid']
            sleep(6)

        status_code, response = tran.submitdexcancelordertx([addr, wiccTxid, test_param])
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitdexcancelordertx')
    def test_submitdexcancelordertx_marketsell_WICC(self):
        """
        @author:elaine.tan
        test submitdexcancelordertx when market sell WICC
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        wicc_balance = 80000000
        addr = account.getnewaddresswithWICC(self.init, wicc_balance)
        logger.info(addr)

        # sell WICC
        fee = self.dexfee
        wicc_amount = 20000000
        params = [addr, "WUSD", "WICC", wicc_amount, fee]
        status_code, response = tran.submitdexsellmarketordertx(params)
        assert status_code == 200
        wiccTxid = ""
        for r in response:
            assert r['result']['txid'] is not None
            wiccTxid = r['result']['txid']

        sleep(10)
        status_code, response = account.getaccountinfo([addr])
        for r in response:
            assert r['result'] is not None
            assert r['result']['tokens']['WICC']['frozen_amount'] == wicc_amount

        # cancle buy wicc tx
        status_code, response = tran.submitdexcancelordertx([addr, wiccTxid, fee])
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

        sleep(10)
        status_code, response = account.getaccountinfo([addr])
        for r in response:
            assert r['result'] is not None
            assert r['result']['tokens']['WICC']['frozen_amount'] == 0
            assert r['result']['tokens']['WICC']['free_amount'] == wicc_balance - fee * 2

    @allure.feature('Test submitdexcancelordertx')
    def test_submitdexcancelordertx_marketsell_WGRT(self):
        """
        @author:elaine.tan
        test submitdexcancelordertx when market sell WGRT
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        wicc_balance = 80000000
        addr = account.getnewaddresswithWICC(self.init, wicc_balance)
        logger.info(addr)
        # add WGRT to address
        wgrt_balance = 80000000
        params = [self.init, addr, "WGRT:" + str(wgrt_balance) + ":sawi", self.sendfee]
        status_code, response = tran.send(params)
        assert status_code == 200
        sleep(10)

        # sell WGRT
        wgrt_amount = 30000000
        params = [addr, "WUSD", "WGRT", wgrt_amount]
        status_code, response = tran.submitdexsellmarketordertx(params)
        assert status_code == 200
        wgrtTxid = ""
        for r in response:
            assert r['result']['txid'] is not None
            wgrtTxid = r['result']['txid']

        sleep(10)
        status_code, response = account.getaccountinfo([addr])
        for r in response:
            assert r['result'] is not None
            assert r['result']['tokens']['WGRT']['frozen_amount'] == wgrt_amount

        # cancle buy wgrt tx
        status_code, response = tran.submitdexcancelordertx([addr, wgrtTxid])
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

        sleep(10)
        status_code, response = account.getaccountinfo([addr])
        for r in response:
            assert r['result'] is not None
            assert r['result']['tokens']['WGRT']['frozen_amount'] == 0
            assert r['result']['tokens']['WGRT']['free_amount'] == wgrt_balance

    @allure.feature('Test submitdexcancelordertx')
    def test_submitdexcancelordertx_limitbuy_WICC(self):
        """
        @author:elaine.tan
        test submitdexcancelordertx when buy WICC
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        wusd_balance = 100000000
        addr = account.getNewAddrWithEnoughWUSD(self.init, wusd_balance)
        logger.info(addr)

        # buy WICC
        wicc_amount = 20000000
        wicc_pri = 150000000
        params = [addr, "WUSD", "WICC", wicc_amount, wicc_pri]
        status_code, response = tran.submitdexbuylimitordertx(params)
        assert status_code == 200
        wiccTxid = ""
        for r in response:
            assert r['result']['txid'] is not None
            wiccTxid = r['result']['txid']

        sleep(10)
        frozen = (wicc_amount * wicc_pri) / 100000000
        status_code, response = account.getaccountinfo([addr])
        for r in response:
            assert r['result'] is not None
            assert r['result']['tokens']['WUSD']['free_amount'] == wusd_balance - frozen
            assert r['result']['tokens']['WUSD']['frozen_amount'] == frozen

        # cancle buy wicc tx
        params = [addr, wiccTxid]
        status_code, response = tran.submitdexcancelordertx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

        sleep(10)
        status_code, response = account.getaccountinfo([addr])
        for r in response:
            assert r['result'] is not None
            assert r['result']['tokens']['WUSD']['frozen_amount'] == 0
            assert r['result']['tokens']['WUSD']['free_amount'] == wusd_balance

    @allure.feature('Test submitdexcancelordertx')
    def test_submitdexcancelordertx_limitbuy_WGRT(self):
        """
        @author:elaine.tan
        test submitdexcancelordertx when buy limit WGRT
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        wusd_balance = 100000000
        addr = account.getNewAddrWithEnoughWUSD(self.init, wusd_balance)
        logger.info(addr)

        # buy WGRT
        wgrt_amount = 30000000
        wgrt_pri = 80000000
        params = [addr, "WUSD", "WGRT", wgrt_amount, wgrt_pri]
        status_code, response = tran.submitdexbuylimitordertx(params)
        assert status_code == 200
        wgrtTxid = ""
        for r in response:
            assert r['result']['txid'] is not None
            wgrtTxid = r['result']['txid']

        sleep(10)
        frozen = (wgrt_amount * wgrt_pri) / 100000000
        status_code, response = account.getaccountinfo([addr])
        for r in response:
            assert r['result'] is not None
            assert r['result']['tokens']['WUSD']['free_amount'] == wusd_balance - frozen
            assert r['result']['tokens']['WUSD']['frozen_amount'] == frozen

        # cancle buy wgrt tx
        status_code, response = tran.submitdexcancelordertx([addr, wgrtTxid])
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

        sleep(10)
        status_code, response = account.getaccountinfo([addr])
        for r in response:
            assert r['result'] is not None
            assert r['result']['tokens']['WUSD']['frozen_amount'] == 0
            assert r['result']['tokens']['WUSD']['free_amount'] == wusd_balance

    @allure.feature('Test submitdexcancelordertx')
    def test_submitdexcancelordertx_limitsell_WICC(self):
        """
        @author:elaine.tan
        test submitdexcancelordertx when sell limit WICC
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getnewaddresswithWICC(self.init, 200000000)
        logger.info(addr)

        # add WGRT
        params = [self.init, addr, "WGRT:80000000", self.sendfee]
        status_code, response = tran.send(params)
        assert status_code == 200
        sleep(20)

        # sell WICC
        params = [addr, "WUSD", "WICC", 20000000, self.dexfee]
        status_code, response = tran.submitdexselllimitordertx(params)
        assert status_code == 200
        wiccTxid = ""
        for r in response:
            assert r['result']['txid'] is not None
            wiccTxid = r['result']['txid']

        sleep(10)
        # cancle buy wicc tx
        status_code, response = tran.submitdexcancelordertx([addr, wiccTxid])
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitdexcancelordertx')
    def test_submitdexcancelordertx_limitsell_WGRT(self):
        """
        @author:elaine.tan
        test submitdexcancelordertx when sell
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getnewaddresswithWICC(self.init, 200000000)
        logger.info(addr)

        # add WGRT
        params = [self.init, addr, "WGRT:80000000", self.sendfee]
        status_code, response = tran.send(params)
        assert status_code == 200
        sleep(20)

        # sell WGRT
        params = [addr, "WUSD", "WGRT", 30000000, self.dexfee]
        status_code, response = tran.submitdexselllimitordertx(params)
        assert status_code == 200
        wgrtTxid = ""
        for r in response:
            assert r['result']['txid'] is not None
            wgrtTxid = r['result']['txid']

        sleep(10)
        # cancle buy wgrt tx
        status_code, response = tran.submitdexcancelordertx([addr, wgrtTxid])
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitdexselllimitordertx')
    def test_submitdexselllimitordertx_WICC(self):
        """
        @author:elaine.tan
        test submitdexselllimitordertx when normal sell WICC
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getnewaddresswithWICC(self.init, 100000000)
        logger.info(addr)

        # sell WICC
        wicc_amount = 20000000
        wicc_price = 200000000
        params = [addr, "WUSD", "WICC", wicc_amount, wicc_price]
        status_code, response = tran.submitdexselllimitordertx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

        sleep(10)
        status_code, response = account.getaccountinfo([addr])
        for r in response:
            assert r['result'] is not None
            assert r['result']['tokens']['WICC']['frozen_amount'] == wicc_amount

    @allure.feature('Test submitdexselllimitordertx')
    def test_submitdexselllimitordertx_price_zero(self):
        """
        @author:otto.kafka
        test submitdexselllimitordertx when price = 0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getnewaddresswithWICC(self.init, 100000000)
        logger.info(addr)

        # sell WICC
        wicc_amount = 20000000
        wicc_price = 0
        params = [addr, "WUSD", "WICC", wicc_amount, wicc_price]
        status_code, response = tran.submitdexselllimitordertx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "invalid-price-range" in r['error']['message']

    # Passed
    @allure.feature('Test submitdexselllimitordertx')
    def test_submitdexselllimitordertx_WICC_price_neg(self):
        """@author:otto.kafka
        test submitdexselllimitordertx when price < 0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getnewaddresswithWICC(self.init, 100000000)
        logger.info(addr)

        # sell WICC
        wicc_amount = 20000
        wicc_price = -1
        params = [addr, "WUSD", "WICC", wicc_amount, wicc_price]
        status_code, response = tran.submitdexselllimitordertx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Invalid amount" in r['error']['message']

    @allure.feature('Test submitdexselllimitordertx')
    def test_submitdexselllimitordertx_addr_none(self):
        """
        @author:otto.kafka
        test submitdexselllimitordertx when addr is None
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # sell WICC
        addr = None
        amount = 30000
        price = 100
        params = [addr, "WUSD", "WICC", amount, price]
        status_code, response = tran.submitdexselllimitordertx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type null, expected str" in r['error']['message']

    @allure.feature('Test submitdexselllimitordertx')
    def test_submitdexselllimitordertx_addr_unregister(self):
        """
        @author:otto.kafka
        test submitdexselllimitordertx when addr is unregister
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # sell WICC
        addr = account.newaddress()
        WICC_amount = 30000
        WICC_price = 100
        params = [addr, "WUSD", "WICC", WICC_amount, WICC_price]
        status_code, response = tran.submitdexselllimitordertx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "account not exists" in r['error']['message']

    @allure.feature('Test submitdexselllimitordertx')
    def test_submitdexselllimitordertx_addr_invalid(self):
        """
        @author:otto.kafka
        test submitdexselllimitordertx WGRT when addr is invalid:
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # sell WGRT
        addr = "invalid"
        WGRT_amount = 30000
        WGRT_price = 100
        params = [addr, "WUSD", "WGRT", WGRT_amount, WGRT_price]
        status_code, response = tran.submitdexselllimitordertx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Invalid addr" in r['error']['message']

    @allure.feature('Test submitdexselllimitordertx')
    def test_submitdexselllimitordertx_WGRT(self):
        """
        @author:elaine.tan
        test submitdexselllimitordertx when normal sell WGRT
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getnewaddresswithWICC(self.init, 100000000)
        logger.info(addr)
        # add WGRT to address
        params = [self.init, addr, "WGRT:80000000:sawi", self.sendfee]
        status_code, response = tran.send(params)
        assert status_code == 200
        sleep(20)

        # sell WGRT
        wgrt_amount = 30000000
        wgrt_price = 50000000
        params = [addr, "WUSD", "WGRT", wgrt_amount, wgrt_price]
        status_code, response = tran.submitdexselllimitordertx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

        sleep(10)
        status_code, response = account.getaccountinfo([addr])
        for r in response:
            assert r['result'] is not None
            assert r['result']['tokens']['WGRT']['frozen_amount'] == wgrt_amount

    @allure.feature('Test submitdexsellmarketordertx')
    def test_submitdexsellmarketordertx_WICC(self):
        """
        @author:elaine.tan
        test submitdexsellmarketordertx when normal sell WICC
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getnewaddresswithWICC(self.init, 100000000)
        logger.info(addr)

        # sell WICC
        assert_amount = 20000000
        params = [addr, "WUSD", "WICC", assert_amount]
        status_code, response = tran.submitdexsellmarketordertx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

        sleep(10)
        status_code, response = account.getaccountinfo([addr])
        for r in response:
            assert r['result'] is not None
            assert r['result']['tokens']['WICC']['frozen_amount'] == assert_amount

    @allure.feature('Test submitdexsellmarketordertx')
    def test_submitdexsellmarketordertx_WGRT(self):
        """
        @author:elaine.tan
        test submitdexsellmarketordertx when normal sell WGRT
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        addr = account.getnewaddresswithWICC(self.init, 100000000)
        params = [self.init, addr, "WGRT:80000000", self.sendfee]
        status_code, response = tran.send(params)
        assert status_code == 200
        sleep(10)

        # sell WGRT
        assert_amount = 30000000
        params = [addr, "WUSD", "WGRT", assert_amount]
        status_code, response = tran.submitdexsellmarketordertx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

        sleep(10)
        status_code, response = account.getaccountinfo([addr])
        for r in response:
            assert r['result'] is not None
            assert r['result']['tokens']['WGRT']['frozen_amount'] == assert_amount

    @allure.feature('Test submitdexsettletx')
    def test_submitdexsettletx_limit_WICC(self):
        """
        @author:elaine.tan
        test submitdexsettletx when limit sell and buy WICC
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # get sell address with WICC
        sellAddr = account.getnewaddresswithWICC(self.init, 50000000)
        assert sellAddr is not None
        # get buy address with WUSD
        buyAddr = account.getNewAddrWithEnoughWUSD(self.init, 10000000000)
        assert buyAddr is not None
        res = account.send([self.init, buyAddr, 20000000, self.sendfee])
        assert res
        res = account.send([self.init, self.match, 100000000, self.sendfee])
        assert res
        sleep(20)

        coin_price = 100000000
        assert_amount = 40000000
        # sell
        params = [sellAddr, "WUSD", "WICC", assert_amount, coin_price]
        status_code, response = tran.submitdexselllimitordertx(params)
        selltxid = ""
        assert status_code == 200
        for r in response:
            assert r['result']['txid'] is not None
            selltxid = r['result']['txid']
        # buy
        params = [buyAddr, "WUSD", "WICC", assert_amount, coin_price]
        status_code, response = tran.submitdexbuylimitordertx(params)
        assert status_code == 200
        buytxid = ""
        for r in response:
            assert r['result']['txid'] is not None
            buytxid = r['result']['txid']

        sleep(20)
        amount = int(assert_amount * (coin_price / 100000000))
        tx = {"buy_order_id": buytxid,
              "sell_order_id": selltxid,
              "deal_price": coin_price,
              "deal_coin_amount": amount,
              "deal_asset_amount": assert_amount}
        status_code, response = tran.submitdexsettletx([self.match, [tx]])
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

        sleep(10)
        print(account.getaccountinfo([buyAddr]))
        print(account.getaccountinfo([sellAddr]))

    @allure.feature('Test submitdexsettletx')
    def test_submitdexsettletx_limit_differprices(self):
        """
        @author:elaine.tan
        test submitdexsettletx when limit:
        buy price>sell price
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # get sell address with WICC
        sellAddr = account.getnewaddresswithWICC(self.init, 50000000)
        assert sellAddr is not None
        # get buy address with WUSD
        buyAddr = account.getNewAddrWithEnoughWUSD(self.init, 10000000000)
        assert buyAddr is not None
        res = account.send([buyAddr, self.match, "WUSD:"+str(self.dexfee)+":sawi", self.sendfee])
        assert res
        sleep(10)
        # account
        sellfree_sta_wicc = account.gettokenbyaddr(sellAddr, "WICC", "free_amount")
        buyfree_sta_wicc = account.gettokenbyaddr(buyAddr, "WICC", "free_amount")
        buyfree_sta_wusd = account.gettokenbyaddr(buyAddr, "WUSD", "free_amount")

        # coin_price = 10000
        assert_amount = 40000000
        sell_pri = 300000000
        buy_pri = sell_pri * 2
        fee = self.dexfee
        # sell
        params = [sellAddr, "WUSD", "WICC", assert_amount, sell_pri]
        status_code, response = tran.submitdexselllimitordertx(params)
        selltxid = ""
        assert status_code == 200
        for r in response:
            assert r['result']['txid'] is not None
            selltxid = r['result']['txid']
        sleep(6)
        # frozen_wicc == assert_amount
        sellfree_be_wicc = account.gettokenbyaddr(sellAddr, "WICC", "free_amount")
        sellfrozen_be_wicc = account.gettokenbyaddr(sellAddr, "WICC", "frozen_amount")
        assert assert_amount == sellfrozen_be_wicc
        assert sellfree_be_wicc == sellfree_sta_wicc - sellfrozen_be_wicc - fee

        # buy
        params = [buyAddr, "WUSD", "WICC", assert_amount, buy_pri]
        status_code, response = tran.submitdexbuylimitordertx(params)
        assert status_code == 200
        buytxid = ""
        for r in response:
            assert r['result']['txid'] is not None
            buytxid = r['result']['txid']
        sleep(6)
        # frozen_wusd == assert_amount*price
        buyfree_be_wusd = account.gettokenbyaddr(buyAddr, "WUSD", "free_amount")
        buyfrozen_be_wusd = account.gettokenbyaddr(buyAddr, "WUSD", "frozen_amount")
        assert buyfrozen_be_wusd == assert_amount * (buy_pri / 100000000)
        assert buyfree_be_wusd == buyfree_sta_wusd - buyfrozen_be_wusd

        # match
        match_pri = sell_pri
        amount = int(assert_amount * (match_pri / 100000000))
        tx = {"buy_order_id": buytxid,
              "sell_order_id": selltxid,
              "deal_price": match_pri,
              "deal_coin_amount": amount,
              "deal_asset_amount": assert_amount}
        print("match params:", tx)
        status_code, response = tran.submitdexsettletx([self.match, [tx], "WUSD:"+str(self.dexfee)])
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

        sleep(10)
        # assert sell amount
        sellfree_af_wusd = account.gettokenbyaddr(sellAddr, "WUSD", "free_amount")
        sellfrozen_af_wicc = account.gettokenbyaddr(sellAddr, "WICC", "frozen_amount")
        assert sellfree_af_wusd == int(assert_amount * match_pri / 100000000 * (1 - 4 / 10000))
        assert sellfree_be_wicc == sellfree_sta_wicc - assert_amount - fee
        assert sellfrozen_af_wicc == 0

        # assert buy amount
        buyfree_af_wicc = account.gettokenbyaddr(buyAddr, "WICC", "free_amount")
        buyfree_af_wusd = account.gettokenbyaddr(buyAddr, "WUSD", "free_amount")
        buyfrozen_af_wusd = account.gettokenbyaddr(buyAddr, "WUSD", "frozen_amount")
        assert buyfrozen_af_wusd == 0
        assert buyfree_af_wicc == buyfree_sta_wicc + int(assert_amount * (1 - 4 / 10000)) - fee
        assert buyfree_af_wusd == buyfree_sta_wusd - (assert_amount * match_pri / 100000000) - buyfrozen_af_wusd

    @allure.feature('Test submitdexsettletx')
    def test_submitdexsettletx_limit_differprices2(self):
        """
        @author:elaine.tan
        test submitdexsettletx when limit:
        buy price<sell price
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # get sell address with WICC
        sellAddr = account.getnewaddresswithWICC(self.init, 50000000)
        assert sellAddr is not None
        # get buy address with WUSD
        buyAddr = account.getNewAddrWithEnoughWUSD(self.init, 10000000000)
        assert buyAddr is not None
        sleep(10)

        # coin_price = 10000
        assert_amount = 40000000
        sell_pri = 200000000
        buy_pri = sell_pri / 2
        fee = self.dexfee
        # sell
        params = [sellAddr, "WUSD", "WICC", assert_amount, sell_pri]
        status_code, response = tran.submitdexselllimitordertx(params)
        selltxid = ""
        assert status_code == 200
        for r in response:
            assert r['result']['txid'] is not None
            selltxid = r['result']['txid']
        sleep(6)

        # buy
        params = [buyAddr, "WUSD", "WICC", assert_amount, buy_pri]
        status_code, response = tran.submitdexbuylimitordertx(params)
        assert status_code == 200
        buytxid = ""
        for r in response:
            assert r['result']['txid'] is not None
            buytxid = r['result']['txid']
        sleep(6)

        # match
        match_pri = sell_pri
        amount = int(assert_amount * (match_pri / 100000000))
        tx = {"buy_order_id": buytxid,
              "sell_order_id": selltxid,
              "deal_price": match_pri,
              "deal_coin_amount": amount,
              "deal_asset_amount": assert_amount}
        status_code, response = tran.submitdexsettletx([self.match, [tx]])
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "deal-price-unmatched" in r['error']['message']

    @allure.feature('Test submitdexsettletx')
    def test_submitdexsettletx_limit_differamount(self):
        """
        @author:elaine.tan
        test submitdexsettletx when limit:
        buy amount>sell amount
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # get sell address with WICC
        sellAddr = account.getnewaddresswithWICC(self.init, 50000000)
        assert sellAddr is not None
        # get buy address with WUSD
        buyAddr = account.getNewAddrWithEnoughWUSD(self.init, 10000000000)
        assert buyAddr is not None
        sleep(10)
        # account
        sellfree_sta_wicc = account.gettokenbyaddr(sellAddr, "WICC", "free_amount")
        buyfree_sta_wicc = account.gettokenbyaddr(buyAddr, "WICC", "free_amount")
        buyfree_sta_wusd = account.gettokenbyaddr(buyAddr, "WUSD", "free_amount")

        sell_amount = 40000000
        buy_amount = sell_amount * 2
        buy_pri = 200000000
        sell_pri = 200000000
        fee = self.dexfee
        # sell
        params = [sellAddr, "WUSD", "WICC", sell_amount, sell_pri]
        status_code, response = tran.submitdexselllimitordertx(params)
        selltxid = ""
        assert status_code == 200
        for r in response:
            assert r['result']['txid'] is not None
            selltxid = r['result']['txid']
        sleep(6)
        # frozen_wicc == assert_amount
        sellfree_be_wicc = account.gettokenbyaddr(sellAddr, "WICC", "free_amount")
        sellfrozen_be_wicc = account.gettokenbyaddr(sellAddr, "WICC", "frozen_amount")
        assert sell_amount == sellfrozen_be_wicc
        assert sellfree_be_wicc == sellfree_sta_wicc - sellfrozen_be_wicc - fee

        # buy
        params = [buyAddr, "WUSD", "WICC", buy_amount, buy_pri]
        status_code, response = tran.submitdexbuylimitordertx(params)
        assert status_code == 200
        buytxid = ""
        for r in response:
            assert r['result']['txid'] is not None
            buytxid = r['result']['txid']
        sleep(6)
        # frozen_wusd == assert_amount*price
        buyfree_be_wusd = account.gettokenbyaddr(buyAddr, "WUSD", "free_amount")
        buyfrozen_be_wusd = account.gettokenbyaddr(buyAddr, "WUSD", "frozen_amount")
        assert buyfrozen_be_wusd == buy_amount * (buy_pri / 100000000)
        assert buyfree_be_wusd == buyfree_sta_wusd - buyfrozen_be_wusd

        # match
        # buyamount>sellamount
        match_amount = sell_amount
        match_pri = sell_pri
        amount = int(match_amount * (match_pri / 100000000))
        tx = {"buy_order_id": buytxid,
              "sell_order_id": selltxid,
              "deal_price": match_pri,
              "deal_coin_amount": amount,
              "deal_asset_amount": match_amount}
        print("match params:", tx)
        status_code, response = tran.submitdexsettletx([self.match, [tx]])
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None
        # 撮合系统手续费
        match_fee = 4 / 10000
        sleep(10)
        # assert sell amount
        sellfree_af_wusd = account.gettokenbyaddr(sellAddr, "WUSD", "free_amount")
        sellfrozen_af_wicc = account.gettokenbyaddr(sellAddr, "WICC", "frozen_amount")
        assert sellfrozen_af_wicc == 0
        assert sellfree_af_wusd == int(amount * (1 - match_fee))
        # assert sellfree_af_wusd == amount*match_pri
        assert sellfree_be_wicc == sellfree_sta_wicc - match_amount - fee

        # assert buy amount
        buyfree_af_wicc = account.gettokenbyaddr(buyAddr, "WICC", "free_amount")
        buyfree_af_wusd = account.gettokenbyaddr(buyAddr, "WUSD", "free_amount")
        buyfrozen_af_wusd = account.gettokenbyaddr(buyAddr, "WUSD", "frozen_amount")
        assert buyfrozen_af_wusd == (buy_amount - match_amount) * match_pri / 100000000
        assert buyfree_af_wicc == buyfree_sta_wicc + int(match_amount * (1 - match_fee)) - fee
        assert buyfree_af_wusd == buyfree_sta_wusd - (match_amount * match_pri / 100000000) - buyfrozen_af_wusd

    @allure.feature('Test submitdexsettletx')
    def test_submitdexsettletx_limit_differamount2(self):
        """
        @author:elaine.tan
        test submitdexsettletx when limit:
        buy amount < sell amount
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # get sell address with WICC
        sellAddr = account.getnewaddresswithWICC(self.init, 100000000)
        assert sellAddr is not None
        # get buy address with WUSD
        buyAddr = account.getNewAddrWithEnoughWUSD(self.init, 10000000000)
        assert buyAddr is not None
        sleep(10)
        # account
        sellfree_sta_wicc = account.gettokenbyaddr(sellAddr, "WICC", "free_amount")
        buyfree_sta_wicc = account.gettokenbyaddr(buyAddr, "WICC", "free_amount")
        buyfree_sta_wusd = account.gettokenbyaddr(buyAddr, "WUSD", "free_amount")

        buy_amount = 40000000
        sell_amount = buy_amount * 2
        buy_pri = sell_pri = 200000000
        # sell_pri = 200000000
        fee = self.dexfee
        # sell
        params = [sellAddr, "WUSD", "WICC", sell_amount, sell_pri]
        status_code, response = tran.submitdexselllimitordertx(params)
        selltxid = ""
        assert status_code == 200
        for r in response:
            assert r['result']['txid'] is not None
            selltxid = r['result']['txid']
        sleep(6)
        # frozen_wicc == assert_amount
        sellfree_be_wicc = account.gettokenbyaddr(sellAddr, "WICC", "free_amount")
        sellfrozen_be_wicc = account.gettokenbyaddr(sellAddr, "WICC", "frozen_amount")
        assert sell_amount == sellfrozen_be_wicc
        assert sellfree_be_wicc == sellfree_sta_wicc - sellfrozen_be_wicc - fee

        # buy
        params = [buyAddr, "WUSD", "WICC", buy_amount, buy_pri]
        status_code, response = tran.submitdexbuylimitordertx(params)
        assert status_code == 200
        buytxid = ""
        for r in response:
            assert r['result']['txid'] is not None
            buytxid = r['result']['txid']
        sleep(6)
        buyfree_be_wusd = account.gettokenbyaddr(buyAddr, "WUSD", "free_amount")
        buyfrozen_be_wusd = account.gettokenbyaddr(buyAddr, "WUSD", "frozen_amount")
        assert buyfrozen_be_wusd == buy_amount * (buy_pri / 100000000)
        assert buyfree_be_wusd == buyfree_sta_wusd - buyfrozen_be_wusd

        # match
        # buyamount<sellamount
        match_amount = buy_amount
        match_pri = sell_pri
        amount = int(match_amount * (match_pri / 100000000))
        tx = {"buy_order_id": buytxid,
              "sell_order_id": selltxid,
              "deal_price": match_pri,
              "deal_coin_amount": amount,
              "deal_asset_amount": match_amount}
        print("match params:", tx)
        status_code, response = tran.submitdexsettletx([self.match, [tx]])
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

        sleep(10)
        # assert sell amount
        sellfree_af_wusd = account.gettokenbyaddr(sellAddr, "WUSD", "free_amount")
        sellfrozen_af_wicc = account.gettokenbyaddr(sellAddr, "WICC", "frozen_amount")
        assert sellfrozen_af_wicc == sell_amount - match_amount
        assert sellfree_af_wusd == int(match_amount * match_pri / 100000000 * (1 - 4 / 10000))
        assert sellfree_be_wicc == sellfree_sta_wicc - match_amount - fee - sellfrozen_af_wicc

        # assert buy amount
        buyfree_af_wicc = account.gettokenbyaddr(buyAddr, "WICC", "free_amount")
        buyfree_af_wusd = account.gettokenbyaddr(buyAddr, "WUSD", "free_amount")
        buyfrozen_af_wusd = account.gettokenbyaddr(buyAddr, "WUSD", "frozen_amount")
        assert buyfrozen_af_wusd == 0
        assert buyfree_af_wicc == buyfree_sta_wicc + int(match_amount * (1 - 4 / 10000)) - fee
        assert buyfree_af_wusd == buyfree_sta_wusd - (match_amount * match_pri / 100000000) - buyfrozen_af_wusd

    @allure.feature('Test submitdexsettletx')
    def test_submitdexsettletx_limit_differamount3(self):
        """
        @author:elaine.tan
        test submitdexsettletx when limit:
        buy amount > sell amount,use buy amount
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # get sell address with WICC
        sellAddr = account.getnewaddresswithWICC(self.init, 500000000)
        assert sellAddr is not None
        # get buy address with WUSD
        buyAddr = account.getNewAddrWithEnoughWUSD(self.init, 10000000000)
        assert buyAddr is not None
        sleep(10)
        # account
        sellfree_sta_wicc = account.gettokenbyaddr(sellAddr, "WICC", "free_amount")
        buyfree_sta_wicc = account.gettokenbyaddr(buyAddr, "WICC", "free_amount")
        buyfree_sta_wusd = account.gettokenbyaddr(buyAddr, "WUSD", "free_amount")

        sell_amount = 400000000
        buy_amount = sell_amount * 2
        buy_pri = 200000000
        sell_pri = 200000000
        fee = self.dexfee
        # sell
        params = [sellAddr, "WUSD", "WICC", sell_amount, sell_pri]
        status_code, response = tran.submitdexselllimitordertx(params)
        selltxid = ""
        assert status_code == 200
        for r in response:
            assert r['result']['txid'] is not None
            selltxid = r['result']['txid']
        sleep(6)
        # frozen_wicc == assert_amount
        sellfree_be_wicc = account.gettokenbyaddr(sellAddr, "WICC", "free_amount")
        sellfrozen_be_wicc = account.gettokenbyaddr(sellAddr, "WICC", "frozen_amount")
        assert sell_amount == sellfrozen_be_wicc
        assert sellfree_be_wicc == sellfree_sta_wicc - sellfrozen_be_wicc - fee

        # buy
        params = [buyAddr, "WUSD", "WICC", buy_amount, buy_pri]
        status_code, response = tran.submitdexbuylimitordertx(params)
        assert status_code == 200
        buytxid = ""
        for r in response:
            assert r['result']['txid'] is not None
            buytxid = r['result']['txid']
        sleep(6)
        buyfree_be_wusd = account.gettokenbyaddr(buyAddr, "WUSD", "free_amount")
        buyfrozen_be_wusd = account.gettokenbyaddr(buyAddr, "WUSD", "frozen_amount")
        assert buyfrozen_be_wusd == buy_amount * (buy_pri / 100000000)
        assert buyfree_be_wusd == buyfree_sta_wusd - buyfrozen_be_wusd

        # match
        # buyamount<sellamount
        match_amount = buy_amount
        match_pri = sell_pri
        amount = int(match_amount * (match_pri / 100000000))
        tx = {"buy_order_id": buytxid,
              "sell_order_id": selltxid,
              "deal_price": match_pri,
              "deal_coin_amount": amount,
              "deal_asset_amount": match_amount}
        status_code, response = tran.submitdexsettletx([self.match, [tx]])
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "sell-deal-amount-exceeded" in r['error']['message']

    @allure.feature('Test submitdexsettletx')
    @pytest.mark.parametrize('test_param', matchaddr, indirect=True)
    def test_submitdexsettletx_limit_matchaddr(self, test_param):
        """
        @author:elaine.tan
        test submitdexsettletx when limit:
        address is None,invalid,0,<0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # get sell address with WICC
        sellAddr = account.getnewaddresswithWICC(self.init, 1000000000)
        assert sellAddr is not None
        # get buy address with WUSD
        buyAddr = account.getNewAddrWithEnoughWUSD(self.init, 10000000000)
        assert buyAddr is not None
        sleep(10)

        # coin_price = 10000
        assert_amount = 400000000
        sell_pri = 200000000
        buy_pri = sell_pri / 2
        fee = self.dexfee
        # sell
        params = [sellAddr, "WUSD", "WICC", assert_amount, sell_pri]
        status_code, response = tran.submitdexselllimitordertx(params)
        selltxid = ""
        assert status_code == 200
        for r in response:
            assert r['result']['txid'] is not None
            selltxid = r['result']['txid']
        sleep(6)

        # buy
        params = [buyAddr, "WUSD", "WICC", assert_amount, buy_pri]
        status_code, response = tran.submitdexbuylimitordertx(params)
        assert status_code == 200
        buytxid = ""
        for r in response:
            assert r['result']['txid'] is not None
            buytxid = r['result']['txid']
        sleep(6)

        # match
        match_pri = sell_pri
        amount = int(assert_amount * (match_pri / 100000000))
        tx = {"buy_order_id": buytxid,
              "sell_order_id": selltxid,
              "deal_price": match_pri,
              "deal_coin_amount": amount,
              "deal_asset_amount": assert_amount}
        status_code, response = tran.submitdexsettletx([test_param, [tx]])
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            print(r['error']['message'])

    @allure.feature('Test submitdexsettletx')
    def test_submitdexsettletx_limit_matchaddr2(self):
        """
        @author:elaine.tan
        test submitdexsettletx when limit:
        address is not registered,sell address,buy address,normal address
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # get sell address with WICC
        sellAddr = account.getnewaddresswithWICC(self.init, 50000000)
        assert sellAddr is not None
        # get buy address with WUSD
        buyAddr = account.getNewAddrWithEnoughWUSD(self.init, 10000000000)
        assert buyAddr is not None
        sleep(10)

        assert_amount = 20000000
        match_pri = buy_pri = sell_pri = 100000000
        fee = self.dexfee
        # sell
        params = [sellAddr, "WUSD", "WICC", assert_amount, sell_pri]
        status_code, response = tran.submitdexselllimitordertx(params)
        selltxid = ""
        assert status_code == 200
        for r in response:
            assert r['result']['txid'] is not None
            selltxid = r['result']['txid']
        sleep(6)

        # buy
        params = [buyAddr, "WUSD", "WICC", assert_amount, buy_pri]
        status_code, response = tran.submitdexbuylimitordertx(params)
        assert status_code == 200
        buytxid = ""
        for r in response:
            assert r['result']['txid'] is not None
            buytxid = r['result']['txid']
        sleep(6)

        # match addr not registered
        match_addr = account.newaddress()
        amount = int(assert_amount * (match_pri / 100000000))
        tx = {"buy_order_id": buytxid,
              "sell_order_id": selltxid,
              "deal_price": match_pri,
              "deal_coin_amount": amount,
              "deal_asset_amount": assert_amount}
        status_code, response = tran.submitdexsettletx([match_addr, [tx]])
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "account not exists" in r['error']['message']

        # match addr is selladdress
        match_addr = sellAddr
        amount = int(assert_amount * (match_pri / 100000000))
        tx = {"buy_order_id": buytxid,
              "sell_order_id": selltxid,
              "deal_price": match_pri,
              "deal_coin_amount": amount,
              "deal_asset_amount": assert_amount}
        status_code, response = tran.submitdexsettletx([match_addr, [tx]])
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "txUid-type-error" in r['error']['message']

        # match addr is buyaddress
        match_addr = buyAddr
        amount = int(assert_amount * (match_pri / 100000000))
        tx = {"buy_order_id": buytxid,
              "sell_order_id": selltxid,
              "deal_price": match_pri,
              "deal_coin_amount": amount,
              "deal_asset_amount": assert_amount}
        status_code, response = tran.submitdexsettletx([match_addr, [tx]])
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "txUid-type-error" in r['error']['message']

        # match addr is normaladdress
        match_addr = account.getnewaddresswithWICC(self.init, self.dexfee)
        amount = int(assert_amount * (match_pri / 100000000))
        tx = {"buy_order_id": buytxid,
              "sell_order_id": selltxid,
              "deal_price": match_pri,
              "deal_coin_amount": amount,
              "deal_asset_amount": assert_amount}
        status_code, response = tran.submitdexsettletx([match_addr, [tx]])
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "txUid-type-error" in r['error']['message']

    @allure.feature('Test submitdexsettletx')
    def test_submitdexsettletx_limit_WGRT(self):
        """
        @author:elaine.tan
        test submitdexsettletx when limit sell and buy WICC
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # get sell address with WGRT
        sellAddr = account.getnewaddresswithWICC(self.init, 200000000)
        assert sellAddr is not None
        params = [self.init, sellAddr, "WGRT:80000000", self.sendfee]
        res = account.send(params)
        assert res

        # get buy address with WUSD
        buyAddr = account.getNewAddrWithEnoughWUSD(self.init, 100000000)
        assert buyAddr is not None
        res = account.send([self.init, buyAddr, 100000000, self.sendfee])
        assert res
        sleep(20)

        price = 200000000
        assert_amount = 50000000
        # sell
        params = [sellAddr, "WUSD", "WGRT", assert_amount, price]
        status_code, response = tran.submitdexselllimitordertx(params)
        print(1111, response)
        selltxid = ""
        assert status_code == 200
        for r in response:
            assert r['result']['txid'] is not None
            selltxid = r['result']['txid']
        # buy
        params = [buyAddr, "WUSD", "WGRT", assert_amount, price]
        status_code, response = tran.submitdexbuylimitordertx(params)
        print(2222, response)
        assert status_code == 200
        buytxid = ""
        for r in response:
            assert r['result']['txid'] is not None
            buytxid = r['result']['txid']

        sleep(20)
        tx = {"buy_order_id": buytxid,
              "sell_order_id": selltxid,
              "deal_price": price,
              "deal_coin_amount": int(assert_amount * price / 100000000),
              "deal_asset_amount": assert_amount}
        params = [self.match, [tx]]
        print(params)
        status_code, response = tran.submitdexsettletx(params)
        assert status_code == 200
        print(response)
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

        sleep(10)
        print(account.getaccountinfo([buyAddr]))
        print(account.getaccountinfo([sellAddr]))

    @allure.feature('Test submitdexsettletx')
    def test_submitdexsettletx_market_WICC(self):
        """
        @author:elaine.tan
        test submitdexsettletx when market sell and buy WICC
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # get sell address with WICC
        sellAddr = account.getnewaddresswithWICC(self.init, 500000000)
        assert sellAddr is not None
        # get buy address with WUSD
        buyAddr = account.getNewAddrWithEnoughWUSD(self.init, 1000000000)
        assert buyAddr is not None
        res = account.send([self.init, buyAddr, 50000000, self.sendfee])
        assert res
        sleep(20)

        price = int(account.getprice("WICC"))
        assert price is not None
        assert_amount = 300000000
        # sell
        params = [sellAddr, "WUSD", "WICC", assert_amount]
        status_code, response = tran.submitdexsellmarketordertx(params)
        selltxid = ""
        assert status_code == 200
        for r in response:
            assert r['result']['txid'] is not None
            selltxid = r['result']['txid']
            print("sell txid:", selltxid)

        # buy
        params = [buyAddr, "WUSD", assert_amount * price, "WICC"]
        status_code, response = tran.submitdexbuymarketordertx(params)
        assert status_code == 200
        buytxid = ""
        for r in response:
            assert r['result']['txid'] is not None
            buytxid = r['result']['txid']
            print("buy txid:", buytxid)

        sleep(10)
        tx = {"buy_order_id": buytxid,
              "sell_order_id": selltxid,
              "deal_price": price * 100000000,
              "deal_coin_amount": int(assert_amount * price),
              "deal_asset_amount": assert_amount}
        params = [self.match, [tx]]
        print("settle params:", params)
        status_code, response = tran.submitdexsettletx(params)
        assert status_code == 200
        for r in response:
            print("settlt res:", response)
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

        sleep(10)
        print(account.getaccountinfo([buyAddr]))
        print(account.getaccountinfo([sellAddr]))

    @allure.feature('Test submitdexsettletx')
    def test_submitdexsettletx_market_WGRT(self):
        """
        @author:elaine.tan
        test submitdexsettletx when market sell and buy WGRT
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # get sell address with WGRT
        sellAddr = account.getnewaddresswithWICC(self.init, 200000000)
        assert sellAddr is not None
        params = [self.init, sellAddr, "WGRT:800000000", self.sendfee]
        status_code, response = tran.send(params)
        assert status_code == 200

        # get buy address with WUSD
        buyAddr = account.getNewAddrWithEnoughWUSD(self.init, 1000000000)
        assert buyAddr is not None
        sleep(20)

        price = account.getprice("WGRT")
        assert_amount = 400000000
        # sell
        params = [sellAddr, "WUSD", "WGRT", assert_amount]
        status_code, response = tran.submitdexsellmarketordertx(params)
        selltxid = ""
        assert status_code == 200
        for r in response:
            assert r['result']['txid'] is not None
            selltxid = r['result']['txid']
        # buy
        params = [buyAddr, "WUSD", int(assert_amount * price), "WGRT", "WUSD:"+str(self.dexfee)]
        status_code, response = tran.submitdexbuymarketordertx(params)
        assert status_code == 200
        buytxid = ""
        for r in response:
            assert r['result']['txid'] is not None
            buytxid = r['result']['txid']

        sleep(10)
        tx = {"buy_order_id": buytxid,
              "sell_order_id": selltxid,
              "deal_price": price * 100000000,
              "deal_coin_amount": int(assert_amount * price),
              "deal_asset_amount": assert_amount}
        params = [self.match, [tx]]
        status_code, response = tran.submitdexsettletx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

        sleep(10)
        print(account.getaccountinfo([buyAddr]))
        print(account.getaccountinfo([sellAddr]))

    @allure.feature('Test submitdexsettletx')
    def test_submitdexsettletx_mix_all2(self):
        """
        @author:elaine.tan
        test submitdexsettletx when market sell and limit buy
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # get sell address with WGRT
        sellAddr = account.getnewaddresswithWICC(self.init, 200000000)
        assert sellAddr is not None
        params = [self.init, sellAddr, "WGRT:800000000", self.sendfee]
        res = account.send(params)
        assert res

        # get buy address with WUSD
        buyAddr = account.getNewAddrWithEnoughWUSD(self.init, 10000000000)
        assert buyAddr is not None
        sleep(20)

        price = account.getprice("WGRT")
        assert_amount = 40000000
        # sell
        params = [sellAddr, "WUSD", "WGRT", assert_amount]
        status_code, response = tran.submitdexsellmarketordertx(params)
        selltxid = ""
        assert status_code == 200
        for r in response:
            print("sell:", response)
            assert r['result']['txid'] is not None
            selltxid = r['result']['txid']

        limit_pri = 300000000
        params = [buyAddr, "WUSD", "WGRT", assert_amount, limit_pri, "WUSD:"+str(self.dexfee)+":sawi"]
        status_code, response = tran.submitdexbuylimitordertx(params)
        assert status_code == 200
        buytxid = ""
        for r in response:
            print("buy:", response)
            assert r['result']['txid'] is not None
            buytxid = r['result']['txid']

        sleep(10)
        tx = {"buy_order_id": buytxid,
              "sell_order_id": selltxid,
              "deal_price": limit_pri,
              "deal_coin_amount": int(assert_amount * limit_pri / 100000000),
              "deal_asset_amount": assert_amount}
        params = [self.match, [tx]]
        print(params)
        status_code, response = tran.submitdexsettletx(params)
        assert status_code == 200
        for r in response:
            print(response)
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitdexsettletx')
    def test_submitdexsettletx_mix_part(self):
        """
        @author:elaine.tan
        test submitdexsettletx when market sell and limit buy
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # get sell address with WGRT
        sellAddr = account.getnewaddresswithWICC(self.init, 10000000000)
        assert sellAddr is not None

        # get buy address with WUSD
        buyAddr = account.getNewAddrWithEnoughWUSD(self.init, 10000000000)
        assert buyAddr is not None
        sleep(20)
        # limit buy 10000000000,price 10000,market sell 30000
        buy_amount = 300000000
        # sell
        params = [sellAddr, "WUSD", "WICC", buy_amount]
        status_code, response = tran.submitdexsellmarketordertx(params)
        selltxid = ""
        assert status_code == 200
        for r in response:
            print("sell:", response)
            assert r['result']['txid'] is not None
            selltxid = r['result']['txid']

        limit_pri = 100000000
        sell_amount = 10000000000
        params = [buyAddr, "WUSD", "WICC", sell_amount, limit_pri]
        status_code, response = tran.submitdexbuylimitordertx(params)
        assert status_code == 200
        buytxid = ""
        for r in response:
            print("buy:", response)
            assert r['result']['txid'] is not None
            buytxid = r['result']['txid']

        sleep(10)
        tx = {"buy_order_id": buytxid,
              "sell_order_id": selltxid,
              "deal_price": limit_pri,
              "deal_coin_amount": int(buy_amount * limit_pri / 100000000),
              "deal_asset_amount": buy_amount}
        params = [self.match, [tx]]
        print(params)
        status_code, response = tran.submitdexsettletx(params)
        assert status_code == 200
        for r in response:
            print(response)
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitdexsettletx')
    def test_submitdexsettletx_mix_all(self):
        """
        @author:elaine.tan
        test submitdexsettletx when market buy and limit sell
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # get sell address with wicc
        sellAddr = account.getnewaddresswithWICC(self.init, 50000000)
        assert sellAddr is not None

        # get buy address with WUSD
        buyAddr = account.getNewAddrWithEnoughWUSD(self.init, 1000000000)
        assert buyAddr is not None
        sleep(20)

        assert_amount = 40000000
        limit_pri = 400000000
        # sell WICC
        params = [sellAddr, "WUSD", "WICC", assert_amount, limit_pri]
        status_code, response = tran.submitdexselllimitordertx(params)
        assert status_code == 200
        selltxid = ""
        for r in response:
            assert r['result']['txid'] is not None
            selltxid = r['result']['txid']

        # market buy WICC
        coin_amount = int(assert_amount * limit_pri / 100000000)
        params = [buyAddr, "WUSD", coin_amount, "WICC"]
        status_code, response = tran.submitdexbuymarketordertx(params)
        assert status_code == 200
        buytxid = ""
        for r in response:
            assert r['result']['txid'] is not None
            buytxid = r['result']['txid']

        # match
        sleep(10)
        tx = {"buy_order_id": buytxid,
              "sell_order_id": selltxid,
              "deal_price": limit_pri,
              "deal_coin_amount": coin_amount,
              "deal_asset_amount": assert_amount}
        params = [self.match, [tx]]
        print(params)
        status_code, response = tran.submitdexsettletx(params)
        assert status_code == 200
        for r in response:
            print(response)
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitdexsettletx')
    def test_submitdexsettletx_mix_differassert(self):
        """
        @author:elaine.tan
        test submitdexsettletx when market buy WGRT and limit sell WICC
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # get sell address with wicc
        sellAddr = account.getnewaddresswithWICC(self.init, 500000000)
        assert sellAddr is not None

        # get buy address with WUSD
        buyAddr = account.getNewAddrWithEnoughWUSD(self.init, 1000000000)
        assert buyAddr is not None
        sleep(20)

        assert_amount = 40000000
        limit_pri = 400000000
        # sell WICC
        params = [sellAddr, "WUSD", "WICC", assert_amount, limit_pri]
        status_code, response = tran.submitdexselllimitordertx(params)
        assert status_code == 200
        selltxid = ""
        for r in response:
            assert r['result']['txid'] is not None
            selltxid = r['result']['txid']

        # market buy WGRT
        coin_amount = int(assert_amount * limit_pri / 100000000)
        params = [buyAddr, "WUSD", coin_amount, "WGRT"]
        status_code, response = tran.submitdexbuymarketordertx(params)
        assert status_code == 200
        buytxid = ""
        for r in response:
            assert r['result']['txid'] is not None
            buytxid = r['result']['txid']

        # match
        sleep(10)
        tx = {"buy_order_id": buytxid,
              "sell_order_id": selltxid,
              "deal_price": limit_pri,
              "deal_coin_amount": coin_amount,
              "deal_asset_amount": assert_amount}
        params = [self.match, [tx]]
        status_code, response = tran.submitdexsettletx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "bad-order-match" in r['error']['message']

    @allure.feature('Test getdexorders')
    def test_getdexorders_normal(self):
        """
        @author:elaine.tan
        test getdexorders when normal
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        status, response = tran.getdexorders([])
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None

    @allure.feature('Test getdexorders')
    def test_getdexorders_beginheight_none(self):
        """
        @author:elaine.tan
        test getdexorders when begin_height is None
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        block = Block(self.host, self.rpcuser, self.rpcpassword)

        bg_ht = None
        end_ht = block.getblockheight()
        status, response = tran.getdexorders([bg_ht, end_ht])
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type null, expected int" in r['error']['message']

    @allure.feature('Test getdexorders')
    def test_getdexorders_beginheight_neg(self):
        """
        @author:elaine.tan
        test getdexorders when begin_height < 0
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        block = Block(self.host, self.rpcuser, self.rpcpassword)

        bg_ht = -100
        end_ht = block.getblockheight()
        status, response = tran.getdexorders([bg_ht, end_ht])
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print(r['error'])

    @allure.feature('Test getdexorders')
    def test_getdexorders_beginheight_zero(self):
        """
        @author:elaine.tan
        test getdexorders when begin_height = 0
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        block = Block(self.host, self.rpcuser, self.rpcpassword)

        bg_ht = 0
        end_ht = block.getblockheight()
        status, response = tran.getdexorders([bg_ht, end_ht])
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None

    @allure.feature('Test getdexorders')
    def test_getdexorders_beginheight_over(self):
        """
        @author:elaine.tan
        test getdexorders when begin_height > current
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        block = Block(self.host, self.rpcuser, self.rpcpassword)

        end_ht = block.getblockheight()
        bg_ht = end_ht + 10
        status, response = tran.getdexorders([bg_ht, end_ht])
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print(r['error'])

    def test_getdexorders_beginheight_invalid(self):
        """
        @author:elaine.tan
        test getdexorders when begin_height is invalid
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        block = Block(self.host, self.rpcuser, self.rpcpassword)

        bg_ht = "invalid"
        end_ht = block.getblockheight()
        status, response = tran.getdexorders([bg_ht, end_ht])
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type str, expected int" in r['error']['message']

    def test_getdexorders_endheight_none(self):
        """
        @author:elaine.tan
        test getdexorders when end_height is none
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        block = Block(self.host, self.rpcuser, self.rpcpassword)

        bg_ht = 0
        end_ht = None
        status, response = tran.getdexorders([bg_ht, end_ht])
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type null, expected int" in r['error']['message']

    def test_getdexorders_endheight_neg(self):
        """
        @author:elaine.tan
        test getdexorders when end_height < 0
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        block = Block(self.host, self.rpcuser, self.rpcpassword)

        bg_ht = 0
        end_ht = -100
        status, response = tran.getdexorders([bg_ht, end_ht])
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print(r['error'])

    def test_getdexorders_endheight_zero(self):
        """
        @author:elaine.tan
        test getdexorders when end_height = 0
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        bg_ht = 0
        end_ht = 0
        status, response = tran.getdexorders([bg_ht, end_ht])
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None

    def test_getdexorders_endheight_invalid(self):
        """
        @author:elaine.tan
        test getdexorders when end_height is invalid
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        bg_ht = 0
        end_ht = "invalid"
        status, response = tran.getdexorders([bg_ht, end_ht])
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type str, expected int" in r['error']['message']

    def test_getdexorders_endheight_over(self):
        """
        @author:elaine.tan
        test getdexorders when end_height > current
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        block = Block(self.host, self.rpcuser, self.rpcpassword)

        bg_ht = 0
        end_ht = block.getblockheight() + 10
        status, response = tran.getdexorders([bg_ht, end_ht])
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print(r['error'])

    def test_getdexorders_count_none(self):
        """
        @author:elaine.tan
        test getdexorders when count is none
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        block = Block(self.host, self.rpcuser, self.rpcpassword)

        bg_ht = 0
        end_ht = block.getblockheight()
        count = None
        status, response = tran.getdexorders([bg_ht, end_ht, count])
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type null, expected int" in r['error']['message']

    def test_getdexorders_count_neg(self):
        """
        @author:elaine.tan
        test getdexorders when count < 0
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        block = Block(self.host, self.rpcuser, self.rpcpassword)

        bg_ht = 0
        end_ht = block.getblockheight()
        count = -100
        status, response = tran.getdexorders([bg_ht, end_ht, count])
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "must >= 0" in r['error']['message']

    def test_getdexorders_count_zero(self):
        """
        @author:elaine.tan
        test getdexorders when count = 0
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        block = Block(self.host, self.rpcuser, self.rpcpassword)

        bg_ht = 0
        end_ht = block.getblockheight()
        count = 0
        status, response = tran.getdexorders([bg_ht, end_ht, count])
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None

    def test_getdexorders_count_equal(self):
        """
        @author:elaine.tan
        test getdexorders when count = current count
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        block = Block(self.host, self.rpcuser, self.rpcpassword)

        count = 0
        status, response = tran.getdexorders([])
        for r in response:
            count = r['result']['count']

        bg_ht = 0
        end_ht = block.getblockheight()
        status, response = tran.getdexorders([bg_ht, end_ht, count])
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['count'] == count

    def test_getdexorders_count_small(self):
        """
        @author:elaine.tan
        test getdexorders when count < current count
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        block = Block(self.host, self.rpcuser, self.rpcpassword)

        count = 0
        status, response = tran.getdexorders([])
        for r in response:
            count = r['result']['count']
            print(count)

        bg_ht = 0
        end_ht = block.getblockheight()
        status, response = tran.getdexorders([bg_ht, end_ht, count - 10])
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['count'] == count - 10

    def test_getdexorders_count_over(self):
        """
        @author:elaine.tan
        test getdexorders when count > current count
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        block = Block(self.host, self.rpcuser, self.rpcpassword)

        count = 0
        status, response = tran.getdexorders([])
        for r in response:
            count = r['result']['count']

        bg_ht = 0
        end_ht = block.getblockheight()
        status, response = tran.getdexorders([bg_ht, end_ht, count + 100])
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None

    @allure.feature('Test getdexorders')
    def test_getdexsysorders_normal(self):
        """
        @author:elaine.tan
        test getdexsysorders when normal
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        status, response = tran.getdexsysorders([])
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None

    @allure.feature('Test getdexsysorders')
    def test_getdexsysorders_height_neg(self):
        """
        @author:elaine.tan
        test getdexsysorders when height < 0
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        status, response = tran.getdexsysorders([-100])
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print(r['error']['message'])

    @allure.feature('Test getdexsysorders')
    def test_getdexsysorders_height_zero(self):
        """
        @author:elaine.tan
        test getdexsysorders when height = 0
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        status, response = tran.getdexsysorders([0])
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None

    @allure.feature('Test getdexsysorders')
    def test_getdexsysorders_height_over(self):
        """
        @author:elaine.tan
        test getdexsysorders when height > current height
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        block = Block(self.host, self.rpcuser, self.rpcpassword)

        height = block.getblockheight()
        status, response = tran.getdexsysorders([height + 10])
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print(r['error']['message'])

    @allure.feature('Test getdexsysorders')
    def test_getdexsysorders_height_invalid(self):
        """
        @author:elaine.tan
        test getdexsysorders when height is invalid
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        height = "invalid"
        status, response = tran.getdexsysorders([height])
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type str, expected int" in r['error']['message']


if __name__ == '__main__':
    pytest.main(
        ['-s', 'dex_transaction_test.py::TestDEXTransaction::test_submitdexcancelordertx_fee_unnormal'])
