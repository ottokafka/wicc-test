import random
from time import sleep

import math
import pytest
import allure

from utils.read_env import ReadEnv
from utils.read_property import ReadProperty
from utils.log import mylog
from wiccsdk.account import Account
from wiccsdk.block import Block
from wiccsdk.contract import Contract
from wiccsdk.transaction import Transaction
from wiccsdk.wallet import Wallet

logger = mylog(__name__).getlog()

# asset testcases
invalid_address = [None, 0, -100, 1000, "invalid"]
invalid_symbol = [None, 0, -11111, 1111, "WICC", "WGRT", "WUSD", "ABCD", "ABCDEFGH", "AB11EF", "-AB@CD0", "不合法",
                  "1111111111111"]
invalid_owner = [None, 0, 100, -1000, "invalid"]
invalid_type = [None, 0, -1000, "123", "invalid"]
unnormal_fee = [None, 'invalid', 0, -1000, 999, 1000000000000, "WICC:-1000:sawi", "WICC:0:sawi", "WICC:999", "WUSD:-1000"]
invalid_name = [None, 0, -100]
invalid_fee = [None, 'invalid', 0, -100, 999, "WICC:-1000:sawi", "WICC:0:sawi", "WICC:999", "WUSD:-1000"]
normal_fee = [1000000, "WUSD:1000000:sawi", "WICC:1000000:sawi", "WICC:2000000:sawi", "WUSD:2000000:sawi"]
update = [("name","update"),("owner_addr","0-2"),("mint_amount","10000")]
invalid_value = [("name", None), ("name", 0), ("name", -100),
                 ("owner_addr", None), ("owner_addr", 0), ("owner_addr", -100),
                 ("owner_addr", "wNX1NpSJ46FCvMdzdgdN4hCFKWgjcEEDnq"),
                 ("mint_amount", None), ("mint_amount", 0), ("mint_amount", -100), ("mint_amount", "invalid")]


class TestOrdinaryTransaction(object):
    """
    软分叉后才支持的交易类型(普通交易)测试用例
    多币种质押、账户免激活、多币种转账、发布资产、资产更新、资产转账
    """

    @pytest.fixture(scope='class', autouse=True)
    def setup(self):
        prop = ReadProperty()
        # env = ReadEnv()
        # host = 'http://' + env.read('ip') + ":" + str(env.read('port'))
        # rpcuser = env.read('username')
        # rpcpassword = env.read('password')
        # walletpassword = prop.read('common', 'walletpassword')

        host = prop.read('common', 'host')
        rpcuser = prop.read('common', 'rpcuser')
        rpcpassword = prop.read('common', 'rpcpassword')
        walletpassword = prop.read('common', 'walletpassword')
        creaction = prop.read('common', 'creation_addr')
        init = prop.read('common', 'init_addr')

        wallet = Wallet(host, rpcuser, rpcpassword)
        account = Account(host, rpcuser, rpcpassword)
        isLocked = wallet.islocked()
        # 解锁钱包
        if isLocked:
            status, response = wallet.walletpassphrase([walletpassword, 99999999])
            assert status == 200
        else:
            pass

        assert not wallet.islocked()
        res = account.feedingPrice(init, "USD", 2, 2)
        assert res
        logger.info('class setup')

    @pytest.fixture(scope='class', autouse=True)
    def teardown(self, request):
        def fin():
            logger.info('class teardown')

        request.addfinalizer(fin)

    @pytest.fixture(scope='function', autouse=True)
    def setup_function(self):
        prop = ReadProperty()
        # env = ReadEnv()
        # self.host = 'http://' + env.read('ip') + ":" + str(env.read('port'))
        # self.rpcuser = env.read('username')
        # self.rpcpassword = env.read('password')
        self.host = prop.read('common', 'host')
        self.rpcuser = prop.read('common', 'rpcuser')
        self.rpcpassword = prop.read('common', 'rpcpassword')
        self.risk = prop.read('common', 'risk_addr')
        self.creaction = prop.read('common', 'creation_addr')
        self.contracturl = prop.read('common', 'contracturl')
        self.init = prop.read('common', 'init_addr')
        self.registerfee = int(prop.read('fee', 'register'))
        self.sendfee = int(prop.read('fee', 'send'))
        self.deployfee = int(prop.read('fee', 'contractdeploy'))
        self.callfee = int(prop.read('fee', 'contractcall'))
        self.stakefee = int(prop.read('fee', 'coinstake'))
        self.votefee = int(prop.read('fee', 'vote'))
        self.assetfee = int(prop.read('fee', 'asset'))
        self.assetcreate = int(prop.read('fee', 'asset_create'))
        self.assetupdate = int(prop.read('fee', 'asset_update'))
        self.dexfee = int(prop.read('fee', 'dex'))
        self.cdpfee = int(prop.read('fee', 'cdp'))

        logger.info('function setup')

    @pytest.fixture(scope='function', autouse=True)
    def teardown_function(self):
        logger.info('function teardown')

    @pytest.fixture(scope='function', autouse=False)
    def test_param(self, request):
        return request.param

    @allure.feature('Test send')
    def test_send_WUSD_normal(self):
        """
        @author:elaine.tan
        Test send WUSD when normal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # send WUSD
        sender = account.getNewAddrWithEnoughWUSD(self.init, 5000000000)
        assert sender is not None
        res = account.send([self.init, sender, "10000", self.sendfee])
        assert res
        sleep(6)
        reciever = account.newaddress()
        assert reciever is not None
        # fee is default
        params = [sender, reciever, "WUSD:10000:sawi", self.sendfee]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

    @allure.feature('Test send')
    def test_send_WUSD_fee_equal(self):
        """
        @author:elaine.tan
        Test send WUSD when fee = 10000
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # send WUSD
        sender = account.getNewAddrWithEnoughWUSD(self.init, 5000000000)
        assert sender is not None
        status_code, response = transaction.send([self.init, sender, "10000", self.sendfee])
        assert status_code == 200
        sleep(6)
        reciever = account.newaddress()
        assert reciever is not None
        # fee=10000
        params = [sender, reciever, "WUSD:1:miwi", "WUSD:100:miwi"]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

    @allure.feature('Test send')
    def test_send_WUSD_fee_large(self):
        """
        @author:elaine.tan
        Test send WUSD when fee > 10000
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # send WUSD
        sender = account.getNewAddrWithEnoughWUSD(self.init, 5000000000)
        assert sender is not None
        status_code, response = transaction.send([self.init, sender, "10000", self.sendfee])
        assert status_code == 200
        sleep(6)
        reciever = account.newaddress()
        assert reciever is not None
        # fee>10000
        params = [sender, reciever, "WUSD:1:lewi", "WUSD:200:lewi"]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

        # repeat send
        params = [sender, reciever, "WUSD:1:fewi", "WUSD:" + str(self.sendfee) + ":sawi"]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

    @allure.feature('Test send')
    def test_sendWUSD_address(self):
        """
        @author:elaine.tan
        Test send WUSD  when：
        sendaddress is None,not register,invalid,
        recieveaddress is None,not register,invalid,
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        fee = self.sendfee
        # send is None
        balance = "10000"
        sender = None
        reciever = account.getEnoughCoindRegisteredAddress(10)
        assert reciever is not None
        params = [sender, reciever, "WUSD:" + balance + ":sawi", fee]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "value is type null, expected str" in r['error']['message']

        # send is not register
        sender = account.newaddress()
        params = [sender, reciever, "WUSD:" + balance + ":sawi", fee]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "account not exists" in r['error']['message']

        # send is invalid
        sender = "invalid"
        params = [sender, reciever, "WUSD:" + balance + ":sawi", fee]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "Invalid address" in r['error']['message']

        # reciever is None
        sender = account.getEnoughCoindRegisteredAddress(10000)
        reciever = None
        assert sender is not None
        params = [sender, reciever, "WUSD:" + balance + ":sawi", fee]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "value is type null, expected str" in r['error']['message']

        # reciever is invalid
        reciever = "invalid"
        params = [sender, reciever, "WUSD:" + balance + ":sawi", fee]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "Invalid address" in r['error']['message']

    @allure.feature('Test send')
    def test_sendWUSD_unnormal(self):
        """
        @author:elaine.tan
        Test send WUSD with unnormal cases：
        amount is None,<0,=0,>balance
        fee is <0,<required,>required,>balance
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        sender = account.getNewAddrWithEnoughWUSD(self.init, 50000000)
        assert sender is not None
        reciever = account.newaddress()
        assert reciever is not None
        fee = self.sendfee
        # amount is None
        amount = None
        params = [sender, reciever, amount, fee]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            print(r['error']['message'])

        # amount=0
        amount = 0
        params = [sender, reciever, "WUSD:" + str(amount) + ":sawi", fee]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "Coins is zero" in r['error']['message']

        # amount<0
        amount = -10000
        params = [sender, reciever, "WUSD:" + str(amount) + ":sawi", fee]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "Invalid combo money format" in r['error']['message']

        # amount>balance
        amount = 10000000000
        params = [sender, reciever, "WUSD:" + str(amount) + ":sawi", fee]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "Account does not have enough WUSD" in r['error']['message']

        # fee=0
        amount = 10000
        fee = 0
        params = [sender, reciever, "WUSD:" + str(amount) + ":sawi", "WUSD:" + str(fee) + ":sawi"]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "fee is too small" in r['error']['message']

        # fee<0
        amount = 10000
        fee = -10000
        params = [sender, reciever, "WUSD:" + str(amount) + ":sawi", "WUSD:" + str(fee) + ":sawi"]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            print(r['error']['message'])
            # assert "Fee ComboMoney format error" in r['error']['message']

        # fee<10000
        amount = 10000
        fee = 9999
        params = [sender, reciever, "WUSD:" + str(amount) + ":sawi", "WUSD:" + str(fee) + ":sawi"]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "fee is too small" in r['error']['message']

        # fee>balance
        amount = 10000
        fee = 100000000
        params = [sender, reciever, "WUSD:" + str(amount) + ":sawi", "WUSD:" + str(fee) + ":sawi"]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "Account does not have enough WUSD" in r['error']['message']

    @allure.feature('Test send')
    def test_sendWGRT_address(self):
        """
        @author:elaine.tan
        Test send WGRT  when：
        sendaddress is None,not register,invalid,
        recieveaddress is None,not register,invalid,
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # send is None
        balance = "10000"
        fee = self.sendfee
        sender = None
        reciever = account.getEnoughCoindRegisteredAddress(10)
        assert reciever is not None
        params = [sender, reciever, "WGRT:" + balance + ":sawi", fee]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "value is type null, expected str" in r['error']['message']

        # send is not register
        sender = account.newaddress()
        params = [sender, reciever, "WGRT:" + balance + ":sawi", fee]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            print(r['error']['message'])

        # send is invalid
        sender = "invalid"
        params = [sender, reciever, "WGRT:" + balance + ":sawi", fee]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "Invalid address" in r['error']['message']

        # reciever is None
        reciever = None
        params = [self.init, reciever, "WGRT:" + balance + ":sawi", fee]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "value is type null, expected str" in r['error']['message']

        # reciever is not register
        sender = self.init
        reciever = account.newaddress()
        params = [sender, reciever, "WGRT:" + balance + ":sawi", fee]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None

        # reciever is invalid
        reciever = "invalid"
        params = [sender, reciever, "WGRT:" + balance + ":sawi", fee]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "Invalid address" in r['error']['message']

    @allure.feature('Test send')
    def test_sendWGRT_unnormal(self):
        """
        @author:elaine.tan
        Test send WGRT with unnormal cases：
        amount is None,<0,=0,>balance
        fee is <0,<required,>required,>balance
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        sender = account.getNewAddrWithEnoughWGRT(self.init, self.init, 30000)
        assert sender is not None
        fee = self.sendfee
        status_code, response = transaction.send([self.init, sender, 40000, fee])
        assert status_code == 200
        reciever = account.newaddress()
        assert reciever is not None

        # amount is None
        amount = None
        params = [sender, reciever, "WGRT:" + str(amount) + ":sawi", fee]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            print(r['error']['message'])

        # amount=0
        amount = 0
        params = [sender, reciever, "WGRT:" + str(amount) + ":sawi", fee]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "Coins is zero" in r['error']['message']

        # amount<0
        amount = -10000
        params = [sender, reciever, "WGRT:" + str(amount) + ":sawi", fee]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "Invalid combo money format" in r['error']['message']

        # amount>balance
        amount = 10000000000
        params = [sender, reciever, "WGRT:" + str(amount) + ":sawi", fee]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "Account does not have enough WGRT" in r['error']['message']

        # fee=0
        amount = 10000
        fee = 0
        params = [sender, reciever, "WGRT:" + str(amount) + ":sawi", "WICC:" + str(fee) + ":sawi"]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "fee is too small" in r['error']['message']

        # fee<0
        amount = 10000
        fee = -10000
        params = [sender, reciever, "WGRT:" + str(amount) + ":sawi", "WICC:" + str(fee) + ":sawi"]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            print(r['error']['message'])
            # assert "Fee ComboMoney format error" in r['error']['message']

        # fee<10000
        amount = 10000
        fee = 9999
        params = [sender, reciever, "WGRT:" + str(amount) + ":sawi", "WICC:" + str(fee) + ":sawi"]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "fee is too small" in r['error']['message']

        # fee>balance
        amount = 10000
        fee = 100000000
        params = [sender, reciever, "WGRT:" + str(amount) + ":sawi", "WICC:" + str(fee) + ":sawi"]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "Account does not have enough WICC" in r['error']['message']

    @allure.feature('Test send')
    def test_send_WGRT_fee_large(self):
        """
        @author:elaine.tan
        Test send WGRT when fee > 10000
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)
        # send WGRT
        reciever = account.newaddress()
        sleep(6)
        # fee>1000000
        params = [self.init, reciever, "WGRT:1:siwi", "WICC:200:siwi"]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

    @allure.feature('Test send')
    def test_send_WGRT_fee_equal(self):
        """
        @author:elaine.tan
        Test send WGRT when fee = 1000000
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)
        # send WGRT
        reciever = account.newaddress()
        sleep(6)
        # fee=1000000
        params = [self.init, reciever, "WGRT:10:huwi", "WICC:1000:huwi"]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

    @allure.feature('Test send')
    def test_send_WGRT_fee_default(self):
        """
        @author:elaine.tan
        Test send WGRT when fee is default
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)
        # send WGRT
        reciever = account.newaddress()
        sleep(6)
        # fee is default
        params = [self.init, reciever, "WGRT:100:muwi", self.sendfee]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

    @allure.feature('Test send')
    def test_send_differfee(self):
        """
        @author:elaine.tan
        Test send  with different fee when：
        transfer is WICC,fee is WGRT、WUSD
        transfer is WGRT,fee is WICC、WUSD
        transfer is WUSD,fee is WICC、WGRT
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        fee = str(self.sendfee)
        sender = account.getNewAddrWithEnoughWUSD(self.init, 50000000)
        assert sender is not None
        res = account.send([self.init, sender, 40000, fee])
        assert res
        res = account.send([self.init, sender, "WGRT:40000:sawi", fee])
        assert res
        sleep(30)
        reciever = account.newaddress()
        assert reciever is not None

        # send WICC,fee is WGRT
        amount = "10000"
        params = [sender, reciever, "WICC:" + amount + ":sawi", "WGRT:" + fee + ":sawi"]
        status_code, response = tran.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "Fee symbol is WGRT, but expect WUSD|WICC" in r['error']['message']

        # send WUSD,fee is WGRT
        params = [sender, reciever, "WUSD:" + amount + ":sawi", "WGRT:" + fee + ":sawi"]
        status_code, response = tran.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            print(r['error']['message'])

        # send WICC,fee is WUSD
        params = [sender, reciever, "WICC:" + amount + ":sawi", "WUSD:" + fee + ":sawi"]
        status_code, response = tran.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

        # send WGRT,fee is WICC
        params = [sender, reciever, "WGRT:" + amount + ":sawi", "WICC:" + fee + ":sawi"]
        status_code, response = tran.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

        # send WGRT,fee is WUSD
        params = [sender, reciever, "WGRT:" + amount + ":sawi", "WUSD:" + fee + ":sawi"]
        status_code, response = tran.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

        # send WUSD,fee is WICC
        params = [sender, reciever, "WUSD:" + amount + ":sawi", "WICC:" + fee + ":sawi"]
        status_code, response = tran.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitcoinstaketx')
    def test_submitcoinstaketx_normal_WGRT(self):
        """
        @author:elaine.tan
        test submitcoinstaketx when normal stake WGRT
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        amount = 10000
        fee = self.sendfee
        address = account.getnewaddresswithWICC(self.init, fee)
        assert address is not None

        res = account.send([self.init, address, "WGRT:" + str(amount), fee])
        assert res
        sleep(10)

        staus_code, response = transaction.submitcoinstaketx([address, "WGRT", amount])
        assert staus_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

        sleep(6)
        free_amount = account.gettokenbyaddr(address, "WGRT")
        stake_amount = account.gettokenbyaddr(address, "WGRT", "staked_amount")
        assert free_amount == 0
        assert stake_amount == amount

    @allure.feature('Test submitcoinstaketx')
    def test_submitcoinstaketx_normal_unstake_WGRT(self):
        """
        @author:elaine.tan
        test cancel submitcoinstaketx when normal unstake WGRT
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        amount = 10000
        fee = self.sendfee
        address = account.getnewaddresswithWICC(self.init, fee * 2)
        assert address is not None
        res = account.send([self.init, address, "WGRT:" + str(amount), fee])
        assert res
        sleep(10)

        staus_code, response = transaction.submitcoinstaketx([address, "WGRT", amount])
        assert staus_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None
        sleep(6)
        free_amount = account.gettokenbyaddr(address, "WGRT")
        stake_amount = account.gettokenbyaddr(address, "WGRT", "staked_amount")
        assert free_amount == 0
        assert stake_amount == amount

        # unstake
        staus_code, response = transaction.submitcoinstaketx([address, "WGRT", -1 * amount])
        assert staus_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None
        sleep(6)
        free_amount = account.gettokenbyaddr(address, "WGRT")
        stake_amount = account.gettokenbyaddr(address, "WGRT", "staked_amount")
        assert free_amount == amount
        assert stake_amount == 0

    @allure.feature('Test submitcoinstaketx')
    def test_submitcoinstaketx_normal_WICC(self):
        """
        @author:elaine.tan
        test submitcoinstaketx when normal stake WICC
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        address = account.getnewaddresswithWICC(self.init, 100000000 + self.stakefee)
        assert address is not None
        amount = 100000000
        staus_code, response = transaction.submitcoinstaketx([address, "WICC", amount])
        assert staus_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

        sleep(6)
        free_amount = account.gettokenbyaddr(address, "WICC")
        assert free_amount == 0
        stake_amount = account.gettokenbyaddr(address, "WICC", "staked_amount")
        assert stake_amount == amount

    @allure.feature('Test submitcoinstaketx')
    def test_submitcoinstaketx_normal_unstake_WICC(self):
        """
        @author:elaine.tan
        test submitcoinstaketx when normal unstake WICC
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        amount = 100000000
        fee = self.sendfee
        address = account.getnewaddresswithWICC(self.init, amount + fee * 2)
        assert address is not None
        staus_code, response = transaction.submitcoinstaketx([address, "WICC", amount])
        assert staus_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None
        sleep(6)
        free_amount = account.gettokenbyaddr(address, "WICC")
        assert free_amount == fee
        stake_amount = account.gettokenbyaddr(address, "WICC", "staked_amount")
        assert stake_amount == amount

        # unstake
        staus_code, response = transaction.submitcoinstaketx([address, "WICC", -1 * amount])
        assert staus_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None
        sleep(6)
        free_amount = account.gettokenbyaddr(address, "WICC")
        assert free_amount == amount
        stake_amount = account.gettokenbyaddr(address, "WICC", "staked_amount")
        assert stake_amount == 0

    @allure.feature('Test submitcoinstaketx')
    def test_submitcoinstaketx_normal_WUSD(self):
        """
        @author:elaine.tan
        test submitcoinstaketx when normal stake WUSD
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        address = account.getNewAddrWithEnoughWUSD(self.init, 100000000)
        assert address is not None
        amount = 100000000
        staus_code, response = transaction.submitcoinstaketx([address, "WUSD", amount])
        assert staus_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

        sleep(6)
        free_amount = account.gettokenbyaddr(address, "WUSD")
        assert free_amount == 0
        stake_amount = account.gettokenbyaddr(address, "WUSD", "staked_amount")
        assert stake_amount == amount

    @allure.feature('Test submitcoinstaketx unstake')
    def test_submitcoinstaketx_normal_unstake_WUSD(self):
        """
        @author:elaine.tan
        test submitcoinstaketx when normal unstake WUSD
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        amount = 100000000
        address = account.getNewAddrWithEnoughWUSD(self.init, amount)
        assert address is not None
        staus_code, response = transaction.submitcoinstaketx([address, "WUSD", amount])
        assert staus_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None
        sleep(6)
        free_amount = account.gettokenbyaddr(address, "WUSD")
        assert free_amount == 0
        stake_amount = account.gettokenbyaddr(address, "WUSD", "staked_amount")
        assert stake_amount == amount

        # unstake
        staus_code, response = transaction.submitcoinstaketx([address, "WUSD", -1 * amount])
        assert staus_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

        sleep(6)
        free_amount = account.gettokenbyaddr(address, "WUSD")
        assert free_amount == amount
        stake_amount = account.gettokenbyaddr(address, "WUSD", "staked_amount")
        assert stake_amount == 0

    @allure.feature('Test submitcoinstaketx')
    def test_submitcoinstaketx_address_None(self):
        """
        @author:elaine.tan
        test submitcoinstaketx when address is null
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # address is null
        address = None
        staus_code, response = transaction.submitcoinstaketx([address, "WGRT", self.sendfee])
        assert staus_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "value is type null, expected str" in r['error']['message']

    @allure.feature('Test submitcoinstaketx')
    def test_submitcoinstaketx_address_invalid(self):
        """
        @author:elaine.tan
        test submitcoinstaketx when address is invalid
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # address is invalid
        address = "invalid"
        staus_code, response = transaction.submitcoinstaketx([address, "WGRT", self.sendfee])
        assert staus_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "Invalid addr" in r['error']['message']

    @allure.feature('Test submitcoinstaketx')
    def test_submitcoinstaketx_address_unregister(self):
        """
        @author:elaine.tan
        test submitcoinstaketx when address is unregister
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # address is not registered
        address = account.newaddress()
        staus_code, response = transaction.submitcoinstaketx([address, "WGRT", self.sendfee])
        assert staus_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "The account not exists" in r['error']['message']

    @allure.feature('Test submitcoinstaketx')
    def test_submitcoinstaketx_amount_None(self):
        """
        @author:elaine.tan
        test submitcoinstaketx when amount is null
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        fbalance = 20000
        address = account.getnewaddresswithWICC(self.init, 20000)
        res = account.send([self.init, address, "WGRT:" + str(fbalance), self.sendfee])
        assert res
        sleep(6)

        # amount is null
        amount = None
        staus_code, response = tran.submitcoinstaketx([address, "WGRT", amount])
        assert staus_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "value is type null, expected int" in r['error']['message']

    @allure.feature('Test submitcoinstaketx')
    def test_submitcoinstaketx_amount_zero(self):
        """
        @author:elaine.tan
        test submitcoinstaketx when amount =0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        fbalance = self.stakefee
        address = account.getnewaddresswithWICC(self.init, fbalance)
        res = account.send([self.init, address, "WGRT:" + str(fbalance), self.sendfee])
        assert res
        sleep(6)

        # amount=0
        amount = 0
        staus_code, response = tran.submitcoinstaketx([address, "WGRT", amount])
        assert staus_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "bad-tx-coins-outofrange" in r['error']['message']

    @allure.feature('Test submitcoinstaketx')
    def test_submitcoinstaketx_amount_balance(self):
        """
        @author:elaine.tan
        test submitcoinstaketx when amount > balance
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        fbalance = self.stakefee
        address = account.getnewaddresswithWICC(self.init, fbalance)
        res = account.send([self.init, address, "WGRT:" + str(fbalance), self.sendfee])
        assert res
        sleep(6)

        # amount>balance
        amount = fbalance * 100
        staus_code, response = tran.submitcoinstaketx([address, "WGRT", amount])
        assert staus_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "insufficient-coin-amount" in r['error']['message']

    @allure.feature('Test submitcoinstaketx')
    def test_submitcoinstaketx_amount_normal(self):
        """
        @author:elaine.tan
        test submitcoinstaketx when amount < balance
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        fbalance = self.stakefee
        address = account.getnewaddresswithWICC(self.init, fbalance)
        res = account.send([self.init, address, "WGRT:" + str(fbalance), self.sendfee])
        assert res
        sleep(6)

        # 0<amount<balance
        amount = 10000
        staus_code, response = tran.submitcoinstaketx([address, "WGRT", amount])
        assert staus_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitcoinstaketx')
    def test_submitcoinstaketx_fee_negative(self):
        """
        @author:elaine.tan
        test submitcoinstaketx when fee <0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        address = account.getnewaddresswithWICC(self.init, 60000)
        sender = account.getNewAddrWithEnoughWUSD(self.init, 100000000)
        res = account.send([self.init, address, "WGRT:200000", self.sendfee])
        assert res
        res = account.send([sender, address, "WUSD:20000", "WUSD:" + str(self.sendfee)])
        assert res
        logger.info(address)
        sleep(6)

        # fee<0
        fee = -100
        staus_code, response = tran.submitcoinstaketx([address, "WGRT", 10000, fee])
        assert staus_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "Invalid" in r['error']['message']

    @allure.feature('Test submitcoinstaketx')
    @pytest.mark.parametrize('test_param', unnormal_fee, indirect=True)
    def test_submitcoinstaketx_fee_unnormal(self, test_param):
        """
        @author:elaine.tan
        test submitcoinstaketx when fee is unnormal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        address = account.getnewaddresswithWICC(self.init, 100000000)
        sender = account.getNewAddrWithEnoughWUSD(self.init, 100000000)
        res = account.send([self.init, address, "WGRT:200000", self.sendfee])
        assert res
        res = account.send([sender, address, "WUSD:20000", "WUSD:" + str(self.sendfee)])
        assert res
        logger.info(address)
        sleep(6)

        staus_code, response = tran.submitcoinstaketx([address, "WGRT", 10000, test_param])
        assert staus_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            print(r['error']['message'])

    @allure.feature('Test submitcoinstaketx')
    def test_submitcoinstaketx_fee_large(self):
        """
        @author:elaine.tan
        test submitcoinstaketx when fee > 10000
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        address = account.getnewaddresswithWICC(self.init, 100000000)
        sender = account.getNewAddrWithEnoughWUSD(self.init, 100000000)
        res = account.send([self.init, address, "WGRT:200000", self.sendfee])
        assert res
        res = account.send([sender, address, "WUSD:20000", "WUSD:" + str(self.sendfee)])
        assert res
        logger.info(address)
        sleep(6)

        # fee>10000
        fee = self.stakefee * 2
        staus_code, response = tran.submitcoinstaketx([address, "WGRT", 10000, "WICC:" + str(fee) + ":sawi"])
        assert staus_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitcoinstaketx')
    def test_submitcoinstaketx_fee_equal(self):
        """
        @author:elaine.tan
        test submitcoinstaketx when fee = 10000
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        address = account.getnewaddresswithWICC(self.init, 100000000)
        sender = account.getNewAddrWithEnoughWUSD(self.init, 100000000)
        res = account.send([self.init, address, "WGRT:200000", self.sendfee])
        assert res
        res = account.send([sender, address, "WUSD:20000", "WUSD:" + str(self.sendfee)])
        assert res
        logger.info(address)
        sleep(6)

        # fee=10000
        fee = self.stakefee
        staus_code, response = tran.submitcoinstaketx([address, "WGRT", 10000, fee])
        assert staus_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitcoinstaketx')
    def test_submitcoinstaketx_fee_WICC(self):
        """
        @author:elaine.tan
        test submitcoinstaketx when fee is WICC
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        address = account.getnewaddresswithWICC(self.init, 100000000)
        sender = account.getNewAddrWithEnoughWUSD(self.init, 100000000)
        res = account.send([self.init, address, "WGRT:200000", self.sendfee])
        assert res
        res = account.send([sender, address, "WUSD:20000", "WUSD:" + str(self.sendfee)])
        assert res
        logger.info(address)
        sleep(6)

        # fee is wicc
        fee = self.stakefee
        staus_code, response = tran.submitcoinstaketx([address, "WGRT", 10000, "WICC:" + str(fee)])
        assert staus_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitcoinstaketx')
    def test_submitcoinstaketx_fee_WUSD(self):
        """
        @author:elaine.tan
        test submitcoinstaketx when fee is WUSD
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        address = account.getnewaddresswithWICC(self.init, 60000)
        assert address is not None
        sender = account.getNewAddrWithEnoughWUSD(self.init, 100000000)
        assert sender is not None
        res = account.send([self.init, address, "WGRT:200000", self.sendfee])
        assert res
        fee = self.stakefee
        amount = 10000
        res = account.send([sender, address, "WUSD:" + str(amount + fee), "WUSD:" + str(self.sendfee)])
        assert res
        logger.info(address)
        sleep(6)

        # fee is wusd
        staus_code, response = tran.submitcoinstaketx([address, "WGRT", amount, "WUSD:" + str(fee)])
        print("stake res", response)
        assert staus_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitcoinstaketx')
    def test_submitcoinstaketx_cancel_None(self):
        """
        @author:elaine.tan
        test cancel submitcoinstaketx when ：未stake时撤销
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        amount = self.stakefee
        address = account.getnewaddresswithWICC(self.init, self.stakefee * 2)
        res = account.send([self.init, address, "WGRT:" + str(amount), self.sendfee])
        assert res
        sleep(6)

        # 未stake时撤销stake
        staus_code, response = tran.submitcoinstaketx([address, "WGRT", amount * (-1)])
        assert staus_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "insufficient-coin-amount" in r['error']['message']

    @allure.feature('Test submitcoinstaketx')
    def test_submitcoinstaketx_cancel_large(self):
        """
        @author:elaine.tan
        test cancel submitcoinstaketx when ：撤销stake大于stake余额
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        amount = self.stakefee
        address = account.getnewaddresswithWICC(self.init, self.stakefee * 2)
        res = account.send([self.init, address, "WGRT:" + str(amount), self.sendfee])
        assert res
        sleep(6)

        # stake fcoins
        staus_code, response = tran.submitcoinstaketx([address, "WGRT", amount])
        assert staus_code == 200
        sleep(10)

        # 撤销stake大于stake余额
        staus_code, response = tran.submitcoinstaketx([address, "WGRT", amount * (-1) * 100])
        assert staus_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "insufficient-coin-amount" in r['error']['message']

    @allure.feature('Test submitcoinstaketx')
    def test_submitcoinstaketx_cancel_small(self):
        """
        @author:elaine.tan
        test cancel submitcoinstaketx when normal：撤销stake小于stake余额
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        amount = self.stakefee
        address = account.getnewaddresswithWICC(self.init, amount * 2)
        staus_code, response = tran.send([self.init, address, "WGRT:" + str(amount), self.sendfee])
        assert staus_code == 200
        sleep(6)

        # stake fcoins
        staus_code, response = tran.submitcoinstaketx([address, "WGRT", amount])
        assert staus_code == 200
        sleep(10)

        # 撤销stake小于stake余额
        staus_code, response = tran.submitcoinstaketx([address, "WGRT", -100])
        assert staus_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

        sleep(10)
        staus_code, response = account.getaccountinfo([address])
        assert staus_code == 200
        for r in response:
            assert r['result']['tokens']['WGRT']['staked_amount'] == amount - 100

    @allure.feature('Test send unregister')
    def test_send_withoutregister(self):
        """
        @author:elaine.tan
        test send when address is not register
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # get address with balance
        address = account.newaddress()
        res = account.send([self.init, address, str(self.sendfee + 10000), self.sendfee])
        assert res
        res = account.isInBlock([address])
        assert res

        res = account.send([address, self.init, "10000", self.sendfee])
        assert res
        sleep(6)
        res = account.isRegistered([address])
        assert res

    @allure.feature('Test callcontracttx unregister')
    def test_callcontracttx_withoutregister(self):
        """
        test callcontracttx when: address is unregister
        """
        contract = Contract(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # 发布智能合约
        # new address
        addr = account.getEnoughCoinMatureAddress(300000000)
        params = [addr, self.contracturl, self.deployfee * 2]
        status, response = contract.submitcontractdeploytx(params)
        for r in response:
            assert r['result']['txid'] is not None
            hash = r['result']['txid']
            logger.info(hash)
        assert contract.ishashconfirmed([hash])

        regid = contract.getregidbytxhash([hash])
        assert regid is not None
        logger.info(regid)

        arguments = "f0170000"

        # get address with balance
        address = account.newaddress()
        res = account.send([self.init, address, "1000000", self.sendfee])
        assert res

        res = account.isInBlock([address])
        assert res
        # 未激活地址调用合约
        params = [address, regid, arguments, 0, self.callfee]
        status, response = contract.callcontracttx(params)
        for r in response:
            assert r['error'] is None
            assert r['result']['txid'] is not None

        # 调用完成后，自动激活地址
        sleep(6)
        res = account.isRegistered([address])
        assert res

    @allure.feature('Test submitdelegatevotetx unregister')
    def test_submitdelegatevotetx_withoutregister(self):
        """
        @author:elaine.tan
        test submitdelegatevotetx when address is not register
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        fee = self.votefee
        votes = 10000
        # get address with balance
        address = account.newaddress()
        res = account.send([self.init, address, str(votes + fee), self.sendfee])
        assert res

        res = account.isInBlock([address])
        assert res

        params = [address, [{"delegate": self.init, "votes": votes}], fee]
        print(params)
        print(999, tran.votedelegatetx(params))
        sleep(6)

        res = account.isRegistered([address])
        assert res

    @allure.feature('Test submitcoinstaketx unregister')
    def test_submitcoinstaketx_withoutregister(self):
        """
        @author:elaine.tan
        test submitcoinstaketx when address is not register
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # get address with balance
        address = account.newaddrwithWICC(self.init, 100000000)

        status, res = tran.submitcoinstaketx([address, "WICC", 10000])
        for r in res:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

        res = account.isRegistered([address])
        assert res

    @allure.feature('Test submitstakecdptx unregister')
    def test_submitstakecdptx_withoutregister(self):
        """
        @author:elaine.tan
        test submitstakecdptx when address is not register
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 200000000
        stake_amount = 100000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)

        # 获取没有激活但有钱的地址
        address = account.newaddrwithWICC(self.init, balance)
        assert address is not None
        # Create a CDP
        params = [address, str(stake_amount), str(mint_amount)]
        status_code, response = tran.submitstakecdptx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['result']['txid'] is not None
            assert r['error'] is None
        sleep(10)
        # 自动激活
        res = account.isRegistered([address])
        assert res

    @allure.feature('Test submitredeemcdptx unregister')
    def test_submitredeemcdptx_withoutregister(self):
        """
        @author:elaine.tan
        test submitredeemcdptx when address is not register
        验证regid未成熟时，是否是用的公钥发起的交易
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        balance = 300000000
        stake_amount = 200000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)

        # 获取没有激活但有钱的地址
        address = account.newaddrwithWICC(self.init, balance)
        assert address is not None
        pubkey = account.getaddrinfo(address, "owner_pubkey")
        assert pubkey is not None
        # Create a CDP
        params = [address, str(stake_amount), str(mint_amount)]
        txid = account.getNewCDP(params)
        sleep(10)

        # 自动激活
        res = account.isRegistered([address])
        assert res

        # cancel order
        status, response = tran.submitredeemcdptx([address, txid, mint_amount / 2, stake_amount / 2])
        assert status == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            tran_txid = r['result']['txid']

        status, response = tran.gettxdetail([tran_txid])
        assert status == 200
        txuid = ''
        for r in response:
            txuid = r['result']['tx_uid']

        assert txuid == pubkey

    @allure.feature('Test submitliquidatecdptx unregister')
    def test_submitliquidatecdptx_withoutregister(self):
        """
        @author:elaine.tan
        test submitliquidatecdptx when address is not register
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        balance = 300000000
        fee = self.sendfee
        stake_amount = 200000000
        price = tran.getprice("WICC")
        mint_amount = int((stake_amount * price) / 2)

        # 获取没有激活但有钱的地址
        address = account.newaddrwithWICC(self.init, stake_amount)
        assert address is not None
        # 获取创建CDP地址
        creater = account.getnewaddresswithWICC(self.init, balance)
        # 创建CDP
        txid = account.getNewCDP([creater, str(stake_amount), str(mint_amount)])
        assert txid is not None
        sleep(6)
        res = account.send([creater, address, "WUSD:10000000", fee])
        assert res
        sleep(6)

        # feeding price ,set rate=150%
        result = account.feedingPrice(self.init, "USD", price * 0.75, price * 0.75)
        assert result is True
        sleep(15)
        price2 = account.getprice("WICC")
        assert price2 != None

        # liquidate
        params = [address, txid, 10000, self.cdpfee]
        print("liqui:", params)
        status_code, response = tran.submitliquidatecdptx(params)
        print("liqui res:", response)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None
        sleep(6)

        # 自动激活
        res = account.isRegistered([address])
        assert res

        result = account.feedingPrice(self.init, "USD", int(price), int(price))
        assert result
        sleep(10)

    @allure.feature('Test submitdexsellmarketordertx unregister')
    def test_submitdexsellmarketordertx_withoutregister(self):
        """
        @author:elaine.tan
        test submitdexsellmarketordertx when address is not register
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # get unregistered address with balance
        address = account.newaddrwithWICC(self.init, 10000000 + self.dexfee)
        # sell WICC
        params = [address, "WUSD", "WICC", 10000000]
        status, response = tran.submitdexsellmarketordertx(params)
        assert status == 200
        for r in response:
            assert r['result'] is not None
        sleep(6)

        # 自动激活
        res = account.isRegistered([address])
        assert res

    @allure.feature('Test submitdexselllimitordertx unregister')
    def test_submitdexselllimitordertx_withoutregister(self):
        """
        @author:elaine.tan
        test submitdexselllimitordertx when address is not register
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # get unregistered address with balance
        address = account.newaddrwithWICC(self.init, 10000000 + self.dexfee)
        # sell WICC
        params = [address, "WUSD", "WICC", 10000000, 100000000]
        status, response = tran.submitdexselllimitordertx(params)
        assert status == 200
        for r in response:
            assert r['result'] is not None
        sleep(6)

        # 自动激活
        res = account.isRegistered([address])
        assert res

    @allure.feature('Test submitdexbuylimitordertx unregister')
    def test_submitdexbuylimitordertx_withoutregister(self):
        """
        @author:elaine.tan
        test submitdexbuylimitordertx when address is not register
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        sender = account.getNewAddrWithEnoughWUSD(self.init, 500000000)
        assert sender is not None
        # get unregistered address with balance
        address = account.newaddrwithWICC(self.init, self.dexfee)
        res = account.send([sender, address, "WUSD:100000000", self.sendfee])
        assert res
        sleep(6)
        # sell WICC
        params = [address, "WUSD", "WICC", 10000000, 100000000]
        status, response = tran.submitdexbuylimitordertx(params)
        assert status == 200
        for r in response:
            assert r['result'] is not None
        sleep(6)

        # 自动激活
        res = account.isRegistered([address])
        assert res

    @allure.feature('Test submitdexbuymarketordertx unregister')
    def test_submitdexbuymarketordertx_withoutregister(self):
        """
        @author:elaine.tan
        test submitdexbuymarketordertx when address is not register
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        sender = account.getNewAddrWithEnoughWUSD(self.init, 500000000)
        assert sender is not None
        # get unregistered address with balance
        address = account.newaddrwithWICC(self.init, self.dexfee)
        res = account.send([sender, address, "WUSD:100000000", self.sendfee])
        assert res
        sleep(6)
        # sell WICC
        params = [address, "WUSD", 10000000, "WICC"]
        status, response = tran.submitdexbuymarketordertx(params)
        assert status == 200
        for r in response:
            print(response)
            assert r['result'] is not None
        sleep(6)

        # 自动激活
        res = account.isRegistered([address])
        assert res

    @allure.feature('Test submitcancelordertx unregister')
    def test_submitcancelordertx_withoutregister(self):
        """
        @author:elaine.tan
        test submitcancelordertx when address is not register
        验证regid未成熟时，是否是用的公钥发起的交易
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # get unregistered address with balance
        address = account.newaddrwithWICC(self.init, 10000000 + self.dexfee * 2)
        pubkey = account.getaddrinfo(address, "owner_pubkey")
        assert pubkey is not None
        # sell WICC
        params = [address, "WUSD", "WICC", 10000000, 100000000]
        status, response = tran.submitdexselllimitordertx(params)
        txid = ''
        assert status == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            txid = r['result']['txid']
        sleep(6)

        # 自动激活
        res = account.isRegistered([address])
        assert res

        # cancel order
        status, response = tran.submitdexcancelordertx([address, txid])
        assert status == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            cancel_txid = r['result']['txid']

        status, response = tran.gettxdetail([cancel_txid])
        assert status == 200
        txuid = ''
        for r in response:
            txuid = r['result']['tx_uid']

        assert txuid == pubkey

    @allure.feature('Test submitassetissuetx')
    def test_submitassetissuetx_normal(self):
        """
        @author:elaine.tan
        test submitassetissuetx when normal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        risk_before = account.gettokenbyaddr(self.risk, "WICC", "free_amount")
        assert risk_before is not None
        asset_fee = self.assetcreate
        fee = self.assetfee
        address = account.getEnoughCoindRegisteredAddress(asset_fee + fee)
        startamount = account.gettokenbyaddr(address, "WICC")
        symbol = account.getrandomsymbol()
        params = [address, str(symbol), address, "RMB", 100000000, True, "WICC:" + str(fee) + ":sawi"]
        status, response = tran.submitassetissuetx(params)
        assert status == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None
        sleep(10)

        # 账户剩余金额 = 开始金额 - 550个WICC - 10000小费
        balance = account.gettokenbyaddr(address, "WICC")
        assert balance == startamount - asset_fee - fee

        # 风险备用金金额 = 550 * 40%
        risk_after = account.gettokenbyaddr(self.risk, "WICC", "free_amount")
        assert risk_after == risk_before + asset_fee * 0.4

    @allure.feature('Test submitassetissuetx')
    @pytest.mark.parametrize('update_type,update_value', update)
    def test_submitassetissuetx_update_false(self, update_type, update_value):
        """
        @author:elaine.tan
        test submitassetissuetx when update is false
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        asset_fee = self.assetcreate
        fee = self.assetfee
        address = account.getEnoughCoindRegisteredAddress(asset_fee + fee)
        symbol = account.getrandomsymbol()
        params = [address, str(symbol), address, "RMB", 100000000, False, "WICC:" + str(fee) + ":sawi"]
        status, response = tran.submitassetissuetx(params)
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None
        sleep(10)

        params = [address, str(symbol), update_type, update_value, "WICC:"+str(fee)+":sawi"]
        status, response = tran.submitassetupdatetx(params)
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "asset-not-mintable" in r['error']['message']

    @allure.feature('Test submitassetissuetx')
    def test_submitassetissuetx_twice(self):
        """
        @author:elaine.tan
        test submitassetissuetx when symbol is created
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        risk_before = account.gettokenbyaddr(self.risk, "WICC", "free_amount")
        assert risk_before is not None
        asset_fee = self.assetcreate
        fee = self.assetfee
        address = account.getEnoughCoindRegisteredAddress(asset_fee + fee)
        startamount = account.gettokenbyaddr(address, "WICC")
        symbol = account.getrandomsymbol()
        params = [address, str(symbol), address, "RMB", 100000000, True, "WICC:" + str(fee) + ":sawi"]
        status, response = tran.submitassetissuetx(params)
        assert status == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None
        sleep(6)

        params = [address, str(symbol), address, "RMB", 100000000, True, "WICC:" + str(fee) + ":sawi"]
        status, response = tran.submitassetissuetx(params)
        assert status == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "asset-existed-error" in r['error']['message']

    @allure.feature('Test submitassetissuetx')
    @pytest.mark.parametrize('test_param', invalid_address, indirect=True)
    def test_submitassetissuetx_address_unnormal(self, test_param):
        """
        @author:elaine.tan
        test submitassetissuetx when address is unnormal
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        symbol = account.getrandomsymbol()
        params = [test_param, str(symbol), test_param, "RMB", 100000000, True, "WICC:10000:sawi"]
        status, response = tran.submitassetissuetx(params)
        assert status == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print(r['error']['message'])

    @allure.feature('Test submitassetissuetx')
    def test_submitassetissuetx_address_unregister(self):
        """
        @author:elaine.tan
        test submitassetissuetx when address is unregister
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        address = account.newaddress()
        symbol = account.getrandomsymbol()
        params = [address, str(symbol), address, "RMB", 100000000, True, "WICC:" + str(self.assetfee) + ":sawi"]
        status, response = tran.submitassetissuetx(params)
        assert status == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "account not exists" in r['error']['message']

    @allure.feature('Test submitassetissuetx')
    def test_submitassetissuetx_address_balance(self):
        """
        @author:elaine.tan
        test submitassetissuetx when address balance is not enough
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        address = account.getnewaddresswithWICC(self.init, self.assetfee)
        symbol = account.getrandomsymbol()
        params = [address, str(symbol), address, "RMB", 100000000, True, "WICC:" + str(self.assetfee) + ":sawi"]
        status, response = tran.submitassetissuetx(params)
        assert status == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Account does not have enough WICC" in r['error']['message']

    @allure.feature('Test submitassetissuetx')
    def test_submitassetissuetx_address_unmature(self):
        """
        @author:elaine.tan
        test submitassetissuetx when address regid is not mature
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        address = account.getnewaddresswithWICC(self.init, self.assetcreate + self.assetfee)
        symbol = account.getrandomsymbol()
        params = [address, str(symbol), address, "RMB", 100000000, True, "WICC:" + str(self.assetfee) + ":sawi"]
        status, response = tran.submitassetissuetx(params)
        assert status == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "not mature" in r['error']['message']

    @allure.feature('Test submitassetissuetx')
    @pytest.mark.parametrize('test_param', invalid_symbol, indirect=True)
    def test_submitassetissuetx_symbol_unnormal(self, test_param):
        """
        @author:elaine.tan
        test submitassetissuetx when symbol is unnormal
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        address = account.getEnoughCoindRegisteredAddress(self.assetfee + self.assetcreate)
        assert address is not None
        params = [address, test_param, address, "RMB", 100000000, True, "WICC:" + str(self.assetfee) + ":sawi"]
        status, response = tran.submitassetissuetx(params)
        assert status == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print(r['error']['message'])

    @allure.feature('Test submitassetissuetx')
    @pytest.mark.parametrize('test_param', invalid_name, indirect=True)
    def test_submitassetissuetx_name_unnormal(self, test_param):
        """
        @author:elaine.tan
        test submitassetissuetx when symbol is unnormal
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        address = account.getEnoughCoindRegisteredAddress(self.assetfee + self.assetcreate)
        assert address is not None
        params = [address, test_param, address, test_param, 100000000, True, "WICC:" + str(self.assetfee) + ":sawi"]
        status, response = tran.submitassetissuetx(params)
        assert status == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print(r['error']['message'])

    @allure.feature('Test submitassetissuetx')
    @pytest.mark.parametrize('test_param', invalid_owner, indirect=True)
    def test_submitassetissuetx_owner_unnormal(self, test_param):
        """
        @author:elaine.tan
        test submitassetissuetx when owner is unnormal
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        address = account.getEnoughCoindRegisteredAddress(10000)
        assert address is not None
        symbol = account.getrandomsymbol()
        params = [address, str(symbol), test_param, "RMB", 100000000, True, "WICC:10000:sawi"]
        status, response = tran.submitassetissuetx(params)
        assert status == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print(r['error']['message'])

    @allure.feature('Test submitassetissuetx')
    def test_submitassetissuetx_owner_unregister(self):
        """
        @author:elaine.tan
        test submitassetissuetx when owner is unregistered
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        fee = self.assetfee
        address = account.getnewaddresswithWICC(self.init, self.assetcreate + fee)
        assert address is not None
        owner = account.newaddress()
        assert address is not None
        symbol = account.getrandomsymbol()
        params = [address, str(symbol), owner, "RMB", 100000000, True, "WICC:" + str(fee) + ":sawi"]
        status, response = tran.submitassetissuetx(params)
        assert status == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "account not exists" in r['error']['message']

    @allure.feature('Test submitassetissuetx')
    def test_submitassetissuetx_owner_unmature(self):
        """
        @author:elaine.tan
        test submitassetissuetx when owner regid is not mature
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        address = account.getEnoughCoinMatureAddress(self.assetcreate + self.assetfee)
        owner = account.getnewaddresswithWICC(self.init, self.assetfee)
        symbol = account.getrandomsymbol()
        params = [address, str(symbol), owner, "RMB", 100000000, True, "WICC:" + str(self.assetfee) + ":sawi"]
        status, response = tran.submitassetissuetx(params)
        assert status == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "not mature" in r['error']['message']

    @allure.feature('Test submitassetissuetx')
    @pytest.mark.parametrize("fee", invalid_fee)
    def test_submitassetissuetx_fee_unnormal(self, fee):
        """
        @author:elaine.tan
        test submitassetissuetx when fee is unnormal
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)

        address = account.getEnoughCoinMatureAddress(self.assetupdate + self.assetfee)
        assert address is not None
        symbol = account.getrandomsymbol()
        params = [address, str(symbol), address, "RMB", 100000000, True, fee]
        status, response = tran.submitassetissuetx(params)
        assert status == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print(r['error']['message'])

    @allure.feature('Test submitassetissuetx')
    def test_submitassetissuetx_owner_differ(self):
        """
        @author:elaine.tan
        test submitassetissuetx when owner is differ from create address
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        fee = self.assetfee
        address = account.getEnoughCoindRegisteredAddress(self.assetcreate + fee)
        assert address is not None
        owner = account.getnewaddrwithmatureregid(self.init, fee)
        assert address is not None
        symbol = account.getrandomsymbol()
        asset_amount = 100000000
        params = [address, str(symbol), owner, "RMB", asset_amount, True, "WICC:" + str(fee) + ":sawi"]
        status, response = tran.submitassetissuetx(params)
        print(response)
        assert status == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

        sleep(10)
        owner_balance = account.gettokenbyaddr(owner, symbol, "free_amount")
        assert owner_balance == asset_amount

        creater_balance = account.gettokenbyaddr(address, symbol, "free_amount")
        assert creater_balance == 0

    @allure.feature('Test submitassetissuetx')
    @pytest.mark.parametrize('test_param', normal_fee, indirect=True)
    def test_submitassetissuetx_fee_normal(self, test_param):
        """
        @author:elaine.tan
        test submitassetissuetx when with normal fee
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        asset_updatefee = self.assetupdate
        asset_fee = self.assetcreate
        fee = str(self.assetfee)
        sender = account.getNewAddrWithEnoughWUSD(self.init, 50000000)
        assert sender is not None
        address = account.getEnoughCoinMatureAddress(asset_fee + asset_updatefee + int(fee) * 2)
        res = account.send([sender,address, "WUSD:"+fee, self.sendfee])
        assert res
        sleep(6)
        symbol = account.getrandomsymbol()
        params = [address, str(symbol), address, "1111", 100000000, True, "WICC:" + fee + ":sawi"]
        status, response = tran.submitassetissuetx(params)
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

    @allure.feature('Test submitassetupdatetx')
    @pytest.mark.parametrize('test_param', invalid_address, indirect=True)
    def test_submitassetupdatetx_address_unnormal(self, test_param):
        """
        @author:elaine.tan
        test submitassetupdatetx when address is unnormal
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)

        symbol = account.getrandomsymbol()
        params = [test_param, str(symbol), "name", "update", "WICC:10000:sawi"]
        status, response = tran.submitassetupdatetx(params)
        assert status == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print(r['error']['message'])

    @allure.feature('Test submitassetupdatetx')
    @pytest.mark.parametrize('test_param', invalid_symbol, indirect=True)
    def test_submitassetupdatetx_symbol_unnormal(self, test_param):
        """
        @author:elaine.tan
        test submitassetupdatetx when symbol is unnormal
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)

        address = account.getEnoughCoinMatureAddress(self.assetupdate + self.assetfee)
        assert address is not None
        symbol = account.getrandomsymbol()
        params = [address, test_param, "name", "update", "WICC:" + str(self.assetfee) + ":sawi"]
        status, response = tran.submitassetupdatetx(params)
        assert status == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print(r['error']['message'])

    @allure.feature('Test submitassetupdatetx')
    @pytest.mark.parametrize('test_param', invalid_type, indirect=True)
    def test_submitassetupdatetx_type_unnormal(self, test_param):
        """
        @author:elaine.tan
        test submitassetupdatetx when updatetype is unnormal
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)

        address = account.getEnoughCoinMatureAddress(self.assetupdate + self.assetfee)
        assert address is not None
        symbol = account.getrandomsymbol()
        params = [address, symbol, test_param, "update", "WICC:" + str(self.assetfee) + ":sawi"]
        status, response = tran.submitassetupdatetx(params)
        assert status == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print(r['error']['message'])

    @allure.feature('Test submitassetupdatetx')
    @pytest.mark.parametrize("type,value", invalid_value)
    def test_submitassetupdatetx_value_unnormal(self, type, value):
        """
        @author:elaine.tan
        test submitassetupdatetx when updatevalue is unnormal
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)

        address = account.getEnoughCoinMatureAddress(self.assetupdate + self.assetfee)
        assert address is not None
        symbol = account.getrandomsymbol()
        params = [address, symbol, type, value, "WICC:" + str(self.assetfee) + ":sawi"]
        status, response = tran.submitassetupdatetx(params)
        assert status == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print(r['error']['message'])

    @allure.feature('Test submitassetupdatetx')
    @pytest.mark.parametrize("fee", invalid_fee)
    def test_submitassetupdatetx_fee_unnormal(self, fee):
        """
        @author:elaine.tan
        test submitassetupdatetx when fee is unnormal
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)

        address = account.getEnoughCoinMatureAddress(self.assetupdate + self.assetfee)
        assert address is not None
        symbol = account.getrandomsymbol()
        params = [address, symbol, "mint_amount", "10000", fee]
        status, response = tran.submitassetupdatetx(params)
        assert status == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print(r['error']['message'])

    @allure.feature('Test submitassetupdatetx')
    def test_submitassetupdatetx_address_notmature(self):
        """
        @author:elaine.tan
        test submitassetupdatetx when address regid is not mature
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        fee = self.assetfee
        address = account.getnewaddresswithWICC(self.init, self.assetupdate + fee)
        assert address is not None
        logger.info(address)
        symbol = account.getrandomsymbol()
        params = [address, str(symbol), "name", "update", "WICC:" + str(fee) + ":sawi"]
        status, response = tran.submitassetupdatetx(params)
        assert status == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "txUid-type-error" in r['error']['message']

    @allure.feature('Test submitassetupdatetx')
    def test_submitassetupdatetx_address_unregister(self):
        """
        @author:elaine.tan
        test submitassetupdatetx when address is unregister
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        address = account.newaddress()
        symbol = account.getrandomsymbol()
        params = [address, str(symbol), "name", "update", "WICC:" + str(self.assetfee) + ":sawi"]
        status, response = tran.submitassetupdatetx(params)
        assert status == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "account not exists" in r['error']['message']

    @allure.feature('Test submitassetupdatetx')
    def test_submitassetupdatetx_address_balance(self):
        """
        @author:elaine.tan
        test submitassetupdatetx when address balance is not enough
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        fee = self.assetfee
        address = account.getnewaddresswithWICC(self.init, fee)
        symbol = account.getrandomsymbol()
        params = [address, str(symbol), "name", "update", "WICC:" + str(fee) + ":sawi"]
        status, response = tran.submitassetupdatetx(params)
        assert status == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Account does not have enough WICC" in r['error']['message']

    @allure.feature('Test getassets')
    def test_getassets_normal(self):
        """
        @author:elaine.tan
        test getassets when normal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        fee = self.assetfee
        address = account.getEnoughCoinMatureAddress(self.assetcreate + fee)
        symbol = account.getrandomsymbol()
        params = [address, str(symbol), address, "asset", 100000000, True, "WICC:" + str(fee) + ":sawi"]
        status, response = tran.submitassetissuetx(params)
        assert status == 200
        sleep(10)
        status, response = tran.getassets()
        assert status == 200
        for r in response:
            assert r['result'] is not None
            assert r['result']['count'] > 0
            assert r['result']['assets'] != []
            assert r['error'] is None

    @allure.feature('Test submitassetupdatetx')
    def test_submitassetupdatetx_name_normal(self):
        """
        @author:elaine.tan
        test submitassetupdatetx when update name normal with normal fee
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        asset_updatefee = self.assetupdate
        asset_fee = self.assetcreate
        fee = str(self.assetfee)
        address = account.getEnoughCoinMatureAddress(asset_fee + asset_updatefee + int(fee) * 2)
        symbol = account.getrandomsymbol()
        params = [address, str(symbol), address, "1111", 100000000, True, "WICC:" + fee + ":sawi"]
        status, response = tran.submitassetissuetx(params)
        assert status == 200
        sleep(10)
        risk_before = account.gettokenbyaddr(self.risk, "WICC", "free_amount")
        print(risk_before)
        startamount = account.gettokenbyaddr(address, "WICC")
        name = "assetupdate"
        params = [address, str(symbol), "name", name, "WICC:" + fee + ":sawi"]
        status, response = tran.submitassetupdatetx(params)
        assert status == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

        sleep(8)
        res = tran.getassetsbysymbol(str(symbol))
        assert res is not None
        assert name == res['asset_name']

        endamount = account.gettokenbyaddr(address, "WICC")
        assert endamount == startamount - asset_updatefee - int(fee)

        # 风险备用金金额 = 110 * 40%
        risk_after = account.gettokenbyaddr(self.risk, "WICC", "free_amount")
        assert risk_after == risk_before + asset_updatefee * 0.4

    @allure.feature('Test submitassetupdatetx')
    @pytest.mark.parametrize('test_param', normal_fee, indirect=True)
    def test_submitassetupdatetx_fee_normal(self, test_param):
        """
        @author:elaine.tan
        test submitassetupdatetx when update with normal fee
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        asset_updatefee = self.assetupdate
        asset_fee = self.assetcreate
        fee = str(self.assetfee)
        sender = account.getNewAddrWithEnoughWUSD(self.init, 50000000)
        assert sender is not None
        address = account.getEnoughCoinMatureAddress(asset_fee + asset_updatefee + int(fee) * 2)
        res = account.send([sender,address, "WUSD:"+fee, self.sendfee])
        assert res
        sleep(6)
        symbol = account.getrandomsymbol()
        params = [address, str(symbol), address, "1111", 100000000, True, "WICC:" + fee + ":sawi"]
        status, response = tran.submitassetissuetx(params)
        assert status == 200
        sleep(10)
        # risk_before = account.gettokenbyaddr(self.risk, "WICC", "free_amount")
        # print(risk_before)
        # startamount = account.gettokenbyaddr(address, "WICC")
        name = "assetupdate"
        params = [address, str(symbol), "name", name, test_param]
        status, response = tran.submitassetupdatetx(params)
        assert status == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

        # sleep(8)
        # res = tran.getassetsbysymbol(str(symbol))
        # assert res is not None
        # assert name == res['asset_name']
        #
        # endamount = account.gettokenbyaddr(address, "WICC")
        # assert endamount == startamount - asset_updatefee - int(fee)
        #
        # # 风险备用金金额 = 110 * 40%
        # risk_after = account.gettokenbyaddr(self.risk, "WICC", "free_amount")
        # assert risk_after == risk_before + asset_updatefee * 0.4

    @allure.feature('Test submitassetupdatetx')
    def test_submitassetupdatetx_owner_normal(self):
        """
        @author:elaine.tan
        test submitassetupdatetx when update owner normal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        asset_updatefee = self.assetupdate
        asset_fee = self.assetcreate
        asset_amount = 100000000
        fee = self.assetfee
        symbol = account.getrandomsymbol()
        update_name = "assetupdate"

        # getaddress creater owner
        address = account.getEnoughCoinMatureAddress(asset_fee + asset_updatefee + fee * 2)
        owner = account.getnewaddrwithmatureregid(self.init, fee)
        logger.info(address)
        logger.info(owner)

        # create asset
        params = [address, str(symbol), address, "asset", asset_amount, True, "WICC:" + str(fee) + ":sawi"]
        print(params)
        status, response = tran.submitassetissuetx(params)
        assert status == 200
        sleep(6)

        # get risk and address balance
        risk_before = account.gettokenbyaddr(self.risk, "WICC", "free_amount")
        startamount = account.gettokenbyaddr(address, "WICC")

        # update owner_id
        params = [address, str(symbol), "owner_addr", owner, "WICC:" + str(fee) + ":sawi"]
        status, response = tran.submitassetupdatetx(params)
        assert status == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None
        sleep(6)
        create_am = account.gettokenbyaddr(address, symbol, "free_amount")
        assert create_am == asset_amount

        # 风险备用金金额 = 110 * 40%
        risk_after = account.gettokenbyaddr(self.risk, "WICC", "free_amount")
        assert risk_after == risk_before + asset_updatefee * 0.4

        # creater不能更新asset
        params = [address, str(symbol), "mint_amount", str(asset_amount * 2), "WICC:" + str(fee) + ":sawi"]
        status, response = tran.submitassetupdatetx(params)
        assert status == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            print(r['error']['message'])

        # creater 转移资产给 owner
        params = [address, owner, symbol + ":" + str(asset_amount), self.sendfee]
        res = tran.send(params)
        assert res
        sleep(6)

        # asset 资产转移成功
        create_am2 = account.gettokenbyaddr(address, symbol, "free_amount")
        owner_am = account.gettokenbyaddr(owner, symbol, "free_amount")
        assert create_am2 == 0
        assert owner_am == asset_amount

    @allure.feature('Test submitassetissuetx')
    def test_submitassetupdatetx_mintamount_normal(self):
        """
        @author:elaine.tan
        test submitassetupdatetx when update mint_amount normal
        """
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        fee = self.assetfee
        address = account.getEnoughCoindRegisteredAddress(self.assetcreate + fee)
        assert address is not None
        owner = account.getnewaddrwithmatureregid(self.init, self.assetupdate + fee)
        assert address is not None
        symbol = account.getrandomsymbol()
        logger.info(symbol)
        logger.info(address)
        logger.info(owner)
        asset_amount = 100000000
        mint_amount = asset_amount * 2
        params = [address, str(symbol), owner, "人民币", asset_amount, True, "WICC:" + str(fee) + ":sawi"]
        status, response = tran.submitassetissuetx(params)
        print(response)
        assert status == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

        sleep(6)
        # update mint_amount
        params = [owner, str(symbol), "mint_amount", str(mint_amount), "WICC:" + str(fee) + ":sawi"]
        status, response = tran.submitassetupdatetx(params)
        print("update res:", response)
        assert status == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

        sleep(15)
        addr_balance = account.gettokenbyaddr(address, symbol, "free_amount")
        owner_balance = account.gettokenbyaddr(owner, symbol, "free_amount")
        # 因为update mint_amount只是追加币量，不只是更新币量
        assert addr_balance == 0
        assert owner_balance == asset_amount + mint_amount

    @allure.feature('Test submitsendtx asset')
    def test_sendasset_normal(self):
        """
        @author:elaine.tan
        test submitsendtx asset
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        fee = self.assetfee
        address = account.getEnoughCoindRegisteredAddress(self.assetcreate + fee)
        recieve = account.newaddress()
        assert recieve is not None
        symbol = account.getrandomsymbol()
        total = 100000000
        send_amount = 10000
        params = [address, symbol, address, "asset", total, True, "WICC:" + str(fee) + ":sawi"]
        status, response = tran.submitassetissuetx(params)
        assert status == 200
        sleep(6)
        res = account.send([address, recieve, symbol + ":" + str(send_amount), self.sendfee])
        assert res
        sleep(6)
        sender_balan = account.gettokenbyaddr(address, symbol, "free_amount")
        reciever_balan = account.gettokenbyaddr(recieve, symbol, "free_amount")

        assert sender_balan == total - send_amount
        assert reciever_balan == send_amount

    @allure.feature('Test submitsendtx asset')
    def test_sendasset_self_normal(self):
        """
        @author:elaine.tan
        test submitsendtx asset when send asset to self
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        fee = self.assetfee
        address = account.getEnoughCoindRegisteredAddress(self.assetcreate + fee)
        symbol = account.getrandomsymbol()
        total = 100000000
        send_amount = 10000
        params = [address, symbol, address, "asset", total, True, "WICC:" + str(fee) + ":sawi"]
        status, response = tran.submitassetissuetx(params)
        assert status == 200
        sleep(6)
        sendwicc_bef = account.gettokenbyaddr(address, "WICC", "free_amount")
        res = account.send([address, address, symbol + ":" + str(send_amount), self.sendfee])
        assert res
        sleep(6)
        sendwicc_af = account.gettokenbyaddr(address, "WICC", "free_amount")
        sender_balan = account.gettokenbyaddr(address, symbol, "free_amount")

        assert sender_balan == total
        assert sendwicc_af == sendwicc_bef - self.sendfee

    @allure.feature('Test submitsendtx asset')
    def test_sendasset_unnormal_balance(self):
        """
        @author:elaine.tan
        test submitsendtx asset when sendamount exceed addr balance
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        fee = self.assetfee
        address = account.getEnoughCoindRegisteredAddress(self.assetcreate + fee)
        recieve = account.newaddress()
        assert recieve is not None
        symbol = account.getrandomsymbol()
        total = 100000000
        send_amount = total * 2
        params = [address, symbol, address, "asset", total, True, "WICC:" + str(fee) + ":sawi"]
        status, response = tran.submitassetissuetx(params)
        assert status == 200
        sleep(6)
        status, res = tran.send([address, recieve, symbol + ":" + str(send_amount), self.sendfee])
        for r in res:
            assert r['error'] is not None
            assert r['result'] is None
            assert "does not have enough" in r['error']['message']


if __name__ == '__main__':
    pytest.main(
        ['-s', 'ordinary_transaction_test.py::TestOrdinaryTransaction::test_submitassetupdatetx_owner_normal'])
