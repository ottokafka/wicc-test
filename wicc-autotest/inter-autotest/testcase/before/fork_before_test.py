from time import sleep
import pytest
import allure
from utils.log import mylog
from utils.read_property import ReadProperty
from wiccsdk.account import Account
from wiccsdk.block import Block
from wiccsdk.contract import Contract
from wiccsdk.transaction import Transaction
from wiccsdk.wallet import Wallet

logger = mylog(__name__).getlog()

# test_getnewaddress
isMine = [[True], [False]]


class TestBefore(object):
    """
    软分叉之前的测试用例，包含：
    1.基本功能验证
    2.不支持账户免激活
    3.不允许调用稳定币相关接口
    """

    @pytest.fixture(scope="class", autouse=True)
    def setup(self):
        prop = ReadProperty()
        # env = ReadEnv()
        # host = 'http://' + env.read('ip') + ":" + str(env.read('port'))
        # rpcuser = env.read('username')
        # rpcpassword = env.read('password')
        # walletpassword = prop.read('common', 'walletpassword')
        host = prop.read('common', 'before_host')
        print("host:", host)
        rpcuser = prop.read('common', 'rpcuser')
        rpcpassword = prop.read('common', 'rpcpassword')
        walletpassword = prop.read('common', 'walletpassword')

        wallet = Wallet(host, rpcuser, rpcpassword)
        isLocked = wallet.islocked()
        # 解锁钱包
        if isLocked:
            status, response = wallet.walletpassphrase([walletpassword, 360000])
            assert status == 200
        else:
            pass

        assert not wallet.islocked()
        logger.info('class setup')

    @pytest.fixture(scope="class", autouse=True)
    def teardown(self, request):
        def fin():
            logger.info('class tear down')

        request.addfinalizer(fin)

    @pytest.fixture('function', autouse=True)
    def setup_function(self):
        # env = ReadEnv()
        # self.host = 'http://' + env.read('ip') + ":" + str(env.read('port'))
        # self.rpcuser = env.read('username')
        # self.rpcpassword = env.read('password')

        prop = ReadProperty()
        self.host = prop.read('common', 'before_host')
        self.rpcuser = prop.read('common', 'rpcuser')
        self.rpcpassword = prop.read('common', 'rpcpassword')
        self.contracturl = prop.read('common', 'contracturl')
        self.init = prop.read('common', 'init_addr')

        logger.info('method setup')

    @pytest.fixture('function', autouse=True)
    def teardown_function(self):
        logger.info('method tear down')

    @pytest.fixture(scope='function')
    def test_param(self, request):
        return request.param

    @allure.feature('Test listaddr')
    def test_listaddr(self):
        """
        test listaddr:查询当前节点地址列表信息
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        params = []
        status_code, response = account.listaddr(params)
        assert status_code == 200
        if status_code == 200:
            for r in response:
                assert r['error'] is None
                assert len(r['result']) > 0

    @allure.feature('Test getaccountinfo')
    def test_getaccountinfo(self):
        """
        Test getaccountinfo ：获取普通账户地址详情
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)

        addr = account.newaddress()
        assert addr is not None
        logger.info(addr)

        status_code, response = account.getaccountinfo([addr])
        assert status_code == 200
        if status_code == 200:
            for r in response:
                assert r['error'] is None
                assert r['result']['address'] is not None
                assert r['result']['position'] is not None
                assert r['result']['position'] == 'inwallet'
                assert r['result']['owner_pubkey'] is not None

        # send to address
        params = [self.init, addr, "20000", 10000]
        res = account.send(params)
        assert res

        isInBlock = account.isInBlock([addr])
        assert isInBlock

        status_code, response = account.getaccountinfo([addr])
        assert status_code == 200
        if status_code == 200:
            for r in response:
                assert r['error'] is None
                assert r['result']['position'] == 'inblock'

        params = [addr, 10000]
        status_code, response = account.registeraccounttx(params)
        assert status_code == 200

        isRegister = account.isRegistered([addr])
        assert isRegister

        regId = ''
        status_code, response = account.getaccountinfo([addr])
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result']['position'] == 'inblock'
            regId = r['result']['regid']

        status_code, response = account.getaccountinfo([regId])
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result']['regid'] == regId

    @allure.feature('Test getaccountinfo')
    def test_getaccountinfo_contract(self):
        """
        Test getaccountinfo：获取合约账户地址详情
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        contract = Contract(self.host, self.rpcuser, self.rpcpassword)

        fee = 110000000
        addr = account.getEnoughCoindRegisteredAddress(fee)
        assert addr is not None

        params = [addr, self.contracturl, fee]
        hash = ''
        status_code, response = contract.submitcontractdeploytx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            hash = r['result']['txid']

        sleep(20)
        regId = ''
        status_code, response = contract.getcontractregid([hash])
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            regId = r['result']['regid']

        status_code, response = account.getaccountinfo([regId])
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['regid'] == regId

    @allure.feature('Test getnewaddress')
    @pytest.mark.parametrize('test_param', isMine, indirect=True)
    def test_getnewaddress(self, test_param):
        """
        Test getnewaddress：创建新地址
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        status_code, response = account.getnewaddress(test_param)
        assert status_code == 200
        if status_code == 200:
            for r in response:
                assert r['error'] is None
                if test_param[0]:
                    assert r['result']['minerpubkey'] != "null"
                else:
                    assert r['result']['minerpubkey'] == "null"
                logger.info('minerpubkey is:' + r['result']['minerpubkey'])

    @allure.feature('Test registeraccounttx_normal')
    def test_registeraccounttx_normal(self):
        """
        Test register account normal：激活账户
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)

        addr = account.newaddress()
        assert addr is not None
        logger.info(addr)

        # send to address
        params = [self.init, addr, "20000", 10000]
        res = account.send(params)
        assert res

        isInBlock = account.isInBlock([addr])
        assert isInBlock

        params = [addr, 10000]
        status_code, response = account.registeraccounttx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result']['txid'] != ''

        isRegister = account.isRegistered([addr])
        assert isRegister

        status_code, response = account.getaccountinfo([addr])
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['result']['regid'] is not None
            assert '-0' not in r['result']['regid']

    @allure.feature('Test registeraccounttx')
    def test_registeraccounttx_addr_unregister(self):
        """
        Test register account, address is unregistered
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)

        addr = account.newaddress()
        assert addr is not None
        logger.info(addr)

        # address is in wallet, so it cannot be registered
        params = [addr, 10000]
        status_code, response = account.registeraccounttx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None

    @allure.feature('Test registeraccounttx')
    def test_registeraccounttx_addr_none(self):
        """
        Test register account, address is none
        """
        addr = None
        params = [addr, 10000]
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        status_code, response = account.registeraccounttx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert 'Expected type str, got null' in r['error']['message']

    @allure.feature('Test registeraccounttx')
    def test_registeraccounttx_addr_invalid(self):
        """
        Test register account, address is invalid
        """
        # address is invalid, so it cannot be registered
        addr = 'invalid'
        params = [addr, 10000]
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        status_code, response = account.registeraccounttx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert 'Invalid address' in r['error']['message']

    @allure.feature('Test registeraccounttx_Registered')
    def test_registeraccounttx_addr_registered(self):
        """
        Test register account, addr is registered
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)

        addr = account.newaddress()
        assert addr is not None
        logger.info(addr)

        # send to address
        params = [self.init, addr, "22000", 10000]
        res = account.send(params)
        assert res

        isInBlock = account.isInBlock([addr])
        assert isInBlock

        params = [addr, 10000]
        status_code, response = account.registeraccounttx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result']['txid'] != ''

        isRegister = account.isRegistered([addr])
        assert isRegister

        params = [addr, 10000]
        status_code, response = account.registeraccounttx(params)

        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert 'Account was already registered' in r['error']['message']

    @allure.feature('Test registeraccounttx_except')
    def test_registeraccounttx_fee_negative(self):
        """
        test registeraccounttx when fee < 0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)

        addr = account.newaddress()
        assert addr is not None
        res = account.send([self.init, addr, "10000", 10000])
        assert res
        inblock = account.isInBlock([addr])
        assert inblock
        logger.info(addr)

        # less zero
        params = [addr, -1]
        status_code, response = account.registeraccounttx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert 'Invalid amount' in r['error']['message']

    @allure.feature('Test registeraccounttx_except')
    def test_registeraccounttx_fee_zero(self):
        """
        test registeraccounttx when fee = 0
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)

        addr = account.newaddress()
        assert addr is not None
        res = account.send([self.init, addr, "10000", 10000])
        assert res
        inblock = account.isInBlock([addr])
        assert inblock
        logger.info(addr)

        # equal zero
        fee = 0
        status_code, response = account.registeraccounttx([addr, fee])
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            # assert r['result'] is None
            # assert r['error'] is not None
            # assert 'fee is too small' in r['error']['message']

    @allure.feature('Test registeraccounttx_except')
    def test_registeraccounttx_fee_none(self):
        """
        test registeraccounttx when fee is None
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)

        addr = account.newaddress()
        assert addr is not None
        res = account.send([self.init, addr, "10000", 10000])
        assert res
        inblock = account.isInBlock([addr])
        assert inblock
        logger.info(addr)

        # equal zero
        fee = None
        status_code, response = account.registeraccounttx([addr, fee])
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Expected type int, got null" in r['error']['message']

    @allure.feature('Test registeraccounttx_except')
    def test_registeraccounttx_fee_small(self):
        """
        test registeraccounttx when fee < 10000
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)

        addr = account.newaddress()
        assert addr is not None
        res = account.send([self.init, addr, "10000", 10000])
        assert res
        inblock = account.isInBlock([addr])
        assert inblock
        logger.info(addr)

        # equal zero
        fee = 9999
        status_code, response = account.registeraccounttx([addr, fee])
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            # assert r['result'] is None
            # assert r['error'] is not None
            # assert "fee is too small" in r['error']['message']

    @allure.feature('Test registeraccounttx_except')
    def test_registeraccounttx_fee_balance(self):
        """
        test registeraccounttx when fee > balance
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)

        addr = account.newaddress()
        assert addr is not None
        res = account.send([self.init, addr, "10000", 10000])
        assert res
        inblock = account.isInBlock([addr])
        assert inblock
        logger.info(addr)

        # equal zero
        fee = 10000000
        status_code, response = account.registeraccounttx([addr, fee])
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Account does not have enough WICC" in r['error']['message']

    @allure.feature('Test registeraccounttx_except')
    def test_registeraccounttx_fee_empty(self):
        """
        test registeraccounttx when fee is empty
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)

        addr = account.newaddress()
        assert addr is not None
        res = account.send([self.init, addr, "10000", 10000])
        assert res
        inblock = account.isInBlock([addr])
        assert inblock
        logger.info(addr)

        # fee is empty
        params = [addr]
        print(addr)
        status_code, response = account.registeraccounttx(params)
        print(response)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result']['txid'] is not None

    @allure.feature('Test dumpprivkey_miner')
    @pytest.mark.parametrize('test_param', isMine, indirect=True)
    def test_dumpprivkey(self, test_param):
        """
        Test dumpprivkey for miner address：矿工标识
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        addr = account.newaddress(test_param)
        status_code, response = account.dumpprivkey([addr])
        assert status_code == 200
        if status_code == 200:
            for r in response:
                assert r['error'] is None
                assert r['result']['privkey'] != ''
                if test_param[0]:
                    assert r['result']['minerkey'] != ' '
                else:
                    assert r['result']['minerkey'] == "null"

    @allure.feature('Test dumpprivkey is registered or not')
    @pytest.mark.parametrize('test_param', isMine, indirect=True)
    def test_dumpprivkey_registered(self, test_param):
        """
        Test dumpprivkey is registered or not
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        addr = account.newaddress(test_param)

        # send to address
        params = [self.init, addr, "20000", 10000]
        res = account.send(params)
        assert res
        # 未激活地址
        isInBlock = account.isInBlock([addr])
        assert isInBlock

        status_code, response = account.dumpprivkey([addr])
        assert status_code == 200
        if status_code == 200:
            for r in response:
                assert r['error'] is None
                assert r['result']['privkey'] != ''
                if test_param[0]:
                    assert r['result']['minerkey'] != ' '
                else:
                    assert r['result']['minerkey'] == "null"
        # 已激活地址
        account.registeraccounttx([addr, 10000])
        isRegister = account.isRegistered([addr])
        assert isRegister

        status_code, response = account.dumpprivkey([addr])
        assert status_code == 200
        if status_code == 200:
            for r in response:
                assert r['error'] is None
                assert r['result']['privkey'] != "null"

    @allure.feature('Test dumpprivkey invalid address')
    def test_dumpprivkey_invalid(self):
        """
            Test dumpprivkey with invalid address
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        addr = 'invalid'
        status_code, response = account.dumpprivkey([addr])
        assert status_code == 200
        if status_code == 200:
            for r in response:
                assert r['error'] is not None
                assert r['result'] is None
                assert 'Invalid address' in r['error']['message']

    @allure.feature('Test importprivkey normal')
    def test_importprikey_normal(self):
        """
        Test  importprivkey normal
        """

        wallet = Wallet(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        assert not wallet.islocked()

        # new address
        addr = account.newaddress()
        privkey = account.getdumpprivkey([addr])
        assert privkey is not None

        # 空标签
        params = [privkey]
        status, response = account.importprivkey(params)
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['imported_key_address'] is not None

        # # 重新扫描交易
        # params = [privkey, "", "True"]
        # status, response = account.importprivkey(params)
        # for r in response:
        #     assert r['error'] is None
        #     assert r['result'] is not None
        #     assert r['result']['imported_key_address'] is not None
        #
        # # 不重新扫描交易
        # params = [privkey, "", "False"]
        # status, response = account.importprivkey(params)
        # for r in response:
        #     assert r['error'] is None
        #     assert r['result'] is not None
        #     assert r['result']['imported_key_address'] is not None

    @allure.feature('Test validateaddress')
    def test_validateaddress(self):
        """
        Test validate address contain normal, registered ,regid and contract address
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        addr = account.newaddress()

        # inwallet address
        status_code, response = account.validateaddress([addr])
        assert status_code == 200
        for r in response:
            assert r['result']['is_valid']

        isSent = account.send([self.init, addr, "20000", 10000])
        assert isSent

        # inblock address
        isInBlock = account.isInBlock([addr])

        if isInBlock:
            status_code, response = account.validateaddress([addr])
            assert status_code == 200
            for r in response:
                assert r['result']['is_valid']

        status_code, response = account.registeraccounttx([addr, 10000])
        assert status_code == 200

        # registered address
        isRegistered = account.isRegistered([addr])
        if isRegistered:
            status_code, response = account.validateaddress([addr])
            assert status_code == 200
            for r in response:
                assert r['result']['is_valid']

        # regid
        regId = ''
        status_code, response = account.getaccountinfo([addr])
        assert status_code == 200
        for r in response:
            regId = r['result']['regid']

        status_code, response = account.validateaddress([regId])
        assert status_code == 200
        for r in response:
            assert r['result']['is_valid']

        # invalid address
        invalidAddr = 'invalid'
        status_code, response = account.validateaddress([invalidAddr])
        assert status_code == 200
        for r in response:
            assert not r['result']['is_valid']

        # contract address
        fee = 110000000
        addr = account.getnewaddresswithWICC(self.init, fee)
        assert addr is not None

        params = [addr, self.contracturl, fee]
        contract = Contract(self.host, self.rpcuser, self.rpcpassword)
        hash = ''
        status_code, response = contract.submitcontractdeploytx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            hash = r['result']['txid']

        sleep(20)
        regId = ''
        status_code, response = contract.getcontractregid([hash])
        print(111, hash)
        print(222, response)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            regId = r['result']['regid']

        status_code, response = account.validateaddress([regId])
        assert status_code == 200
        for r in response:
            assert r['result']['is_valid']

    @allure.feature('Test votedelegatetx')
    def test_votedelegatetx_miners_reward_old(self):
        """
        test votedelegatetx miners reward : 验证投票收益规则的正确性
        """
        block = Block(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)
        votes = 10000000
        fee = 10000
        balance = (votes + fee) * 2
        recieveaddr = account.getEnoughCoindRegisteredAddress(0)
        assert recieveaddr is not None
        sendaddr = account.getnewaddresswithWICC(self.init, balance)
        logger.info(sendaddr)
        logger.info(recieveaddr)

        # 第一次投票
        height1 = block.getblockheight()
        assert height1 is not None
        params = [sendaddr, [{"delegate": recieveaddr, "votes": votes}], fee, height1]
        print("vote param1 ", params)
        status_code, response = transaction.votedelegatetx(params)
        print("vote res1 ", response)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None

        sleep(300)
        # 第二次投票
        height2 = block.getblockheight()
        assert height2 is not None
        params = [sendaddr, [{"delegate": recieveaddr, "votes": votes}], fee, height2]
        print("vote param2 ", params)
        status_code, response = transaction.votedelegatetx(params)
        print("vote res2 ", response)
        assert status_code == 200

        # 判断投票收益的正确性
        sleep(30)
        endbalance = account.gettokenbyaddr(sendaddr, "WICC")
        sum = transaction.getvoteminersreward_old(height1, height2, votes)
        assert sum is not None
        assert endbalance == sum

    @allure.feature('Test send unregister')
    def test_send_withoutregister(self):
        """
        @author:elaine.tan
        send 账户免激活
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # get address with balance
        address = account.newaddress()
        tran.send([self.init, address, "1000000", 10000])

        res = account.isInBlock([address])
        assert res

        status, response = tran.send([address, self.init, "10000", 10000])
        assert status == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None

    @allure.feature('Test callcontracttx unregister')
    def test_callcontracttx_withoutregister(self):
        """
        callcontracttx 账户免激活
        """
        contract = Contract(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # 发布智能合约
        # new address
        addr = account.getnewaddresswithWICC(self.init, 300000000)
        params = [addr, self.contracturl, 110000000]
        status, response = contract.submitcontractdeploytx(params)
        for r in response:
            print(666, response)
            assert r['result']['txid'] is not None
            hash = r['result']['txid']
            logger.info(hash)
        assert contract.ishashconfirmed([hash])

        regid = contract.getregidbytxhash([hash])
        assert regid is not None
        logger.info(regid)

        arguments = "f0170000"
        fee = 100000

        # get address with balance
        address = account.newaddress()
        res = account.send([self.init, address, "1000000", fee])
        assert res
        res = account.isInBlock([address])
        assert res
        # 未激活地址调用合约
        params = [address, regid, arguments, 0, fee]
        status, response = contract.callcontracttx(params)
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            print(r['error']['message'])

    @allure.feature('Test submitdelegatevotetx unregister')
    def test_submitdelegatevotetx_withoutregister(self):
        """
        @author:elaine.tan
        test submitdelegatevotetx 账户免激活
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # get address with balance
        address = account.newaddress()
        tran.send([self.init, address, "1000000", 10000])

        res = account.isInBlock([address])
        assert res

        params = [address, [{"delegate": self.init, "votes": 10000}], 10000]
        status, res = tran.votedelegatetx(params)
        assert status == 200
        for r in res:
            assert r['result'] is None
            assert r['error'] is not None
            assert "txUid-type-error" in r['error']['message']

    @allure.feature('Test submitcoinstaketx')
    def test_submitcoinstaketx_unsupport(self):
        """
        @author:elaine.tan
        test submitcoinstaketx unsupport
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        fee = 10000
        address = account.getnewaddresswithWICC(self.init, fee)
        sleep(6)

        staus_code, response = tran.submitcoinstaketx([address, "WICC", fee, fee])
        assert staus_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "unsupported-tx" in r['error']['message']

    @allure.feature('Test submitcdpstaketx')
    def test_submitcdpstaketx_unsupport(self):
        """
        @author:elaine.tan
        test submitcdpstaketx unsupport
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        fee = 10000
        address = account.getnewaddresswithWICC(self.init, fee)
        sleep(6)

        params = [address, "100000000", "100000000"]
        staus_code, response = tran.submitstakecdptx(params)
        assert staus_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "unsupported-tx" in r['error']['message']

    @allure.feature('Test submitcdpredeemtx')
    def test_submitcdpredeemtx_unsupport(self):
        """
        @author:elaine.tan
        test submitcdpredeemtx unsupport
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        fee = 10000
        address = account.getnewaddresswithWICC(self.init, fee)
        sleep(6)

        params = [address, "txid", 100000000, 100000000]
        staus_code, response = tran.submitredeemcdptx(params)
        assert staus_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "unsupported-tx" in r['error']['message']

    @allure.feature('Test submitcdpliquidatetx')
    def test_submitcdpliquidatetx_unsupport(self):
        """
        @author:elaine.tan
        test submitcdpliquidatetx unsupport
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        fee = 10000
        address = account.getnewaddresswithWICC(self.init, fee)
        sleep(6)

        params = [address, "ee71f5c86c5be35504c9d9996914bc9527569e0d406bbd10f3bbca63183f49c7", 100000000]
        staus_code, response = tran.submitliquidatecdptx(params)
        assert staus_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "unsupported-tx" in r['error']['message']

    @allure.feature('Test submitpricefeedtx')
    def test_submitpricefeedtx_unsupport(self):
        """
        @author:elaine.tan
        test submitpricefeedtx unsupport
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        WGRT = {"coin": "WGRT", "currency": "USD", "price": 100000000}
        address = account.getnewaddresswithWICC(self.init, 10000)
        assert address is not None
        params = [address, [WGRT]]
        status, response = tran.submitpricefeedtx(params)
        assert status == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "unsupported-tx" in r['error']['message']

    @allure.feature('Test submitdexsellmarketordertx')
    def test_submitdexsellmarketordertx_unsupport(self):
        """
        @author:elaine.tan
        test submitdexsellmarketordertx unsupport
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        address = account.getnewaddresswithWICC(self.init, 20000)
        params = [address, "WUSD", "WICC", 10000]
        status_code, response = tran.submitdexsellmarketordertx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "unsupported-tx" in r['error']['message']

    @allure.feature('Test submitdexselllimitordertx')
    def test_submitdexselllimitordertx_unsupport(self):
        """
        @author:elaine.tan
        test submitdexselllimitordertx unsupport
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        address = account.getnewaddresswithWICC(self.init, 20000)
        params = [address, "WUSD", "WICC", 10000, 100000000]
        status_code, response = tran.submitdexselllimitordertx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "unsupported-tx" in r['error']['message']

    @allure.feature('Test submitdexselllimitordertx')
    def test_submitdexselllimitordertx_unsupport(self):
        """
        @author:elaine.tan
        test submitdexselllimitordertx unsupport
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        address = account.getnewaddresswithWICC(self.init, 20000)

        tx = {"buy_order_id": "ee71f5c86c5be35504c9d9996914bc9527569e0d406bbd10f3bbca63183f49c7",
              "sell_order_id": "ee71f5c86c5be35504c9d9996914bc9527569e0d406bbd10f3bbca63183f49c7",
              "deal_price": 100000000,
              "deal_coin_amount": 100000000,
              "deal_asset_amount": 100000000}
        params = [address, [tx]]
        status_code, response = tran.submitdexsettletx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "unsupported-tx" in r['error']['message']

if __name__ == '__main__':
    pytest.main(['-s', 'fork_before_test.py::TestBefore::test_submitcoinstaketx_unsupport'])
