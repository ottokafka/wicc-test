import random
from time import sleep

import math
import pytest
import allure

from utils.read_env import ReadEnv
from utils.read_property import ReadProperty
from utils.log import mylog
from wiccsdk.account import Account
from wiccsdk.block import Block
from wiccsdk.contract import Contract
from wiccsdk.transaction import Transaction
from wiccsdk.wallet import Wallet

logger = mylog(__name__).getlog()

voteinvalid = [{'sendaddr': '', 'recieveaddr': '', 'votes': 0, 'fee': 10000},
               {'sendaddr': '', 'recieveaddr': '', 'votes': 10000000000, 'fee': 10000},
               {'sendaddr': '', 'recieveaddr': '', 'votes': 10000, 'fee': -1},
               {'sendaddr': '', 'recieveaddr': '', 'votes': 10000, 'fee': 0},
               {'sendaddr': '', 'recieveaddr': '', 'votes': 10000, 'fee': 10000000000}
               ]
# asset testcases
invalid_address = [None, 0, -100, 1000, "invalid"]
invalid_symbol = [None, 0, 111, "WICC", "WGRT", "WUSD", "不合法", "1111111111111"]
invalid_owner = [None, 0, 100, -1000, "invalid"]
unnormal_fee = [None, 'invalid', 0, -1000, 999, 10000000000000000]

class TestTransaction(object):
    """
    普通交易测试用例
    用例的执行不需要先做喂价操作
    """
    @pytest.fixture(scope='class', autouse=True)
    def setup(self):
        prop = ReadProperty()
        # env = ReadEnv()
        # host = 'http://' + env.read('ip') + ":" + str(env.read('port'))
        # rpcuser = env.read('username')
        # rpcpassword = env.read('password')
        # walletpassword = prop.read('common', 'walletpassword')

        host = prop.read('common', 'host')
        rpcuser = prop.read('common', 'rpcuser')
        rpcpassword = prop.read('common', 'rpcpassword')
        walletpassword = prop.read('common', 'walletpassword')

        wallet = Wallet(host, rpcuser, rpcpassword)
        account = Account(host, rpcuser, rpcpassword)
        isLocked = wallet.islocked()
        # 解锁钱包
        if isLocked:
            status, response = wallet.walletpassphrase([walletpassword, 99999999])
            assert status == 200
        else:
            pass

        assert not wallet.islocked()
        logger.info('class setup')

    @pytest.fixture(scope='class', autouse=True)
    def teardown(self, request):
        def fin():
            logger.info('class teardown')

        request.addfinalizer(fin)

    @pytest.fixture(scope='function', autouse=True)
    def setup_function(self):
        prop = ReadProperty()
        # env = ReadEnv()
        # self.host = 'http://' + env.read('ip') + ":" + str(env.read('port'))
        # self.rpcuser = env.read('username')
        # self.rpcpassword = env.read('password')
        self.host = prop.read('common', 'host')
        self.rpcuser = prop.read('common', 'rpcuser')
        self.rpcpassword = prop.read('common', 'rpcpassword')
        self.creaction = prop.read('common', 'creation_addr')
        self.creamount = prop.read('common', 'creation_amount')
        self.risk = prop.read('common', 'risk_addr')
        self.riskamount = prop.read('common', 'risk_amount')
        self.contracturl = prop.read('common', 'contracturl')
        self.init = prop.read('common', 'init_addr')
        self.registerfee = int(prop.read('fee', 'register'))
        self.sendfee = int(prop.read('fee', 'send'))
        self.deployfee = int(prop.read('fee', 'contractdeploy'))
        self.votefee = int(prop.read('fee', 'vote'))

        logger.info('function setup')

    @pytest.fixture(scope='function', autouse=True)
    def teardown_function(self):
        logger.info('function teardown')

    @pytest.fixture(scope='function', autouse=False)
    def test_param(self, request):
        return request.param

    @allure.feature('Test listtx')
    def test_listtx(self):
        """
        Test listtx when：
        存在未确认交易、存在已确认交易
        """
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)

        addr = account.newaddress()
        assert addr is not None
        fee = self.sendfee
        status, response = transaction.send([self.init, addr, fee, fee])
        assert status == 200

        # 存在未确认
        status, response = transaction.listtx()
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['unconfirmed_tx'] is not None

        sleep(30)
        # 存在已确认
        status, response = transaction.listtx()
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['confirmed_tx'] is not None

    @allure.feature('Test gettxdetail')
    def test_gettxdetail(self):
        """
        Test gettxdetail when：
        未确认交易、已确认交易、无效的交易
        """
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)

        addr = account.newaddress()
        assert addr is not None
        fee = self.sendfee
        hash = transaction.getsendhash([self.init, addr, fee, fee])
        assert hash is not None

        params = [hash]
        # 未确认交易
        status, response = transaction.gettxdetail(params)
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None

        sleep(30)
        # 已确认交易
        status, response = transaction.gettxdetail(params)
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['block_hash'] is not None
            assert r['result']['confirmed_time'] is not None

        # 无效hash
        params = ['000000']
        status, response = transaction.gettxdetail(params)
        for r in response:
            assert r['error'] is None
            assert r['result'] == {}

    @allure.feature('Test votedelegatetx normal')
    def test_votedelegatetx_normal(self):
        """
        test votedelegatetx normal : 正常投票
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        votes = 10000
        fee = self.votefee
        balance = votes + fee
        sendaddr = account.getnewaddresswithWICC(self.init, balance)
        assert sendaddr is not None
        logger.info(sendaddr)

        recieveaddr = account.getEnoughCoindRegisteredAddress(0)
        assert recieveaddr is not None
        startvotes = account.getvotes([recieveaddr])
        logger.info(recieveaddr)

        params = [sendaddr, [{"delegate": recieveaddr, "votes": votes}], fee]
        hash = ''
        status_code, response = transaction.votedelegatetx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None
            hash = r['result']['txid']

        sleep(6)
        endvotes = account.getvotes([recieveaddr])
        endbalance = account.gettokenbyaddr(sendaddr,"WICC")

        assert endvotes == startvotes + votes
        assert endbalance == balance - fee - votes

    @allure.feature('Test votedelegatetx transfer')
    def test_votedelegatetx_transfer(self):
        """
        test votedelegatetx transfer: 投票后再给地址转账，转账金额大于余额 and 转账金额小于余额
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # 投票
        votes = 10000
        fee = self.votefee
        sendfee = self.sendfee
        sends = 10000
        balance = votes + fee + sendfee + sends
        sendaddr = account.getnewaddresswithWICC(self.init, balance)
        assert sendaddr is not None

        recieveaddr = account.getEnoughCoindRegisteredAddress(0)
        assert recieveaddr is not None

        params = [sendaddr, [{"delegate": recieveaddr, "votes": votes}], fee]
        status_code, response = transaction.votedelegatetx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None
        sleep(20)

        # 转账
        transferaddr = account.newaddress()
        assert transferaddr is not None

        senderbalance = account.gettokenbyaddr(sendaddr, "WICC")

        # 转账金额大于余额
        params = [sendaddr, transferaddr, str(sends*2), self.sendfee]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert "does not have enough" in r['error']['message']

        # 转账金额小于余额
        params = [sendaddr, transferaddr, str(sends), self.sendfee]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

    @allure.feature('Test votedelegatetx cancel')
    def test_votedelegatetx_cancel(self):
        """
        test votedelegatetx cancel when： 撤销投票，votes为负数
        1.A未给B投票时，撤销投票
        2.A已给B投票，撤销投票，撤销投票额大于已投票额
        3.A已给B投票，撤销投票的正确性，撤销投票额小于等于已投票额
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)
        fee = self.votefee
        balance = fee * 3
        sendaddr = account.getnewaddresswithWICC(self.init, balance)
        assert sendaddr is not None

        recieveaddr = account.getnewaddresswithWICC(self.init, fee)
        assert recieveaddr is not None
        logger.info(sendaddr)
        logger.info(recieveaddr)

        # 未投票时，撤销投票
        votes = -1
        params = [sendaddr, [{"delegate": recieveaddr, "votes": votes}], fee]
        status_code, response = transaction.votedelegatetx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            print(r['error']['message'])

        votes = 1
        params = [sendaddr, [{"delegate": recieveaddr, "votes": votes}], fee]
        status_code, response = transaction.votedelegatetx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None
        sleep(6)
        # 判断投票是否成功
        assert account.getvotes([recieveaddr]) == votes

        # 已投票，撤销投票额大于投票额
        votes = -100
        params = [sendaddr, [{"delegate": recieveaddr, "votes": votes}], fee]
        status_code, response = transaction.votedelegatetx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            print(r['error']['message'])

        # 已投票，撤销投票额小于等于投票额
        votes = -1
        params = [sendaddr, [{"delegate": recieveaddr, "votes": votes}], fee]
        status_code, response = transaction.votedelegatetx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

    @allure.feature('Test votedelegatetx contractaddr')
    def test_votedelegatetx_contractaddr(self):
        """
         test votedelegatetx when:
         投票发送地址为合约地址、投票接收地址为合约地址
        """
        contract = Contract(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        votes = 100000000
        fee = self.votefee
        contractfee = self.deployfee

        addr = account.getEnoughCoindRegisteredAddress(votes + fee + contractfee)
        params = [addr, self.contracturl, 110000000]
        hash = ''
        status_code, response = contract.submitcontractdeploytx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            hash = r['result']['txid']

        sleep(30)
        regId = ''
        status_code, response = contract.getcontractregid([hash])
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            regId = r['result']['regid']

        # 投票发送地址为合约地址
        sendaddr = account.getaddrbyregid([regId])
        assert sendaddr is not None

        recieveaddr = account.getEnoughCoindRegisteredAddress(fee)
        params = [sendaddr, [{"delegate": recieveaddr, "votes": votes}], fee]
        status_code, response = transaction.votedelegatetx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "not found" in r['error']['message']

        contractaddr = sendaddr
        sendaddr = recieveaddr
        recieveaddr = contractaddr
        # 投票接收地址为合约地址
        params = [sendaddr, [{"delegate": recieveaddr, "votes": votes}], fee]
        status_code, response = transaction.votedelegatetx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print(r['error']['message'])

    @allure.feature('Test votedelegatetx invalidaddr')
    def test_votedelegatetx_invalidaddr(self):
        """
        test votedelegatetx when :
        sendaddr is: 无效的投票发送地址、未被激活的发送地址
        address is: 无效的投票接收地址、未被激活的接收地址
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)
        votes = 10000
        fee = self.votefee
        balance = votes + fee
        # 无效的投票发送地址
        sendaddr = 'valid'
        recieveaddr = account.getEnoughCoindRegisteredAddress(0)
        params = [sendaddr, [{"delegate": recieveaddr, "votes": votes}], fee]
        status_code, response = transaction.votedelegatetx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Invalid address" in r['error']['message']

        # 投票发送地址未被激活
        sendaddr = account.newaddress()
        assert sendaddr is not None
        recieveaddr = account.getEnoughCoindRegisteredAddress(0)
        params = [sendaddr, [{"delegate": recieveaddr, "votes": votes}], fee]
        status_code, response = transaction.votedelegatetx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "account not exists" in r['error']['message']

        # 投票接收地址未被激活
        recieveaddr = sendaddr
        sendaddr = account.getnewaddresswithWICC(self.init, balance)
        assert sendaddr is not None
        params = [sendaddr, [{"delegate": recieveaddr, "votes": votes}], fee]
        status_code, response = transaction.votedelegatetx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "not exist" in r['error']['message']

        # 无效的投票接收地址
        recieveaddr = 'valid'
        params = [sendaddr, [{"delegate": recieveaddr, "votes": votes}], fee]
        status_code, response = transaction.votedelegatetx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "address error" in r['error']['message']

    @allure.feature('Test votedelegatetx exception')
    @pytest.mark.parametrize('test_param', voteinvalid, indirect=True)
    def test_votedelegatetx_exception(self, test_param):
        """
        test votedelegatetx when :
        votes is: 等于0、大于账户余额
        fee is: 小于0、等于0、大于账户余额
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        votes = test_param['votes']
        fee = test_param['fee']
        balance = 20000
        sendaddr = account.getnewaddresswithWICC(self.init, balance)
        assert sendaddr is not None
        recieveaddr = account.getEnoughCoindRegisteredAddress(1000)
        assert recieveaddr is not None

        params = [sendaddr, [{"delegate": recieveaddr, "votes": votes}], fee]
        status_code, response = transaction.votedelegatetx(params)

        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print(r['error']['message'])

    @allure.feature('Test votedelegatetx height')
    def test_votedelegatetx_height(self):
        """
        test votedelegatetx when:
        height is : 小于0、等于0、大于0、小于高度范围（-250）、超出高度范围（+250）
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)
        block = Block(self.host, self.rpcuser, self.rpcpassword)

        votes = 10000
        fee = self.votefee

        sendaddr = account.getnewaddresswithWICC(self.init, votes + fee)
        assert sendaddr is not None

        recieveaddr = account.getEnoughCoindRegisteredAddress(0)
        assert recieveaddr is not None

        logger.info(sendaddr)
        logger.info(recieveaddr)

        # 高度大于0
        height = block.getblockheight()
        params = [sendaddr, [{"delegate": recieveaddr, "votes": votes}], fee, height]
        status_code, response = transaction.votedelegatetx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

        # 小于高度范围
        height = block.getblockheight() - 251
        params = [sendaddr, [{"delegate": recieveaddr, "votes": votes}], fee, height]
        status_code, response = transaction.votedelegatetx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "tx-invalid-height" in r['error']['message']

        # 超出高度范围
        height = block.getblockheight() + 255
        params = [sendaddr, [{"delegate": recieveaddr, "votes": votes}], fee, height]
        status_code, response = transaction.votedelegatetx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "tx-invalid-height" in r['error']['message']

        # 高度小于0
        height = -1
        params = [sendaddr, [{"delegate": recieveaddr, "votes": votes}], fee, height]
        status_code, response = transaction.votedelegatetx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "tx-invalid-height" in r['error']['message']

    # @allure.feature('Test votedelegatetx')
    # def test_votedelegatetx_reward_WGRT(self):
    #     """
    #     test votedelegatetx reward : 验证投票WGRT奖励规则的正确性
    #     """
    #     block = Block(self.host, self.rpcuser, self.rpcpassword)
    #     account = Account(self.host, self.rpcuser, self.rpcpassword)
    #     tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
    #     votes = 10000000000
    #     fee = 10000
    #     balance = (votes + fee) * 3
    #
    #     recieveaddr = account.getnewaddresswithWICC(self.init, 0)
    #     sendaddr = account.getnewaddresswithWICC(self.init, balance)
    #     assert recieveaddr is not None
    #     assert sendaddr is not None
    #     logger.info(sendaddr)
    #     logger.info(recieveaddr)
    #
    #     # 第一次投票
    #     height1 = block.getblockheight()
    #     assert height1 is not None
    #     params = [sendaddr, [{"delegate": recieveaddr, "votes": votes * 2}], fee, height1]
    #     status_code, response = tran.votedelegatetx(params)
    #     assert status_code == 200
    #     for r in response:
    #         assert r['error'] is None
    #         assert r['result']['txid'] is not None
    #         print("第一次txid:", r['result']['txid'])
    #
    #     sleep(30)
    #     # 第二次投票
    #     height2 = block.getblockheight()
    #     assert height2 is not None
    #     params = [sendaddr, [{"delegate": recieveaddr, "votes": votes}], fee, height2]
    #     status_code, response = tran.votedelegatetx(params)
    #     assert status_code == 200
    #     for r in response:
    #         assert r['error'] is None
    #         assert r['result']['txid'] is not None
    #         print("第二次txid:", r['result']['txid'])
    #
    #     # 判断投票收益的正确性
    #     sleep(30)
    #     endbalance = account.gettokenbyaddr(sendaddr, "WGRT")
    #     sum = tran.getvotereward(height1, height2, votes * 2)
    #     print("voteparam:", height1, height2, votes)
    #     print("sum:", sum, ",endbalance:", endbalance)
    #     assert sum is not None
    #     assert endbalance == sum

    @allure.feature('Test send')
    def test_send_WICC_normal(self):
        """
        @author:elaine.tan
        Test send WICC when fee is default
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # send WICC
        balance = self.sendfee + 10000
        sender = account.getEnoughCoindRegisteredAddress(balance)
        reciever = account.newaddress()
        # fee is default
        params = [sender, reciever, "WICC:10000", self.sendfee]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None

    @allure.feature('Test send')
    @pytest.mark.parametrize('test_param', unnormal_fee, indirect=True)
    def test_send_WICC_feeunnormal(self, test_param):
        """
        @author:elaine.tan
        Test send WICC when fee is unnormal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # send WICC
        balance = self.sendfee + 10000
        sender = account.getnewaddresswithWICC(self.init,balance)
        reciever = account.newaddress()
        params = [sender, reciever, "WICC:10000", test_param]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            print(r['error']['message'])

    @allure.feature('Test send')
    def test_sendWICC_address_sender_None(self):
        """
        @author:elaine.tan
        Test send WICC  when：sendaddress is None
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # send is None
        balance = "10000"
        sender = None
        reciever = account.getEnoughCoindRegisteredAddress(10)
        assert reciever is not None
        params = [sender, reciever, "WICC:" + balance + ":sawi", self.sendfee]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "value is type null, expected str" in r['error']['message']

    @allure.feature('Test send')
    def test_sendWICC_address_sender_unregister(self):
        """
        @author:elaine.tan
        Test send WICC  when：sendaddress is unregister
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        balance = "10000"
        reciever = account.getEnoughCoindRegisteredAddress(10)

        # send is not register
        sender = account.newaddress()
        params = [sender, reciever, "WICC:" + balance + ":sawi", self.sendfee]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "account not exists" in r['error']['message']

    @allure.feature('Test send')
    def test_sendWICC_address_sender_invalid(self):
        """
        @author:elaine.tan
        Test send WICC  when：sendaddress is invalid
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        balance = "10000"
        reciever = account.getEnoughCoindRegisteredAddress(10)
        # send is invalid
        sender = "invalid"
        params = [sender, reciever, "WICC:" + balance + ":sawi", self.sendfee]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "Invalid address" in r['error']['message']

    @allure.feature('Test send')
    def test_sendWICC_address_reciever_None(self):
        """
        @author:elaine.tan
        Test send WICC  when：sendaddress is invalid
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        # reciever is None
        reciever = None
        balance = "10000"
        sender = account.getEnoughCoindRegisteredAddress(20000)
        assert sender is not None
        params = [sender, reciever, "WICC:" + balance + ":sawi", self.sendfee]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "value is type null, expected str" in r['error']['message']

    @allure.feature('Test send')
    def test_sendWICC_address_reciever_unregister(self):
        """
        @author:elaine.tan
        Test send WICC  when：sendaddress is unregister
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        sends = 10000
        fee = self.sendfee
        sender = account.getEnoughCoindRegisteredAddress(sends+fee)

        # reciever is not register
        reciever = account.newaddress()
        params = [sender, reciever, "WICC:" + str(sends) + ":sawi", fee]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None

    @allure.feature('Test send')
    def test_sendWICC_address_reciever_invalid(self):
        """
        @author:elaine.tan
        Test send WICC  when：sendaddress is invalid
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        balance = "10000"
        sender = account.getEnoughCoindRegisteredAddress(10)

        # reciever is invalid
        reciever = "invalid"
        params = [sender, reciever, "WICC:" + balance + ":sawi", self.sendfee]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            assert "Invalid address" in r['error']['message']

    @allure.feature('Test send')
    @pytest.mark.parametrize('test_param', unnormal_fee, indirect=True)
    def test_sendWICC_amount_unnormal(self,test_param):
        """
        @author:elaine.tan
        Test send WICC when amount is unnormal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host, self.rpcuser, self.rpcpassword)

        sender = account.getnewaddresswithWICC(self.init, self.sendfee * 2)
        reciever = account.newaddress()
        assert reciever is not None

        params = [sender, reciever, test_param, self.sendfee]
        status_code, response = transaction.send(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            print(r['error']['message'])

    @allure.feature('Test submittxraw')
    def test_submittxraw_confirmed(self):
        """
        @author:elaine.tan
        Test submittxraw when tx is confirmed
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        amount = 10000
        fee = self.sendfee
        txid = ''
        sender = account.getEnoughCoindRegisteredAddress(amount+fee)
        reciever = account.getEnoughCoindRegisteredAddress(fee)
        status, response = tran.send([sender,reciever,amount,fee])
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            txid = r['result']['txid']
        sleep(6)
        comfirmed_height = 0
        rawtx = ''
        status, response = tran.gettxdetail([txid])
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            comfirmed_height = r['result']['confirmed_height']
            rawtx = r['result']['rawtx']

        if comfirmed_height > 0:
            status,response = tran.submittxraw([rawtx])
            for r in response:
                assert r['error'] is not None
                assert r['result'] is None
                assert "tx-duplicate-confirmed" in r['error']['message']

if __name__ == '__main__':
    pytest.main(['-s', 'transaction_test.py::TestTransaction::test_send_WICC_normal'])
