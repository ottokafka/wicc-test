from time import sleep
import pytest
import allure
from utils.log import mylog
from utils.read_property import ReadProperty
from wiccsdk.account import Account
from wiccsdk.block import Block
from wiccsdk.contract import Contract
from wiccsdk.wallet import Wallet

logger = mylog(__name__).getlog()

# test getblockhash
blockHash = [[-1], [0], [10], [10000000]]
blocknu = [None, -100, 0, 100000000, "invalid"]


class TestBlock(object):
    @pytest.fixture(scope='class', autouse=True)
    def setup(self):
        prop = ReadProperty()
        # env = ReadEnv()
        # host = 'http://' + env.read('ip') + ":" + str(env.read('port'))
        # rpcuser = env.read('username')
        # rpcpassword = env.read('password')
        # walletpassword = prop.read('common', 'walletpassword')

        host = prop.read('common', 'host')
        rpcuser = prop.read('common', 'rpcuser')
        rpcpassword = prop.read('common', 'rpcpassword')
        walletpassword = prop.read('common', 'walletpassword')

        wallet = Wallet(host, rpcuser, rpcpassword)
        isLocked = wallet.islocked()
        # 解锁钱包
        if isLocked:
            status, response = wallet.walletpassphrase([walletpassword, 360000])
            assert status == 200
        else:
            pass

        assert not wallet.islocked()
        logger.info('class setup')

    @pytest.fixture(scope='class', autouse=True)
    def teardown(self, request):
        def fin():
            logger.info('class teardown')

        request.addfinalizer(fin)

    @pytest.fixture(scope='function', autouse=True)
    def setup_function(self):
        logger.info('function setup')

    @pytest.fixture(scope='function', autouse=True)
    def teardown_function(self):
        # env = ReadEnv()
        # self.host = 'http://' + env.read('ip') + ":" + str(env.read('port'))
        # self.rpcuser = env.read('username')
        # self.rpcpassword = env.read('password')
        prop = ReadProperty()
        self.host = prop.read('common', 'host')
        self.rpcuser = prop.read('common', 'rpcuser')
        self.rpcpassword = prop.read('common', 'rpcpassword')

        logger.info('function teardown')

    @pytest.fixture(scope='function')
    def test_param(self, request):
        return request.param

    @allure.feature('Test getblockcount')
    def test_getblockcount(self):
        """
        Test getblockcount, if blockcount is less than sync blockheight
        """
        block = Block(self.host, self.rpcuser, self.rpcpassword)
        status_code, response = block.getblockcount()
        assert status_code == 200
        blockcount = 0
        for r in response:
            assert r['error'] is None
            blockcount = r['result']

        status_code, response = block.getinfo()
        assert status_code == 200
        expectedBlockCount = 0
        for r in response:
            expectedBlockCount = r['result']['syncblock_height']

        assert (expectedBlockCount - blockcount) < 2

    @allure.feature('Test getinfo')
    def test_getinfo(self):
        """
        Test getinfo：获取节点相关信息
        """
        block = Block(self.host, self.rpcuser, self.rpcpassword)
        status_code, response = block.getinfo()

        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result']['version'] is not None
            assert r['result']['syncblock_height'] > 0
            assert r['result']['tipblock_height'] > 0

    @allure.feature('Test getscoininfo')
    def test_getscoininfo(self):
        """
        Test getscoininfo：获取稳定币相关信息
        """
        block = Block(self.host, self.rpcuser, self.rpcpassword)
        status_code, response = block.getscoininfo()

        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['tipblock_height'] > 0

    # @allure.feature('Test getblockhash')
    # @pytest.mark.parametrize('test_param', blockHash, indirect=True)
    # def test_getblockhash(self, test_param):
    #     """
    #     Test getblockhash when:
    #     height is :小于0、等于0、大于0、超过高度范围
    #     """
    #     block = Block(self.host, self.rpcuser, self.rpcpassword)
    #     status_code, response = block.getblockhash(test_param)
    #     assert status_code == 200
    #     for r in response:
    #         if test_param == [10] or test_param == [0]:
    #             assert r['error'] is None
    #         else:
    #             assert 'Block number out of range' in r['error']['message']

    @allure.feature('Test getchaininfo')
    def test_getchaininfo_normal(self):
        """
        Test getchaininfo when unnormal
        """
        block = Block(self.host, self.rpcuser, self.rpcpassword)
        status_code, response = block.getchaininfo([5])
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['blocktime'] is not None
            assert r['result']['transactions'] is not None
            assert r['result']['miner'] is not None

    @allure.feature('Test getchaininfo')
    @pytest.mark.parametrize('test_param', blocknu, indirect=True)
    def test_getchaininfo_unnormal(self, test_param):
        """
        Test getchaininfo when unnormal
        """
        block = Block(self.host, self.rpcuser, self.rpcpassword)
        status_code, response = block.getchaininfo([test_param])
        assert status_code == 200
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            print(r['error']['message'])

    @allure.feature('Test getblock by hash height')
    @pytest.mark.parametrize('test_param', blockHash, indirect=True)
    def test_getblock_height(self, test_param):
        """
        Test getblock by block height：根据区块高度获取区块信息
        """
        block = Block(self.host, self.rpcuser, self.rpcpassword)
        status_code, response = block.getblock(test_param)
        assert status_code == 200
        print(response)
        for r in response:
            if test_param == [10] or test_param == [0]:
                assert r['error'] is None
            else:
                assert 'Block height out of range' in r['error']['message']

    @allure.feature('Test getblock by hash')
    def test_getblock_hash(self):
        """
        Test getblock by hash：根据区块哈希获取区块信息
        """
        tipBlockHash = ''
        block = Block(self.host, self.rpcuser, self.rpcpassword)
        status_code, response = block.getinfo()
        assert status_code == 200
        for r in response:
            tipBlockHash = r['result']['tipblock_hash']

        status_code, response = block.getblock([tipBlockHash])
        assert status_code == 200
        for r in response:
            assert r['result']['tx']


if __name__ == '__main__':
    pytest.main(['-s', 'block_test.py::TestBlock::test_getscoininfo'])
