import binascii
from time import sleep

import allure

import pytest
from utils.log import mylog
from utils.read_property import ReadProperty
from wiccsdk.account import Account
from wiccsdk.contract import Contract
from wiccsdk.transaction import Transaction
from wiccsdk.wallet import Wallet

logger = mylog(__name__).getlog()
isMine = [[True], [False], [None]]
unnormal_txid = [None, -1, 0, "invalid"]
unnormal_fee = [None, 'invalid', 0, -1000, 999]
unnormal_addr = [None, 'invalid', 0, "wMiQuyZj3LdYrJ3p5wad9HhZqtfZSPHFNw"]

class TestContract(object):
    @pytest.fixture(scope="class", autouse=True)
    def setup(self):
        prop = ReadProperty()
        # env = ReadEnv()
        # host = 'http://' + env.read('ip') + ":" + str(env.read('port'))
        # rpcuser = env.read('username')
        # rpcpassword = env.read('password')
        # walletpassword = prop.read('common', 'walletpassword')

        host = prop.read('common', 'host')
        rpcuser = prop.read('common', 'rpcuser')
        rpcpassword = prop.read('common', 'rpcpassword')
        walletpassword = prop.read('common', 'walletpassword')

        wallet = Wallet(host, rpcuser, rpcpassword)
        isLocked = wallet.islocked()
        # 解锁钱包
        if isLocked:
            status, response = wallet.walletpassphrase([walletpassword, 360000])
            assert status == 200
        else:
            pass

        assert not wallet.islocked()
        logger.info('class setup')

    @pytest.fixture(scope="class", autouse=True)
    def teardown(self, request):
        def fin():
            logger.info('class tear down')

        request.addfinalizer(fin)

    @pytest.fixture('function', autouse=True)
    def setup_function(self):
        prop = ReadProperty()
        # env = ReadEnv()
        # self.host = 'http://' + env.read('ip') + ":" + str(env.read('port'))
        # self.rpcuser = env.read('username')
        # self.rpcpassword = env.read('password')
        self.host = prop.read('common', 'host')
        self.rpcuser = prop.read('common', 'rpcuser')
        self.rpcpassword = prop.read('common', 'rpcpassword')

        self.walletpassword = prop.read('common', 'walletpassword')
        self.contracturl = prop.read('common', 'contracturl')
        self.key = prop.read('common', 'key')
        self.key2 = prop.read('common', 'key2')
        self.init = prop.read('common', 'init_addr')
        self.registerfee = int(prop.read('fee', 'register'))
        self.sendfee = int(prop.read('fee', 'send'))
        self.deployfee = int(prop.read('fee', 'contractdeploy'))
        self.callfee = int(prop.read('fee', 'contractcall'))

        logger.info('method setup')

    @pytest.fixture('function', autouse=True)
    def teardown_function(self):
        logger.info('method tear down')

    @pytest.fixture(scope='function')
    def test_param(self, request):
        return request.param

    @allure.feature('Test getcontractregid')
    def test_getcontractregid(self):
        """
        test getcontractregid when：
        交易哈希为：空、错误、正确
        未发布过合约的地址发布合约
        """
        contract = Contract(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)

        # 交易哈希正确
        fee = self.deployfee * 2
        addr = account.getnewaddrwithmatureregid(self.init, fee)
        assert addr is not None
        params = [addr, self.contracturl, fee]
        status, response = contract.submitcontractdeploytx(params)
        assert status == 200
        for r in response:
            txhash = r['result']['txid']
        # 正确交易哈希
        isConfirmed = contract.ishashconfirmed([txhash])
        assert isConfirmed
        status, response = contract.getcontractregid([txhash])
        for r in response:
            assert r['error'] is None
            assert r['result']['regid'] is not None

    @allure.feature('Test getcontractregid')
    @pytest.mark.parametrize('test_param', unnormal_txid, indirect=True)
    def test_getcontractregid_unnormal(self, test_param):
        """
        test getcontractregid when unnormal
        """
        contract = Contract(self.host, self.rpcuser, self.rpcpassword)

        # 错误交易哈希
        txhash = '1'
        status, response = contract.getcontractregid([txhash])
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print(r['error']['message'])

    @allure.feature('Test getcontractaccountinfo')
    def test_getcontractaccountinfo(self):
        """
        test getcontractaccountinfo when：
        regid is：空、正确、错误
        addr is：空、正确、错误
        """
        contract = Contract(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)

        fee = self.deployfee*2
        addr = account.getnewaddrwithmatureregid(self.init, fee)
        assert addr is not None
        params = [addr, self.contracturl, fee]
        status, response = contract.submitcontractdeploytx(params)
        txhash = ''
        for r in response:
            print(response)
            assert r['result'] is not None
            txhash = r['result']['txid']

        isConfirmed = contract.ishashconfirmed([txhash])
        assert isConfirmed

        regid = contract.getregidbytxhash([txhash])
        assert regid is not None

        # regid 正确，addr正确
        params = [regid, addr]
        status, response = contract.getcontractaccountinfo(params)
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None

        # regid为空，addr正确
        params = ["", addr]
        status, response = contract.getcontractaccountinfo(params)
        for r in response:
            assert r['result'] is None
            assert "invalid contract regid" in r['error']['message']

        # regid 错误，addr正确
        params = ["0", addr]
        status, response = contract.getcontractaccountinfo(params)
        for r in response:
            assert r['result'] is None
            assert "invalid contract regid" in r['error']['message']

        # regid正确，addr错误
        params = [regid, "0"]
        status, response = contract.getcontractaccountinfo(params)
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None

        # regid正确，addr为空
        params = [regid, ""]
        status, response = contract.getcontractaccountinfo(params)
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None

    @allure.feature('Test listcontracts')
    @pytest.mark.parametrize('test_param', isMine, indirect=True)
    def test_listcontracts(self, test_param):
        """
        test listcontracts when:
        showDetail is : True、False、空
        """
        contract = Contract(self.host, self.rpcuser, self.rpcpassword)
        status, response = contract.listcontracts(test_param)
        assert status == 200
        for r in response:
            if test_param[0] is True:
                assert r['error'] is None
                assert r['result'] is not None
            elif test_param[0] is False:
                assert r['error'] is None
                assert r['result'] is not None
            else:
                assert r['error']['code'] == -1
                assert r['error']['message'] is not None

    @allure.feature('Test getcontractinfo')
    def test_getcontractinfo(self):
        """
        test getcontractinfo when:
        regid is : 正确、错误、空
        """
        contract = Contract(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        # 获取信息正确
        fee = self.deployfee*2
        addr = account.getnewaddrwithmatureregid(self.init, fee)
        assert addr is not None
        params = [addr, self.contracturl, fee]
        status, response = contract.submitcontractdeploytx(params)
        txhash = ''
        for r in response:
            txhash = r['result']['txid']

        isConfirmed = contract.ishashconfirmed([txhash])
        assert isConfirmed

        regid = contract.getregidbytxhash([txhash])
        assert regid is not None

        status, response = contract.getcontractinfo([regid])
        print(regid, 123, response)
        assert status == 200
        for r in response:
            assert r['error'] is None
            assert r['result']['contract_regid'] is not None

        # 获取信息失败
        regid = '0-1'
        status, response = contract.getcontractinfo([regid])
        assert status == 200
        for r in response:
            assert r['result'] is None
            assert "Invalid contract regid" in r['error']['message']

        # regid为空
        regid = ''
        status, response = contract.getcontractinfo([regid])
        assert status == 200
        for r in response:
            assert r['result'] is None
            assert "Invalid contract regid" in r['error']['message']

    @allure.feature('Test submitcontractdeploytx')
    def test_submitcontractdeploytx_normal(self):
        """
        test submitcontractdeploytx when normal
        """
        contract = Contract(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)

        # 地址发布合约正确
        fee = self.deployfee*2
        contracturl = self.contracturl
        addr = account.getEnoughCoinMatureAddress(fee)
        params = [addr, contracturl, fee]
        status, response = contract.submitcontractdeploytx(params)
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['txid'] is not None
        sleep(6)

    @allure.feature('Test submitcontractdeploytx')
    @pytest.mark.parametrize('test_param', unnormal_fee, indirect=True)
    def test_submitcontractdeploytx_fee_unnormal(self, test_param):
        """
        test submitcontractdeploytx when fee is unnormal
        """
        contract = Contract(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)

        contracturl = self.contracturl
        fee = self.deployfee
        addr = account.getEnoughCoinMatureAddress(fee)
        params = [addr, contracturl, test_param]
        status, response = contract.submitcontractdeploytx(params)
        for r in response:
            assert r['result'] is None
            assert r['error']['message'] is not None
            print(r['error']['message'])

    @allure.feature('Test submitcontractdeploytx')
    @pytest.mark.parametrize('test_param', unnormal_addr, indirect=True)
    def test_submitcontractdeploytx_addrees_unnormal(self, test_param):
        """
        test submitcontractdeploytx when addr is unnormal
        """
        contract = Contract(self.host, self.rpcuser, self.rpcpassword)

        contracturl = self.contracturl
        fee = self.deployfee
        params = [test_param, contracturl, fee]
        status, response = contract.submitcontractdeploytx(params)
        for r in response:
            assert r['result'] is None
            assert r['error']['message'] is not None
            print(r['error']['message'])

    @allure.feature('Test submitcontractdeploytx')
    def test_submitcontractdeploytx_unnormal(self):
        """
        test submitcontractdeploytx when：
        文件路径错误, 账户余额不足
        """
        contract = Contract(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)

        fee = self.deployfee
        contracturl = self.contracturl
        addr = account.getEnoughCoinMatureAddress(self.deployfee)
        balance = account.gettokenbyaddr(addr,"WICC")

        # 文件路径错误
        contracturlErr = '/root/helloworld.lua'
        params = [addr, contracturlErr, fee]
        status, response = contract.submitcontractdeploytx(params)
        for r in response:
            assert r['result'] is None
            assert "Lua Script file not exist" in r['error']['message']

        # 账户余额不足
        params = [addr, contracturl, balance+10000]
        status, response = contract.submitcontractdeploytx(params)
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Account does not have enough WICC" in r['error']['message']

    @allure.feature('Test callcontracttx')
    @pytest.mark.parametrize('test_param', unnormal_fee, indirect=True)
    def test_callcontracttx_fee_unnormal(self, test_param):
        """
        test callcontracttx when addr is unnormal
        """
        sleep(6)
        contract = Contract(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)

        # 发布智能合约
        creater = account.getEnoughCoinMatureAddress(self.deployfee*2)
        assert creater is not None
        params = [creater, self.contracturl, self.deployfee*2]
        status, response = contract.submitcontractdeploytx(params)
        print("deploy res", response)
        for r in response:
            assert r['result']['txid'] is not None
            hash = r['result']['txid']
            logger.info(hash)
        assert contract.ishashconfirmed([hash])
        regid = contract.getregidbytxhash([hash])
        assert regid is not None
        logger.info(regid)

        arguments = "f0170000"
        # fee小于0
        params = [creater, regid, arguments, 10, test_param]
        status, response = contract.callcontracttx(params)
        for r in response:
            assert r['error'] is not None
            assert r['result'] is None
            print(r['error']['message'])

    @allure.feature('Test callcontracttx')
    def test_callcontracttx(self):
        """
        test callcontracttx when:
        userregid is: 正确、错误、regid为空
        amount is : 小于0、等于0
        arguments is ：空
        """
        sleep(6)
        contract = Contract(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)

        # 发布智能合约
        createfee = self.deployfee*2
        creater = account.getEnoughCoinMatureAddress(createfee)
        assert creater is not None
        params = [creater, self.contracturl, createfee]
        status, response = contract.submitcontractdeploytx(params)
        print("deploy res",response)
        for r in response:
            assert r['error'] is None
            assert r['result']['txid'] is not None
            hash = r['result']['txid']
            logger.info(hash)
        assert contract.ishashconfirmed([hash])

        regid = contract.getregidbytxhash([hash])
        assert regid is not None
        logger.info(regid)

        arguments = "f0170000"
        fee = self.callfee
        addr = account.newaddrwithWICC(self.init, fee*3)
        # 正常调用情况
        params = [addr, regid, arguments, 0, fee]
        status, response = contract.callcontracttx(params)
        for r in response:
            assert r['error'] is None
            assert r['result']['txid'] is not None

        # 维基币数量小于0
        params = [addr, regid, arguments, -1, fee]
        status, response = contract.callcontracttx(params)
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Invalid amount" in r['error']['message']

        # 合约命令为空
        params = [addr, regid, "", 0, fee]
        status, response = contract.callcontracttx(params)
        for r in response:
            assert r['result'] is None
            assert "Param length error (<4)" in r['error']['message']

        # userregid错误
        params = ['0-0', arguments, regid, 0, fee]
        status, response = contract.callcontracttx(params)
        for r in response:
            assert r['result'] is None
            assert "Invalid address" in r['error']['message']

        # user地址没有regid
        addr = account.newaddress()
        params = [addr, regid, arguments, 0, fee]
        status, response = contract.callcontracttx(params)
        for r in response:
            assert r['result'] is None
            assert "account not exists" in r['error']['message']

    @allure.feature('Test getcontractdata')
    def test_getcontractdata(self):
        """
        test getcontractdata when:
        regid is : 正确、错误、空
        key is ：正确、错误、空
        """
        sleep(6)
        contract = Contract(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)

        # 发布智能合约
        # new address
        fee = self.deployfee*2
        addr = account.getEnoughCoinMatureAddress(fee)
        params = [addr, self.contracturl, fee]
        status, response = contract.submitcontractdeploytx(params)
        print("deploy res:",response)
        for r in response:
            assert r['result']['txid'] is not None
            hash = r['result']['txid']
            logger.info(hash)

        assert contract.ishashconfirmed([hash])

        regid = contract.getregidbytxhash([hash])
        assert regid is not None
        logger.info(regid)

        # callcontracttx
        arguments = "f0170000"
        fee = self.callfee
        params = [addr, regid, arguments, 0, fee]
        status, response = contract.callcontracttx(params)
        for r in response:
            assert r['error'] is None
            assert r['result']['txid'] is not None
        sleep(6)

        # 正确的regid，正确key
        params = [regid, self.key]
        status, response = contract.getcontractdata(params)
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            logger.info(r['result']['value'])

        status2, response2 = contract.getcontractdata([regid, self.key2, True])
        for r in response2:
            assert r['error'] is None
            assert r['result'] is not None
            logger.info(r['result']['value'])

        # 错误key，正确regid
        key = '.'
        status, response = contract.getcontractdata([regid, key, True])
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Failed to acquire contract data" in r['error']['message']

        status, response = contract.getcontractdata([regid, key, True])
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Failed to acquire contract data" in r['error']['message']

        # key为空，正确regid
        key = ''
        status, response = contract.getcontractdata([regid, key])
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Failed to acquire contract data" in r['error']['message']
        status, response = contract.getcontractdata([regid, key, True])
        for r in response:
            assert r['result'] is None
            assert "Failed to acquire contract data" in r['error']['message']

        # 错误的regid，正确key
        regid = '0-1'
        status, response = contract.getcontractdata([regid, self.key])
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Failed to acquire contract data" in r['error']['message']
        status, response = contract.getcontractdata([regid, self.key2, True])
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Failed to acquire contract data" in r['error']['message']

        # regid为空，正确key
        regid = ''
        status, response = contract.getcontractdata([regid, self.key])
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Invalid contract regid" in r['error']['message']
        status, response = contract.getcontractdata([regid, self.key2, True])
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Invalid contract regid" in r['error']['message']

    @allure.feature('Test contract WRC20')
    def test_contract_WRC20(self):
        url = "/tmp/lua/WRC20.lua"
        argument = "f0110000"
        contract = Contract(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        sender = account.getEnoughCoinMatureAddress((self.callfee+self.deployfee)*2)
        assert sender is not None
        # 发布智能合约
        params = [sender, url, self.deployfee*2]
        print(params)
        status, response = contract.submitcontractdeploytx(params)
        print(response)
        assert status == 200
        cont_id = ""
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None
            cont_id = r['result']['txid']
        sleep(10)

        # 查询智能合约regid
        status, response = contract.getcontractregid([cont_id])
        assert status == 200
        cont_regid = ""
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['regid'] is not None
            cont_regid = r['result']['regid']

        # 调用合约对代币进行激活
        params = [sender, cont_regid, argument, 0, self.callfee]
        status, response = contract.callcontracttx(params)
        assert status == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None

        sleep(10)
        # 获取合约owner
        params = [cont_regid, "owner", False]
        status, response = contract.getcontractdata(params)
        assert status == 200
        owner = ""
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['value'] is not None
            owner = r['result']['value']

        # 检查合约owner代币总额
        params = [cont_regid, owner]
        status, response = contract.getcontractaccountinfo(params)
        assert status == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['free_value'] is not None

        # address = 0-2 wNPWqv9bvFCnMm1ddiQdH7fUwUk2Qgrs2N
        # 774e447565316a4863675253696f53444c346f31417a587a3344373267434d6b5036
        # 对代币进行转账
        params = [owner, cont_regid,
                  "f0160000774e447565316a4863675253696f53444c346f31417a587a3344373267434d6b50360010a5d4e8000000",
                  0, self.callfee]
        status, response = contract.callcontracttx(params)
        assert status == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None
        sleep(10)

        # 确认接收者代币是否到账
        params = [cont_regid, "wNDue1jHcgRSioSDL4o1AzXz3D72gCMkP6"]
        status, response = contract.getcontractaccountinfo(params)
        assert status == 200
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['free_value'] is not None
            assert r['result']['free_value'] == 1000000000000

    @allure.feature('Test contract')
    def test_contract_multi_transfer_normal(self):
        """
        test contract when: 合约多币种转账
        """
        url = "/tmp/lua/send.lua"
        argument = "f001"
        contract = Contract(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)

        sender = account.getEnoughCoinMatureAddress((self.callfee+self.deployfee)*2)
        assert sender is not None
        # 发布智能合约
        params = [sender, url, self.deployfee*2]
        print(params)
        status, response = contract.submitcontractdeploytx(params)
        print(response)
        assert status == 200
        cont_id = ""
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None
            cont_id = r['result']['txid']
        sleep(10)

        # 查询智能合约regid
        status, response = contract.getcontractregid([cont_id])
        assert status == 200
        cont_regid = ""
        for r in response:
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['regid'] is not None
            cont_regid = r['result']['regid']

        params = ["wLKf2NqwtHk3BfzK5wMDfbKYN1SC3weyR4", cont_regid, "f001", "WGRT:0", str(self.callfee)]
        print(params)
        status, response = contract.submituniversalcontractcalltx(params)
        for r in response:
            print("call res:",response)
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None
        sleep(6)

        # address = 0-2  wNDue1jHcgRSioSDL4o1AzXz3D72gCMkP6
        # 774e447565316a4863675253696f53444c346f31417a587a3344373267434d6b5036
        # 多币种转账 WGRT
        recieve = "wNDue1jHcgRSioSDL4o1AzXz3D72gCMkP6"
        start_amount = account.gettokenbyaddr(recieve, "WGRT")
        amount = 10000
        params = ["wLKf2NqwtHk3BfzK5wMDfbKYN1SC3weyR4", cont_regid, "f002774e447565316a4863675253696f53444c346f31417a587a3344373267434d6b5036", "WGRT:"+str(amount), str(self.callfee)]
        print(params)
        status, response = contract.submituniversalcontractcalltx(params)
        for r in response:
            print("call res:", response)
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None
        sleep(10)
        end_amount = account.gettokenbyaddr(recieve, "WGRT")
        assert end_amount-start_amount == amount

        # ["wLKf2NqwtHk3BfzK5wMDfbKYN1SC3weyR4", "178993-2",
        #  "f0037757667574384359617a7a4476656341346e716a6d357073434e38376b6453423163", "QWERTD:42345",
        #  "1000000"], "id": 345799772}
        recieve = "wNDue1jHcgRSioSDL4o1AzXz3D72gCMkP6"
        start_amount = account.gettokenbyaddr(recieve, "WGRT")
        params = ["wLKf2NqwtHk3BfzK5wMDfbKYN1SC3weyR4", cont_regid, "f003774e447565316a4863675253696f53444c346f31417a587a3344373267434d6b5036", "WGRT:"+str(amount), str(self.callfee)]
        print(params)
        status, response = contract.submituniversalcontractcalltx(params)
        for r in response:
            print("call res:", response)
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None
        sleep(10)
        end_amount = account.gettokenbyaddr(recieve, "WGRT")
        assert end_amount-start_amount == amount

        recieve1 = "wNDue1jHcgRSioSDL4o1AzXz3D72gCMkP6"
        recieve2 = "wNuJM44FPC5NxearNLP98pg295VqP7hsqu"
        start_amount1 = account.gettokenbyaddr(recieve1, "WGRT")
        start_amount2 = account.gettokenbyaddr(recieve2, "WGRT")
        # 0-3 wNuJM44FPC5NxearNLP98pg295VqP7hsqu 774e754a4d3434465043354e786561724e4c50393870673239355671503768737175
        params = ["wLKf2NqwtHk3BfzK5wMDfbKYN1SC3weyR4", cont_regid, "f004774e447565316a4863675253696f53444c346f31417a587a3344373267434d6b5036774e754a4d3434465043354e786561724e4c50393870673239355671503768737175", "WGRT:"+str(amount), str(self.callfee)]
        print(params)
        status, response = contract.submituniversalcontractcalltx(params)
        for r in response:
            print("call res:", response)
            assert r['result'] is not None
            assert r['error'] is None
            assert r['result']['txid'] is not None
        sleep(10)
        end_amount1 = account.gettokenbyaddr(recieve1, "WGRT")
        end_amount2 = account.gettokenbyaddr(recieve2, "WGRT")
        assert end_amount1-start_amount1 == amount/2
        assert end_amount2-start_amount2 == amount/2

if __name__ == '__main__':
    pytest.main(['-s', 'contract_test.py::TestContract::test_contract_multi_transfer_normal'])
