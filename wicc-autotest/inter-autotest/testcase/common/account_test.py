from time import sleep
import pytest
import allure
from utils.log import mylog
from utils.read_property import ReadProperty
from wiccsdk.account import Account
from wiccsdk.contract import Contract
from wiccsdk.transaction import Transaction
from wiccsdk.wallet import Wallet

logger = mylog(__name__).getlog()

# test_getnewaddress
isMine = [[True], [False]]
unnormal_fee = [None, 'invalid', 0, -1000, 999, 10000000000000000]

class TestAccount(object):
    @pytest.fixture(scope="class", autouse=True)
    def setup(self):
        prop = ReadProperty()
        # env = ReadEnv()
        # host = 'http://' + env.read('ip') + ":" + str(env.read('port'))
        # rpcuser = env.read('username')
        # rpcpassword = env.read('password')
        # walletpassword = prop.read('common', 'walletpassword')
        host = prop.read('common','host')
        rpcuser = prop.read('common','rpcuser')
        rpcpassword = prop.read('common','rpcpassword')
        walletpassword = prop.read('common','walletpassword')

        wallet = Wallet(host, rpcuser, rpcpassword)
        isLocked = wallet.islocked()
        # 解锁钱包
        if isLocked:
            status, response = wallet.walletpassphrase([walletpassword, 360000])
            assert status == 200
        else:
            pass

        assert not wallet.islocked()
        logger.info('class setup')

    @pytest.fixture(scope="class", autouse=True)
    def teardown(self, request):
        def fin():
            logger.info('class tear down')
        request.addfinalizer(fin)

    @pytest.fixture('function', autouse=True)
    def setup_function(self):
        # env = ReadEnv()
        # self.host = 'http://' + env.read('ip') + ":" + str(env.read('port'))
        # self.rpcuser = env.read('username')
        # self.rpcpassword = env.read('password')

        prop = ReadProperty()
        self.host = prop.read('common', 'host')
        self.rpcuser = prop.read('common', 'rpcuser')
        self.rpcpassword = prop.read('common', 'rpcpassword')
        self.contracturl = prop.read('common', 'contracturl')
        self.init = prop.read('common', 'init_addr')
        self.risk = prop.read('common', 'risk_addr')
        self.riskamount = prop.read('common', 'risk_amount')
        self.registerfee = int(prop.read('fee', 'register'))
        self.sendfee = int(prop.read('fee', 'send'))
        self.deployfee = int(prop.read('fee', 'contractdeploy'))

        logger.info('method setup')

    @pytest.fixture('function', autouse=True)
    def teardown_function(self):
        logger.info('method tear down')

    @pytest.fixture(scope='function')
    def test_param(self, request):
        return request.param

    @allure.feature('Test send')
    def test_creation_address_send(self):
        """
        @author:elaine.tan
        Test creation address H-1
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        tran = Transaction(self.host, self.rpcuser, self.rpcpassword)
        address = self.risk
        # 风险备用金余额验证
        amount = account.gettokenbyaddr(address, "WUSD")
        assert amount == int(self.riskamount)
        # 不允许转账
        status, response = tran.send([address, self.init, "WUSD:100000", "WUSD:"+str(self.sendfee)])
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Sender address not found in wallet" in r['error']['message']

    @allure.feature('Test listaddr')
    def test_listaddr(self):
        """
        test listaddr:查询当前节点地址列表信息
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        params = []
        status_code, response = account.listaddr(params)
        assert status_code == 200
        if status_code == 200:
            for r in response:
                assert r['error'] is None
                assert len(r['result']) > 0

    @allure.feature('Test getaccountinfo')
    def test_getaccountinfo(self):
        """
        Test getaccountinfo ：获取普通账户地址详情
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)

        addr = account.newaddress()
        assert addr is not None
        logger.info(addr)

        status_code, response = account.getaccountinfo([addr])
        assert status_code == 200
        if status_code == 200:
            for r in response:
                assert r['error'] is None
                assert r['result']['address'] is not None
                assert r['result']['position'] is not None
                assert r['result']['position'] == 'inwallet'
                assert r['result']['owner_pubkey'] is not None

        # send to address
        balance = self.sendfee + self.registerfee
        params = [self.init,addr, balance, self.sendfee]
        res = account.send(params)
        assert res

        isInBlock = account.isInBlock([addr])
        assert isInBlock

        status_code, response = account.getaccountinfo([addr])
        assert status_code == 200
        if status_code == 200:
            for r in response:
                assert r['error'] is None
                assert r['result']['position'] == 'inblock'

        params = [addr, self.registerfee]
        status_code, response = account.registeraccounttx(params)
        assert status_code == 200

        isRegister = account.isRegistered([addr])
        assert isRegister

        regId = ''
        status_code, response = account.getaccountinfo([addr])
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result']['position'] == 'inblock'
            regId = r['result']['regid']

        status_code, response = account.getaccountinfo([regId])
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result']['regid'] == regId

    @allure.feature('Test getaccountinfo')
    def test_getaccountinfo_contract(self):
        """
        Test getaccountinfo：获取合约账户地址详情
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        contract = Contract(self.host, self.rpcuser, self.rpcpassword)

        fee = self.deployfee*2
        addr = account.getEnoughCoindRegisteredAddress(fee)
        assert addr is not None

        params = [addr, self.contracturl, fee]
        hash = ''
        status_code, response = contract.submitcontractdeploytx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            hash = r['result']['txid']

        sleep(10)
        regId = ''
        status_code, response = contract.getcontractregid([hash])
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            regId = r['result']['regid']

        status_code, response = account.getaccountinfo([regId])
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['regid'] == regId

    @allure.feature('Test getnewaddress')
    @pytest.mark.parametrize('test_param', isMine, indirect=True)
    def test_getnewaddress(self, test_param):
        """
        Test getnewaddress：创建新地址
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        status_code, response = account.getnewaddress(test_param)
        assert status_code == 200
        if status_code == 200:
            for r in response:
                assert r['error'] is None
                if test_param[0]:
                    assert r['result']['minerpubkey'] != "null"
                else:
                    assert r['result']['minerpubkey'] == "null"
                logger.info('minerpubkey is:' + r['result']['minerpubkey'])

    @allure.feature('Test registeraccounttx_normal')
    def test_registeraccounttx_normal(self):
        """
        Test register account normal：激活账户
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)

        addr = account.newaddress()
        assert addr is not None
        logger.info(addr)

        # send to address
        params = [self.init, addr, str(self.registerfee), self.sendfee]
        res = account.send(params)
        assert res

        isInBlock = account.isInBlock([addr])
        assert isInBlock

        params = [addr, self.registerfee]
        status_code, response = account.registeraccounttx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result']['txid'] != ''

        isRegister = account.isRegistered([addr])
        assert isRegister

        status_code,response = account.getaccountinfo([addr])
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            assert r['result']['regid'] is not None
            assert '-0' not in r['result']['regid']

    @allure.feature('Test registeraccounttx')
    def test_registeraccounttx_addr_unregister(self):
        """
        Test register account, address is unregistered
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)

        addr = account.newaddress()
        assert addr is not None
        logger.info(addr)

        # address is in wallet, so it cannot be registered
        params = [addr, self.registerfee]
        status_code, response = account.registeraccounttx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None

    @allure.feature('Test registeraccounttx')
    def test_registeraccounttx_addr_none(self):
        """
        Test register account, address is none
        """
        addr = None
        params = [addr, self.registerfee]
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        status_code, response = account.registeraccounttx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert 'Expected type str, got null' in r['error']['message']

    @allure.feature('Test registeraccounttx')
    def test_registeraccounttx_addr_invalid(self):
        """
        Test register account, address is invalid
        """
        # address is invalid, so it cannot be registered
        addr = 'invalid'
        params = [addr, self.registerfee]
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        status_code, response = account.registeraccounttx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert 'Invalid address' in r['error']['message']

    @allure.feature('Test registeraccounttx_Registered')
    def test_registeraccounttx_addr_registered(self):
        """
        Test register account, addr is registered
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)

        addr = account.newaddress()
        assert addr is not None
        logger.info(addr)

        # send to address
        balance = self.sendfee + self.registerfee*2
        params = [self.init, addr, balance, self.sendfee]
        res = account.send(params)
        assert res

        isInBlock = account.isInBlock([addr])
        assert isInBlock

        params = [addr, self.registerfee]
        status_code, response = account.registeraccounttx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result']['txid'] != ''

        isRegister = account.isRegistered([addr])
        assert isRegister

        params = [addr, self.registerfee]
        status_code, response = account.registeraccounttx(params)

        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert 'Account was already registered' in r['error']['message']

    @allure.feature('Test registeraccounttx')
    @pytest.mark.parametrize('test_param', unnormal_fee, indirect=True)
    def test_registeraccounttx_fee_unnormal(self, test_param):
        """
        test registeraccounttx when fee is unnormal
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)

        addr = account.newaddress()
        assert addr is not None
        res = account.send([self.init, addr, self.registerfee, self.sendfee])
        assert res
        sleep(6)
        logger.info(addr)

        params = [addr, test_param]
        status_code, response = account.registeraccounttx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print(r['error']['message'])

    @allure.feature('Test registeraccounttx_except')
    def test_registeraccounttx_fee_empty(self):
        """
        test registeraccounttx when fee is empty
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)

        addr = account.newaddress()
        assert addr is not None
        res = account.send([self.init, addr, self.registerfee, self.sendfee])
        assert res
        sleep(6)
        logger.info(addr)

        # fee is empty
        params = [addr]
        status_code, response = account.registeraccounttx(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result']['txid'] is not None

    @allure.feature('Test dumpprivkey_miner')
    @pytest.mark.parametrize('test_param', isMine, indirect=True)
    def test_dumpprivkey(self, test_param):
        """
        Test dumpprivkey for miner address：矿工标识
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        addr = account.newaddress(test_param)
        status_code, response = account.dumpprivkey([addr])
        assert status_code == 200
        if status_code == 200:
            for r in response:
                assert r['error'] is None
                assert r['result']['privkey'] != ''
                if test_param[0]:
                    assert r['result']['minerkey'] != ' '
                else:
                    assert r['result']['minerkey'] == "null"

    @allure.feature('Test dumpprivkey is registered or not')
    @pytest.mark.parametrize('test_param', isMine, indirect=True)
    def test_dumpprivkey_registered(self, test_param):
        """
        Test dumpprivkey is registered or not
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        addr = account.newaddress(test_param)

        # send to address
        balance = self.sendfee+self.registerfee
        params = [self.init,addr, balance, self.sendfee]
        res = account.send(params)
        assert res
        # 未激活地址
        isInBlock = account.isInBlock([addr])
        assert isInBlock

        status_code, response = account.dumpprivkey([addr])
        assert status_code == 200
        if status_code == 200:
            for r in response:
                assert r['error'] is None
                assert r['result']['privkey'] != ''
                if test_param[0]:
                    assert r['result']['minerkey'] != ' '
                else:
                    assert r['result']['minerkey'] == "null"
        # 已激活地址
        account.registeraccounttx([addr, self.registerfee])
        isRegister = account.isRegistered([addr])
        assert isRegister

        status_code, response = account.dumpprivkey([addr])
        assert status_code == 200
        if status_code == 200:
            for r in response:
                assert r['error'] is None
                assert r['result']['privkey'] != "null"

    @allure.feature('Test dumpprivkey invalid address')
    def test_dumpprivkey_invalid(self):
        """
            Test dumpprivkey with invalid address
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        addr = 'invalid'
        status_code, response = account.dumpprivkey([addr])
        assert status_code == 200
        if status_code == 200:
            for r in response:
                assert r['error'] is not None
                assert r['result'] is None
                assert 'Invalid address' in r['error']['message']

    @allure.feature('Test importprivkey normal')
    def test_importprikey_normal(self):
        """
        Test  importprivkey normal
        """
        wallet = Wallet(self.host,self.rpcuser,self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        assert not wallet.islocked()

        # new address
        addr = account.newaddress()
        privkey = account.getdumpprivkey([addr])
        assert privkey is not None

        # 空标签
        params = [privkey]
        status,response = account.importprivkey(params)
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['imported_key_address'] is not None

        # # 重新扫描交易
        # params = [privkey, "", "True"]
        # status, response = account.importprivkey(params)
        # for r in response:
        #     assert r['error'] is None
        #     assert r['result'] is not None
        #     assert r['result']['imported_key_address'] is not None
        #
        # # 不重新扫描交易
        # params = [privkey, "", "False"]
        # status, response = account.importprivkey(params)
        # for r in response:
        #     assert r['error'] is None
        #     assert r['result'] is not None
        #     assert r['result']['imported_key_address'] is not None

    @allure.feature('Test validateaddress')
    def test_validateaddress(self):
        """
        Test validate address contain normal, registered ,regid and contract address
        """
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        addr = account.newaddress()

        # inwallet address
        status_code, response = account.validateaddress([addr])
        assert status_code == 200
        for r in response:
            assert r['result']['is_valid']

        isSent = account.send([self.init, addr, self.registerfee, self.sendfee])
        assert isSent

        # inblock address
        isInBlock = account.isInBlock([addr])

        if isInBlock:
            status_code, response = account.validateaddress([addr])
            assert status_code == 200
            for r in response:
                assert r['result']['is_valid']

        status_code, response = account.registeraccounttx([addr, self.registerfee])
        assert status_code == 200

        # registered address
        isRegistered = account.isRegistered([addr])
        if isRegistered:
            status_code, response = account.validateaddress([addr])
            assert status_code == 200
            for r in response:
                assert r['result']['is_valid']

        # regid
        regId = ''
        status_code, response = account.getaccountinfo([addr])
        assert status_code == 200
        for r in response:
            regId = r['result']['regid']

        status_code, response = account.validateaddress([regId])
        assert status_code == 200
        for r in response:
            assert r['result']['is_valid']

        # invalid address
        invalidAddr = 'invalid'
        status_code, response = account.validateaddress([invalidAddr])
        assert status_code == 200
        for r in response:
            assert not r['result']['is_valid']

        # contract address
        fee = self.deployfee*2
        addr = account.getnewaddrwithmatureregid(self.init, fee)
        assert addr is not None

        params = [addr, self.contracturl, fee]
        contract = Contract(self.host, self.rpcuser, self.rpcpassword)
        hash = ''
        status_code,response = contract.submitcontractdeploytx(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            hash = r['result']['txid']

        sleep(10)
        regId = ''
        status_code,response = contract.getcontractregid([hash])
        assert status_code == 200
        for r in response:
            assert r['result'] is not None
            regId = r['result']['regid']

        status_code, response = account.validateaddress([regId])
        assert status_code == 200
        for r in response:
            assert r['result']['is_valid']


if __name__ == '__main__':
    pytest.main(['-s', 'account_test.py::TestAccount::test_creation_address_send'])
