import os
import time

import allure
import pytest
from utils.log import mylog
from utils.read_env import ReadEnv
from utils.read_property import ReadProperty
from wiccsdk.account import Account
from wiccsdk.contract import Contract
from wiccsdk.transaction import Transaction
from wiccsdk.wallet import Wallet

logger = mylog(__name__).getlog()


class TestWallet(object):
    @pytest.fixture(scope="class", autouse=True)
    def setup(self):
        prop = ReadProperty()
        # env = ReadEnv()
        # host = 'http://' + env.read('ip') + ":" + str(env.read('port'))
        # rpcuser = env.read('username')
        # rpcpassword = env.read('password')
        # walletpassword = prop.read('common', 'walletpassword')
        # contracturl = prop.read('common', 'contracturl')

        host = prop.read('common', 'host')
        rpcuser = prop.read('common', 'rpcuser')
        rpcpassword = prop.read('common', 'rpcpassword')
        walletpassword = prop.read('common', 'walletpassword')
        contracturl = prop.read('common', 'contracturl')

        wallet = Wallet(host, rpcuser, rpcpassword)
        isEncrypted = wallet.isencrypted()

        # 加密钱包，加密后重启容器
        if not isEncrypted:
            isEncrypted = wallet.encryptandrestart(walletpassword)
            assert isEncrypted
            # wallet.encryptwallet(self.walletpassword)
        else:
            pass
        assert wallet.isencrypted()

        logger.info('class setup')

    @pytest.fixture(scope="class", autouse=True)
    def teardown(self, request):
        def fin():
            logger.info('class tear down')
        request.addfinalizer(fin)

    @pytest.fixture('function', autouse=True)
    def setup_function(self):
        prop = ReadProperty()
        # env = ReadEnv()
        # self.host = 'http://' + env.read('ip') + ":" + str(env.read('port'))
        # self.rpcuser = env.read('username')
        # self.rpcpassword = env.read('password')
        # self.walletpassword = prop.read('common', 'walletpassword')
        # self.contracturl = prop.read('common', 'contracturl')

        self.host = prop.read('common', 'host')
        self.rpcuser = prop.read('common', 'rpcuser')
        self.rpcpassword = prop.read('common', 'rpcpassword')
        self.walletpassword = prop.read('common', 'walletpassword')
        self.contracturl = prop.read('common', 'contracturl')
        self.init = prop.read('common', 'init_addr')
        self.sendfee = int(prop.read('fee', 'send'))
        wallet = Wallet(self.host, self.rpcuser, self.rpcpassword)
        isLocked = wallet.islocked()
        # 解锁钱包
        if isLocked:
            status, response = wallet.walletpassphrase([self.walletpassword, 360000])
            assert status == 200
        else:
            pass
        assert not wallet.islocked()

        logger.info('method setup')

    @pytest.fixture('function', autouse=True)
    def teardown_function(self):
        logger.info('method tear down')

    @pytest.fixture(scope='function')
    def test_param(self, request):
        return request.param

    @allure.feature('Test encryptwallet')
    def test_encryptwallet(self):
        """
        test encryptwallet when:
        钱包已被加密过
        """
        wallet = Wallet(self.host, self.rpcuser, self.rpcpassword)
        status_code, response = wallet.encryptwallet([self.walletpassword])
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Wallet was already encrypted and shall not be encrypted again" in r['error']['message']

    @allure.feature('Test walletpassphrase')
    def test_walletpassphrase(self):
        """
        test walletpassphrase when:
        密码正确、密码错误、持续时间（持续时间内未被锁、超过持续时间被锁）
        """
        wallet = Wallet(self.host, self.rpcuser, self.rpcpassword)
        # lock wallet
        wallet.walletlock()
        isLocked = wallet.islocked()
        assert isLocked

        # 密码错误
        walletpassword = self.walletpassword
        password = walletpassword.join('00')
        params = [password, 3600]
        status_code, response = wallet.walletpassphrase(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "The wallet passphrase entered was incorrect" in r['error']['message']

        # 密码正确
        params = [self.walletpassword, 30]
        status_code, response = wallet.walletpassphrase(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['wallet_unlocked'] is True

        # 持续时间
        time.sleep(10)
        assert not wallet.islocked()

        time.sleep(30)
        assert wallet.islocked()

    @allure.feature('Test walletlock')
    def test_walletlock(self):
        """
        test walletlock when:
        钱包未锁（已加密）
        """
        wallet = Wallet(self.host, self.rpcuser, self.rpcpassword)

        # 未锁
        status_code, response = wallet.walletlock()
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result']['wallet_lock'] is True

    @allure.feature('Test walletpassphrasechange')
    def test_walletpassphrasechange(self):
        """
        test walletpassphrasechange:
        oldpassphrase is : 正确、错误、为空
        newpassphrase is : 为空、不为空
        """
        wallet = Wallet(self.host, self.rpcuser, self.rpcpassword)

        # 旧密码错误
        newpassword = '123456'
        params = [self.walletpassword.join('00'), newpassword]
        status_code, response = wallet.walletpassphrasechange(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "The wallet passphrase entered was incorrect" in r['error']['message']
            print(r['error'])

        # 旧密码为空
        newpassword = '123456'
        params = ['', newpassword]
        status_code, response = wallet.walletpassphrasechange(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print(r['error'])

        # 新密码为空
        params = [self.walletpassword, '']
        status_code, response = wallet.walletpassphrasechange(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            print(r['error'])

        # 旧密码正确且新密码不为空
        newpassword = '123456'
        params = [self.walletpassword, newpassword]
        status_code,response = wallet.walletpassphrasechange(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['chgpwd'] is True

        # 将密码改回来
        status_code,response = wallet.walletpassphrasechange([newpassword,self.walletpassword])
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['chgpwd'] is True

    @allure.feature('Test dumpwallet')
    def test_dumpwallet(self):
        """
        test dumpwallet when:
        正确路径、不存在的目录、包含文件的目录、有同名文件的目录
        """
        wallet = Wallet(self.host,self.rpcuser,self.rpcpassword)

        # 正确路径
        params = ["/root/dumpwallet"]
        status_code, response = wallet.dumpwallet(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            print(r['result']['info'])

        # 不存在的路径
        params = ["/root/dump/dumpwallet"]
        status_code, response = wallet.dumpwallet(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Failed to open dump file" in r['error']['message']

        # 同名文件
        params = ["/root/dumpwallet"]
        status_code, response = wallet.dumpwallet(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result']is not None
            print(r['result']['info'])

    @allure.feature('Test backupwallet')
    def test_backupwallet(self):
        """
        test backupwallet when:
        正确钱包文件、存在同名文件目录、不存在路径
        """
        wallet = Wallet(self.host,self.rpcuser,self.rpcpassword)
        # 正确路径
        params = ["/root/backupwallet"]
        status_code, response = wallet.backupwallet(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is None

        # 同名文件
        params = ["/root/backupwallet"]
        status_code, response = wallet.backupwallet(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is  None

        # 不存在路径
        params = ["/root/backup/backupwallet"]
        status_code, response = wallet.backupwallet(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Wallet backup failed" in r['error']['message']
            print(r['error'])

    @allure.feature('Test importwallet')
    def test_importwallet(self):
        """
        test  importwallet when:
        正确钱包文件、错误钱包文件、不存在的钱包文件
        """
        # dumpwallet
        params = ["/root/wallet"]
        wallet = Wallet(self.host, self.rpcuser, self.rpcpassword)
        status_code,response= wallet.dumpwallet(params)
        assert status_code == 200

        # 正确钱包文件
        status_code,response = wallet.importwallet(params)
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None

        # 错误的钱包文件（不能直接导入备份的钱包文件）
        params = ["/root/backwallet"]
        status_code, response = wallet.backupwallet(params)
        assert status_code == 200

        status_code, response = wallet.importwallet(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "value is type null, expected obj" in r['error']['message']
            print(r['error'])

        # 不存在的钱包文件
        params = ["/root/import/wallet"]
        status_code, response = wallet.importwallet(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Fail to open dump file" in r['error']['message']

    @allure.feature('Test getwalletinfo')
    def test_getwalletinfo(self):
        """
        test getwalletinfo when:
        存在未确认交易、存在已确认交易、钱包未锁
        """
        wallet = Wallet(self.host,self.rpcuser,self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)
        transaction = Transaction(self.host,self.rpcuser,self.rpcpassword)

        # 存在未确认交易
        addr = account.newaddress()
        status_code,response = transaction.send([self.init, addr, "10000", self.sendfee])
        assert status_code == 200
        status_code,response = wallet.getwalletinfo()
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['unconfirmed_tx_num'] is not 0

        # 存在已确认交易
        time.sleep(30)
        status_code, response = wallet.getwalletinfo()
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['coinfirmed_tx_num'] is not 0

        # 钱包未锁
        status_code, response = wallet.getwalletinfo()
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['wallet_locked'] is False

    @allure.feature('Test when wallet is locked')
    def test_wallet_locked(self):
        """
        test when wallet has been locked:
        已锁时锁定钱包、已锁时获取钱包信息、已锁时导出钱包、已锁时导入钱包、已锁时解锁钱包
        """
        wallet = Wallet(self.host,self.rpcuser,self.rpcpassword)
        # lock wallet
        status_code,response = wallet.walletlock()
        assert status_code == 200
        assert wallet.islocked()

        # 再次锁定
        status_code, response = wallet.walletlock()
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result']['wallet_lock'] is True

        # 获取钱包信息
        status_code, response = wallet.getwalletinfo()
        assert status_code == 200
        for r in response:
            assert r['error'] is None
            assert r['result'] is not None
            assert r['result']['wallet_locked'] is True

        # 导出钱包
        params = ["/root/wallet"]
        status_code, response = wallet.dumpwallet(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Please enter the wallet passphrase with walletpassphrase first" in r['error']['message']
            print(r['error'])

        # 导入钱包
        status_code, response = wallet.importwallet(params)
        assert status_code == 200
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Please enter the wallet passphrase with walletpassphrase first" in r['error']['message']
            print(r['error'])

    @allure.feature('Test importprivkey locked')
    def test_importprikey_locked(self):
        """
        Account模块
        Test importprikey when ：
        钱包已锁时将私钥导入钱包
        """
        wallet = Wallet(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)

        # dumpprivkey
        privkey = ''
        addr = account.newaddress()
        assert addr is not None
        status, response = account.dumpprivkey([addr])
        assert status == 200
        for r in response:
            assert r['result'] is not None
            privkey = r['result']['privkey']

        # lock wallet
        status,response = wallet.walletlock()
        assert status == 200
        isLocked = wallet.islocked()
        assert isLocked

        status, response = account.importprivkey([privkey])
        for r in response:
            assert r['result'] is None
            assert r['error'] is not None
            assert "Please enter the wallet passphrase with walletpassphrase first" in r['error']['message']

    @allure.feature('Test registercontracttx wallet locked')
    def test_registercontracttx_locked(self):
        """
        Contract 模块
        test registercontracttx when ：
        钱包已锁时发布智能合约
        """
        contract = Contract(self.host, self.rpcuser, self.rpcpassword)
        wallet = Wallet(self.host, self.rpcuser, self.rpcpassword)
        account = Account(self.host, self.rpcuser, self.rpcpassword)

        # lock wallet
        status, response = wallet.walletlock()
        assert status == 200
        isLocked = wallet.islocked()
        assert isLocked

        fee = 110000000
        addr = account.getEnoughCoindRegisteredAddress(fee)
        params = [addr, self.contracturl, fee]
        status, response = contract.submitcontractdeploytx(params)
        assert status == 200
        for r in response:
            assert r['error'] is not None
            assert 'Please enter the wallet passphrase with walletpassphrase first' in r['error']['message']


if __name__ == '__main__':
         pytest.main(['-s', 'wallet_test.py::TestWallet::test_getwalletinfo'])




