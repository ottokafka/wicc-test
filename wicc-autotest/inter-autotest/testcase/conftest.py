import pytest


def pytest_addoption(parser):
    parser.addoption('--env', action='store', default='test', help='myoption: test or pri')


@pytest.fixture
def env(request):
    return request.config.getoption('--env')
