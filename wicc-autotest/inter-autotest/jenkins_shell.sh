#!/bin/sh
echo '#####本次编译环境########'
echo '重新编译：'$rebuild', 环境：'$env', 结果通知：'$access_token', 用例：'$testcases

if [ $access_token = "prod" ]
then
	token="99a8ff828d93d6bb37898cdab3e48d81cf17144fba106b5329abcb2e17efbdb0"
else
	token="5b0a4339c12c901e43fa69bf7ac25d58b8f41517c70c1544f72f23f937fc31ce"
fi


# 配置要执行的用例
#Testcases="testcase/stablecoin"
#Testcases="testcase/stablecoin/feeding_transaction_test.py"
#Testcases="testcase"
Testcases=$testcases



# rebuild coind
if [ $rebuild = "True" ]
then
	# build_dir=/root/jenkins-workspace/wicc-test/wicc-operation/wicc-build
    # build
	# cd $build_dir
	# sh build-coind.sh

	cd ~
    if [ $env = "pri" ]
    then
    	sh re_run_pri.sh
    else
    	sh re_run_dev.sh
    fi
	#	# replace latest coind by test branch
	#	docker cp $build_dir/waykichain/coind waykicoind-pri:/opt/wicc/
	#	# restart coind
    #	docker stop waykicoind-pri
    #	docker start waykicoind-pri
    #else
    # 	docker cp $build_dir/waykichain/coind waykicoind-dev:/opt/wicc/
    #   # restart coind
    #	docker stop waykicoind-dev
    #	docker start waykicoind-dev
    #fi


fi

sleep 10

# ready test envirment
cd /root/jenkins-workspace/wicc-test/wicc-autotest/inter-autotest
git pull
. venv/bin/activate

# 拷贝智能合约到容器内部
docker cp  /root/workspace/wicc-pri/lua/ waykicoind-pri:/tmp/


# excute testcases with pytest
jenkins_workspace=/home/jenkins/workspace/wicc-sdk
lastest_report=/data/test_report/lastest_report
rm -rf $jenkins_workspace/allure_results
RUN_ENV=$env pytest $PWD/$Testcases --alluredir=$jenkins_workspace/allure_results
allure generate $jenkins_workspace/allure_results/ -o $lastest_report --clean

# parse allure report to statistic data
python aa.py \
	--history_file=$jenkins_workspace/allure-report/history/history-trend.json \
    --access_token=$token \
    --test_summary_file=$lastest_report/widgets/summary.json


exit 0