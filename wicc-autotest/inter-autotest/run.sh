#!/usr/bin/env bash
ENV=$1
[[ -z "$ENV" ]] && ENV=test

cd /root/jenkins-workspace/wicc-test/wicc-autotest/inter-autotest
#cd /root/workspace/jenkins/wicc-test/wicc-autotest/inter-autotest

git pull

source venv/bin/activate

# 拷贝智能合约到容器内部
docker cp /root/workspace/wicc-pri/lua/* waykicoind-pri:/tmp/lua/


curTime=$(date "+%Y%m%d_%H%M%S")
report_name=html_$curTime
echo $report_name > report_path.txt
rm -rf ./report/test/
RUN_ENV=$ENV pytest $PWD/testcase/normal --alluredir=$PWD/report/test/
allure generate $PWD/report/test/ -o $PWD/report/$report_name
python aa.py
mv $PWD/report/$report_name /data/test_report

#RUN_ENV=$ENV pytest -s ./testcase/ --alluredir=/home/jenkins/workspace/wicc-sdk/allure-results --junit-xml=/home/jenkins/workspace/wicc-sdk/junit-xml/test_result.xml
#RUN_ENV=$1 pytest -s ./testcase/wallet/ --alluredir=/home/jenkins/workspace/wicc-sdk/allure-results --junit-xml=/home/jenkins/workspace/wicc-sdk/junit-xml/test_result.xml

#RUN_ENV=$ENV pytest -s ./testcase/ --alluredir=/root/jenkins/workspace/wicc-sdk/allure-results --junit-xml=/home/jenkins/workspace/wicc-sdk/junit-xml/test_result.xml

exit 0