# -*- coding: utf-8 -*-
import base64
import json
import datetime
import time
import requests
import os
import configparser
import argparse
import re


def get_test_config(tag, key, cf_path):
    cf = configparser.ConfigParser()
    cf.read(cf_path)
    value = cf.get(tag, key)
    return value


def get_timestamp_datetime(timestamp):
    now = timestamp // 1000
    time_array = time.localtime(now)
    style_time = time.strftime("%Y-%m-%d %H:%M:%S", time_array)
    return style_time


obj = {"path": ""}


def make_path_recusive(base_path, path_raw):
    item_list = path_raw.split(".")
    item = item_list.pop(0)
    path_base = os.path.join(base_path, item)
    obj["path"] = path_base
    temp = ".".join(item_list)
    if item_list:
        make_path_recusive(path_base, temp)


class SendDingMsg(object):
    def __init__(self, history_file, access_token, test_summary_file):
        self.report_statistic = {}
        self.content_data = {}
        self.access_token = access_token
        self.history_file = history_file
        self.test_summary_file = test_summary_file
        self.ip_port = None

    def conifg_init(self):
        config_path = os.path.join(os.getcwd(), "config")
        config_path = os.path.join(config_path, "config.ini")
        self.ip_port = get_test_config("Ding", "ReportIPPORT", config_path)

    def get_report_url(self):
        with open(self.history_file) as f:
            history_trend = json.loads(f.read())
            report_url = history_trend[0]["reportUrl"]
            temp = report_url.split("/")
            temp[-2] = str(int(temp[-2]) + 1)
            print(temp)
            report_url = r"/".join(temp)
            report_url = re.sub(r'[\d.]+:\d{4}', self.ip_port, report_url)
            self.report_statistic.update({"reportUrl": report_url})

    def get_summary(self):
        with open(self.test_summary_file) as f:
            summary = json.loads(f.read())
            data = summary["statistic"]
            self.report_statistic.update(data)

    def get_chain_info(self):
        host = 'http://127.0.0.1:1968'
        username = 'waykichain'
        password = 'wicc123'
        bytesString = (username + ":" + password).encode(encoding='utf-8')
        token = base64.b64encode(bytesString)
        headers = {'content-type': 'application/json', 'Authorization': 'Basic ' + token.decode('utf-8')}
        print(headers)
        payloads = [{
            "method": "getinfo",
            'params': [],
            'jsonrpc': '2.0',
            'id': 'curltext'
        }]
        response = requests.post(url=host, data=json.dumps(payloads), headers=headers)
        ret = json.loads(response.text)[0]["result"]
        self.report_statistic.update({"version": ret["version"], "full_version": ret["full_version"], "protocol_version": ret["protocol_version"]})

    def add_releative_param(self):
        current_time = datetime.datetime.fromtimestamp(time.time()).strftime('%Y/%m/%d %H:%M:%S')
        self.report_statistic.update({"current_time": current_time})
        self.report_statistic.update({"username": "waykichain", "password": 123456})

    def make_ding_msg(self):
        self.content_data = {
            "msgtype": "markdown",
            "markdown": {
                "title": "公链测试报告",
                "text": ""
            },
            "isAtAll": True
        }
        part1 = "## Waykichain Test Report\n" + \
                "![screenshot](https://hx24.huoxing24.com/image/crawler/2019/06/01/1559356004930659.jpg)\n" + \
                "## TestCases Summary:\n" + \
                '1. total：{total}\n' + \
                '2. <font color=#97cc64 size=15 face="黑体">passed：{passed}</font>\n'
        if self.report_statistic["failed"] > 0:
            case_failed = '3. <font color=#fb0525 size=15 face="黑体">failed：{failed}</font>\n'
        else:
            case_failed = '3. failed：{failed}\n'
        if self.report_statistic["broken"] > 0:
            case_broken = '4. <font color=#ffd050 size=6 face="黑体">broken: {broken}</font>\n'
        else:
            case_broken = '4. broken: {broken}\n'
        if self.report_statistic["skipped"] > 0:
            case_skipped = '5. <font color=#aaa size=6 face="黑体">skipped: {skipped}</font>\n'
        else:
            case_skipped = '5. skipped: {skipped}\n'
        if self.report_statistic["unknown"] > 0:
            case_unknown = '6. <font color=#d35ebe size=6 face="黑体">unknown: {unknown}</font>\n\n'
        else:
            case_unknown = '6. unknown: {unknown}\n\n\n'
        part2 = "## Node Infomation:\n" + \
                '#### Version：{version}\n\n' + \
                '#### Protocol_version：{protocol_version}\n' + \
                '#### Full_version：{full_version}\n\n'
        report_url = '### [View Details]({reportUrl})\n\n'
        authorize = '<font size=15 face="黑体">(user: {username}\n & pwd: {password})</font>\n\n'
        current_time = '{current_time}\n\n'
        content = part1 + case_failed + case_broken + case_skipped + case_unknown + \
                  part2 + report_url + authorize + current_time

        self.content_data["markdown"]["text"] = content.format(**self.report_statistic)

    def send_ding_msg(self):
        url = 'https://oapi.dingtalk.com/robot/send?access_token={}'.format(self.access_token)
        headers = {'Content-Type': 'application/json'}
        json_data = json.dumps(self.content_data)
        r = requests.post(url, data=json_data, headers=headers)
        ret = json.loads(r.text)
        print(ret)

    def send_msg(self):
        self.conifg_init()
        self.get_report_url()
        self.get_summary()
        self.get_chain_info()
        self.add_releative_param()
        self.make_ding_msg()
        self.send_ding_msg()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="123")
    parser.add_argument('--history_file', default="", help="history_file")
    parser.add_argument('--access_token', default="", help="access_token")
    parser.add_argument('--test_summary_file', default="", help="test_summary_file")
    args = parser.parse_args()
    print("history_file: {}, access_token: {}, test_summary_file:{}".format(args.history_file, args.access_token, args.test_summary_file))
    aa = SendDingMsg(args.history_file, args.access_token, args.test_summary_file)
    aa.send_msg()
    # print(aa.content_data["markdown"]["text"])
