#!/bin/bash
ENV=$1
[[ -z "$ENV" ]] && ENV=test

# $2: timestamp
f=$2
[[ -z "$f" ]] && f=tmp

# deploy dir, need to replace if deploy to new directory
workspace=/opt/autotest/wicc-test/wicc-autotest

reportdir=$workspace/autotest/frontend/dist/static/media/$f

mkdir -p $reportdir

testcase=$3

echo $testcase

rootdir=$workspace/inter-autotest

cd $rootdir

git pull

source venv/bin/activate

# 拷贝智能合约到容器内部
# docker cp /root/workspace/wicc-pri/lua/ waykicoind-pri:/tmp/lua

testcasedir=$rootdir"/testcase"
tmp=${testcase//@/$testcasedir}

echo $tmp

RUN_ENV=$ENV pytest -s ${tmp//EMPTY/" "} --alluredir=$reportdir/allure-results --junit-xml=$reportdir/junit-xml/test_result.xml --html=$reportdir/index.html
#RUN_ENV=$ENV pytest -s ./testcase/wallet/ --alluredir=/home/jenkins/workspace/wicc-sdk/allure-results --junit-xml=/home/jenkins/workspace/wicc-sdk/junit-xml/test_result.xml

exit 0
