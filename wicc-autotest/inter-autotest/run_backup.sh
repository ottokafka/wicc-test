#!/usr/bin/env bash
ENV=$1
[[ -z "$ENV" ]] && ENV=test

cd /root/jenkins-workspace/wicc-test/wicc-autotest/inter-autotest
#cd /root/workspace/jenkins/wicc-test/wicc-autotest/inter-autotest

git pull

source venv/bin/activate

# 拷贝智能合约到容器内部
docker cp /root/workspace/wicc-pri/lua/* waykicoind-pri:/tmp/lua/

RUN_ENV=$ENV pytest -s ./testcase/ --alluredir=/home/jenkins/workspace/wicc-sdk/allure-results --junit-xml=/home/jenkins/workspace/wicc-sdk/junit-xml/test_result.xml
#RUN_ENV=$1 pytest -s ./testcase/wallet/ --alluredir=/home/jenkins/workspace/wicc-sdk/allure-results --junit-xml=/home/jenkins/workspace/wicc-sdk/junit-xml/test_result.xml

#RUN_ENV=$ENV pytest -s ./testcase/ --alluredir=/root/jenkins/workspace/wicc-sdk/allure-results --junit-xml=/home/jenkins/workspace/wicc-sdk/junit-xml/test_result.xml

exit 0