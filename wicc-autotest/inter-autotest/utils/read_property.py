import configparser
import os


class ReadProperty:
    def __init__(self, env='pri'):
        """
        read property from config.ini
        such as below example: RUN_ENV=test pytest -s test_file.py
        :param env: test or pri
        """
        # configfile = 'config.ini'
        if 'RUN_ENV' in os.environ.keys():
            env = os.environ['RUN_ENV']
        # self.env = env

        if env == 'dev' or env == 'test':
            configfile = 'test_config.ini'
        else:
            configfile = 'pri_config.ini'

        curpath = os.path.dirname(os.path.realpath(__file__))
        cfgpath = os.path.join(curpath, '..', 'config', configfile)
        self.conf = configparser.ConfigParser()
        self.cfgpath = cfgpath

    def read(self, section, item):
        self.conf.read(self.cfgpath, encoding='utf-8')
        value = self.conf.get(section, item)
        return value


if __name__ == '__main__':
    prop = ReadProperty()
    value = prop.read('common', 'ip')
    print(value)
