import testlink


class Testlink:
    def __init__(self, url, key):
        self.tls = testlink.TestlinkAPIClient(url, key)

    def get_test_project(self, projectname):
        proj = self.tls.getTestProjectByName(projectname)
        return proj

    def get_testcase(self, testcaseid):
        testcase = self.tls.getTestCase(testcaseid)
        return testcase


if __name__ == '__main__':
    TESTLINK_API_PYTHON_SERVER_URL = "http://10.0.0.127/lib/api/xmlrpc/v1/xmlrpc.php"
    TESTLINK_API_PYTHON_DEVKEY = "94a6fb7099c7e9e370ea81f9b39bc158"
    tlc = Testlink(TESTLINK_API_PYTHON_SERVER_URL, TESTLINK_API_PYTHON_DEVKEY)
    projId = tlc.get_test_project('waykichain')
    print('projectId is ', projId)
    print('log')
