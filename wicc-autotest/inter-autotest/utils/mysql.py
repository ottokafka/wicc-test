import pymysql
from utils.log import mylog

logger = mylog(logger='Testlog').getlog()


class Sql(object):
    """
    sql class, contain init, query, insert, close
    """
    def __init__(self, host, user, password, db, port):
        self.db = pymysql.connect(host=host, user=user, password=password, db=db, port=port)

    def query(self, sql):
        """
        query by sql
        return results
        """
        cur = self.db.cursor()
        try:
            cur.execute(sql)
            results = cur.fetchall()
            return results
        except Exception as e:
            logger.error(e)
        finally:
            self.db.close()

    def insert(self, sql):
        cur = self.db.cursor()
        try:
            cur.execute(sql)
            self.db.commit()
        except Exception as ex:
            self.db.rollback()
            logger.error(ex)
        finally:
            self.db.close()

    def update(self, sql):
        cur = self.db.cursor()
        try:
            cur.execute(sql)
            self.db.commit()
        except Exception as ex:
            self.db.rollback()
            logger.err(ex)
        finally:
            self.db.close()

    def delete(self, sql):
        cur = self.db.cursor()
        try:
            cur.execute(sql)
            self.db.commit()
        except Exception as ex:
            self.db.rollback()
            logger.error(ex)
        finally:
            self.db.close()


if __name__ == '__main__':
    host = '10.0.0.127'
    user = 'root'
    password = 'xx1-j0bs@123'
    db = 'autotest'
    sql = Sql(host=host, user=user, password=password, db=db)
    create_sql = 'CREATE TABLE `water_issue` (`id` int(11) NOT NULL AUTO_INCREMENT,' \
                 '`issue_type` int(11) NOT NULL,`description` longtext NOT NULL,' \
                 '`owner` varchar(200) NOT NULL,`createtime` datetime(6) NOT NULL,' \
                 '`updatetime` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,PRIMARY KEY (`id`)) ' \
                 'ENGINE=InnoDB DEFAULT CHARSET=latin1;'
    sql.query(create_sql)
