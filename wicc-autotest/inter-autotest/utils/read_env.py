import configparser
import os

from utils.mysql import Sql
from utils.read_property import ReadProperty


class ReadEnv:
    def __init__(self, env='test'):
        """
        read property from config.ini
        such as below example: RUN_ENV=test pytest -s test_file.py
        :param env: test or pri
        """
        if 'RUN_ENV' in os.environ.keys():
            env = os.environ['RUN_ENV']
        self.env = env

    def read(self, item):
        value = self.read_mysql(item)

        return value

    def read_mysql(self, item):
        """
        read property from sql
        :param item:
        :return:
        """
        prop = ReadProperty()
        host = prop.read('sql', 'host')
        user = prop.read('sql', 'user')
        password = prop.read('sql', 'password')
        port = prop.read('sql', 'port')
        db = prop.read('sql', 'db')
        sql = Sql(host=host, user=user, password=password, db=db, port=int(port))
        cmd = "select " + item + " from backend_environment where name='{}'".format(self.env)
        envs = sql.query(cmd)

        if envs:
            return envs[0][0]


if __name__ == '__main__':
    prop = ReadEnv(env='test_env')
    value = prop.read('ip')
    print(value)
