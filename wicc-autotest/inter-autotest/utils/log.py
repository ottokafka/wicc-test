import logging
import logging.handlers
import os


class mylog(object):
    def __init__(self, logger):
        self.logger = logging.getLogger(logger)
        self.logger.setLevel(logging.DEBUG)

        log_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', "log", "mylog.log")
        fh = logging.FileHandler(log_path)
        fh.setLevel(logging.DEBUG)

        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)

        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh.setFormatter(formatter)
        ch.setFormatter(formatter)

        self.logger.addHandler(fh)
        self.logger.addHandler(ch)

    def getlog(self):
        return self.logger


if __name__ == '__main__':
    mylogger = mylog(logger='Testlog').getlog()
    mylogger.info('open browser')
    mylogger.debug('test debug')
