### Introduction
This project is for automation to test wicc-sdk.

### Install
+ Get latestcode
> $ git clone git@gitlab.com:waykichain/wicc-test.git
+ chdir to workspace
> $ cd $workspace/wicc-test/wicc-autotest/inter-autotest
+ execute run.sh
> $ sh run.sh

### Prepare
+ Install python3.7
+ Install virtualenv
  > $ pip install virtualenv
+ chdir to $workspace and install one env
  > $ virtualenv venv -p python3
  
  > $ source venv/bin/activate

### Add new testcase
+ Create a folder under testcase
+ Create a XXX_test.py file

### Pytest user guide
> [pytest](https://docs.pytest.org/en/latest/)

### allure user guide
> [allure](https://www.allure.com/)

