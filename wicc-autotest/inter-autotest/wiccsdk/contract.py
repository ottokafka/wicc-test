import time

from utils.log import mylog
from wiccsdk.wicc import Wicc

logger = mylog(__name__).getlog()


class Contract(Wicc):
    def submitcontractdeploytx(self, params):
        return super(Contract, self).call('submitcontractdeploytx', params)

    def getcontractregid(self, params):
        return super(Contract, self).call('getcontractregid', params)

    def getcontractaccountinfo(self, params):
        return super(Contract, self).call('getcontractaccountinfo', params)

    def callcontracttx(self, params):
        return super(Contract, self).call('submitcontractcalltx', params)

    def submituniversalcontractcalltx(self, params):
        return super(Contract, self).call('submituniversalcontractcalltx', params)

    def getcontractdata(self, params):
        return super(Contract, self).call('getcontractdata', params)

    def getcontractdataraw(self, params):
        return super(Contract, self).call('getcontractdataraw', params)

    def listcontracts(self, params):
        return super(Contract, self).call('listcontracts', params)

    def getcontractinfo(self, params):
        return super(Contract, self).call('getcontractinfo', params)

    def getregidbytxhash(self,params):
        """
        get regid by txhash
        :param params:txhash
        :return: return regid
        """
        status, response = super(Contract, self).call('getcontractregid', params)
        if status == 200:
            for r in response:
                if r['result'] is not None:
                    return r['result']['regid']
        return None

    def getregidlist(self,params=[False]):
        """
        get all contract regidlist
        :param params:
        :return:regidlist
        """
        status, response = super(Contract, self).call('listcontracts', params)
        if status == 200:
            for r in response:
                if r['result'] is not None:
                    list = r['result']['contracts']
                    regidlist = []
                    for l in list:
                        regidlist.append(l['contractregid'])
                    return regidlist
        return None

    def ishashconfirmed(self,params):
        """
        hash is confirmed or not
        :param params: txhash
        :return:True or False
        """
        timeout = 60
        while timeout > 0:
            regId = self.getregidbytxhash(params)
            if regId is not None:
                return True
            time.sleep(1)
            timeout = timeout - 1
        return False

    def iscontractregister(self,params):
        """
        contract is register or not
        :param params: txhash
        :return:True or False
        """
        timeout = 60
        while timeout > 0:
            regId = self.getregidbytxhash(params)
            if regId is not None:
                return True
            time.sleep(1)
            timeout = timeout - 1
        return False

if __name__ == '__main__':
    pass