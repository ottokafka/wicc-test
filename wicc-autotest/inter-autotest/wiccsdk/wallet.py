import os
import time

from utils.log import mylog
from wiccsdk.wicc import Wicc

logger = mylog(__name__).getlog()
cmd = 'sh restart.sh'

class Wallet(Wicc):
    def __init__(self, host, user, password):
        super().__init__(host, user, password)

    def encryptwallet(self, params):
        return super(Wallet, self).call('encryptwallet', params)

    def walletpassphrase(self, params):
        return super(Wallet, self).call('walletpassphrase', params)

    def walletlock(self):
        return super(Wallet, self).call('walletlock', [])

    def walletpassphrasechange(self,params):
        return super(Wallet, self).call('walletpassphrasechange', params)

    def dumpwallet(self,params):
        return super(Wallet, self).call('dumpwallet', params)

    def importwallet(self, params):
        return super(Wallet, self).call('importwallet', params)

    def backupwallet(self, params):
        return super(Wallet, self).call('backupwallet', params)

    def getwalletinfo(self):
        return super(Wallet, self).call('getwalletinfo', [])

    def islocked(self):
        """
        check wallet is locked or not
                :return: true or false
        """
        # timeout = 60
        # while timeout > 0:
        status_code, response = self.getwalletinfo()
        if status_code == 200:
            for r in response:
                locked = r['result']['wallet_locked']
                if locked is True:
                    return True
                else:
                    return False

    def isencrypted(self):
        """
        check wallet is encrypted or not
                :return: true or false
        """
        timeout = 60
        while timeout > 0:
            status_code, response = self.getwalletinfo()
            if status_code == 200:
                for r in response:
                    isEncrypted = r['result']['wallet_encrypted']
                    if isEncrypted== True:
                        return True
                    time.sleep(1)
                    timeout = timeout - 1
        return False

    def encryptandrestart(self,walletpassword):
        """
        私有链，加密钱包后，docker会自动关闭，此方法实现加密钱包并重新启动docker
        remark: 重新启动后钱包为已加密、已锁状态
        """
        # 加密钱包
        isencrypted = self.isencrypted()
        if not isencrypted:
            status, response = self.encryptwallet([walletpassword])
            assert status == 200
        else:
            pass

        # 加密成功后，docker会stop，且wallet是被锁状态
        # 执行启动docker脚本
        data = os.popen(cmd)
        print(data.read())

        time.sleep(30)
        self.walletpassphrase([walletpassword,360000])
        assert self.isencrypted()
        assert not self.islocked()

        return self.isencrypted()

if __name__ == '__main__':
    pass
