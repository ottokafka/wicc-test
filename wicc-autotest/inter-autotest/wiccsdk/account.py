import os
import random
import string
from time import sleep

import math

from utils.read_property import ReadProperty
from wiccsdk.wicc import Wicc
from utils.log import mylog

logger = mylog(__name__).getlog()


class Account(Wicc):
    def __init__(self, host, username, password):
        super().__init__(host, username, password)

    def listaddr(self, params):
        return super(Account, self).call('listaddr', params)

    def getnewaddress(self, params=[False]):
        return super(Account, self).call('getnewaddr', params)

    def getaccountinfo(self, params):
        return super(Account, self).call('getaccountinfo', params)

    def registeraccounttx(self, params):
        return super(Account, self).call('submitaccountregistertx', params)

    def genregisteraccountraw(self, params):
        return super(Account, self).call('genregisteraccountraw', params)

    def dumpprivkey(self, params):
        return super(Account, self).call('dumpprivkey', params)

    def importprivkey(self, params):
        return super(Account, self).call('importprivkey', params)

    def validateaddress(self, params):
        return super(Account, self).call('validateaddr', params)

    def listdelegates(self):
        return super(Account, self).call('listdelegates', [])

    # def votedelegatetx(self):
    #     return super(Account, self).call('votedelegatetx', [])

    def send(self, params):
        """
        send to address
        :return: true if success, or false
        """
        print(999,params)
        status_code, response = super(Account, self).call('submitsendtx', params)
        if status_code == 200:
            for r in response:
                print("send res:",response)
                if r['result']['txid'] != "":
                    return True
                else:
                    return False
        else:
            return False

    def newaddress(self, params=[False]):
        """
        get new address
        :param params:
        :return: return the new address
        """
        status_code, response = super(Account, self).call('getnewaddr', params)
        if status_code == 200:
            for r in response:
                return r['result']['addr']
        return None

    def newaddrwithWICC(self, init, balance, fee=1000000, miner=False):
        """
        get a unregistered newaddress with balance WICC
        :param balance:
        :return:
        """
        addr = self.newaddress()
        assert addr is not None
        if addr is not None:
            res = self.send([init, addr, str(balance), fee])
            assert res
            inblock = self.isInBlock([addr])
            if inblock:
                return addr
        return None

    def getvotes(self, params):
        """
        获取当前地址收到的投票总数
        :param params:addr
        :return:votes
        """
        status_code, response = super(Account, self).call('getaccountinfo', params)
        if status_code == 200:
            for r in response:
                return r['result']['received_votes']
        return 0

    def getnewaddresswithWICC(self, init, balance, fee=10000000):
        """
        new address and sendtoaddress with expected balance and register account
        :param balance:账户金额
        :param fee:激活账户的小费
        :return: addr
        """
        addr = self.newaddress()
        assert addr is not None
        if addr is not None:
            res = self.send([init, addr, str(balance + fee), fee])
            inblock = self.isInBlock([addr])
            response = self.registeraccounttx([addr, fee])
            isRegister = self.isRegistered([addr])
            if res & inblock & isRegister:
                return addr
        return None

    def getnewaddrwithmatureregid(self, init, balance, fee=10000000):
        """
        new address and with mature regid
        :param balance:账户金额
        :param fee:激活账户的小费
        :return: addr
        """
        addr = self.newaddress()
        assert addr is not None
        if addr is not None:
            res = self.send([init, addr, str(balance + fee), fee])
            inblock = self.isInBlock([addr])
            self.registeraccounttx([addr, fee])
            isRegister = self.isRegistered([addr])
            if res & inblock & isRegister:
                mature = self.regidIsMature(addr)
                if mature:
                    return addr
        return None

    def isInBlock(self, params):
        """
        check address is in block or not
        :param params: address
        :return: true or false
        """
        timeout = 60
        while timeout > 0:
            status_code, response = self.getaccountinfo(params)
            if status_code == 200:
                for r in response:
                    regId = r['result']['position']
                    if regId == 'inblock':
                        return True
                    sleep(1)
                    timeout = timeout - 1
        return False

    def isRegistered(self, params):
        """
        check address is registered or not
        :param params: address
        :return: true or false
        """
        timeout = 60
        while timeout > 0:
            status_code, response = self.getaccountinfo(params)
            if status_code == 200:
                for r in response:
                    regId = r['result']['regid']
                    if regId != '':
                        return True
                    sleep(1)
                    timeout = timeout - 1
        return False

    def regidIsMature(self, address):
        """
        check address's regid is mature or not
        :param params: address
        :return: true or false
        """
        timeout = 150*3
        while timeout > 0:
            status_code, response = self.getaccountinfo([address])
            if status_code == 200:
                for r in response:
                    mature = r['result']['regid_mature']
                    if mature == True:
                        return True
                    sleep(1)
                    timeout = timeout - 1
        return False

    def getEnoughCoindRegisteredAddress(self, balance):
        """
        Get an address which is registered and more than balance
        :param balance: address has more than coin
        """
        params = []
        status_code, response = self.listaddr(params)
        if status_code != 200:
            return ''
        for r in response:
            for s in r['result']:
                if s['tokens'] != {}:
                    if "WICC" in s['tokens']:
                        if s['regid'] != '' and s['tokens']['WICC']['free_amount'] >= balance:
                            return s['addr']

    def getEnoughCoinMatureAddress(self, balance):
        """
        Get an address which is registered and more than balance and regiid is mature
        :param balance: address has more than coin
        """
        params = []
        status_code, response = self.listaddr(params)
        if status_code != 200:
            return ''
        for r in response:
            for s in r['result']:
                if s['tokens'] != {}:
                    if "WICC" in s['tokens']:
                        if s['regid'] != '' and s['tokens']['WICC']['free_amount'] >= balance and s['regid_mature'] == True:
                            return s['addr']

    def gettokenbyaddr(self, addr, coin_type, amount_type="free_amount"):
        """
        get account  balance by  address
        :param addr:address
        :param coin_type:"WICC"、"WUSD"、"WGRT"、"other asset"
        :param amount_type:"free_amount"、"staked_amount"、"frozen_amount"、"voted_amount"
        :return: balance
        """
        status_code, response = self.getaccountinfo([addr])
        balance = 0
        if status_code == 200:
            for r in response:
                if coin_type in r['result']['tokens']:
                    balance = r['result']['tokens'][coin_type][amount_type]
                    if balance is not None:
                        return balance
                else:
                    return 0
        return None

    def getaddrbyregid(self, params):
        """
        get address by regid
        :param params: regid
        :return: return the account address
        """
        status_code, response = super(Account, self).call('getaccountinfo', params)
        if status_code == 200:
            for r in response:
                return r['result']['address']
        return None

    def getaddrinfo(self, address, field):
        """
        get address info by address
        :param address: regid or address
        :param field: which field you want to get
        :return: return the account info
        """
        status_code, response = super(Account, self).call('getaccountinfo', [address])
        if status_code == 200:
            for r in response:
                if field in r['result']:
                    return r['result'][field]
        return None

    def getdumpprivkey(self, params):
        """
        get dumped privkey
        :param params: address
        :return:privkey
        """
        status_code, response = super(Account, self).call('dumpprivkey', params)
        if status_code == 200:
            for r in response:
                return r['result']['privkey']
        return None

    def getFeedingAddress(self, init, balance=210000000000000, env='pri', fee = 1000000):
        """
        Get an address that meets the feeding requirements,
        if not exist,get one miner address with 210000000000000 votes
        :param senderaddr: creation address
        """
        prop = ReadProperty()
        if 'RUN_ENV' in os.environ.keys():
            env = os.environ['RUN_ENV']
        if env == 'test':
            return prop.read('common', 'feedaddr')

        status_code, response = super(Account, self).call('listdelegates', [])
        assert status_code == 200
        addr = None
        for res in response:
            for re in res['result']['delegates']:
                mineraddress = re['address']
                stake_wicc = re['tokens']['WICC']['staked_amount']
                if stake_wicc >= balance:
                    addr = mineraddress
                    return addr
                else:
                    continue

        if addr == None:
            params = [init, mineraddress, str(balance+fee), fee]
            res = self.send(params)
            assert res
            sleep(6)
            params = [mineraddress, "WICC", balance]
            status_code, response = self.call('submitcoinstaketx', params)
            assert status_code == 200
            print('stake res:',response)
            sleep(10)
            return mineraddress

    def getNewAddrWithEnoughWUSD(self, init, balance, fee=1000000*3, address=None):
        """
        :param balance:WUSD balance
        :return: address with WUSD balance
        """
        if address == None:
            price = self.getprice("WICC")
            print("price:", price)
            if price > 0:
                addr = self.getnewaddresswithWICC(init, int(price * balance * 3) + fee)
            else:
                return None
        else:
            addr = address
        assert addr is not None
        # sleep(6)
        params = [addr, str(int(price * balance * 3)), str(balance)]
        print("cdpparam", params)
        status_code, response = super(Account, self).call('submitcdpstaketx', params)
        print("stakecdp res", response)
        if status_code == 200:
            sleep(20)
            status_code, response = super(Account, self).call('getaccountinfo', [addr])
            assert status_code == 200
            for r in response:
                if balance == r['result']['tokens']['WUSD']['free_amount']:
                    return addr
        return None

    def getprice(self, type=None, height=None):
        """
        get the price at height
        :param type: the price want to know,can be None,"WICC","WGRT"
        :param height:price want to know at height
        :return:type is None,means get all price;height is None,means get now height price
        """
        if height == None:
            params = []
        else:
            params = [height]

        status, response = super(Account, self).call('getscoininfo', params)
        if status == 200:
            for res in response:
                if res['result'] != '':
                    if res['result']['median_price'] != '':
                        result = res['result']['median_price']
                        if type is None:
                            return result
                        else:
                            for r in range(0, len(result)):
                                if result[r]["coin_symbol"] == type:
                                    return result[r]["price"]
                return None

    def getNewAddrWithEnoughWGRT(self, init, sender, balance, reciever=None, fee=1000000):
        """
        :param balance:WGRT balance
        :return: address with WGRT balance
        """
        if reciever == None:
            addr = self.getnewaddresswithWICC(init, fee)
        else:
            addr = reciever
        assert addr is not None
        status_code, response = super(Account, self).call('submitsendtx', [init, sender, "50000", fee])
        assert status_code == 200
        sleep(10)
        params = [sender, addr, "WGRT:" + str(balance) + ":sawi", fee]
        status_code, response = super(Account, self).call('submitsendtx', params)
        if status_code == 200:
            sleep(30)
            status_code, response = super(Account, self).call('getaccountinfo', [addr])
            assert status_code == 200
            for r in response:
                assert r['result'] is not None
                assert r['result']['tokens'] is not None
                assert balance == r['result']['tokens']['WGRT']['free_amount']
                return addr
        return None

    def getNewCDP(self, params):
        """
        @kafka李大龙🐲 getNewCDP
        create a CDP with new created address with balance of 200
        :param params:
        :return: return new CDP and new address with balance of 200
        """
        status_code, response = super(Account, self).call('submitcdpstaketx', params)
        print("new cdp res:",response)
        if status_code == 200:
            for r in response:
                if r['result'] is not None:
                    return r['result']['txid']
        return None

    def feedingPrice(self, init, cointype, wiccprice=1, wgrtprice=1, address=None, env='pri'):
        prop = ReadProperty()
        # get env
        if 'RUN_ENV' in os.environ.keys():
            env = os.environ['RUN_ENV']

        if address == None:
            if env == 'pri':
                addr = self.getFeedingAddress(init)
                assert addr is not None
            elif env == 'test':
                addr = prop.read('common', 'feedaddr')
                self.host = prop.read('common', 'feedhost')
                self.username = prop.read('common', 'feedrpcuser')
                self.password = prop.read('common', 'feedrpcpassword')
        else:
            addr = address

        WICC = {"coin": "WICC", "currency": cointype, "price": int(wiccprice * 100000000)}
        WGRT = {"coin": "WGRT", "currency": cointype, "price": int(wgrtprice * 100000000)}
        params = [addr, [WICC, WGRT]]
        status, response = self.call('submitpricefeedtx', params)
        print("feeding res:", response)
        if status == 200:
            for r in response:
                if r['result'] != None:
                    return True
                else:
                    if "duplicated-pricefeed" in r['error']['message']:
                        sleep(10)
                        status, res = self.call('submitpricefeedtx', params)
                        for r in res:
                            if r['result'] != None:
                                return True
                            else:
                                return False
                    else:
                        return False
        return False

    def getCdpInterest(self, cdpid, H1, amount, a=2, b=1, time=10):
        """
        get cdp interest by cdpid
        :param cdpid: cdpid
        :param height: current height
        :param a:default param
        :param b:default param
        :return:interest
        """
        oneday_height = 60 * 60 * 24 / 3
        status, response = super(Account, self).call('getcdp', [cdpid])
        assert status == 200
        for r in response:
            if r['result'] != "":
                H2 = r['result']['cdp']['last_height']
                print("H2:", H2)
                if H2 > 0 and amount > 0:
                    rate = 0.1 * a / math.log10(1 + b * amount)
                    interest = (amount * math.ceil((H2 - H1) / oneday_height) / 365.0) * rate

                    return int(interest * 100000000)
                else:
                    return None
            else:
                return None

    def getliquidateres(self, liquidate_value, stake_value, mint_value):
        """
        :param liquidate_value: 清算人付出的清算价值
        :param stake_value: CDP抵押量价值
        :param mint_value: CDP贷出量价值
        :return:
            {
            re_liquidater_value：反还清算人的价值
            re_owner_value：反还CDP所有者的价值
            left_stake_value：剩余待清算的资产
            fine：罚金
            }
        """
        # 获取抵押率
        stake_rate = stake_value/mint_value
        mint_ratio = mint_value / 10000000000
        # 抵押率>=1.13,CDP所有者有反还
        if stake_rate > 1.13 and stake_rate <= 1.5:
            ratio = liquidate_value / (10961000000 * mint_ratio)
            wusd = ratio * mint_value
            re_liquidater_value = liquidate_value / 0.97
            re_owner_value = (stake_value - 11300000000 * mint_ratio) * ratio
            left_stake_value = stake_value - re_liquidater_value - re_owner_value
            left_mint_value = mint_value -wusd
            fine = liquidate_value - wusd
            if fine < 0:
                return None
        # 抵押率<1.13,CDP所有者无反还
        elif stake_rate > 1.04 and stake_rate < 1.13:
            re_owner_value = 0
            re_liquidater_value = liquidate_value / 0.97
            # 获取清算的比率
            ratio = liquidate_value / (stake_value*0.97 * mint_ratio)
            print(ratio)
            # 罚金 = 清算总量 - 还债总量
            fine = liquidate_value - ratio * mint_value
            # 剩余待清算资产量 = 总资产 - 反还清算人 - 反还CDP owner
            left_stake_value = stake_value - re_liquidater_value - re_owner_value
            left_mint_value = mint_value - ratio * mint_value
            if fine < 0:
                return None

        res = {"re_liquidater_value": re_liquidater_value, "re_owner_value": re_owner_value, "fine": fine,
               "left_stake_value": left_stake_value, "left_mint_value": left_mint_value}
        return res

    def getrandomsymbol(self):
        # s是大写字母
        s = string.ascii_uppercase
        # 生成6-7位大写字母的集合，并将列表转字符串
        temp = random.randint(6, 7)
        code = ''.join(random.sample(s, temp))
        return code

if __name__ == '__main__':
    account = Account('http://10.0.0.19:1968', 'waykichain', 'wicc123')
    # print(account.getEnoughCoinMatureAddress(100))
    # print(account.gettokenbyaddr("0-1", "WGRT", "free_amount"))
    # print(account.getrandomsymbol())
    # print(account.getnewaddrwithmatureregid("0-1",100000))
    # print(account.getNewAddrWithEnoughWUSD("0-1",50000000))
    # print(account.getFeedingAddress("0-1"))
    print(account.feedingPrice("0-1", "USD", 0.75, 0.75))
    # print(account.getnewaddrwithmatureregid("0-1",10000,10000))
    # fine = account.getliquidateres(10670000000, 22000000000, 20000000000)
    # print(fine)
