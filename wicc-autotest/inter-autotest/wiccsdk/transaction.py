import os

from math import ceil, floor

from utils.read_property import ReadProperty
from wiccsdk.wicc import Wicc


class Transaction(Wicc):
    def __init__(self, host, username, password):
        super().__init__(host, username, password)

    # def sendtoaddress(self, params):
    #     return super(Transaction, self).call('send', params)

    # def sendtoaddresswithfee(self, params):
    #     return super(Transaction, self).call('sendtoaddresswithfee', params)

    def gensendtoaddressraw(self, params):
        return super(Transaction, self).call('gensendtoaddressraw', params)

    def submittxraw(self, params):
        return super(Transaction, self).call('submittxraw', params)

    def gettxdetail(self, params):
        return super(Transaction, self).call('gettxdetail', params)

    def listtx(self):
        return super(Transaction, self).call('listtx', [])

    def listunconfirmedtx(self):
        return super(Transaction, self).call('listunconfirmedtx', [])

    # def getalltxinfo(self, params):
    #     return super(Transaction, self).call('getalltxinfo', params)

    def decodetxraw(self, params):
        return super(Transaction, self).call('decodetxraw', params)

    def genvotedelegateraw(self, params):
        return super(Transaction, self).call('genvotedelegateraw', params)

    def votedelegatetx(self, params):
        return super(Transaction, self).call('submitdelegatevotetx', params)

    def submitpricefeedtx(self, params, env='pri'):
        # get env
        prop = ReadProperty()
        if 'RUN_ENV' in os.environ.keys():
            env = os.environ['RUN_ENV']

        if env == 'pri':
            return super(Transaction, self).call('submitpricefeedtx', params)
        elif env == 'test':
            self.host = prop.read('common', 'feedhost')
            self.username = prop.read('common', 'feedrpcuser')
            self.password = prop.read('common', 'feedrpcpassword')
            return self.call('submitpricefeedtx', params)

    def send(self, params):
        return super(Transaction, self).call('submitsendtx', params)

    def submitcoinstaketx(self, params):
        return super(Transaction, self).call('submitcoinstaketx', params)

    def submitstakecdptx(self, params):
        return super(Transaction, self).call('submitcdpstaketx', params)

    def submitredeemcdptx(self, params):
        return super(Transaction, self).call('submitcdpredeemtx', params)

    def submitliquidatecdptx(self, params):
        return super(Transaction, self).call('submitcdpliquidatetx', params)

    def getusercdp(self, params):
        return super(Transaction, self).call('getusercdp', params)

    def getcdp(self, params):
        return super(Transaction, self).call('getcdp', params)

    def submitdexselllimitordertx(self, params):
        return super(Transaction, self).call('submitdexselllimitordertx', params)

    def submitdexbuylimitordertx(self, params):
        return super(Transaction, self).call('submitdexbuylimitordertx', params)

    def submitdexbuymarketordertx(self, params):
        return super(Transaction, self).call('submitdexbuymarketordertx', params)

    def submitdexsellmarketordertx(self, params):
        return super(Transaction, self).call('submitdexsellmarketordertx', params)

    def submitdexcancelordertx(self, params):
        return super(Transaction, self).call('submitdexcancelordertx', params)

    def submitdexsettletx(self, params):
        return super(Transaction, self).call('submitdexsettletx', params)

    def getdexorder(self, params):
        return super(Transaction, self).call('getdexorder', params)

    def getdexorders(self, params):
        return super(Transaction, self).call('getdexorders', params)

    def getdexsysorders(self, params):
        return super(Transaction, self).call('getdexsysorders', params)

    def submitassetissuetx(self, params):
        return super(Transaction, self).call('submitassetissuetx', params)

    def getassets(self):
        return super(Transaction, self).call('getassets', [])

    def submitassetupdatetx(self, params):
        return super(Transaction, self).call('submitassetupdatetx', params)

    def getcdpinfo(self, txid, field):
        status, res = self.getcdp([txid])
        assert status == 200
        for r in res:
            assert r['result'] is not None
            result = r['result']['cdp'][field]
            return result
        return None

    def getsendhash(self, params):
        """
        get sendtoaddress result hash
        :param params:[senderaddr, reciveraddr, amount]
        :return:hash
        """
        status, response = super(Transaction, self).call('submitsendtx', params)
        if status == 200:
            for r in response:
                hash = r['result']['txid']
                if hash is not None:
                    return hash
        return None

    def getvoteminersreward_old(self, height1, height2, votes):
        """
        获取投票收益
        :param height1: 第一次投票高度
        :param height2: 第二次投票高度
        :param votes: 投票额
        :return: reward
        """
        # 获取当前环境
        env = 'pri'
        if 'RUN_ENV' in os.environ.keys():
            env = os.environ['RUN_ENV']

        # 1期的区块数量(为固定数量)
        blockNum = 0
        if env == 'pri':
            blockNum = 500
            blockSum = 365*24*3600/10
        elif env == 'test' or env == 'main':
            blockNum = 3153600
        else:
            return None

        # 投票期数
        if height1 == height2 or height1 == 0 or height2 == 0 or height1 > height2:
            return None
        else:
            periodNum1 = ceil(height1 / blockNum)
            periodNum2 = ceil(height2 / blockNum)

        # 计算投票收益
        sum = 0
        num = 0
        for n in range(periodNum1, periodNum2 + 1):
            if n < 5:
                inter = 0.05 - 0.01 * (n - 1)
                inter = round(inter, 2)
            elif n >= 5:
                inter = 0.01

            if height1 <= n * blockNum:
                if height2 <= n * blockNum:
                    num = (height2 - height1) / blockSum
                else:
                    num = (n * blockNum - height1) / blockSum
                    height1 = n * blockNum
            else:
                continue
            x = votes * inter * num
            sum = sum + x
        return int(sum)

    def getvotereward(self, height1, height2, votes, type="WGRT"):
        """
        获取投票收益
        :param height1: 第一次投票高度
        :param height2: 第二次投票高度
        :param votes: 投票额
        :param type: WICC|WGRT
        :return: reward
        """
        # 获取当前环境
        env = 'pri'
        if 'RUN_ENV' in os.environ.keys():
            env = os.environ['RUN_ENV']

        # 1年的区块数量(为固定数量)
        blockNum = 0
        # 区块减少的间隔高度
        blockRed = 0
        if env == 'pri':
            # range = 500
            blockRed = 1500
            blockNum = 365 * 24 * 60 * 60 / 3
            # blockNum = 500
        elif env == 'test' or env == 'main':
            blockRed = 365 * 24 * 60 * 60 / 3
            blockNum = 365 * 24 * 60 * 60 / 3
        else:
            return 0

        # 投票期数
        if height1 == height2 or height1 == 0 or height2 == 0 or height1 > height2:
            return 0
        else:
            periodNum1 = ceil(height1 / blockRed)
            periodNum2 = ceil(height2 / blockRed)

        # 计算投票收益
        sum = 0
        num = 0
        for n in range(periodNum1, periodNum2 + 1):
            # 获取利率
            if n < 5:
                inter = 0.05 - 0.01 * (n - 1)
                inter = round(inter, 2)
            elif n >= 5:
                inter = 0.01

            if height1 <= n * blockNum:
                if height2 <= n * blockNum:
                    num = (height2 - height1) / blockNum
                else:
                    num = (n * blockNum - height1) / blockNum
                    height1 = n * blockNum
            else:
                continue
            x = votes * inter * num
            print(votes, inter, num, x)
            if type == "WICC":
                sum = sum + 11 * x
                return int(sum)
            elif type == "WGRT":
                sum = sum + x
                print(sum)
                return int(sum)
            else:
                return None

    def getmedian(self, params):
        """
        get median of params
        :params: price array
        :return: median price
        """
        params.sort()
        half = len(params) // 2
        return int((params[half] + params[~half]) / 2)

    def getprice(self, type=None, height=None):
        """
        get the price at height
        :param type: the price want to know,can be None,"WICC","WGRT"
        :param height:price want to know at height
        :return:type is None,means get all price;height is None,means get now height price
        """
        if height == None:
            params = []
            status, response = super(Transaction, self).call('getscoininfo', params)
        else:
            params = [height]
            status, response = super(Transaction, self).call('getblock', params)

        if status == 200:
            for res in response:
                if res['result'] != '':
                    if res['result']['median_price'] != '':
                        result = res['result']['median_price']
                        if type is None:
                            return result
                        else:
                            for r in range(0, len(result)):
                                if result[r]["coin_symbol"] == type:
                                    print("actual price:", result[r]["price"])
                                    return result[r]["price"]
                return None

    def getassetsbysymbol(self, symbol):
        status, response = super(Transaction, self).call('getassets', [])
        if status == 200:
            for res in response:
                if res['result'] is not None:
                    if res['result']['count'] > 0:
                        for i in range(0, len(res['result']['assets'])):
                            asset = res['result']['assets'][i]
                            if symbol == asset['asset_symbol']:
                                return asset
        else:
            return None


if __name__ == '__main__':
    tran = Transaction('http://10.0.0.19:1968', 'waykichain', 'wicc123')
    res = tran.getassetsbysymbol("CNY123456")
    print(res["owner_uid"])
