import os
import base64
import requests
import json
from utils.log import mylog

logger = mylog(__name__).getlog()


class Wicc(object):
    def __init__(self, host, username, password):
        self.username = username
        self.password = password
        self.host = host


    def call(self, methodname, params):
        payloads = [{
            "method": methodname,
            'params': params,
            'jsonrpc': '2.0',
            'id': 'curltext'
        }]
        bytesString = (self.username + ":" + self.password).encode(encoding='utf-8')
        token = base64.b64encode(bytesString)
        self.headers = {'content-type': 'application/json', 'Authorization': 'Basic ' + token.decode('utf-8')}

        response = requests.post(url=self.host, data=json.dumps(payloads), headers=self.headers)
        if response.status_code == 200:
            return response.status_code, response.json()
        else:
            return response.status_code, response

    def is_ready(self):
        """
        check node is ready through tipblockheight and syncblockheight
        :return:
        """
        status_code, response = self.call('getinfo', [])
        ready = False
        if status_code == 200:
            for r in response:
                syncblockheight = r['result']['syncblockheight']
                tipblockheight = r['result']['tipblockheight']
                if syncblockheight - tipblockheight <= 1:
                    ready = True
        return ready


if __name__ == '__main__':
    rpc = Wicc('http://10.0.0.127:1968', 'waykichain', 'wicc123')
    status_code, response = rpc.call('getinfo', [])
    ready = rpc.is_ready()
    logger.info(status_code)
    logger.info(response)
    print(ready)
