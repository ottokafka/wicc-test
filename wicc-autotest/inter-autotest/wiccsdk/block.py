from wiccsdk.wicc import Wicc
from utils.log import mylog

logger = mylog(__name__).getlog()

class Block(Wicc):
    def __init__(self, host, user, password):
        super().__init__(host, user, password)
    
    def getblockcount(self):
        return super(Block, self).call('getblockcount', [])

    # def getblockhash(self, params):
    #     return super(Block, self).call('getblockhash', params)

    def getblock(self, params):
        return super(Block, self).call('getblock', params)

    def getchaininfo(self, params):
        return super(Block, self).call('getchaininfo',params)

    def getscoininfo(self):
        return super(Block, self).call('getscoininfo', [])

    def getinfo(self):
        """
        get info
        """
        return super(Block, self).call('getinfo', [])

    def getblockheight(self):
        """
        get block height:获取当前最新区块高度
        """
        status_code,response = super(Block, self).call('getinfo', [])
        if status_code == 200:
            for r in response:
                height = r['result']['syncblock_height']
                assert height is not None
                return height
        return None



if __name__ == '__main__':
    pass
    
    

