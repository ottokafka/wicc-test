import datetime
import os

import os,django
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "autotest.settings")
django.setup()

import time
from celery_app import app
from backend.models import Execute, Testcase, History
from autotest.settings import BASE_DIR

autotest_shell = os.path.join(BASE_DIR, '../inter-autotest/run_autotest.sh')


@app.task
def task1():
    """
    task for execute the task
    :return:
    """
    executes = Execute.objects.filter(status=0, duration='立即')
    if executes:
        execute(executes)
        return 'success'
    else:
        return 'no task executed'


@app.task
def task2():
    """
    task for execute the task
    :return:
    """
    now = datetime.datetime.now()
    current_hour = now.hour
    executes = Execute.objects.filter(hour=current_hour, duration='定时')
    if executes:
        execute(executes)
        return 'success'
    else:
        return 'no task executed'


def execute(executes):
    """
    execute and operation for database
    :param executes:
    :return:
    """
    for exe in executes:
        id = exe.id
        env = exe.env
        execute_time = int(time.time())
        testcases = Testcase.objects.values('case_name').filter(execute_id=id)

        cases = ''
        for testcase in testcases:
            tmp_list = testcase['case_name'].split('.')
            module = tmp_list[0]
            name = tmp_list[1]

            module_file = module + '_test.py'
            class_name = '::Test' + module.replace(module[0], module[0].upper(), 1)

            # EMPTY is the connect string which will be replaced with empty space
            cases += '@/' + module + '/' + module_file + class_name + '::test_' + name + 'EMPTY'

        cmd = 'bash {} {} {} {}'.format(autotest_shell, env, execute_time, cases)
        update(id, 1)
        os.system(cmd)
        update(id, 2)
        add_history(id, execute_time)


def update(exe_id, status):
    """
    update status according to the execute id
    :param exe_id:
    :param status:
    :return:
    """
    execute = Execute.objects.filter(id=exe_id).first()
    execute.status = status
    execute.save()


def add_history(exe_id, execute_time):
    """
    add execute history
    :param exe_id:
    :param execute_time:
    :return:
    """
    history = History()
    history.execute_id = exe_id
    history.name = str(execute_time)
    history.save()


if __name__ == '__main__':
    pass
