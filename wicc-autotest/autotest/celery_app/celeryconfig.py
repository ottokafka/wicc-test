from datetime import timedelta
from celery.schedules import crontab
# 配置broker为redis
BROKER_URL = 'redis://:wicc1234@auto.wicctest.cn:6379/0'
# 配置结果存储至redis
CELERY_RESULT_BACKEND = 'redis://:wicc1234@auto.wicctest.cn:6379/1'
# 时区设置
CELERY_TIMEZONE = 'Asia/Shanghai'
# 导入任务
CELERY_IMPORTS = (
    'celery_app.tasks',
    )
# 配置定时任务的调度器
CELERYBEAT_SCHEDULE = {
    # 任务名字
    'task1': {
        # 任务启动的函数
        'task': 'celery_app.tasks.task1',
        # 定时时间设置，每10秒一次
        'schedule': timedelta(seconds=10),
        # 传递的参数
        # 'args': (2, 8)
        'args': ()
    },
    'task2': {
        'task': 'celery_app.tasks.task2',
        # 定时时间设置
        'schedule': crontab(minute=5),
        'args': ()
    }
}
