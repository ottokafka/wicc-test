#!/usr/bin/env bash

# kill old process
ps -ef|grep celery_app |cut -c 9-15|xargs kill -9
ps -ef|grep manage|grep venv|cut -c 9-15|xargs kill -9


source venv/bin/activate

cd frontend

npm run build

cd ..

# start django server
nohup python manage.py runserver 0.0.0.0:8081 &

# start celery service
nohup celery worker -A celery_app --pool=solo -l INFO > system.log &

# start celery beat
nohup celery beat -A celery_app -l INFO &

# start celery flower
nohup  celery -A celery_app flower -l info &