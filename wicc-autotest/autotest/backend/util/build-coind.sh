#!/usr/bin/env bash

docker run -i --rm -v /root/workspace/wicc-build/waykichain/:/tmp/waykichain \
 	wicc-build \
	sh -c "git pull && make && strip ./src/coind  && mv ./src/coind /tmp/waykichain/"