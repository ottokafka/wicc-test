"""autotest URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from backend import views

urlpatterns = [
    url(r'^gettestsuite/$', views.get_testsuite, name='gettestsuite'),
    url(r'^getenv/$', views.getenv, name='getenv'),
    url(r'^saveenv/$', views.saveenv, name='saveenv'),
    url(r'^deleteenv/$', views.deleteenv, name='deleteenv'),
    url(r'^saveexecute/$', views.saveexecute, name='saveexecute'),
    url(r'^getexecute/$', views.getexecute, name='getexecute'),
    url(r'^deleteexecute/$', views.deleteexecute, name='deleteexecute'),
    url(r'^gethistory/$', views.gethistory, name='gethistory'),
]
