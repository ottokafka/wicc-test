# -*- coding:utf8 -*-
import json
import time

from django.http import HttpResponse
from django.shortcuts import render
from autotest.settings import BASE_DIR, STATIC_URL
import os

# Create your views here.
from django.views.decorators.csrf import csrf_exempt

from backend.models import Environment, Execute, Testcase, History


@csrf_exempt
def get_testsuite(request):
    folder_path = os.path.join(BASE_DIR, '../inter-autotest/testcase')
    files_dict = []
    for root, dirs, files in os.walk(folder_path):
        for f in files:
            if f.endswith('.py') and '_test' in f:
                test_suite = f.split('_')[0]
                suite = {'label': test_suite, 'children': []}
                with open(os.path.join(root, f), 'r') as fp:
                    case_content = ''
                    case_name = ''
                    for line in fp.readlines():
                        if line.startswith('    def test_'):
                            if case_content != '' and case_name != '' and case_name != 'param':
                                suite['children'].append({'label': case_name, 'content': case_content})
                            case_content = line
                            case_name = line.split('(')[0].split('test_')[1]
                        else:
                            if '@allure' not in line:
                                case_content += line
                files_dict.append(suite)
    return HttpResponse(json.dumps(files_dict), content_type='application/json')


@csrf_exempt
def getenv(request):
    result = Environment.objects.all()
    env_json = []
    for item in result:
        env_json.append({'id': item.id, 'name': item.name, 'env': item.env, 'ip': item.ip, 'port': item.port, 'username': item.username, 'password': item.password})

    return HttpResponse(json.dumps(env_json), content_type="application/json")


@csrf_exempt
def saveenv(request):
    name = request.POST.get('name', None)
    env = request.POST.get('env', None)
    ip = request.POST.get('ip', None)
    port = request.POST.get('port', None)

    env_exist = Environment.objects.filter(name=name).count()

    if env_exist > 0:
        result = {'status': False, 'message': '已存在该环境名称'}
        return HttpResponse(json.dumps(result), content_type="application/json")

    try:
        createtime = int(time.time())
        environment = Environment()
        environment.name = name
        environment.ip = ip
        environment.env = env
        environment.port = port
        environment.createtime = createtime
        environment.save()

        tmp = Environment.objects.filter(createtime=createtime).first()
        id = tmp.id
        status = True
    except Exception as ex:
        print(ex)
        id = 0
        status = False

    result = {'status': status, 'id': id}
    return HttpResponse(json.dumps(result), content_type="application/json")


@csrf_exempt
def deleteenv(request):
    id = request.POST.get('id', None)

    try:
        env = Environment.objects.filter(id=id).first()

        execute = Execute.objects.filter(env=env.name)
        if execute:
            status = False
            message = 'this environment is used by execute, cannot be deleted'
        else:
            env.delete()
            status = True
            message = 'delete enviroment sucessfully'

    except Exception as ex:
        message = str(ex)
        status = False

    result = {'status': status, 'message': message}

    return HttpResponse(json.dumps(result), content_type="application/json")


@csrf_exempt
def saveexecute(request):
    duration = request.POST.get('duration[label]', None)
    env = request.POST.get('env[label]', None)
    name = request.POST.get('name', None)
    hour = request.POST.get('hour', None)
    testcases = {}
    testsuite = {}

    if name:
        exes = Execute.objects.filter(name=name)
        if exes:
            result = {'status': False, 'message': '已存在同名的执行任务，请更换执行名称'}
            return HttpResponse(json.dumps(result), content_type="application/json")

    for r in request.POST:
        label = request.POST.get(r)
        if 'testsuite' in r and 'parent' in r and 'children' not in r:
            s_index = r.index('[')
            e_index = r.index(']')
            index = r[s_index + 1: e_index]
            if label not in testsuite.keys():
                testsuite[label] = [index]
            else:
                if index not in testsuite[label]:
                    testsuite[label].append(index)

    for r in request.POST:
        if 'testsuite' in r:
            label = request.POST.get(r)
            s_index = r.index('[')
            e_index = r.index(']')
            index = r[s_index + 1: e_index]
            if 'label' in r and 'parent' not in r and 'children' not in r:
                for k, v in testsuite.items():
                    if index in v:
                        parent = k
                        if parent not in testcases.keys():
                            testcases[parent] = [label]
                        else:
                            testcases[parent].append(label)

    if hour == '':
        hour = 0
    createtime = int(time.time())
    execute = Execute()
    execute.name = name
    execute.duration = duration
    execute.env = env
    execute.hour = hour
    # 0: new 1: executing 2: completed 3: removed
    execute.status = 0
    execute.createtime = createtime
    execute.save()

    e_result = Execute.objects.filter(createtime=createtime).first()
    execute_id = e_result.id

    createtime = int(time.time())
    for k, v in testcases.items():
        for item in v:
            testcase = Testcase()
            testcase.case_name = k + '.' + item
            testcase.createtime = createtime
            testcase.execute_id = execute_id
            testcase.save()

    result = {'status': True}
    return HttpResponse(json.dumps(result), content_type="application/json")


@csrf_exempt
def getexecute(request):
    executes_dict = []
    executes = Execute.objects.filter(status__in=[0, 1, 2]).all()
    for exe in executes:
        tmp = {
                'id': exe.id,
                'name': exe.name,
                'env': exe.env,
                'status': exe.status,
                'duration': exe.duration,
                'hour': exe.hour,
                'updatetime': str(exe.updatetime)
               }
        testcases = Testcase.objects.filter(execute__id=exe.id).all()
        testcase = []
        for c in testcases:
            testcase.append(c.case_name)
        tmp['testcase'] = testcase
        executes_dict.append(tmp)
    return HttpResponse(json.dumps(executes_dict), content_type="application/json")


@csrf_exempt
def deleteexecute(request):
    id = request.POST.get('id', None)
    if id is not None:
        try:
            exe = Execute.objects.get(pk=id)

            his = History.objects.filter(execute_id=id)
            for h in his:
                h.delete()

            testcases = Testcase.objects.filter(execute_id=id)
            for testcase in testcases:
                testcase.delete()

            exe.delete()
            status = True
            message = 'Delete execute successfully'
        except Exception as ex:
            message = str(ex)
            status = False

        result = {'status': status, 'message': message}
    else:
        result = {'status': False, 'message': 'Id is null'}

    return HttpResponse(json.dumps(result), content_type="application/json")


@csrf_exempt
def gethistory(request):
    history_dict = []
    executes = Execute.objects.all()
    for exe in executes:
        his = {
                'label': exe.name,
                'children': []
            }
        histories = History.objects.filter(execute_id=exe.id).order_by('-updatetime').all()

        index = len(histories)
        for history in histories:
            name = os.path.join(STATIC_URL, 'media', history.name, 'index.html')
            child = {'index': index, 'name': name, 'updatetime': str(history.updatetime)}

            his['children'].append(child)

            index -= 1
        if histories:
            history_dict.append(his)

    return HttpResponse(json.dumps(history_dict), content_type="application/json")






