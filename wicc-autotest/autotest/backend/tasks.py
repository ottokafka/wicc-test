from celery import shared_task
from time import sleep
import os


@shared_task
def Task_A(message):
    Task_A.update_state(state='PROGRESS', meta={'progress': 0})
    sleep(10)
    Task_A.update_state(state='PROGRESS', meta={'progress': 30})
    sleep(10)
    return message


def get_task_status(task_id):
    task = Task_A.AsyncResult(task_id)

    status = task.state
    progress = 0

    if status == u'SUCCESS':
        progress = 100
    elif status == u'FAILURE':
        progress = 0
    elif status == 'PROGRESS':
        progress = task.info['progress']

    return {'status': status, 'progress': progress}


def get_testsuite(folder_path):
    files_dict = {}
    for root, dirs, files in os.walk(folder_path):
        for f in files:
            if f.endswith('.py') and '_test' in f:
                print(f.split('_')[0])
                testsuite = f.split('_')[0]
                files_dict[testsuite] = []
                with open(os.path.join(root, f), 'r') as fp:
                    for line in fp.readlines():
                        if line.startswith('    def test_'):
                            files_dict[testsuite].append(line.split('(')[0].split('test_')[1])



if __name__ == '__main__':
    folder_path = '/Users/zhixiangliu/Documents/waykichain/wicc-test/wicc-autotest/inter-autotest/testcase'
    get_testsuite(folder_path)
