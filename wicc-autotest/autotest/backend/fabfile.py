from fabric.api import run, cd
from fabric.state import env

env.hosts = ['root@10.0.0.127']
env.password = 'Password01?'


def get_latest():
    print('remote ls')
    with cd('/root/workspace'):
        run('docker stop build-waykicoind-official && docker rm build-waykicoind-official '
            '&& HOME_DIR=/root/workspace/build-waykicoind-official '
            '&& SRC_DIR=/root/workspace/node-build2deploy-official '
        '&& mkdir -p $HOME_DIR && mkdir -p $SRC_DIR '
        '&& docker run --name build-waykicoind-official \
       -v $HOME_DIR/conf/WaykiChain.conf:/root/.WaykiChain/WaykiChain.conf \
       -v $HOME_DIR/data:/root/.WaykiChain/testnet \
       -v $HOME_DIR/bin:/opt/wicc/bin \
       -v $SRC_DIR:/opt/wicc/src:rw \
       -it wicc/waykicoind bash')


def build():
    print('build')


def replace_coind():
    print('replace')
