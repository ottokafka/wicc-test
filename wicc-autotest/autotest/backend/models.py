from django.db import models
import datetime
# Create your models here.


class Environment(models.Model):
    id = models.IntegerField(primary_key=True, auto_created=True)
    name = models.CharField(max_length=20)
    ip = models.GenericIPAddressField()
    port = models.IntegerField()
    username = models.CharField(max_length=20, default='')
    password = models.CharField(max_length=20, default='')
    env = models.CharField(max_length=20)
    createtime = models.IntegerField(verbose_name='创建时间戳')
    updatetime = models.DateTimeField(auto_now=True)


class Execute(models.Model):
    id = models.IntegerField(primary_key=True, auto_created=True)
    name = models.CharField(max_length=20, default='')
    duration = models.CharField(max_length=20)
    hour = models.IntegerField(default=0)
    env = models.CharField(max_length=20)
    # 0: new 1: executing 2: completed 3: removed
    status = models.IntegerField(default=0)
    createtime = models.IntegerField(verbose_name='创建时间戳')
    updatetime = models.DateTimeField(auto_now=True)


class Testcase(models.Model):
    id = models.IntegerField(primary_key=True, auto_created=True)
    description = models.CharField(max_length=500, default='')
    case_name = models.CharField(max_length=100)
    execute = models.ForeignKey(Execute, on_delete=True, default=None)
    createtime = models.IntegerField(verbose_name='创建时间戳')
    updatetime = models.DateTimeField(auto_now=True)


class History(models.Model):
    id = models.IntegerField(primary_key=True, auto_created=True)
    execute_id = models.IntegerField(default=0)
    name = models.CharField(max_length=50, default='')
    updatetime = models.DateTimeField(auto_now=True)
