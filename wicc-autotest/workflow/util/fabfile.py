from fabric.api import local


def deploy_latest_coind(filepath, destfilepath, container_name):
    # download latest coind
    download_cmd = 'curl -L --silent {} > {}'.format(filepath, destfilepath)
    local(download_cmd)

    # tar
    tar_cmd = 'tar -xvf {}'.format(destfilepath)
    local(tar_cmd)

    # restart container
    restart_cmd = 'docker restart {}'.format(container_name)
    local(restart_cmd)
