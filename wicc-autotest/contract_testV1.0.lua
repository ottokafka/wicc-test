mylib = require ("mylib")

LOG_LEVEL =
{
    INFO   = "[INFO]",
    OUTPUT = "[OUTPUT]",
    ERROR  = "[ERROR]"
}

LOG_TYPE =
{
    ENUM_STRING = 0,
    ENUM_NUMBER = 1
}

APP_OPERATE_TYPE=
{
    TEST_CONTRACT   = 0x11,  --合约测试
    TEST_DEBUG  = 0x12
}

ADDR_TYPE = {
    REGID  = 1,
    BASE58 = 2
}

OP_TYPE = {
    ADD_FREE = 1,
    SUB_FREE = 2
}
magicnum_len = 4
funcname_len = 32
parameternum_len = 4
parameter_len = 32
basedata_len = magicnum_len + funcname_len + parameternum_len



------------------------------------------引用自开发者中心-----------------------------------------------------

--[[
  功能:获取表从开始索引指定长度的元素集合
  参数：
	tbl:表
	start:开始索引
	length:长度
  返回值：
	一个新表
--]]
function GetValueFromArray(tbl, start, length)
    assert(start > 0,"GetValueFromArray start error.")
    assert(length > 0, "GetValueFromArray length error.")
    local newTab = {}
    local i
    for i = 0,length-1 do
        newTab[1 + i] = tbl[start + i]
    end
    return newTab
end


--遍历并输出table数组所有元素
Unpack = function (t,i)
    i = i or 1
    if t[i] then
        return t[i], Unpack(t,i+1)
    end
end

--判断传入的table是否为非空
TableIsNotEmpty = function (t)
    return _G.next(t) ~= nil
end
--判断传入的table是否为空
TableIsEmpty = function (t)
    return _G.next(t) == nil
end

--获取链上该合约中key对应的value值
GetContractValue = function (key)
    assert(#key > 0, "Key is empty")

    local tValue = { mylib.ReadData(key) }
    if TableIsNotEmpty(tValue) then
        return true,tValue
    else
        LogMsg(LOG_LEVEL.ERROR,"Key:"..key.." not exist")
        return false,nil
    end
end

--写/改数据
WriteOrModify = function (isConfig, writeTbl)
    if not isConfig then
        if not mylib.WriteData(writeTbl) then    error("Write error") end
    else
        if not mylib.ModifyData(writeTbl) then   error("Modify error") end
    end
end

--获取调用合约的内容
--参数startIndex为起始索引， 参数length为获取字段的长度， 另外contract为全局变量。
--返回contract 以startIndex为起始索引，length为长度获取的contract部分字段
GetContractTxParam = function (startIndex, length)
    assert(startIndex > 0, "GetContractTxParam start error(<=0).")
    assert(length > 0, "GetContractTxParam length error(<=0).")
    assert(startIndex+length-1 <= #contract, "GetContractTxParam length ".. length .." exceeds limit: " .. #contract)

    local newTbl = {}
    local i = 1
    for i = 1,length do
        newTbl[i] = contract[startIndex+i-1]
    end
    return newTbl
end

--获取字符串的byte table数组
--传入的参数param是字符串
--返回值为 字符串param转换成的byte table数组
getByte = function(param)
    return {string.byte(param,1,string.len(param))}
end

--字符转换拼接
--将table类型的数据，拼接成字符串，传入参数obj必须为table类型
--传入参数 hex 为bool值，是否为hex类型
serialize = function(obj, hex)
    local lua = ""
    local t = type(obj)

    if t == "table" then
        for i=1, #obj do
            if hex == false then
                lua = lua .. string.format("%c",obj[i])
            else
                lua = lua .. string.format("%02x",obj[i])
            end
        end
    elseif t == "nil" then
        return nil
    else
        error("can not serialize a " .. t .. " type.")
    end

    return lua
end
--字符转换拼接
--将table类型的数据，去除0x00元素,拼接成字符串，传入参数obj必须为table类型
--传入参数 hex 为bool值，是否为hex类型
serialize2 = function(obj, hex)
    local lua = ""
    local t = type(obj)

    if t == "table" then
        for i=1, #obj do
            if obj[i] ~= 0x00 then
                if hex == false then
                    lua = lua .. string.format("%c",obj[i])
                else
                    lua = lua .. string.format("%02x",obj[i])
                end
            end
        end
    elseif t == "nil" then
        return nil
    else
        error("can not serialize a " .. t .. " type.")
    end

    return lua
end

--检查是否为管理员账号
CheckIsAdmin = function ()
    local key = "admin"
    if key == nil then error("Admin type error") end
    local isConfig,adminAddr = GetContractValue(key)
    if not isConfig then error("Admin not set error") end
    local txAccountAddr = GetCurrTxAccountAddress()
    if not MemIsEqual(txAccountAddr, adminAddr) then error("Fake admin error")    end
    return adminAddr
end

--设置管理员账户
FuncSetAdmin = function ()
    --CheckIsAdmin()
    local adminAddress = GetContractTxParam(5, 34)
    local key = "admin"
    local isConfig,_ = GetContractValue(key)
    WriteOrModify(isConfig, {key=key, length=34, value=adminAddress})
end

--比较t1和t2是否相等
--参数t1和t2均为table类型
MemIsEqual = function (t1,t2)
    assert(TableIsNotEmpty(t1), "t1 is empty")
    assert(TableIsNotEmpty(t2), "t2 is empty")

    if(#t1 ~= #t2) then
        return false
    end

    local i = 1
    for i = #t1,1,-1 do
        if t1[i] ~= t2[i] then
            return false
        end
    end
    return true
end


------------------------------------------引用自开发者中心 End-----------------------------------------------------
LogMsg = function (log_level,msg)
    local logTable = {
        key = 0,
        length = string.len(log_level..msg),
        value = log_level..msg
    }
    mylib.LogPrint(logTable)
end


--[[
  功能: 增加key-value对至链数据库
  参数：
    Strkey:     key
    Value:      value - String或者Table类型
  返回值：
    无
--]]
Add_Strkey_Value_ToDb = function (Strkey,Value)
    local t = type(Value)
    local value_table = {}
    if t == "table" then
        value_table = Value
    else
        value_table = getByte(Value)
    end

    local writeTbl = {
        key = Strkey,
        length = #value_table,
        value = {}
    }
    writeTbl.value = value_table

    WriteOrModify(flase,writeTbl)
end



--[[
--解密
function mylib_Des2 ()

    local desTbl =
    {
        dataLen = 8,
        data = {},
        keyLen = 8,
        key = {},
        flag = 1
    }
    desTbl.data = {0xad, 0xdd, 0x1e, 0x1b, 0xeb, 0x8c, 0x10, 0x8d}
    for i = 1,8 do
        desTbl.key[i] = 0x33 + i
    end

    desTbl.flag = 0
    local content2 = {mylib.Des(desTbl) }

    print("content2=",serialize(content2,true))
    LogPrint(LOG_TYPE.ENUM_NUMBER,#content2,content2)
end

--加密

function mylib_Des1 ()

    local desTbl =
    {
        dataLen = 8,
        data = {},
        keyLen = 8,
        key = {},
        flag = 1
    }
    desTbl.data = {0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38} --"12345678"
    for i = 1,8 do
        desTbl.key[i] = 0x33 + i
    end

    desTbl.flag = 1
    local content1 = {mylib.Des(desTbl) }

    print("content1=",serialize(content1,true))
    LogPrint(LOG_TYPE.ENUM_NUMBER,#content1,content1)
end
--]]

mylib_LogPrint = function(LogType,Value)
    local LogTable = {
        key = 0, --日志类型
        length = 0, --value数据流的总长
        value = nil -- 字符串或数字流
    }
    --保存数据流
    LogTable.key = LogType
    LogTable.length = #Value
    LogTable.value = Value
    mylib.LogPrint(LogTable)
end
Test_LogPrint = function(typeTbl,valueTbl)
    LogMsg(LOG_LEVEL.INFO,"Start Test LogPrint")
    if serialize2(typeTbl,false) == "string" then --string
        mylib_LogPrint(LOG_TYPE.ENUM_STRING,serialize2(valueTbl,false))
    elseif serialize2(typeTbl,false) == "number" then --Number
        mylib_LogPrint(LOG_TYPE.ENUM_NUMBER,valueTbl)
    end
    LogMsg(LOG_LEVEL.INFO,"Test LogPrint End")
end


--获取当前交易hash
--返回值
--    正常: true,32bytes txhashTbl
--    异常：false
mylib_GetCurTxHash = function()

    local txhashTbl = {mylib.GetCurTxHash()}
    if #txhashTbl == 32 then
        return 1,txhashTbl
    else
        return 0
    end
end
Test_GetCurTxHash = function()
    LogMsg(LOG_LEVEL.INFO,"Start Test GetCurTxHash")
    local err_code = "mylib.GetCurTxHash error,result len ~= 32!"
    local output_key = "GetCurTxHash txhash is:"
    local ret,txhashTbl = mylib_GetCurTxHash()
    if ret == 1 then
        LogMsg(LOG_LEVEL.OUTPUT,output_key..serialize(txhashTbl,true))
    elseif ret == 0 then
        LogMsg(LOG_LEVEL.ERROR,err_code)
    end
    LogMsg(LOG_LEVEL.INFO,"Test GetCurTxHash End")
end


--获取当前调用账户
--返回值
--    正常: true,6bytes regid
--    异常: false
mylib_GetCurTxAccount = function()

    local result = {mylib.GetCurTxAccount() }
    if #result == 6 then
        return 1, result
    else
        return 0
    end
end
Test_GetCurTxAccount = function()
    LogMsg(LOG_LEVEL.INFO,"Start Test GetCurTxAccount")
    local err_code = "mylib.GetCurTxAccount error,result len ~= 6!"
    local output_key = "GetCurTxAccount txaddr is:"
    local ret,regidTbl = mylib_GetCurTxAccount()
    if ret == 1 then
        LogMsg(LOG_LEVEL.OUTPUT,output_key..serialize({mylib.GetBase58Addr(Unpack(regidTbl))},false))
    elseif ret == 0 then
        LogMsg(LOG_LEVEL.ERROR,err_code)
    end
    LogMsg(LOG_LEVEL.INFO,"Test GetCurTxAccount End")
end

--获取指定hash对应的发起者sender regid
--返回值
--    正常: true,6bytes regid
--    异常: false
mylib_GetTxRegID = function(txhashTbl)

    local result = {mylib.GetTxRegID(Unpack(txhashTbl)) }
    if #result == 6 then
        return 1, result
    else
        return 0
    end
end
Test_GetTxRegID = function(txhashTbl)
    LogMsg(LOG_LEVEL.INFO,"Start Test GetTxRegID")
    local err_code_0 = "mylib.GetTxRegID error,input txhash len ~= 32!"
    local err_code_1 = "mylib.GetTxRegID error,result len ~= 6!"
    local output_key = "GetTxRegID txaddr is:"
    local ret,regidTbl = mylib_GetTxRegID(txhashTbl)
    if ret == 1 then
        LogMsg(LOG_LEVEL.OUTPUT,output_key..serialize({mylib.GetBase58Addr(Unpack(regidTbl))},false))
    elseif ret == 0 then
        if #txhashTbl ~= 32 then
            LogMsg(LOG_LEVEL.ERROR,err_code_0)
        end
        LogMsg(LOG_LEVEL.ERROR,err_code_1)
    end
    LogMsg(LOG_LEVEL.INFO,"Test GetTxRegID End")
end

--获取用户地址的公钥
--返回值
--    正常: 1,公钥(hex)
--    异常: 0
mylib_GetAccountPublickey = function ()

    local err_code = "mylib.GetCurTxAccount error,result len ~= 6!"
    local exist,regidtbl = mylib_GetCurTxAccount()
    if exist == 0 then
        LogMsg(LOG_LEVEL.ERROR,err_code)
    end

    local result = {mylib.GetAccountPublickey(Unpack(regidtbl)) }
    if #result == 33 then
        local  pubkey = serialize(result,true)
        return 1, pubkey
    else
        return 0
    end
end
Test_GetAccountPublickey = function()
    LogMsg(LOG_LEVEL.INFO,"Start Test GetAccountPublickey")
    local err_code = "mylib.GetAccountPublickey error,result len ~= 33!"
    local output_key = "GetAccountPublickey publickey is:"
    local ret,pubkey = mylib_GetAccountPublickey()
    if ret == 1  then
        LogMsg(LOG_LEVEL.OUTPUT,output_key..pubkey)
    elseif ret == 0 then
        LogMsg(LOG_LEVEL.ERROR,err_code)
    end
    LogMsg(LOG_LEVEL.INFO,"Test GetAccountPublickey End")
end

--获取用户base58地址
--返回值
--    正常: 1,{base58地址}
--    异常: 0
mylib_GetBase58Addr = function (regidTbl)

    local base58addrTbl = {mylib.GetBase58Addr(Unpack(regidTbl)) }
    if #base58addrTbl == 34 then
        return 1,base58addrTbl
    else
        return 0
    end
end
Test_GetBase58Addr = function()
    LogMsg(LOG_LEVEL.INFO,"Start Test GetBase58Addr")
    local err_code_0 = "mylib.GetCurTxAccount error,result len ~= 6!"
    local err_code_1 = "mylib.GetBase58Addr error,result len ~= 34!"
    local output_key = "GetBase58Addr address is:"
    local exist,regidtbl = mylib_GetCurTxAccount()
    if exist == 0 then
        LogMsg(LOG_LEVEL.ERROR,err_code_0)
    end

    local ret,addrTbl = mylib_GetBase58Addr(regidtbl)
    if ret == 1  then
        LogMsg(LOG_LEVEL.OUTPUT,output_key..serialize(addrTbl,false))
    elseif ret == 0 then
        LogMsg(LOG_LEVEL.ERROR,err_code_1)
    end
    LogMsg(LOG_LEVEL.INFO,"Test GetBase58Addr End")
end

--获取合约的regid地址
--返回值
--    正常: 1,{regid}
--    异常: 0
mylib_GetScriptID = function ()
    local result = {mylib.GetScriptID() }
    if #result == 6 then
        return 1, result
    else
        return 0
    end
end
Test_GetScriptID = function()
    LogMsg(LOG_LEVEL.INFO,"Start Test GetScriptID")
    local err_code = "mylib.GetScriptID error,result len ~= 6!"
    local output_key = "GetScriptID contract address is:"
    local ret,regidTbl = mylib_GetScriptID()
    if ret == 1  then
        LogMsg(LOG_LEVEL.OUTPUT,output_key..serialize({mylib.GetBase58Addr(Unpack(regidTbl))},false))
    elseif ret == 0 then
        LogMsg(LOG_LEVEL.ERROR,err_code)
    end
    LogMsg(LOG_LEVEL.INFO,"Test GetScriptID End")
end


--获取当前调用合约的交易金额
--返回值
--    正常: 1,amount
--    异常: 0
mylib_GetCurTxPayAmount = function()
    local result = {mylib.GetCurTxPayAmount()}
    if #result == 8 then
        local amount = mylib.ByteToInteger(Unpack(result))
        return 1,amount
    else
        return 0
    end
end
Test_GetCurTxPayAmount = function()
    LogMsg(LOG_LEVEL.INFO,"Start Test GetCurTxPayAmount")
    local err_code = "mylib.GetCurTxPayAmount error,result len ~= 8!"
    local output_key = "GetCurTxPayAmount amount is:"
    local ret,amount = mylib_GetCurTxPayAmount()
    if ret == 1  then
        LogMsg(LOG_LEVEL.OUTPUT,output_key..amount)
    elseif ret == 0 then
        LogMsg(LOG_LEVEL.ERROR,err_code)
    end
    LogMsg(LOG_LEVEL.INFO,"Test GetCurTxPayAmount End")
end


--根据交易hash(不能是当前交易hash)获取调用合约的内容
--返回值
--    正常: 1,{调用合约的内容}
--    异常: false
mylib_GetTxContract = function(txhashTbl)

    local result = {mylib.GetTxContract(Unpack(txhashTbl))}
    if #result > 0 then
        return 1,result
    else
        return 0
    end
end
Test_GetTxContract = function(txhashTbl)
    LogMsg(LOG_LEVEL.INFO,"Start Test GetTxContract")
    local err_code_0 = "mylib.GetTxContract error,input txhash len ~= 32!"
    local err_code_1 = "mylib.GetTxContract error,result nil!"
    local output_key = "GetTxContract content is:"
    local ret,contentTbl = mylib_GetTxContract(txhashTbl)
    if ret == 1 then
        LogMsg(LOG_LEVEL.OUTPUT,output_key..serialize(contentTbl,true))
    elseif ret == 0 then
        if #txhashTbl ~= 32 then
            LogMsg(LOG_LEVEL.ERROR,err_code_0)
        end
        LogMsg(LOG_LEVEL.ERROR,err_code_1)
    end
    LogMsg(LOG_LEVEL.INFO,"Test GetTxContract End")
end

--根据合约regid和key值获取value值
--返回值
--    正常: 1,{value}值
--    异常: 0
mylib_GetScriptData = function(regidTbl,keyTbl)

    local paraTbl = {
        id = regidTbl,
        key = serialize2(keyTbl,false), --关键字

    }
    local result = {mylib.GetScriptData(paraTbl)}
    if #result > 0 then
        return 1,result
    else
        return 0
    end
end
Test_GetScriptData = function(parameter1Tbl,keyTbl)
    LogMsg(LOG_LEVEL.INFO,"Start Test GetScriptData")
    local err_code = "mylib.GetScriptData error,result nil!"
    local output_key = "GetScriptData value is:"
    local regidTbl = {parameter1Tbl[27],parameter1Tbl[28],parameter1Tbl[29],parameter1Tbl[30],parameter1Tbl[31],parameter1Tbl[32]}
    local ret,valueTbl = mylib_GetScriptData(regidTbl,keyTbl)
    if ret == 1 then
        LogMsg(LOG_LEVEL.OUTPUT,output_key..serialize(valueTbl,true))
    elseif ret == 0 then
        LogMsg(LOG_LEVEL.ERROR,err_code)
    end
    LogMsg(LOG_LEVEL.INFO,"Test GetScriptData End")
end

--获取当前运行高度
--返回值
--    正常: 1,height值
--    异常: 0
mylib_GetCurRunEnvHeight = function ()
    local result = mylib.GetCurRunEnvHeight()
    if result > 0 then
        return 1,result
    else
        return 0
    end
end
Test_GetCurRunEnvHeight = function()
    LogMsg(LOG_LEVEL.INFO,"Start Test GetCurRunEnvHeight")
    local err_code = "mylib.GetCurRunEnvHeight error,result <= 0!"
    local output_key = "GetCurRunEnvHeight height is:"

    local ret,height = mylib_GetCurRunEnvHeight()
    if ret == 1 then
        LogMsg(LOG_LEVEL.OUTPUT,output_key..height)
    elseif ret == 0 then
        LogMsg(LOG_LEVEL.ERROR,err_code)
    end
    LogMsg(LOG_LEVEL.INFO,"Test GetCurRunEnvHeight End")
end

--根据交易hash获取确认高度
--返回值
--    正常: 1,高度值-Number-1234.0
--    异常: 0
mylib_GetTxConfirmHeight = function (txhashTbl)
    local result = mylib.GetTxConfirmHeight(Unpack(txhashTbl))
    if result > 0 then
        return 1,result
    else
        return 0
    end
end
Test_GetTxConfirmHeight = function(txhashTbl)
    LogMsg(LOG_LEVEL.INFO,"Start Test GetTxConfirmHeight")
    local err_code = "mylib.GetTxConfirmHeight error,result <= 0!"
    local output_key = "GetTxConfirmHeight height is:"
    local ret,height = mylib_GetTxConfirmHeight(txhashTbl)
    if ret == 1 then
        LogMsg(LOG_LEVEL.OUTPUT,output_key..height)
    elseif ret == 0 then
        LogMsg(LOG_LEVEL.ERROR,err_code)
    end
    LogMsg(LOG_LEVEL.INFO,"Test GetTxConfirmHeight End")
end

--根据指定高度获取区块hash
--返回值
--    正常: 1,{hash}
--    异常: 0
mylib_GetBlockHash = function(height)
    local result = {mylib.GetBlockHash(height)}
    if #result == 32 then
        return 1,result
    else
        return 0
    end
end
Test_GetBlockHash = function(parameter1Tbl)
    LogMsg(LOG_LEVEL.INFO,"Start Test GetBlockHash")
    local err_code_0 = "mylib.ByteToInteger error,result < 0!"
    local err_code_1 = "mylib.GetBlockHash error,result <= 0!"
    local output_key = "GetBlockHash blockhash is:"
    local heightTbl = {parameter1Tbl[1],parameter1Tbl[2],parameter1Tbl[3],parameter1Tbl[4],parameter1Tbl[5],parameter1Tbl[6],parameter1Tbl[7],parameter1Tbl[8]}
    local ret0,height =  mylib_ByteToInteger(heightTbl)
    if ret0 == 0 then
        LogMsg(LOG_LEVEL.ERROR,err_code_0)
    end

    local ret1,hashTbl = mylib_GetBlockHash(height)
    if ret1 == 1 then
        LogMsg(LOG_LEVEL.OUTPUT,output_key..serialize(hashTbl,true))
    elseif ret1 == 0 then
        LogMsg(LOG_LEVEL.ERROR,err_code_1)
    end
    LogMsg(LOG_LEVEL.INFO,"Test GetBlockHash End")
end

--根据区块高度获取区块生成时间戳
--返回值
--    正常: 1,生成区块的时间戳
--    异常: 0
mylib_GetBlockTimestamp = function(height)
    local ts = mylib.GetBlockTimestamp(height)
    if ts > 0 then
        return 1,ts
    else
        return 0
    end
end
Test_GetBlockTimestamp = function(parameter1Tbl)
    LogMsg(LOG_LEVEL.INFO,"Start Test GetBlockTimestamp")
    local err_code_0 = "mylib.ByteToInteger error,result < 0!"
    local err_code_1 = "mylib.GetBlockTimestamp error,result <= 0!"
    local output_key = "GetBlockTimestamp timestamp is:"
    local heightTbl = {parameter1Tbl[1],parameter1Tbl[2],parameter1Tbl[3],parameter1Tbl[4],parameter1Tbl[5],parameter1Tbl[6],parameter1Tbl[7],parameter1Tbl[8]}
    local ret0,height =  mylib_ByteToInteger(heightTbl)
    if ret0 == 0 then
        LogMsg(LOG_LEVEL.ERROR,err_code_0)
    end

    local ret1,ts = mylib_GetBlockTimestamp(height)
    if ret1 == 1 then
        LogMsg(LOG_LEVEL.OUTPUT,output_key..ts)
    elseif ret1 == 0 then
        LogMsg(LOG_LEVEL.ERROR,err_code_1)
    end
    LogMsg(LOG_LEVEL.INFO,"Test GetBlockTimestamp End")
end

--将key-value写到链上数据库
--返回值
--    正常: 1
--    异常: 0
mylib_WriteData = function(key,value)

    local value_table = {}
    if type(value) == "table" then
        value_table = value
    else
        value_table = getByte(value)
    end
    local writeTbl = {
        key = key,
        length = #value_table,
        value = value_table
    }
    local result = mylib.WriteData(writeTbl)
    if result ~= nil then
        return 1
    else
        return 0
    end
end
Test_WriteData = function(parameter1Tbl,parameter2Tbl)
    LogMsg(LOG_LEVEL.INFO,"Start Test WriteData")
    local err_code = "mylib.WriteData failed!"
    local output_key = "WriteData successfully"
    local key = serialize2(parameter1Tbl,false)
    local value = serialize2(parameter2Tbl,false)
print("key=",key)
    print("value=",value)
    local ret = mylib_WriteData(key,value)
    if ret == 1 then
        LogMsg(LOG_LEVEL.OUTPUT,output_key)
    elseif ret == 0 then
        LogMsg(LOG_LEVEL.ERROR,err_code)
    end
    LogMsg(LOG_LEVEL.INFO,"Test WriteData End")
end


--修改链上数据库key-value对的值
--返回值
--    正常: 1
--    异常: 0
mylib_ModifyData = function(key,value)

    local value_table = {}
    if type(value) == "table" then
        value_table = value
    else
        value_table = getByte(value)
    end
    local writeTbl = {
        key = key,
        length = #value_table,
        value = value_table
    }
    local result = mylib.ModifyData(writeTbl)
    if result ~= nil then
        return 1
    else
        return 0
    end
end
Test_ModifyData = function(parameter1Tbl,parameter2Tbl)
    LogMsg(LOG_LEVEL.INFO,"Start Test ModifyData")
    local err_code = "mylib.ModifyData failed!"
    local output_key = "ModifyData successfully"
    local key = serialize2(parameter1Tbl,false)
    local value = serialize2(parameter2Tbl,false)
    local ret = mylib_WriteData(key,value)
    if ret == 1 then
        LogMsg(LOG_LEVEL.OUTPUT,output_key)
    elseif ret == 0 then
        LogMsg(LOG_LEVEL.ERROR,err_code)
    end
    LogMsg(LOG_LEVEL.INFO,"Test ModifyData End")
end

--查询链上数据库key-value对的值
--返回值
--    正常: 1,{value}
--    异常: 0
mylib_ReadData = function (key)
    local result = {mylib.ReadData(key) }
    if #result > 0 then
        return 1,result
    else
        return 0
    end
end
Test_ReadData = function(parameter1Tbl)
    LogMsg(LOG_LEVEL.INFO,"Start Test ReadData")
    local err_code = "mylib.ReadData failed!"
    local output_key = "ReadData valuehex:"
    local key = serialize2(parameter1Tbl,false)
    local ret,valueTbl = mylib_ReadData(key)
    if ret == 1 then
        LogMsg(LOG_LEVEL.OUTPUT,output_key..serialize(valueTbl,true)) --输出hex
    elseif ret == 0 then
        LogMsg(LOG_LEVEL.ERROR,err_code)
    end
    LogMsg(LOG_LEVEL.INFO,"Test ReadData End")
end

--删除链上数据库key-value对
--返回值
--    正常: 1
--    异常: 0
mylib_DeleteData = function (key)
    local result = mylib.DeleteData(key)
    if result == true then
        return 1
    else
        return 0
    end
end
Test_DeleteData = function(parameter1Tbl)
    LogMsg(LOG_LEVEL.INFO,"Start Test DeleteData")
    local err_code = "mylib.DeleteData failed!"
    local output_key = "DeleteData successfully"
    local key = serialize2(parameter1Tbl,false)
    local ret = mylib_DeleteData(key)
    if ret == 1 then
        LogMsg(LOG_LEVEL.OUTPUT,output_key)
    elseif ret == 0 then
        LogMsg(LOG_LEVEL.ERROR,err_code)
    end
    LogMsg(LOG_LEVEL.INFO,"Test DeleteData End")
end

--获取sha256返回值
--返回值
--    正常: 1,{sha256值}
--    异常: 0
mylib_Sha256Once = function(srcStr)

    local result = {mylib.Sha256Once(srcStr)}
    if #result == 32 then
        return 1,result
    else
        return 0
    end
end
Test_Sha256Once = function(parameter1Tbl)
    LogMsg(LOG_LEVEL.INFO,"Start Test Sha256Once")
    local err_code = "mylib.Sha256Once error,#result ~= 32!"
    local output_key = "Sha256Once value is:"
    local src =serialize2(parameter1Tbl,false)
    local ret,sha256 = mylib_Sha256Once(src)
    if ret == 1 then
        LogMsg(LOG_LEVEL.OUTPUT,output_key..serialize(sha256,true))
    elseif ret == 0 then
        LogMsg(LOG_LEVEL.ERROR,err_code)
    end
    LogMsg(LOG_LEVEL.INFO,"Test Sha256Once End")
end

--对签名结果进行验签
--返回值
--    正常: 1
--    异常: 0
mylib_VerifySignature = function(datarawTbl,publickeyTbl,signatureTbl)
    local sigTbl =
    {
        dataLen = #datarawTbl,  --原始数据长度
        data = datarawTbl,--原始签名数据
        pubKeyLen = #publickeyTbl, --签名公钥长度
        pubKey = publickeyTbl,  --公钥
        signatureLen = #signatureTbl,--已签名数据长度
        signature = signatureTbl --已签名数据
    }
    local result = mylib.VerifySignature(sigTbl)
    if result then
        return 1
    else
        return 0
    end
end
Test_VerifySignature = function(parameter1Tbl)
    LogMsg(LOG_LEVEL.INFO,"Start Test VerifySignature")
    local err_code = "mylib.VerifySignature failed"
    local output_key = "VerifySignature successfully"
    local _,dataLen = mylib_ByteToInteger(GetValueFromArray(parameter1Tbl,1,4))
    local datarawTbl = GetValueFromArray(parameter1Tbl,4+1,dataLen)
    local _,pubKeyLen = mylib_ByteToInteger(GetValueFromArray(parameter1Tbl,4+dataLen+1,4))
    local publickeyTbl = GetValueFromArray(parameter1Tbl,4+dataLen+4+1,pubKeyLen)
    local _,signatureLen = mylib_ByteToInteger(GetValueFromArray(parameter1Tbl,4+dataLen+4+pubKeyLen+1,4))
    local signatureTbl = GetValueFromArray(parameter1Tbl,4+dataLen+4+pubKeyLen+4+1,signatureLen)
    local ret = mylib_VerifySignature(datarawTbl,publickeyTbl,signatureTbl)
    if ret == 1 then
        LogMsg(LOG_LEVEL.OUTPUT,output_key)
    elseif ret == 0 then
        LogMsg(LOG_LEVEL.ERROR,err_code)
    end
    LogMsg(LOG_LEVEL.INFO,"Test VerifySignature End")
end

--Des加解密
--返回值
--    正常: 返回加解密{结果}
--    异常: 0
mylib_Des = function(datatbl,keytbl,flag)

    local desTbl =
    {
        dataLen = #datatbl,--加密数据长度
        data = datatbl,--加密数据
        keyLen = #keytbl, --私钥长度
        key = keytbl, --私钥
        flag = flag --1加密 0解密
    }
    local result = {mylib.Des(desTbl)}
    if #result > 0 then
        return 1,result
    else
        return 0
    end
end
Test_Des = function(parameter1Tbl)
    LogMsg(LOG_LEVEL.INFO,"Start Test Des")
    local err_code = "mylib.Des failed"
    local output_key = "Des result is:"
    local _,dataLen = mylib_ByteToInteger(GetValueFromArray(parameter1Tbl,1,4))
    local datarawTbl = GetValueFromArray(parameter1Tbl,4+1,dataLen)
    local _,keyLen = mylib_ByteToInteger(GetValueFromArray(parameter1Tbl,4+dataLen+1,4))
    local keytbl = GetValueFromArray(parameter1Tbl,4+dataLen+4+1,keyLen)
    local flagStr = serialize({parameter1Tbl[#parameter1Tbl]},false)

    local ret,value = mylib_Des(datarawTbl,keytbl,tonumber(flagStr))
    if ret == 1 then
        LogMsg(LOG_LEVEL.OUTPUT,output_key..serialize(value,true))
    elseif ret == 0 then
        LogMsg(LOG_LEVEL.ERROR,err_code)
    end
    LogMsg(LOG_LEVEL.INFO,"Test Des End")
end


--将Number类型数据转成4 bytes类型数据
--参数 0 ~ 2^32-1 4294967295
--返回值
--    正常: 1,{byte4}
--    异常: 0
mylib_IntegerToByte4 = function(num)
    local result = {mylib.IntegerToByte4(num) }
    if #result == 4 then
        return 1,result
    else
        return 0
    end
end
Test_IntegerToByte4 = function(parameter1Tbl)
    LogMsg(LOG_LEVEL.INFO,"Start Test IntegerToByte4")
    local err_code_0 = "mylib.ByteToInteger error,result nil!"
    local err_code_1 = "mylib.IntegerToByte4 error,result len ~= 4!"
    local output_key = "IntegerToByte4 result is:"
    local numTbl = {parameter1Tbl[1],parameter1Tbl[2],parameter1Tbl[3],parameter1Tbl[4] }
    local ret0,num = mylib_ByteToInteger(numTbl)
    if ret0 == 0 then
        LogMsg(LOG_LEVEL.ERROR,err_code_0)
    end

    local ret1,numTbl = mylib_IntegerToByte4(num)
    if ret1 == 1 then
        LogMsg(LOG_LEVEL.OUTPUT,output_key..serialize(numTbl,true))
    else
        LogMsg(LOG_LEVEL.ERROR,err_code_1)
    end
    LogMsg(LOG_LEVEL.INFO,"Test IntegerToByte4 End")
end

--将Number类型数据转成8 bytes类型数据
--参数
--返回值
--    正常: 1,{byte8}
--    异常: 0
mylib_IntegerToByte8 = function(num)

    local result = {mylib.IntegerToByte8(num) }
    if #result == 8 then
        return 1,result
    else
        return 0
    end
end
Test_IntegerToByte8 = function(parameter1Tbl)
    LogMsg(LOG_LEVEL.INFO,"Start Test IntegerToByte8")
    local err_code_0 = "mylib.ByteToInteger error,result nil!"
    local err_code_1 = "mylib.IntegerToByte8 error,result len ~= 4!"
    local output_key = "IntegerToByte8 result is:"
    local numTbl = {parameter1Tbl[1],parameter1Tbl[2],parameter1Tbl[3],parameter1Tbl[4],
        parameter1Tbl[5],parameter1Tbl[6],parameter1Tbl[7],parameter1Tbl[8]}
    local ret0,num = mylib_ByteToInteger(numTbl)
    if ret0 == 0 then
        LogMsg(LOG_LEVEL.ERROR,err_code_0)
    end

    local ret1,numTbl = mylib_IntegerToByte8(num)
    if ret1 == 1 then
        LogMsg(LOG_LEVEL.OUTPUT,output_key..serialize(numTbl,true))
    else
        LogMsg(LOG_LEVEL.ERROR,err_code_1)
    end
    LogMsg(LOG_LEVEL.INFO,"Test IntegerToByte8 End")
end

--将8bytes/4bytes类型数据转成Number类型数据
--返回值
--    正常: 1,Number数据
--    异常: 0
mylib_ByteToInteger = function(table)

    local result = mylib.ByteToInteger(Unpack(table))
    if result ~= nil then
        return 1,result
    else
        return 0
    end

end
Test_ByteToInteger = function(parameter1Tbl)
    LogMsg(LOG_LEVEL.INFO,"Start Test ByteToInteger")
    local err_code = "mylib.ByteToInteger error,result nil!"
    local output_key = "ByteToInteger result is:"
    local byteTbl = {parameter1Tbl[1],parameter1Tbl[2],parameter1Tbl[3],parameter1Tbl[4],
        parameter1Tbl[5],parameter1Tbl[6],parameter1Tbl[7],parameter1Tbl[8]}
    local ret,num = mylib_ByteToInteger(byteTbl)
    if ret == 1 then
        LogMsg(LOG_LEVEL.OUTPUT,output_key..num)
    else
        LogMsg(LOG_LEVEL.ERROR,err_code)
    end
    LogMsg(LOG_LEVEL.INFO,"Test ByteToInteger End")
end

--用于token的发行、转账、冻结等操作
--返回值
--    正常: 1
--    异常: 0
mylib_WriteOutAppOperate = function(type,addrTbl,amountTbl)
    local appOperateTbl = {
        operatorType = 0,
        outHeight = 0,
        moneyTbl = {},
        userIdLen = 0,
        userIdTbl = {},
        fundTagLen = 0,   --fund tag len
        fundTagTbl = {}   --fund tag
    }
    appOperateTbl.operatorType = type
    appOperateTbl.moneyTbl = amountTbl
    appOperateTbl.userIdLen = #addrTbl
    appOperateTbl.userIdTbl = addrTbl

    local result = mylib.WriteOutAppOperate(appOperateTbl)
    if result == true then
        return 1
    else
        return 0
    end

end
Test_WriteOutAppOperate = function(parameter1Tbl)
    LogMsg(LOG_LEVEL.INFO,"Start Test WriteOutAppOperate")
    assert(#parameter1Tbl > 34,"WriteOutAppOperate parameter <= 34!")
    local err_code = "mylib.WriteOutAppOperat error,result false!"
    local output_key = "WriteOutAppOperat successfully"
    local addrTbl = GetValueFromArray(parameter1Tbl,1,34)
    local amountTbl = GetValueFromArray(parameter1Tbl,34+1,8)
    local ret = mylib_WriteOutAppOperate(OP_TYPE.ADD_FREE,addrTbl,amountTbl)
    if ret == 1 then
        LogMsg(LOG_LEVEL.OUTPUT,output_key)
    else
        LogMsg(LOG_LEVEL.ERROR,err_code)
    end
    LogMsg(LOG_LEVEL.INFO,"Test WriteOutAppOperate End")
end

--用于查询指定账户的合约代币余额
--返回值
--    正常: 1,{byte8余额}
--    异常: 0
mylib_GetUserAppAccValue = function (addrTbl)
    local idTbl = {
        idLen = #addrTbl, --base58地址长度
        idValueTbl = addrTbl
    }
    local moneyTbl = { mylib.GetUserAppAccValue(idTbl) }
    if #moneyTbl > 0 then
        return 1,moneyTbl
    else
        return 0
    end
end
Test_GetUserAppAccValue = function (parameter1Tbl)
    LogMsg(LOG_LEVEL.INFO,"Start Test GetUserAppAccValue")
    assert(#parameter1Tbl >= 34,"GetUserAppAccValue parameter < 34!")
    local err_code = "mylib.GetUserAppAccValue error,result nil!"
    local output_key = "GetUserAppAccValue amount is:"
    local addrTbl = GetValueFromArray(parameter1Tbl,1,34)
    local ret,moneyTbl = mylib_GetUserAppAccValue(addrTbl)
    print("money=",mylib.ByteToInteger(Unpack(moneyTbl)))
    if ret == 1 then
        LogMsg(LOG_LEVEL.OUTPUT,output_key..mylib.ByteToInteger(Unpack(moneyTbl)))
    else
        LogMsg(LOG_LEVEL.ERROR,err_code)
    end
    LogMsg(LOG_LEVEL.INFO,"Test GetUserAppAccValue End")
end


--用于操作地址账户里的WICC
--返回值
--    正常: 1
--    异常: 0
mylib_WriteOutput = function (opType, addrType, accountIdTbl, moneyTbl)
    local writeOutputTbl = {
        addrType = addrType,
        accountIdTbl = accountIdTbl,
        operatorType = opType,
        outHeight = 0,
        moneyTbl = moneyTbl
    }
    local result = mylib.WriteOutput(writeOutputTbl)
    if result == true then
        return 1
    else
        return 0
    end
end
Test_WriteOutput = function (parameter1Tbl)
    LogMsg(LOG_LEVEL.INFO,"Start Test WriteOutput")
    assert(#parameter1Tbl >= 34+8,"WriteOutput parameter < 34+8!")
    local err_code_0 = "mylib.WriteOutput error, result false!"
    local err_code_1 = "mylib.WriteOutput error, balance not enough!"
    local output_key = "WriteOutput successfully"
    local toTbl   = GetValueFromArray(parameter1Tbl,1,34)
    local amountTbl   = GetValueFromArray(parameter1Tbl,34+1,8)
    local _,contract_regidTbl = mylib_GetScriptID()
    local _,base58fromTbl = mylib_GetBase58Addr(contract_regidTbl)
    local _,balanceTbl = mylib_QueryAccountBalance(base58fromTbl)
    if mylib.ByteToInteger(Unpack(balanceTbl)) < mylib.ByteToInteger(Unpack(amountTbl)) then
        LogMsg(LOG_LEVEL.ERROR,err_code_1)
    end
    local ret_add = mylib_WriteOutput(OP_TYPE.SUB_FREE, ADDR_TYPE.BASE58, base58fromTbl, amountTbl)
    local ret_sub = mylib_WriteOutput(OP_TYPE.ADD_FREE, ADDR_TYPE.BASE58, toTbl, amountTbl)
    if ret_add == 1 and ret_sub == 1 then
        LogMsg(LOG_LEVEL.OUTPUT,output_key)
    else
        LogMsg(LOG_LEVEL.ERROR,err_code_0)
    end
    LogMsg(LOG_LEVEL.INFO,"Test WriteOutput End")
end



--获取用户WICC余额
--返回值
--    正常: 1,balance
--    异常: 0
mylib_QueryAccountBalance = function(addrTbl)
    local result = {mylib.QueryAccountBalance(Unpack(addrTbl)) }
    if #result == 8 then
        return 1,result
    else
        return 0
    end
end
Test_QueryAccountBalance = function(parameter1Tbl)
    LogMsg(LOG_LEVEL.INFO,"Start Test QueryAccountBalance")
    assert(#parameter1Tbl >= 34,"QueryAccountBalance parameter < 34!")
    local err_code = "mylib.QueryAccountBalance error,result len ~= 8!"
    local output_key = "QueryAccountBalance amount is:"
    local addrTbl = GetValueFromArray(parameter1Tbl,1,34)
    local ret, moneyTbl = mylib_QueryAccountBalance(addrTbl)
    if ret == 1 then
        LogMsg(LOG_LEVEL.OUTPUT,output_key..mylib.ByteToInteger(Unpack(moneyTbl)))
    else
        LogMsg(LOG_LEVEL.ERROR,err_code)
    end
    LogMsg(LOG_LEVEL.INFO,"Test QueryAccountBalance End")
end

RunAutoTestTask = function(funcname,parameter1Tbl,parameter2Tbl)

    if funcname == "LogPrint" then  --2入参
        Test_LogPrint(parameter1Tbl,parameter2Tbl)
    elseif funcname == "GetCurTxHash" then --0入参
        Test_GetCurTxHash()
    elseif funcname == "GetCurTxAccount" then --0入参
        Test_GetCurTxAccount()
    elseif funcname == "GetTxRegID" then --1入参:txhash
        Test_GetTxRegID(parameter1Tbl)
    elseif funcname == "GetAccountPublickey" then --0入参
        Test_GetAccountPublickey()
    elseif funcname == "GetBase58Addr" then --0入参
        Test_GetBase58Addr()
    elseif funcname == "GetScriptID" then --0入参
        Test_GetScriptID()
    elseif funcname == "GetCurTxPayAmount" then --0入参
        Test_GetCurTxPayAmount()
    elseif funcname == "GetTxContract" then --1入参:txhash
        Test_GetTxContract(parameter1Tbl)
    elseif funcname == "GetScriptData" then --2入参:regid、key
        Test_GetScriptData(parameter1Tbl,parameter2Tbl)
    elseif funcname == "GetCurRunEnvHeight" then --0入参
        Test_GetCurRunEnvHeight() --异常，出现连续调用问题，高度不一致
    elseif funcname == "GetTxConfirmHeight" then --1入参：txhash
        Test_GetTxConfirmHeight(parameter1Tbl)
    elseif funcname == "GetBlockHash" then --1入参：height
        Test_GetBlockHash(parameter1Tbl)
    elseif funcname == "GetBlockTimestamp" then --1入参：height
        Test_GetBlockTimestamp(parameter1Tbl)
    elseif funcname == "WriteData" then     --2入参：key、value
        Test_WriteData(parameter1Tbl,parameter2Tbl)
    elseif funcname == "ModifyData" then   --2入参：key、value
        Test_ModifyData(parameter1Tbl,parameter2Tbl)
    elseif funcname == "ReadData" then --1入参：key
        Test_ReadData(parameter1Tbl)
    elseif funcname == "DeleteData" then --1入参：key
        Test_DeleteData(parameter1Tbl)
    elseif funcname == "Sha256Once" then --1入参：sha256的入参
        Test_Sha256Once(parameter1Tbl)
    elseif funcname == "VerifySignature" then
        Test_VerifySignature(parameter1Tbl)
    elseif funcname == "Des" then --
        Test_Des(parameter1Tbl) --异常，结果与期望值不同
    elseif funcname == "IntegerToByte4" then --
        Test_IntegerToByte4(parameter1Tbl)
    elseif funcname == "IntegerToByte8" then --
        Test_IntegerToByte8(parameter1Tbl)
    elseif funcname == "ByteToInteger" then --
        Test_ByteToInteger(parameter1Tbl)
    elseif funcname == "WriteOutAppOperate" then --
        Test_WriteOutAppOperate(parameter1Tbl) --WriteOutAppOperate用来发行token、转账token
    elseif funcname == "GetUserAppAccValue" then --
        Test_GetUserAppAccValue(parameter1Tbl)  --GetUserAppAccValue 查询token余额
    elseif funcname == "WriteOutput" then --
        Test_WriteOutput(parameter1Tbl) --WriteOutput
    elseif funcname == "QueryAccountBalance" then --
        Test_QueryAccountBalance(parameter1Tbl) -- QueryAccountBalance 查询wicc余额

    --[[
       GetUserAppAccFoudWithTag
       TransferContactAsset
       TransferSomeAsset
    --]]

    end

end

write_table = function()

    local abc = {
        a  = {0x11,0x12,0x13},
        b  = {0x14,0x15,0x16},
        c  = {0x17,0x18,0x19},
    }

    Add_Strkey_Value_ToDb("abc",abc)

    local readResult = {mylib.ReadData("abc") }
    local result = serialize(readResult,true)

    print("result=",result)

end


Main = function()
    --  for i=1, #contract do
    --    print(i, contract[i])
    -- end
    assert(#contract >=4, "Param length error (<4): " ..#contract )
    assert(contract[1] == 0xf0, "Param MagicNo error (~=0xf0): " .. contract[1])

    if contract[2] == APP_OPERATE_TYPE.TEST_CONTRACT then

        local funcname = serialize2(GetContractTxParam(magicnum_len+1,funcname_len),false)  --函数名 32bytes
        local parameternum = serialize2(GetContractTxParam(magicnum_len+funcname_len+1,parameternum_len),false)--入参数 4bytes
        local parameter1Tbl,parameter2Tbl,parameter3Tbl
        if parameternum == "1" then
             parameter1Tbl = GetContractTxParam(basedata_len+1, parameter_len)
             assert(#parameter1Tbl == 32  ,"parameter1 length error.")
        elseif parameternum == "2" then
             parameter1Tbl = GetContractTxParam(basedata_len+1, parameter_len)
             parameter2Tbl = GetContractTxParam(basedata_len+parameter_len+1,parameter_len)
             assert(#parameter1Tbl == 32  ,"parameter1 length error.")
             assert(#parameter2Tbl == 32  ,"parameter2 length error.")
        elseif parameternum == "all" then
            parameter1Tbl = GetContractTxParam(basedata_len+1, #contract - basedata_len)
        else
            assert("parameternum error.")
        end

        RunAutoTestTask(funcname,parameter1Tbl,parameter2Tbl)
    elseif contract[2] == APP_OPERATE_TYPE.TEST_DEBUG then
        print("1234567")
        write_table()
    else
        error('method# '..string.format("%02x", contract[2])..' not found')
    end
end

Main()

