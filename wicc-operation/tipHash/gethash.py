from influx import *

from common import rpc_request, read_property, send_dingding
import threading


def gettiphash():
    rpcuser = read_property.read('common', 'rpcuser')
    rpcpassword = read_property.read('common', 'rpcpassword')
    wiccrpc = rpc_request.WiccRPC(rpcuser, rpcpassword)
    params = []
    res = wiccrpc.call(params)

    tipBlockTime = res[0]['result']['tipblocktime']
    tipBlockHash = res[0]['result']['tip block hash']
    syncHeight = res[0]['result']['syncheight']
    blocks = res[0]['result']['blocks']

    return tipBlockTime, tipBlockHash, syncHeight, blocks


def add_points(host, port, nodeName, username='root', password='root'):
    database = read_property.read('common', 'influxdatabase')
    tipBlockTime, tipBlockHash, syncHeight, blocks = gettiphash()
    json_body = [
        {
            "measurement": "nodeHash2",
            "tags": {
                "nodeName": nodeName,
            },
            "fields": {
                'tipBlockTime': tipBlockTime,
                'tipBlockHash': '212121',
                'syncHeight': 1000,
                "blocks": 10
            }
        }
    ]
    influx = Influx(host, port, username, password, database)
    influx.write_points(json_body)


def query_points_latest(host, port, username, password, database):
    influx = Influx(host, port, username, password, database)
    result = influx.query_points('nodeHash2', 'nodeName', 'node2')

    # column: time, blocks, nodeName, syncHeight, tipBlockHash, tipBlockTime

    return result.raw['series'][0]['values'][0]


def compare():
    host = '10.0.0.127'
    port = 8086
    username = 'root'
    password = 'root'
    database = 'wicc-operation'
    node_list = ['node1', 'node2', 'node3', 'node4', 'node5', 'node6', 'node7', 'node8', 'node9', 'node10', 'node10', 'node11']

    measurement = 'nodeHash2'
    key = 'nodeName'
    influx = Influx(host, port, username, password, database)
    sync_dict = {
    }
    for item in node_list:

        result = influx.query_points(measurement, key, item)
        latest = result.raw['series'][0]['values'][0]
        # column: time, blocks, nodeName, syncHeight, tipBlockHash, tipBlockTime
        sync_dict[latest[2]] = int(latest[3])
        if latest[3] - latest[1] > 1:
            content = 'Node:' + latest[2] + ' sync wrong, please check'

            send_dingding.send_group_robot(content)

    arr = sorted(sync_dict.items(), key=lambda item: item[1])
    length_list = len(arr)
    err_list = []
    content = ''
    for item in arr:
        if item[1] + 1 < arr[length_list-1][1]:
            err_list.append(item[0])
            content += item[0]

    send_dingding.send_group_robot(content)
    return err_list


def wiccthread(host, port, nodename):
    add_points(host, port, nodename)


def add_points_threading():
    node_list = [
        {
            'nodename': 'node1',
            'host': '10.0.0.127',
            'port': 8086
        },
        {
            'nodename': 'node2',
            'host': '10.0.0.127',
            'port': 8086
        },
        {
            'nodename': 'node3',
            'host': '10.0.0.127',
            'port': 8086
        },
        {
            'nodename': 'node4',
            'host': '10.0.0.127',
            'port': 8086
        },
        {
            'nodename': 'node5',
            'host': '10.0.0.127',
            'port': 8086
        },
        {
            'nodename': 'node6',
            'host': '10.0.0.127',
            'port': 8086
        },
        {
            'nodename': 'node7',
            'host': '10.0.0.127',
            'port': 8086
        },
        {
            'nodename': 'node8',
            'host': '10.0.0.127',
            'port': 8086
        },
        {
            'nodename': 'node9',
            'host': '10.0.0.127',
            'port': 8086
        },
        {
            'nodename': 'node10',
            'host': '10.0.0.127',
            'port': 8086
        },
        {
            'nodename': 'node11',
            'host': '10.0.0.127',
            'port': 8086
        }

    ]

    for item in node_list:
        th = threading.Thread(target=wiccthread, args=(item['host'], item['port'], item['nodename']))
        th.start()


if __name__ == '__main__':
    compare()
