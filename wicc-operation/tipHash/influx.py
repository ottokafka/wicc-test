from influxdb import InfluxDBClient


class Influx:
    client = None

    def __init__(self, host, port, userName, password, database='wicc-operation'):
        self.host = host
        self.port = port
        self.userName = userName
        self.password = password
        self.database = database
        self.client = InfluxDBClient(host=host, port=port, username=userName, password=password, database=database)

    def write_points(self, json_body):
        self.client.write_points(json_body)

    def query_points(self, measurement, key, value):
        query_string = "select * from {} where {}='{}' order by time desc".format(measurement, key, value)
        result = self.client.query(query_string)
        return result
