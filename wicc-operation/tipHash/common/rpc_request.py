import requests
import json
import base64


class WiccRPC:
    def __init__(self, username, password):
        self.username = username
        self.password = password
        bytesString = (username + ":" + password).encode(encoding='utf-8')
        token = base64.b64encode(bytesString)
        self.headers = {'content-type': 'application/json', 'Authorization': 'Basic ' + token.decode('utf-8')}

    def call(self, params):
        payloads = [{
            "method": "getinfo",
            'params': params,
            'jsonrpc': '2.0',
            'id': 'curltext'
        }]
        response = requests.post(url="http://10.0.0.127:1968", data=json.dumps(payloads), headers=self.headers)
        if response.status_code == 200:
            return response.json()
        else:
            print('RPC call failed, error is ', response.status_code)
            return response.status_code




