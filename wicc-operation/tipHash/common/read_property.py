import configparser
import os


def read(section, item, cfgpath=''):
    if cfgpath == '':
        curpath = os.path.dirname(os.path.realpath(__file__))
        cfgpath = os.path.join(curpath, '..', 'wicc.conf')

    conf = configparser.ConfigParser()

    conf.read(cfgpath, encoding='utf-8')

    value = conf.get(section, item)
    return value


if __name__ == '__main__':
    value = read('common', 'host')
    print(value)
