import requests
import json


def get_access_token():
    id = 'dingjrwgjnt6ssprnhry'
    secret = '1icJmvdxqZmRqTVGDrMm-VIOC6r0EDiMBs_UP8ic3tfZ3r9zROjBOqutbNRoZZTQ'
    url = 'https://oapi.dingtalk.com/gettoken?appkey={}&appsecret={}'.format(id, secret)
    res = requests.get(url)
    response_dict = res.json()
    err_code_key = 'errcode'
    access_token_key = 'access_token'
    if err_code_key in response_dict and response_dict[err_code_key] == 0 and access_token_key in response_dict:
        return response_dict[access_token_key]
    else:
        return ''


def send_to_users(access_token, users, text):
    msg_type, msg = _gen_text_msg(text)
    return _send_msg_to_users(access_token, users, msg_type, msg)


def _gen_text_msg(text):
    msg_type = 'text'
    msg = {'content': text}
    return msg_type, msg


def _send_msg_to_users(access_token, users, msg_type, msg):
    to_users = '|'.join(users)
    agent_id = '1'
    body_dict = {'touser': to_users, 'agentid': agent_id, 'msgtype': msg_type, msg_type: msg}

    body = json.dumps(body_dict)
    return _send_msg('https://oapi.dingtalk.com/message/send?access_token={}'.format(access_token), body)


def send_group_robot(content):
    header = {
        "Content-Type": "application/json",
        "Charset": "UTF-8"
    }
    my_data = {
        "msgtype": "markdown",
        "markdown": {
            "title": "分叉监控",
            "text": " "
        },
        "at": {
            "isAtAll": True
        }
    }
    my_url = 'https://oapi.dingtalk.com/robot/send?access_token=ddaa7c9127a9eaedfa572a71d519a38410a5be2a0042adbe81ae5778ade8bb8e'

    my_data['markdown']['text'] = content
    requests.post(my_url, data=json.dumps(my_data), headers=header)


def _send_msg(url, body):
    r = requests.post(url, data=body)


if __name__ == '__main__':
    send_group_robot()
