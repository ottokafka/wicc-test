COUNT=$1

END=`coind getblockcount`

cat /dev/null > regid
cat /dev/null > result

for ((i=$END; i>$END - $COUNT; --i))
do
    TX=`coind getblock $i|sed -n 10p|awk -F'"' '{print $2}'`

    REG_ID=`coind gettxdetail $TX|grep regid|awk -F '"' '{print $4}'`

    echo "height: $i, tx: $TX, regid: $REG_ID"

    echo $REG_ID >> regid
done

echo "all delegates list:" >> result
coind listdelegates|grep RegID|awk -F '"' '{print $4}'|sort|uniq >> result

echo "" >> result

echo "all miners list:" >> result