package util

import (
	"operation/mylog"

	"gopkg.in/ini.v1"
)

// WiccConf wicc configure
var WiccConf = "./conf/wicc.conf"

// ReadProperty read property form config file
func ReadProperty(section, key, path string) (string, error) {
	cfg, err := ini.Load(path)

	if err != nil {
		mylog.Logger.Println("local config file fail")
		return "", err
	}

	sec, err := cfg.GetSection(section)

	if err != nil {
		mylog.Logger.Println("get section fail")
		return "", err
	}

	yes := sec.HasKey(key)

	if !yes {
		return "", err
	}

	value := sec.Key(key).String()
	return value, nil
}

// GetNodes get nodes
func GetNodes(section, path string) map[string]string {
	var result map[string]string
	result = make(map[string]string)

	cfg, _ := ini.Load(path)

	sec, _ := cfg.GetSection(section)
	for _, item := range sec.KeyStrings() {
		result[item] = sec.Key(item).String()
	}
	return result
}

// GetNodesName get node name list
func GetNodesName(section, path string) []string {
	var result []string
	cfg, _ := ini.Load(path)

	sec, _ := cfg.GetSection(section)
	for _, item := range sec.KeyStrings() {
		result = append(result, item)
	}
	return result
}
