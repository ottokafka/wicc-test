package util

import (
	"operation/mylog"

	client "github.com/influxdata/influxdb1-client/v2"
)

var database string

const (
	username = "root"
	password = "root"
	// MyMeasurement table name
	MyMeasurement = "totalcoins"
)

// ConnInflux connect influx
func ConnInflux() client.Client {
	influxdb, err := ReadProperty("common", "influxdb", WiccConf)
	if err != nil {
		mylog.Logger.Println("read influxdb failed, ", err.Error())
	}

	tmpDatabase, err := ReadProperty("common", "database", WiccConf)
	if err != nil {
		mylog.Logger.Println("read influxdb failed, ", err.Error())
	}
	database = tmpDatabase

	cli, err := client.NewHTTPClient(client.HTTPConfig{
		Addr:     influxdb,
		Username: username,
		Password: password,
	})
	if err != nil {
		mylog.Logger.Fatal(err)
	}
	return cli
}

// WritesPoints write points
// measurement: fluxdb measurement
// tagsKey
// tagsValue
// fields:
func WritesPoints(cli client.Client, measurement string, tagsKey, tagsValue string, fields map[string]interface{}) {

	bp, err := client.NewBatchPoints(client.BatchPointsConfig{
		Database:  database,
		Precision: "s",
	})
	if err != nil {
		mylog.Logger.Fatal(err)
	}

	tags := map[string]string{tagsKey: tagsValue}

	pt, err := client.NewPoint(
		measurement,
		tags,
		fields,
	)
	if err != nil {
		mylog.Logger.Fatal(err)
	}
	bp.AddPoint(pt)

	if err := cli.Write(bp); err != nil {
		mylog.Logger.Fatal(err)
	}
}

// QueryPoints query points
func QueryPoints(cli client.Client, measurement, cmd string) (res []client.Result, err error) {

	q := client.Query{
		Command:  cmd,
		Database: database,
	}

	if response, err := cli.Query(q); err == nil {
		if response.Error() != nil {
			return res, response.Error()
		}
		res = response.Results
	} else {
		return res, err
	}

	return res, nil
}

// WriteAlert write alert
func WriteAlert(cli client.Client, nodeName, alertType string, needAlert bool) {

	measurement, _ := ReadProperty("common", "dingdingmeasurement", WiccConf)

	tagsKey, tagsValue := "nodeName", nodeName
	var fields map[string]interface{}
	fields = make(map[string]interface{})
	fields["alertType"] = alertType
	fields["needAlert"] = needAlert
	WritesPoints(cli, measurement, tagsKey, tagsValue, fields)
}
