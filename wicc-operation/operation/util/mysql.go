package util

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

// DbWorker mysql data source name
type DbWorker struct {
	//mysql data source name
	Dsn      string
	Db       *sql.DB
	CoinInfo CoinDB
}

// CoinDB coin db
type CoinDB struct {
	ID         int64
	CoinAmount sql.NullFloat64
	Regids     sql.NullFloat64
}

// Connect mysql connect
func Connect() (DbWorker, error) {
	var err error
	dbw := DbWorker{
		Dsn: "root:xx1-j0bs@123@tcp(10.0.0.100:3306)/wicc-operation",
	}
	dbw.Db, err = sql.Open("mysql", dbw.Dsn)
	if err != nil {
		panic(err)
	}
	return dbw, nil
}

// InsertData insert data
func (dbw *DbWorker) InsertData(sql string, args ...interface{}) {
	stmt, _ := dbw.Db.Prepare(sql)
	defer stmt.Close()

	ret, err := stmt.Exec(args...)
	if err != nil {
		fmt.Printf("insert data error: %v\n", err)
		return
	}
	if LastInsertId, err := ret.LastInsertId(); nil == err {
		fmt.Println("LastInsertId:", LastInsertId)
	}
	if RowsAffected, err := ret.RowsAffected(); nil == err {
		fmt.Println("RowsAffected:", RowsAffected)
	}
}
