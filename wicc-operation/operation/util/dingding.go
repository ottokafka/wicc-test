package util

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"operation/mylog"
)

// SendDing send ding
func SendDing(content string) {
	formt := `
        {
            "msgtype": "markdown",
            "markdown": {
                "title":"分叉监控",
                "text": "%s"
            }
        }`
	body := fmt.Sprintf(formt, content)

	jsonValue := []byte(body)
	dingdingURL, err := ReadProperty("common", "dingding", WiccConf)

	if err != nil {
		mylog.Logger.Println("Read dingdingurl address failed, ", err.Error())
	}

	res, _ := http.Post(dingdingURL, "application/json", bytes.NewBuffer(jsonValue))

	if res.StatusCode == 200 {
		body, _ := ioutil.ReadAll(res.Body)
		mylog.Logger.Println(string(body))
	}
}
