module operation

require (
	github.com/go-sql-driver/mysql v1.4.1
	github.com/influxdata/influxdb1-client v0.0.0-20190124185755-16c852ea613f
	github.com/mikemintang/go-curl v0.1.0
	gopkg.in/ini.v1 v1.42.0
)
