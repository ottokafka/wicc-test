package wiccsdk

import (
	"bytes"
	"encoding/json"
	"fmt"
	"operation/common"
	"operation/mylog"
	"operation/util"
	"runtime"
	"strconv"
	"strings"
	"time"
)

// Wicc wicc request by json rpc request
// func Wicc(method string, params []interface{}) (map[string]interface{}, error) {
// 	host, err := common.GetHost()

// 	if err != nil {
// 		mylog.Logger.Println("Get host failed", err.Error())
// 	}
// 	// response, err := util.RPCRequest(host, header, postData)
// 	response, err := util.RPCRequest2(host, method, params)
// 	if err != nil {
// 		mylog.Logger.Println("Response err ", err.Error())
// 	}
// 	return response, err
// }

// WiccHost wicc request by json rpc request
func WiccHost(host, username, password, method string, params []interface{}) (map[string]interface{}, error) {
	// response, err := util.RPCRequest(host, header, postData)
	response, err := util.RPCRequest2(host, username, password, method, params)
	if err != nil {
		mylog.Logger.Println("Response err ", err.Error())
	}
	return response, err
}

// GetTipHash get tip hash and insert info influxdb
func GetTipHash(host, username, password string) (float64, string, float64, float64, error) {
	method := "getinfo"
	res, err := WiccHost(host, username, password, method, nil)
	if err != nil {
		mylog.Logger.Println("Getinfo request failed, ", err.Error())
		return 0, "", 0, 0, err
	}
	tipBlockTime, err := GetResult(res, "tipblocktime")
	tipBlockHash, err := GetResult(res, "tipblockhash")
	syncHeight, err := GetResult(res, "syncblockheight")
	blocks, err := GetResult(res, "tipblockheight")
	return tipBlockTime.(float64), tipBlockHash.(string), syncHeight.(float64), blocks.(float64), err
}

// GetResult get result's value by key
func GetResult(res map[string]interface{}, key string) (interface{}, error) {
	for k, v := range res {
		switch value := v.(type) {
		case nil:
			mylog.Logger.Println(k, "is nil", "null")
		case string:
			mylog.Logger.Println(k, "is string", value)
		case int:
			mylog.Logger.Println(k, "is int", value)
		case float64:
			mylog.Logger.Println(k, "is float64", value)
		case []interface{}:
			mylog.Logger.Println(k, "is an array:")
			for i, u := range value {
				mylog.Logger.Println(i, u)
			}
		case map[string]interface{}:
			mylog.Logger.Println(k, "is a map")
			for i, u := range value {
				if strings.ToLower(i) == strings.ToLower(key) {
					return u, nil
				}
			}
		default:
			mylog.Logger.Println(k, "is unkown type", fmt.Sprintf("%T", v))
		}
	}
	return "", nil
}

// InsertGetInfoInfluxDB insert getinfo to influxdb
func InsertGetInfoInfluxDB(quit chan int, nodeName, nodeInfo string) {
	infoList := strings.Split(nodeInfo, ",")
	var host, username, password string
	if len(infoList) < 3 {
		mylog.Logger.Println("node must contain host, username and password")
		host = infoList[0]
		username = ""
		password = ""
	} else if len(infoList) == 3 {
		host, username, password = infoList[0], infoList[1], infoList[2]
	}
	tipBlockTime, tipBlockHash, syncHeight, blocks, err := GetTipHash(host, username, password)

	if err != nil {
		mylog.Logger.Println("Getinfo failed, ", err.Error())
	}

	measurement, _ := util.ReadProperty("common", "tipHashmeasurement", util.WiccConf)

	tagKey, tagValue := "tipHash", nodeName
	var fields map[string]interface{}
	fields = make(map[string]interface{})
	fields["nodeName"] = nodeName
	fields["tipBlockTime"] = tipBlockTime
	fields["tipBlockHash"] = tipBlockHash
	fields["syncHeight"] = syncHeight
	fields["blocks"] = blocks
	mylog.Logger.Println("Write point,", nodeName)
	conn := util.ConnInflux()
	util.WritesPoints(conn, measurement, tagKey, tagValue, fields)
	quit <- 0
	mylog.Logger.Println("Write end, ", nodeName)
}

// BlockHashMonitor block hash monitor
func BlockHashMonitor() {
	var hostList map[string]string
	hostList = util.GetNodes("nodes", util.WiccConf)

	var quit chan int
	quit = make(chan int)
	runtime.GOMAXPROCS(2)

	for k, v := range hostList {
		go InsertGetInfoInfluxDB(quit, k, v)
	}

	for i := 0; i < 2; i++ {
		<-quit
	}
}

// Compare compare
func Compare() {
	nodeList := util.GetNodesName("nodes", util.WiccConf)
	tipMeasurement, _ := util.ReadProperty("common", "tipHashmeasurement", util.WiccConf)
	key := "nodeName"
	conn := util.ConnInflux()
	defer conn.Close()
	var syncDict map[string]int
	syncDict = make(map[string]int)

	for _, item := range nodeList {

		// get the cmd string
		var buffer bytes.Buffer
		buffer.WriteString("select * from ")
		buffer.WriteString(tipMeasurement)
		buffer.WriteString(" where ")
		buffer.WriteString(key)
		buffer.WriteString("=")
		buffer.WriteString("'" + item + "'")
		buffer.WriteString(" order by time desc")
		cmd := buffer.String()

		res, err := util.QueryPoints(conn, tipMeasurement, cmd)
		if err != nil {
			mylog.Logger.Println("Query points failed, ", err.Error())
		}
		mylog.Logger.Println(res)
		if len(res) == 0 {
			mylog.Logger.Println("res length is zero")
			continue
		}
		if len(res[0].Series) == 0 {
			mylog.Logger.Println("Length is zero")
			continue
		}
		latest := res[0].Series[0].Values[0]

		tmp := latest[3].(json.Number).String()
		syncHeight, _ := strconv.Atoi(tmp)

		syncDict[item] = syncHeight

		tmp = latest[1].(json.Number).String()
		blocks, _ := strconv.Atoi(tmp)
		// if syncHeight is more 2 than block, or syncHeight is less than blocks, will send alert
		if syncHeight-blocks > 1 || syncHeight < blocks {
			mylog.Logger.Println("syncHeight is: ", syncHeight)
			mylog.Logger.Println("tipblockheight is: ", blocks)
			nodeName := latest[2].(string)
			content := "Node:" + nodeName + " sync wrong, please check"

			isNeedAlert := IsNeedSendAlert(nodeName, "syncerror")
			needAlert := false
			if isNeedAlert {
				needAlert = true
				// send dingding message
				util.SendDing(content)
			}
			util.WriteAlert(conn, nodeName, "syncerror", needAlert)
		}
	}

	// fencha alert if any node is less than most
	sortResult := common.MapSort(syncDict)
	length := len(sortResult)
	for _, item := range sortResult {
		if item.Value+1 < sortResult[length-1].Value {
			isNeedAlert := IsNeedSendAlert(item.Key, "fen")
			needAlert := false
			content := "Node:" + item.Key + " maybe fencha, please check"
			if isNeedAlert {
				needAlert = true
				util.SendDing(content)
			}
			util.WriteAlert(conn, item.Key, "fen", needAlert)
		}
	}
}

// IsNeedSendAlert is need to send alert
func IsNeedSendAlert(nodeName, alertType string) bool {
	var buffer bytes.Buffer

	dingdingMeasurement, _ := util.ReadProperty("common", "dingdingmeasurement", util.WiccConf)
	buffer.WriteString("select * from ")
	buffer.WriteString(dingdingMeasurement)
	buffer.WriteString(" where needAlert=true and nodeName=")
	buffer.WriteString("'" + nodeName + "'" + " and alertType=")
	buffer.WriteString("'" + alertType + "'")
	buffer.WriteString(" order by time desc")
	cmd := buffer.String()
	conn := util.ConnInflux()
	res, err := util.QueryPoints(conn, dingdingMeasurement, cmd)
	if err != nil {
		mylog.Logger.Println("Query points failed, ", err.Error())
	}

	if len(res[0].Series) == 0 {
		mylog.Logger.Println("Length is zero")
		return true
	}
	latest := res[0].Series[0].Values[0]

	now := time.Now()
	h, _ := time.ParseDuration("-1h")
	h2, _ := time.ParseDuration("1h")
	duration := 2 * h
	h1 := now.Add(duration)
	t1str := latest[0].(string)
	t1time, _ := time.Parse("2006-01-02T15:04:05Z", t1str)
	t2 := t1time.Add(8 * h2)
	if t2.Before(h1) {
		return true
	}

	return false

}
