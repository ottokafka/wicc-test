package mylog

import (
	"fmt"
	"log"
	"os"
	"path"
	"runtime"
	"strings"
)

// Logger my logger
var Logger *log.Logger

// GetCurFilename Get current file name, without suffix
func GetCurFilename() string {
	_, fulleFilename, _, _ := runtime.Caller(0)
	//fmt.Println(fulleFilename)
	var filenameWithSuffix string
	filenameWithSuffix = path.Base(fulleFilename)
	//fmt.Println("filenameWithSuffix=", filenameWithSuffix)
	var fileSuffix string
	fileSuffix = path.Ext(filenameWithSuffix)
	//fmt.Println("fileSuffix=", fileSuffix)

	var filenameOnly string
	filenameOnly = strings.TrimSuffix(filenameWithSuffix, fileSuffix)
	//fmt.Println("filenameOnly=", filenameOnly)

	return filenameOnly
}

// GetLogger get logger
func GetLogger() {
	var filenameOnly string
	filenameOnly = GetCurFilename()
	fmt.Println("filenameOnly=", filenameOnly)

	var logFilename = filenameOnly + ".log"
	fmt.Println("logFilename=", logFilename)
	logFile, err := os.OpenFile(logFilename, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0777)
	if err != nil {
		fmt.Printf("open file error=%s\r\n", err.Error())
		os.Exit(-1)
	}

	Logger = log.New(logFile, "\r\n", log.Ldate|log.Ltime|log.Lshortfile)
}
