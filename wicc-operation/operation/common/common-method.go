package common

import (
	"encoding/base64"
	"log"
	"operation/util"
	"sort"
	"time"
)

// Conf config file path

var conf = "./conf/wicc.conf"

// GetHost get host from config file
func GetHost() (string, error) {
	host, err := util.ReadProperty("common", "host", conf)
	if err != nil {
		log.Println("Read property failed, ", err.Error())
		return "", err
	}

	return host, nil
}

// GetToken get token according to rpcuser and rpcpassword
func GetToken() (string, error) {
	rpcuser, err := util.ReadProperty("common", "rpcuser", conf)

	rpcpassword, err := util.ReadProperty("common", "rpcpassword", conf)

	if err != nil {
		log.Println("read rpcuser or rpcpassword failed, ", err.Error())
	}

	token := base64.StdEncoding.EncodeToString([]byte(rpcuser + ":" + rpcpassword))

	return token, nil
}

// GetOriginAddress get originaddr
func GetOriginAddress() string {
	address, err := util.ReadProperty("common", "originaddr", conf)

	if err != nil {
		log.Println("Get original address failed, ", err.Error())
		return ""
	}
	return address
}

// Sleep wait for some seconds
func Sleep(num int) {
	time.Sleep(time.Duration(num) * time.Second)
}

// KV kv
type KV struct {
	Key   string
	Value int
}

// MapSort sort for map
func MapSort(m map[string]int) []KV {
	var ss []KV
	for k, v := range m {
		ss = append(ss, KV{k, v})
	}
	sort.Slice(ss, func(i, j int) bool {
		return ss[i].Value < ss[j].Value
	})

	return ss
}
