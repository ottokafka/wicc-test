package main

import (
	"operation/mylog"
	"operation/wiccsdk"
	"time"
)

func main() {
	mylog.GetLogger()
	mylog.Logger.Print("Start to operate")
	timer := time.NewTicker(5 * time.Second)
	for {
		select {
		case <-timer.C:
			go func() {
				wiccsdk.BlockHashMonitor()
				wiccsdk.Compare()
			}()
		}
	}
}
