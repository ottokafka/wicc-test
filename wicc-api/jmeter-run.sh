docker run --rm --privileged \
       -v `pwd`/jmeter-data/:/tmp \
       -v `pwd`/wicc-test/wicc-testcases:/mnt/jmeter \
       wicc-ant \
       -buildfile /root/ \
       -DtestName=$1 \
       -DbuildNumber=$2
