# Setting up testing environment

First you need to create a node. Using Docker is the recommended method.
Go download Docker then setup up a Node if you havent already. 

1. Download [docker](docker.io)
2. Learn how to setup a [WaykiChain](https://wicc-devbook.readthedocs.io/en/master/) node. 

---

<br>


Clone the repo
```
git clone git@gitlab.com:ottokafka/wicc-test.git
```

Download [Pycharm](https://www.jetbrains.com/pycharm/download/) community edition

Next open up the project with Pycharm

Edit the config file

wicc-test/wicc-autotest/inter-autotest/config/pri_config
```
[common]
host=http://10.0.0.19:1968
before_host=http://10.0.0.19:1966
rpcuser=waykichain
rpcpassword=wicc123
walletpassword=1234567890
```

We need to change address to the address of your docker machine. You dont need to change the port leave it as default.






















## 测试组文档

* 测试用例

|项目|文档|报告|
|--|--|--|
|维基链官网| [测试用例](https://docs.qq.com/sheet/BqI21X2yZIht17ZAuA39YTAS2EJImy0CTSlj332CxO3M72PT3IQmKC2Cjyb92vhbXP4aaVpS0DZJ75222pvW4) |[测试报告](https://docs.qq.com/doc/BqI21X2yZIht17ZAuA39YTAS2Ws1zZ12UOIZ3cFDK60F22Ef2)|
|维基链竞猜| [测试用例](https://docs.qq.com/sheet/BqI21X2yZIht17ZAuA39YTAS2unXjF2xHMTB1MyiZp1dDoo11IQmKC2Cjyb92ngGn34C1ESp2EItGd24nwKv0) |[测试报告](https://docs.qq.com/doc/DR3RPY2d5QUhtS3p1)|
|区块链彩票| [](wicc-lottery/test-cases.md) |
